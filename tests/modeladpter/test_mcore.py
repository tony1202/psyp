import pytest
import uuid
import json
from psyp.scenario.modeladpter import mcore
from psyp.utility.excelio import read_data_from_table
from psyp.dataio.shp.shpio import json2shp
import os

#region weia
def test_cal_c_estuary_2d_center_plot():
   res= mcore.cal_c_estuary_2d_center_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度等值线图',
    0.005,5000,100,1,1,1,1,20,10,1.5,1.5,
    2,
    False) 
   print(json.dumps(res))

def test_cal_c_river_2d_side_plot():
   res= mcore.cal_c_river_2d_side_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度等值线图',
    5,50,30,0.3,1,0.2,0.0001,1000,20,25,6,
    2,
    False) 
   print(json.dumps(res))

def test_cal_c_river_2d_side_reflex_plot():
   res= mcore.cal_c_river_2d_side_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度等值线图',
    5,50,30,0.3,1,0.2,0.0001,1000,20,25,6,
    2,
    False) 
   print(json.dumps(res))

def test_cal_c_river_1d_plot():
   res = mcore.cal_c_river_1d_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度扩散图',
   0.0001,0.5,0.05,30,30,200,10,50,50,233,1000,45)
   print(json.dumps(res))

def test_cal_c_river_1d_instant_plot():
   res = mcore.cal_c_river_1d_instant_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度扩散图',
   8.64,150,0.5,5,5,0,0,1000,10,20*72,20,2)
   print(json.dumps(res))

def test_cal_c_river_2d_instant_side():
   res = mcore.cal_c_river_2d_side_instant_plot(str(uuid.uuid1()),
   'D:/ClientScenarioes',
   '浓度扩散图',
   0,760.66,0.5,0.315,0.024,0.35,0.1,5000,30,24*3600,50,5,3600,2,False)
   print(json.dumps(res))
#endregion

#region swmm
def test_inp2geojson():
    dir = r'tests\testdata\models\swmm'
    gjson = mcore.inp2geojson('',
    dir,
    'RR_Base.inp',
    'EPSG:4546',
    'EPSG:4490'
    )
    sjson = json.dumps(gjson)
    jsonfile = os.path.join(dir,'RR_Base.json')
    f = open(jsonfile,'w',encoding='utf-8')
    f.write(sjson)
    f.close()

    json2shp(dir,'RR_Base')

def test_generateSWMM():
   mcore.generateSWMM('',
   r'tests\testdata\models\swmm',
   'mzh.inp')

def test_updateSWMM():
   mcore.updateSWMM('',
   'tests\testdata\models\swmm',
   'mzh.inp')

def test_update_cat_params():
   mcore.update_cat_params('',
   r'tests\testdata\models\swmm',
   'mzh.inp','')

def test_update_ldu_buildup_params():
   params = {
        'LU_20':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_30':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_40':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_50':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_60':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_80':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        },
        'LU_90':{
            'NH3N':{
                'Coeff1':1.05,
                'Coeff2':0.5,
                'Coeff3':1.0
            },
            'COD':{
                'Coeff1':1,
                'Coeff2':2,
                'Coeff3':0.0
            },
            'TP':{
                'Coeff1':1,
                'Coeff2':0.5,
                'Coeff3':0.0
            }
        }
    }

   mcore.update_ldu_buildup_params('',
   r'tests\testdata\models\swmm',
   'mzh.inp',
   params)

def test_update_ldu_washoff_params():
   params = {
        'LU_20':{
            'NH3N':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':2,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            }
        },
        'LU_30':{
            'NH3N':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':2,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            }
        },
        'LU_40':{
            'NH3N':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':2,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            }
        },
        'LU_50':{
            'NH3N':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':2,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            }
        },
        'LU_60':{
            'NH3N':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':2,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.5,
                'Coeff2':0.5,
            }
        },
        'LU_80':{
            'NH3N':{
                'Coeff1':1,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':3,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.6,
                'Coeff2':0.5,
            }
        },
        'LU_90':{
            'NH3N':{
                'Coeff1':1,
                'Coeff2':0.5,
            },
            'COD':{
                'Coeff1':3,
                'Coeff2':0.5,
            },
            'TP':{
                'Coeff1':0.6,
                'Coeff2':0.5,
            }
        }
    }

   mcore.update_ldu_washoff_params('',
   r'tests\testdata\models\swmm',
   'mzh.inp',
   params)

def test_update_raingage_tsdata():
    mcore.update_raingage_tsdata('',
    r'tests\testdata\models\swmm',
    'mzh.inp',
    '2022-09-10 8:00:00',
    '2022-09-13 8:00:00',
    '2022-09-16 8:00:00',
    '1')

def test_update_raingage_tsdata2():
    mcore.update_raingage_tsdata2('',
    # r'tests\testdata\models\swmm',
    # 'mzh.inp',
    r'D:\ClientScenarioes\d6ca55dd-d546-4444-8327-b963cfdfd7c9\SWMM',
    'RR_Base-Forecasting.inp',
    ['2022-10-12 00:00:00','2022-10-12 01:00:00'],
    [2,1])

def test_update_options_simulationTime():
    mcore.update_options_simulationTime('',
    r'tests\testdata\models\swmm',
    'mzh.inp',
    '2022-09-10 8:00:00',
    '2022-09-16 8:00:00',)

def test_runAndGetStatistic():
   mcore.runAndGetStatistic('',
   r'tests\testdata\models\swmm',
   'mzh.inp')

def test_runAndExtractSubcatchmentResults():
   mcore.runAndExtractSubcatchmentResults(
      '',
      r'tests\testdata\models\swmm',
      'mzh.inp',
      r'tests\testdata\models\swmm\timeseries',
      '') # 不支持中文路径

def test_runAndExtractJuncionResults():
   mcore.runAndExtractJuncionResults(
      '',
      r'tests\testdata\models\swmm',
      'mzh.inp',
      r'tests\testdata\models\swmm\timeseries',
      r'tests\testdata\models\swmm\swmm-m11-map.xlsx') # 不支持中文路径

def test_runAndExtractCatchmentsResults4HecRas():
    mcore.runAndExtractCatchmentsResults4HecRas(
        '',
        r'tests\testdata\models\swmm',
        'mzh2.inp',
        r'tests\testdata\models\swmm\timeseries',
        r'tests\testdata\models\swmm\swmm-hecras-map.xls'
    )

def test_runAndExtractResults():
   mcore.runAndExtractResults(
      '',
      r'tests\testdata\models\swmm',
      'Example1.inp',
      '18',
      'total_inflow',
      True
   )

def test_runSwmmFollowedMike11():
   mcore.runSwmmFollowedMike11('',
   '',
   '',
   '',
   '',
   '',
   '',
   '')

def test_run_swmm_pest():
   # 需要率定参数的数据结构
   cal_params = [{
        'subc_name': "1",
        'params': [
            {
                'name':"Area",
                'value':20,
                'low_limit': 10,
                'up_limit': 100
            }
        ]
        }
    ]
    # 实测数据结构
   obs_mapdata = [
        {
            'type': "total_inflow",
            'name': "18",
            'data': [
               # {
               #  'time': "2021-1-1 00:00:00",
               #  'data': 0
               # }
            ]
        }
    ]
   obs_dir = r'C:\sypcloud_git\sypcloud\trunk\projects\doc\91_nwh_mzh\m11'
   obs_file = 'swmm自动率定.xls'

   obs_data = read_data_from_table(obs_dir,obs_file,'total_inflow')
   cols_name = obs_data.columns
   for idx, row in obs_data.iterrows():
      obs_mapdata[0]['data'].append({
         'time': row[cols_name[0]],
         'data': row[cols_name[1]]
      })
   mcore.run_swmm_pest(
      '',
      r'C:\sypcloud_git\sypcloud\trunk\modelbase\swmm\SWMM2PEST V2.1\pest14\pest.exe',
      r'D:\python\conda\python.exe',
      r'D:\python\psyp\main.py',
      r'C:\Users\jip\Documents\EPA SWMM Projects\Examples',
      'Example1.inp',
      cal_params,
      obs_mapdata)

def test_getSwmmModelRainData():
    sdir = r'C:\sypcloud_git\projects\94_hdy_mzh\model\swmm'
    prjname = 'RR_Base.inp'
    res = mcore.getSwmmModelRainData(
        '',
        sdir,
        prjname
    )
    sjson = json.dumps(res)
    jsonfile = os.path.join(sdir,prjname[:-4]+'.json')
    f = open(jsonfile,'w',encoding='utf-8')
    f.write(sjson)
    f.close()

    print(sjson)

#endregion

#region mike
def test_runMike11():
   mcore.runMike11('','C:\\Program Files (x86)\\DHI\\2014\\bin\\x64\\mike11.exe',
   'C:\\sypcloud_git\\sypcloud\\trunk\\projects\\doc\\91_nwh_mzh\\m11',
   '1-gk-cal-01.sim11'
   )
#endregion

#region hec-ras
def test_run_hecras():
    mcore.run_hecras('',
    r'C:\sypcloud_git\sypcloud\trunk\modelbase\HEC\IO\NSM-Calibrator\Estudo_de_Caso_ 1B',
    'Project.prj')
#endregion

#region air diffussion model
def test_run_air_diffusion_gauss_model():
    appid='dsfds-dsfs'
    dir=r'tests\testdata\models\air'
    deltx=100 # m，x方向计算步长
    delty=100 # m，y方向计算步长
    delt_angle=22.5 # 度，计算范围为风向左右两侧多少多少度的扇形区域
    wind_angle=90 # 度，风向，正北为0，顺时针
    radius=3500 # m，计算范围，扇形半径

    x0=107.6314 # 排放口经度
    y0=37.73866 # 排污口纬度

    crs_in = 'EPSG:4326'
    crs_out = 'EPSG:4526'

    q=7342
    w='D'
    u=2.5
    k=0.0052194

    h_fix_method='Holland'

    us=3.42
    usa=2.5
    Ts=393
    Ta=290
    D=4
    A = 100     #面源单元边长
    num=4       #模式选择 1-点源化学反应：2-点源无反应模型：3-面源化学反应模型；4-面源无反应模型

    vs=15
    h=80
    Pa=10.057
    Qv=141.95
    mcore.run_air_diffusion_gauss_model(appid,dir,num,x0,y0,crs_in,crs_out,A,q,Qv,vs,h,us,usa,Ts,D,u,wind_angle,Ta,w)
#endregion

#region hspf
def test_init_hspf():
    appid=''
    # dir = r'tests\testdata\models\hspf\testcase\1\hspf'
    dir = r'C:\sypcloud_git\projects\114_yn_jsj\model\basins'
    prj_name='jsj'
    if not os.path.exists(dir):
        os.makedirs(dir)
    mcore.init_hspf(appid, dir,prj_name)
#endregion