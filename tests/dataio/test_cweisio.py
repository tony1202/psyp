def test_process_data_4_cweis():
    from psyp.dataio.cweis import cweisio
    import os

    root=r'C:\sypcloud_git\projects\108_cweis\gis'
    folder = '乌海市'
    folder_py = '150300000000'
    code='150300000000'

    xkz_csvfile='固定源基本信息（许可）.csv'
    dj_csvfile='固定源基本信息（登记）.csv'
    pfkb_dj = '排放口_登记.csv'
    cweisio.process_data_4_cweis(root,folder,folder_py,code)