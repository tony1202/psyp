from psyp.dataio.webfetch.zfcg import webfetch2
from psyp.dataio.webfetch.syq import fetch_bjsq
from psyp.dataio.webfetch.zfcg import fetch_zbxx
from psyp.dataio.webfetch.zfwz import fetchHLJGOVDATA, fetchJXWRYJKDATA
from psyp.dataio.webfetch.pwxkz import fetch_pwxkz   
from psyp.dataio.webfetch.ripe import fetch_ripe 

def test_capture_webinfo_zyczxm():
    webfetch2.capture_webinfo_zyczxm('2021-08-17 10:00:00','D:/python/psyp/psyp/dataio/webfetch')
    print('successfully finished!')

def test_capture_webinfo_jxwryjk():
    fetchJXWRYJKDATA.capture_webinfo_jxwryjk('2020-12-1','2020-12-31','JSESSIONID=6C7292B731EDD8AFE5AB2326FC6E6744')

def test_capture_webinfo_hljrmzf():
    cookies = 'JSESSIONID=25DF695D02538035F7B91C6659D28248; wdcid=387fba58321ff500; wdses=2ccbc268fdb70ea2; wdlast=1638589319'
    fetchHLJGOVDATA.capture_webinfo_hljrmzf(cookies)

def test_fetch_zbxx():
    fetch_zbxx.fetch_zbxx_gj('D:/psyp/psyp/dataio/webfetch',
    '2022:1:27','2022:1:27')

def test_fectch_bjsq_data():
    fetch_bjsq.fectch_bjsq_data('d:/bjsq.csv','2011-10-14','2022-2-11')

def test_getxxgkContent():
    fetch_pwxkz.getxxgkContent2(r'D:\psyp\psyp\dataio\webfetch\pwxkz')

def test_getProductsInfo():
    fetch_ripe.getProductsInfo()
