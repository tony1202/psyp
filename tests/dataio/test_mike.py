from psyp.dataio.mike import dfs0io,dfsuio
import pytest

def test_dfs02wndrose():
   dfs0io.dfs02wndrose(
       r'tests\testdata\dataio\mike_dfs\fnl-wind.dfs0',
       r'tests\testdata\dataio\mike_dfs\fnl-wind.png') 

def test_dfs02wndroseMap():
   dfs0io.dfs02wndroseMap(
       r'tests\testdata\dataio\mike_dfs\fnl-wind.dfs0',
       r'tests\testdata\dataio\mike_dfs\fnl-wind-map.png',
       116.0,116.4,39.9,40.2,
       116.18,40.1
       ) 

def test_dfsu_flood_risk_2shp():
    dfsuio.dfsu_flood_risk_2shp(
        r'tests\testdata\dataio\mike_dfs\hd.dfsu',
        r'tests\testdata\dataio\mike_dfs\hd.shp',        
    )

def test_dfsu_2png():
    dfsuio.dfsu_2png(
        r'tests\testdata\dataio\mike_dfs\100A.dfsu',
        r'tests\testdata\dataio\mike_dfs\100A.shp',
        r'tests\testdata\dataio\mike_dfs\imgs',
        #r'tests\testdata\dataio\mike_dfs\100A.jpg',
        None,
        1,2,3,5
    )

def test_dfsu_2shp():
    dfsuio.dfsu_2shp(
        # r'tests\testdata\dataio\mike_dfs\100A.dfsu',
        # r'tests\testdata\dataio\mike_dfs\shp',
        r'C:\sypcloud_git\projects\100_wyc\04Jamaica-flood\m21\2dmodel_01\xz-10yr.m21fm - Result Files\hd_Stat-wd.dfsu',
        r'C:\sypcloud_git\projects\100_wyc\04Jamaica-flood\m21\2dmodel_01\xz-10yr.m21fm - Result Files\shp',
        8,
        item_num=1       
    )