from psyp.modelbase.hydraulic import manning2
from psyp.utility.excelio import read_data_from_table
import csv
import os
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('_mpl-gallery')

def test_CalDepthFromQ():
    sdir = r'D:\psyp\psyp\modelbase\hydraulic'
    fn='K13+690-改河拓宽-.csv'
    id = 'test'
    x=[]
    y=[]
    marks=[]
    with open(os.path.join(sdir,fn), "r") as f:
        reader = csv.reader(f, delimiter=",")
        for i, line in enumerate(reader):
            if i==0:
                qlist = [float(q.strip()) for q in line[1:] if len(q)>0]
            if i==1:
                s = float(line[1].strip())
            if i==2:
                nc = float(line[1].strip())
            if i==3:
                ntl = float(line[1].strip())
            if i==4:
                ntr = float(line[1].strip())
            if i>5: # 断面xz
                if len(line)>0:
                    x.append(float(line[0].strip()))
                    y.append(float(line[1].strip()))
                    if len(line[2])>0:
                        marks.append(i-6)

    reslist=[]        
    for i, q in enumerate(qlist):
        res = manning2.CalDepthFromQ(
            sdir,
            i,
            x,
            y,
            q,
            s,
            nc,
            ntl,
            ntr,
            marks[0],
            marks[1],
            marks[2]        
        )
        reslist.append(res)
    
    # plot
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 5)
    labels=['P100','P50','P25','P10']
    color_map = ["#d71345","#f58220","#b2d235","#4e72b8", "#d3c6a6"]
    
    labels = [labels[0],labels[3]]
    color_map = [color_map[0],color_map[3],color_map[4]]

    with open(os.path.join(sdir,fn+'-res.csv'),'w+') as fw:
        fw.write('channel elevation(m),')
        fw.write('slope,')
        fw.write("manning\'s n of the left floodplain,")
        fw.write("manning\'s n of the channel,")
        fw.write("manning\'s n of the right floodplain,")
        fw.write('flow(m3/s),')
        fw.write("flow area of the left floodplain(m2),")
        fw.write("flow area of the channel(m2),")
        fw.write("flow area of the right floodplain(m2),")
        fw.write('wetted perimeter(m),')
        fw.write('water surface width(m),')
        fw.write("flow of the left floodplain(m3/s),")
        fw.write("flow of the channel(m3/s),")
        fw.write("flow of the right floodplain(m3/s),")
        fw.write("max water depth of the left floodplain(m),")
        fw.write("max water depth of the right floodplain(m3/s),")        
        fw.write('velocity(m/s),')
        fw.write('max water depth(m),')
        fw.write('water surface elevation(m)\n')
    # res=   0:yn,1:velocity,2:q,3:area,4:w,5:p,6:qtl,7:area_tl,8:w_tl, \
    # 9:p_tl,10:qc,11:area_c,12:w_c,13:p_c,14:qtr,15:area_tr,16:w_tr,17:p_tr, \
    # 18:x_tmp,19:y_tmp, \
    # 20:xl_tmp,21:yl_tmp,22:dp_tl,23:(dp_tl-dl), \
    # 24:xc_tmp,25:yc_tmp,26:dp_c,27:(dp_c-dc) \
    # 28:xr_tmp,29:yr_tmp,30:dp_tr,31:(dp_tr-dr)
        qlist=qlist[::-1]
        for i,res in enumerate(reslist[::-1]):
            fw.write(str(min(y))+',')
            fw.write(str(s)+',')
            fw.write(str(ntl)+',')
            fw.write(str(nc)+',')
            fw.write(str(ntr)+',')
            fw.write(str(qlist[i])+',')
            fw.write(str(res[7])+',')
            fw.write(str(res[11])+',')
            fw.write(str(res[15])+',')
            fw.write(str(res[5])+',')
            fw.write(str(res[4])+',')
            fw.write(str(res[6])+',')
            fw.write(str(res[10])+',')
            fw.write(str(res[14])+',')
            fw.write(str(res[23])+',')
            fw.write(str(res[31])+',')
            fw.write(str(res[1])+',')
            fw.write(str(res[0])+',')
            fw.write(str(res[0]+min(y)) +'\n')

            # 河槽
            ax.stackplot([res[24][0],res[24][-1]],
            np.vstack([[res[0]+min(y),res[0]+min(y)]]),
            labels=[labels[i]],
            colors=color_map[i],alpha=0.8)
            # 左滩
            if len(res[20])>0:
                ax.stackplot([res[20][0],res[20][-1]],
                np.vstack([[res[22],res[22]]]),                
                colors=color_map[i],alpha=0.8)
            # 右滩
            if len(res[28])>0:
                ax.stackplot([res[28][0],res[28][-1]],
                np.vstack([[res[30],res[30]]]),
                colors=color_map[i],alpha=0.8)            

        fw.close()
        ax.stackplot(x,[y],colors=[color_map[-1]])
        ax.legend(loc='upper left')
        ax.set_title('Flood level of Cross section')
        ax.set_xlabel('Distance(m)')
        ax.set_ylabel('Elevation(m)')
        fig.savefig(os.path.join(sdir,fn+'-xs.png'),dpi=300,bbox_inches='tight')           
    
    print(res)