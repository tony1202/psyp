# import matplotlib
import numpy as np
# import matplotlib.cm as cm
# from matplotlib.animation import FuncAnimation, PillowWriter
import os
# from psyp.utility import matplot
# from colorama import init,deinit,Fore

#region 地表水环评解析解
def cal_c_estuary_2d_center_plot(appid, sdir, title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数
    fill : true/false 是否填充

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_estuary_2d_center(
        Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy)
    x = res[:, 1, 0]
    y = res[1, :, 1]
    X, Y = np.meshgrid(x, y)
    Z = res[:, :, 2]
    Z = np.transpose(Z)

    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签
    if(fill):
        contour = plt.contourf(X, Y, Z)
        plt.colorbar(contour)
    else:
        contour = plt.contour(X, Y, Z)
    plt.xlabel("河流纵向距离（m）")
    plt.ylabel("河流横向距离（m）")
    # 等高线上标明z（即高度）的值，字体大小是10，颜色分别是黑色和红色
    plt.clabel(contour, fontsize=10, fmt='%.'+str(num)+'f')
    plt.title(title)
    plt.savefig(os.path.join(sdir, appid+'.png'))

    data = []
    for index, item in enumerate(x):
        data.append({
            "x": item,
            "concerntration": Z[:, index].tolist()
        })

    return {"id": appid, "x": x.tolist(), "y": y.tolist(), "data": data}


def cal_c_river_2d_side_plot(appid, sdir, title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数
    fill : true/false 是否填充

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_river_2d_side(Ch, Qp*Cp, h, Ey, u, k, x, y, Δx, Δy)
    x = res[:, 1, 0]
    y = res[1, :, 1]
    X, Y = np.meshgrid(x, y)
    Z = res[:, :, 2]
    Z = np.transpose(Z)

    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签
    if(fill):
        contour = plt.contourf(X, Y, Z)
        plt.colorbar(contour)
    else:
        contour = plt.contour(X, Y, Z)
    plt.xlabel("河流纵向距离（m）")
    plt.ylabel("河流横向距离（m）")
    # 等高线上标明z（即高度）的值，字体大小是10，颜色分别是黑色和红色
    plt.clabel(contour, fontsize=10, fmt='%.'+str(num)+'f')
    plt.title(title)
    plt.savefig(os.path.join(sdir, appid+'.png'))

    data = []
    for index, item in enumerate(x):
        data.append({
            "x": item,
            "concerntration": Z[:, index].tolist()
        })

    return {"id": appid, "x": x.tolist(), "y": y.tolist(), "data": data}


def cal_c_river_2d_side_reflex_plot(appid, sdir, title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数
    fill : true/false 是否填充

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_river_2d_side_reflex(
        Ch, Qp*Cp, h, y, Ey, u, k, x, y, Δx, Δy)
    x = res[:, 1, 0]
    y = res[1, :, 1]
    X, Y = np.meshgrid(x, y)
    Z = res[:, :, 2]
    Z = np.transpose(Z)

    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签
    if(fill):
        contour = plt.contourf(X, Y, Z)
        plt.colorbar(contour)
    else:
        contour = plt.contour(X, Y, Z)
    plt.xlabel("河流纵向距离（m）")
    plt.ylabel("河流横向距离（m）")
    # 等高线上标明z（即高度）的值，字体大小是10，颜色分别是黑色和红色
    plt.clabel(contour, fontsize=10, fmt='%.'+str(num)+'f')
    plt.title(title)
    plt.savefig(os.path.join(sdir, appid+'.png'))

    data = []
    for index, item in enumerate(x):
        data.append({
            "x": item,
            "concerntration": Z[:, index].tolist()
        })

    return {"id": appid, "x": x.tolist(), "y": y.tolist(), "data": data}


def cal_c_river_1d_plot(appid, sdir, title, k, Ex, u, B, A, Cp, Qp, Ch, Qh, x1, x2, Δx):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_river_1d(k, Ex, u, B, A, Cp, Qp, Ch, Qh, x1, x2, Δx)
    x = res[:, 0]
    y = res[:, 1]

    plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签

    plt.plot(x, y, color="red", linewidth=1)  # plt.subplots()
    # ax.plot(x, y)
    plt.xlabel("河流纵向距离（m）")
    plt.ylabel("浓度（mg/l）")
    plt.title(title)
    plt.grid()
    plt.savefig(os.path.join(sdir, appid+'.png'))

    data = []
    for index, item in enumerate(x):
        data.append({
            "concerntration": y[index]
        })

    return {"id": appid, "x": x.tolist(), "data": data}


def cal_c_river_1d_instant_plot(appid, sdir, title, k, Ex, u, A, M, Ch, Qh, x, Δx, t, Δt, num):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数

    """
    import matplotlib.pyplot as plt    
    from psyp.utility import matplot
    from psyp.modelbase.weia import cal_c_jxj
    outfile = os.path.join(sdir, appid+'.gif')
    res = cal_c_jxj.cal_c_river_1d_instant(
        k, Ex, u, A, M, Ch, Qh, x, Δx, t*3600, Δt)
    t = res[:, 1, 0]
    x = res[1, :, 1]
    y = res[:, :, 2]
    matplot.generateAnimateLines(x, y, t, outfile)

    data = []
    for index, item in enumerate(t):
        data.append({
            "t": item,
            "concerntration": y[index].tolist()
        })

    return {"id": appid, "x": x.tolist(), "t": t.tolist(), "data": data}


def cal_c_river_2d_side_instant_plot(appid, sdir, title, Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数
    fill : true/false 是否填充

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_river_2d_instant_side(
        Ch, M, h, Ex, Ey, u, k, x, y, t*3600, Δx, Δy, Δt)
    t = res[:, 1, 1, 0]
    x = res[0, :, 1, 1]
    y = res[0, 1, :, 2]
    X, Y = np.meshgrid(x, y)
    tdata = []
    for index, item in enumerate(t):
        Z = res[index, :, :, 3]
        Z = np.transpose(Z)

        plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签
        if(fill):
            contour = plt.contourf(X, Y, Z)
            plt.colorbar(contour)
        else:
            contour = plt.contour(X, Y, Z)
        plt.xlabel("河流纵向距离（m）")
        plt.ylabel("河流横向距离（m）")
        # 等高线上标明z（即高度）的值，字体大小是10，颜色分别是黑色和红色
        plt.clabel(contour, fontsize=10, fmt='%.'+str(num)+'f')
        plt.title(title + ' t='+str(item)+' s')
        plt.savefig(os.path.join(sdir, appid+'-'+str(index)+'.png'))
        plt.close()

        data = []
        for indexx, itemx in enumerate(x):
            data.append({
                "x": itemx,
                "concerntration": Z[:, index].tolist()
            })
        tdata.append(data)

        # 输出为csv数据文件
        fn=os.path.join(sdir, appid+'-'+str(index)+'.csv')
        with open(fn, 'w') as f:
            f.write('x/y')
            for index, item in enumerate(y):
                f.write(','+str(item))
            f.write('\n')
            for indexx, itemx in enumerate(x):
                f.write(str(itemx))
                for indexy, itemx in enumerate(y):
                    f.write(','+str(data[indexx]['concerntration'][indexy]))
                f.write('\n')

    return {"id": appid, "x": x.tolist(), "y": y.tolist(), "data": tdata}


def cal_c_river_2d_side_reflex_instant_plot(appid, sdir, title, Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill):
    """
    appid: 独立标识, 可认为是某次计算的id，这里作为结果文件的文件名
    sdir: 存储结果文件的路径
    num : z 小数点位数
    fill : true/false 是否填充

    """
    import matplotlib.pyplot as plt    
    from psyp.modelbase.weia import cal_c_jxj
    res = cal_c_jxj.cal_c_river_2d_instant_side_reflex(
        Ch, M, h, Ex, Ey, u, k, x, y, t*3600, Δx, Δy, Δt)
    t = res[:, 1, 1, 0]
    x = res[0, :, 1, 1]
    y = res[0, 1, :, 2]
    X, Y = np.meshgrid(x, y)
    tdata = []
    for index, item in enumerate(t):
        Z = res[index, :, :, 3]
        Z = np.transpose(Z)

        plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 用来正常显示中文标签
        if(fill):
            contour = plt.contourf(X, Y, Z)
            plt.colorbar(contour)
        else:
            contour = plt.contour(X, Y, Z)
        plt.xlabel("河流纵向距离（m）")
        plt.ylabel("河流横向距离（m）")
        # 等高线上标明z（即高度）的值，字体大小是10，颜色分别是黑色和红色
        plt.clabel(contour, fontsize=10, fmt='%.'+str(num)+'f')
        plt.title(title + ' t='+str(item)+' s')
        plt.savefig(os.path.join(sdir, appid+'-'+str(index)+'.png'))
        plt.close()

        data = []
        for indexx, itemx in enumerate(x):
            data.append({
                "x": itemx,
                "concerntration": Z[:, index].tolist()
            })
        tdata.append(data)

    return {"id": appid, "x": x.tolist(), "y": y.tolist(), "data": tdata}
#endregion

#region SWMM API
def inp2geojson(appid,sdir,prjname,crs_in,crs_out):
    from psyp.modelbase.swmm import swmmio
    return swmmio.inp2geojson(sdir,prjname,crs_in,crs_out)

def generateSWMM(appid, sdir, title):
    from psyp.modelbase.swmm import swmmio
    swmmio.generateInp(sdir, title)

def updateSWMM(appid,sdir,prjname):
    from psyp.modelbase.swmm import swmmio
    swmmio.updateInp(sdir, prjname)

def update_cat_params(appid,sdir,prjname,params):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_cat_params(sdir,prjname,params)

def update_ldu_buildup_params(appid,sdir,prjname,params):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_ldu_buildup_params(sdir,prjname,params)

def update_ldu_washoff_params(appid,sdir,prjname,params):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_ldu_washoff_params(sdir,prjname,params)

def update_raingage_tsdata(appid,sdir,prjname,startTime,forecastngTime,endTime,forecastRain):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_raingage_tsdata(sdir,prjname,startTime,forecastngTime,endTime,forecastRain)

def update_raingage_tsdata2(appid,sdir, prjname, rainData_time,rainData_value):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_raingage_tsdata2(sdir, prjname, rainData_time,rainData_value)

def update_options_simulationTime(appid,sdir,prjname,startTime,endTime):
    from psyp.modelbase.swmm import swmmio
    swmmio.update_options_simulationTime(sdir,prjname,startTime,endTime)

def runAndGetStatistic(appid,sdir,prjname):
    from psyp.modelbase.swmm import swmmio
    swmmio.runAndGetStatistic(sdir,prjname)

def runAndExtractSubcatchmentResults(appid, sdir, prjname, m11tsFolder,
                                     swmm_m11_mapfile):
    from psyp.modelbase.swmm import swmmio
    res = swmmio.runAndExtractSubcatchmentResults(sdir, prjname, m11tsFolder,
                                            swmm_m11_mapfile)
    return res

def runAndExtractJuncionResults(appid, sdir, prjname, m11tsFolder,
                                     swmm_m11_mapfile):
    from psyp.modelbase.swmm import swmmio
    res = swmmio.runAndExtractJuncionResults(sdir, prjname, m11tsFolder,
                                            swmm_m11_mapfile)
    return res

def runAndExtractCatchmentsResults4HecRas(appid, sdir, prjname, tsFolder,
                                     mapfile):
    from psyp.modelbase.swmm import swmmio
    res = swmmio.runAndExtractCatchmentsResults4HecRas(sdir,prjname,tsFolder,
                                            mapfile)
    return res

def runAndExtractResults(appid,sdir, prjname, objectId,dataType,out2file=False):
    from psyp.modelbase.swmm import swmmio
    res = swmmio.runAndExtractResults(sdir,prjname,objectId,dataType,out2file)
    return res

def runMike11(appid,m11engine,m11Dir,m11Prjname):
    from psyp.modelbase.mike import sypMikeio
    configfile = os.path.join(m11Dir,m11Prjname)
    sypMikeio.runMike11(m11engine,configfile)

def runSwmmFollowedMike11(appid, swmmDir, swmmPrjname, m11engine, m11Dir, m11Prjname,
                          m11tsFolder, swmm_m11_mapfile
                          ):
    from psyp.modelbase.mike import sypMikeio
    from psyp.modelbase.swmm import swmmio
    # 运行 smww
    m11tsFolder = os.path.join(m11Dir, m11tsFolder)
    swmmio.runAndExtractSubcatchmentResults(swmmDir, swmmPrjname, m11tsFolder,
                                            swmm_m11_mapfile)
    # 运行 mike11
    configfile = os.path.join(m11Dir, m11Prjname+'.sm11')
    sypMikeio.runMike11(m11engine, configfile)

def run_swmm_pest(appid,pest_engine, python_engine,method_file,
swmm_dir,prjname, cal_params,obs_mapdata):
    from psyp.modelbase.swmm import swmmio
    swmmio.run_swmm_pest(pest_engine, python_engine,method_file,
    swmm_dir,prjname, cal_params,obs_mapdata)

def getSwmmModelRainData(appid,sdir,prjname):
    from psyp.modelbase.swmm import swmmio
    return swmmio.getSwmmModelRainData(sdir,prjname)

def getSwmmSimulationTime(appid,sdir,prjname):
    from psyp.modelbase.swmm import swmmio
    return swmmio.getSwmmSimulationTime(sdir,prjname)
    
#endregion

#region HEC-RAS API
def run_hecras(appid,dir,prj_name):
    from psyp.modelbase.hecras import rasio
    rasio.runModel(dir,prj_name)
#endregion

#region Air Diffusion Model API
def run_air_diffusion_gauss_model(appid,dir,method,
                                  x0, y0, crs_in, crs_out,
                                  A,
                                  q, Qv, h, us, usa, Ts,
                                  D, surfaceType,
                                  u, wind_angle, Ta, w,
                                  k=0.0052194, Pa=10.057,
                                  h_fix_method='Holland',
                                  deltx=50, delty=50, radius=3500, delt_angle=30):
    from psyp.modelbase.air import gauss_model
    return gauss_model.run_air_diffusion_gauss_model(appid,dir,method,
                                  x0, y0, crs_in, crs_out,
                                  A,
                                  q, Qv, h, us, usa, Ts,
                                  D, surfaceType,
                                  u, wind_angle, Ta, w,
                                  k, Pa,
                                  h_fix_method,
                                  deltx, delty, radius, delt_angle)
#endregion

#region HSPF API
def init_hspf(appid,dir,prj_name):
    from psyp.modelbase.hspf import hspfio
    hspfio.init2(dir,prj_name)
    return 'ok!'
#endregion