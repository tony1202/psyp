import json
import urllib.request
import socket
from io import BytesIO
import gzip
import time

def webrequest(url, params, headers):
    # 使用parse模块中的urlencode(返回值类型是字符串类型)进行编码处理
    data = urllib.parse.urlencode(params)

    # 将步骤二的编码结果转换成byte类型
    data = data.encode()
    
    # timeout in seconds
    timeout = 200
    socket.setdefaulttimeout(timeout)
    if(headers==''):
        request = urllib.request.Request(url=url) # 有些网站不给header会报 405错误
    else: 
        request = urllib.request.Request(url=url,headers=headers)

    try:        
        # with urllib.request.urlopen(url, data=data, header=headers) as respoonse:
        with urllib.request.urlopen(request,data) as respoonse:
            html = respoonse.read()#.decode('utf-8')
            return html
    except socket.timeout:
        return -1

def webrequest2(url, params, headers):

    data = json.dumps(params).encode('utf-8')
    
    # timeout in seconds
    timeout = 200
    socket.setdefaulttimeout(timeout)
    if(headers==''):
        request = urllib.request.Request(url=url) # 有些网站不给header会报 405错误
    else: 
        request = urllib.request.Request(url=url,headers=headers)

    try:        
        # with urllib.request.urlopen(url, data=data, header=headers) as respoonse:
        with urllib.request.urlopen(request,data) as respoonse:
            html = respoonse.read()
            respoonse.close()
            # buff = BytesIO(html)
            # f = gzip.GzipFile(fileobj=buff)
            time.sleep(10)
            return html.decode('utf-8')
    except socket.timeout:
        return -1
