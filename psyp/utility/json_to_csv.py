import os
import json
import csv


def json_to_csv(directory):
    csv_data = []

    # Iterate over files in the directory
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".txt"):
                file_path = os.path.join(root, file)
                with open(file_path, "r", encoding='utf-8') as json_file:
                    # Load JSON data
                    json_data = json.load(json_file)
                    # Assuming each JSON file contains a list of dictionaries
                    if isinstance(json_data['data'], dict):
                        item = json_data['data']
                        for key in item.keys():
                            if key == 'title':
                                continue
                            ts = item[key]['name']
                            data = item[key]['value']
                            if len(ts) > 0:
                                csv_file_path = os.path.splitext(
                                    file_path)[0] + '-' + key + ".csv"
                                with open(csv_file_path, "w", newline="") as csv_file:
                                    writer = csv.writer(csv_file)
                                    zipped = zip(ts, data)
                                    writer.writerows(zipped)
                    else:
                        # Create a CSV file for each JSON file
                        csv_file_path = os.path.splitext(file_path)[0] + ".csv"
                        with open(csv_file_path, "w", newline="") as csv_file:
                            writer = csv.writer(csv_file)
                            for item in json_data['data']:
                                # Extract values and write to CSV
                                writer.writerow(list(item.values()))

def json_to_csv2(file_path):
    with open(file_path, "r", encoding='utf-8') as json_file:
         # Load JSON data
         json_data = json.load(json_file)
         # Extract required fields
    rows = []
    for entry in json_data["data"]:
        tips_code = entry["tipsCode"]
        for location_detail in entry["tipsLocationDetailVOS"]:
            location_id = location_detail["id"]
            location_name = location_detail["name"]
            location = location_detail["location"]
            rows.append([tips_code, location_id, location_name, location])

    # Write to CSV
    with open(file_path+'.csv', 'w', newline='', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile)
        # Write header
        csv_writer.writerow(["tipsCode", "id", "name", "location"])
        # Write rows
        csv_writer.writerows(rows)

if __name__ == '__main__':
    # directory_path = r'C:\sypcloud_git\projects\109_cz\rawdata\CZ在线监测'
    # Call the function to convert JSON files to CSV
    # json_to_csv(directory_path)
    jsonfile = r'C:\sypcloud_git\projects\120_西南项目\rawdata\大陆溪平台监测点位.json'
    json_to_csv2(jsonfile)

