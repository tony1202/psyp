
import json
import pandas as pd
from geojson import Point, LineString, Polygon, FeatureCollection, Feature
import os
import shutil

def write_shapefile(df, filename, geomtype='line', prj=None):
    """
    create a shapefile given a pandas Dataframe that has coordinate data in a
    column called 'coords'.
    """

    import shapefile
    df['Name'] = df.index

    # create a shp file writer object of geom type 'point'
    if geomtype == 'point':
        w = shapefile.Writer(shapefile.POINT)
    elif geomtype == 'line':
        w = shapefile.Writer(shapefile.POLYLINE)
    elif geomtype == 'polygon':
        w = shapefile.Writer(shapefile.POLYGON)

    # use the helper mode to ensure the # of records equals the # of shapes
    # (shapefile are made up of shapes and records, and need both to be valid)
    w.autoBalance = 1

    #region
    # # add the fields
    # for fieldname in df.columns:
    #     w.field(fieldname, "C")

    # for k, row in df.iterrows():
    #     w.record(*row.tolist())
    #     w.line(parts=[row.coords])

    # w.save(filename)

    # # add projection data to the shapefile,
    # if prj is None:
    #     # if not sepcified, the default, projection is used (PA StatePlane)
    #     prj ='' # os.path.join(ROOT_DIR, 'swmmio/defs/default.prj')
    # prj_filepath = os.path.splitext(filename)[0] + '.prj'
    # shutil.copy(prj, prj_filepath)
    #endregion

def write_geojson(df, crs_in, crs_out, filename=None, geomtype='linestring',
     drop_na=True):
    """
    convert dataframe with coords series to geojson format
    :param df: target dataframe
    :param filename: optional path of new file to contain geojson
    :param geomtype: geometry type [linestring, point, polygon]
    :param drop_na: whether to remove properties with None values
    :return: geojson.FeatureCollection
    >>> from swmmio.examples import philly
    >>> geoj = write_geojson(philly.links.dataframe, drop_na=True)
    >>> print(json.dumps(geoj['features'][0]['properties'], indent=2))
    {
      "InletNode": "J1-025",
      "OutletNode": "J1-026",
      "Length": 309.456216,
      "Roughness": 0.014,
      "InOffset": 0,
      "OutOffset": 0.0,
      "InitFlow": 0,
      "MaxFlow": 0,
      "Shape": "CIRCULAR",
      "Geom1": 1.25,
      "Geom2": 0,
      "Geom3": 0,
      "Geom4": 0,
      "Barrels": 1,
      "Name": "J1-025.1"
    }
    >>> print(json.dumps(geoj['features'][0]['geometry'], indent=2))
    {
      "type": "LineString",
      "coordinates": [
        [
          2746229.223,
          1118867.764
        ],
        [
          2746461.473,
          1118663.257
        ]
      ]
    }
    """

    # CONVERT THE DF INTO JSON
    df['Name'] = df.index  # add a name column (we wont have the index)
    records = json.loads(df.to_json(orient='records'))

    # ITERATE THROUGH THE RECORDS AND CREATE GEOJSON OBJECTS
    features = []
    for rec in records:

        coordinates = rec['coords']
        coordinates = change_crs(coordinates,crs_in,crs_out)
        del rec['coords']  # delete the coords so they aren't in the properties
        if drop_na:
            rec = {k: v for k, v in rec.items() if v is not None}
        latlngs = coordinates

        if geomtype == 'linestring':
            geometry = LineString(latlngs)
        elif geomtype == 'point':
            geometry = Point(latlngs)
        elif geomtype == 'polygon':
            geometry = Polygon([latlngs])

        feature = Feature(geometry=geometry, properties=rec)
        features.append(feature)

    if filename is not None:
        with open(filename, 'wb') as f:
            f.write(json.dumps(FeatureCollection(features)))
        return filename

    else:
        return FeatureCollection(features)

def change_crs(series, in_crs, to_crs):
    """
    Change the projection of a series of coordinates
    :param series:
    :param to_crs:
    :param in_crs:
    :return: series of reprojected coordinates
    >>> import swmmio
    >>> m = swmmio.Model(MODEL_FULL_FEATURES_XY)
    >>> proj4_str = '+proj=tmerc +lat_0=36.16666666666666 +lon_0=-94.5 +k=0.9999411764705882 +x_0=850000 +y_0=0 +datum=NAD83 +units=us-ft +no_defs' #"+init=EPSG:102698"
    >>> m.crs = proj4_str
    >>> nodes = m.nodes()
    >>> change_crs(nodes['coords'], proj4_str, "EPSG:4326")
    Name
    J3    [(39.236286854940964, -94.64346373821752)]
    1      [(39.23851590020802, -94.64756446847099)]
    2       [(39.2382157223383, -94.64468629488778)]
    3      [(39.23878251491925, -94.64640342340165)]
    4     [(39.238353081411915, -94.64603818939938)]
    5      [(39.23797714290924, -94.64589184224722)]
    J2     [(39.23702605103406, -94.64543916929885)]
    J4     [(39.23633648359375, -94.64190240294558)]
    J1     [(39.23723558954326, -94.64583338271147)]
    Name: coords, dtype: object
    """
    try:
        import pyproj
        from pyproj import Transformer
    except ImportError:
        raise ImportError('pyproj module needed. get this package here: ',
                          'https://pypi.python.org/pypi/pyproj')

    # SET UP THE TO AND FROM COORDINATE PROJECTION
    transformer = Transformer.from_crs(in_crs, to_crs, always_xy=True)

    # convert coords in coordinates, vertices, and polygons inp sections
    # transform to the typical 'WGS84' coord system
    def get_xys(xy_row):
        # need to reverse to lat/long after conversion
        return [transformer.transform(x, y) for x, y in xy_row]

    if isinstance(series, pd.Series):

        # unpack the nested coords
        xs = [xy[0][0] for xy in series.to_list()]
        ys = [xy[0][1] for xy in series.to_list()]

        # transform the whole series at once
        xs_trans, ys_trans = transformer.transform(xs, ys)

        # increase nest level and return pd.Series
        return pd.Series(index=series.index, data=[[coords] for coords in zip(xs_trans, ys_trans)])

    if isinstance(series, pd.DataFrame):
        # zipped_coords = list(zip(series.X, series.Y))
        xs_trans, ys_trans = transformer.transform(series.X, series.Y)
        df = pd.DataFrame(data=zip(xs_trans, ys_trans), columns=["X", "Y"], index=series.index)
        return df
    elif isinstance(series, (list, tuple)):
        if isinstance(series[0], (list, tuple)):
            # unpack the nested coords
            xs = [x for x, y in series]
            ys = [y for x, y in series]
            # add and modified by jip
            xs_trans, ys_trans = transformer.transform(xs, ys)
            return [[lat,lng] for lat, lng in zip(xs_trans,ys_trans)]
            # return list(zip(transformer.transform(xs, ys)))
        else:
            return transformer.transform(*series)

def mkprofile(src,lon_1,lat_1,lon_2,lat_2,width=100,dist=100,resample='near',tif='',csv=''):
    '''
    author blog: https://kokoalberti.com/articles/creating-elevation-profiles-with-gdal-and-two-point-equidistant-projection/
    author github: https://github.com/kokoalberti/geocmd/blob/master/mkprofile/mkprofile.py
    modified by jip
    '''
    import sys
    from pyproj import CRS, Transformer
    from osgeo import gdal

    # Open/validate the source dataset
    ds = gdal.Open(src)
    if not ds:
        print("Could not open dataset.")
        sys.exit(1)

    # Set up coordinate transform
    proj_str = "+proj=tpeqd +lon_1={} +lat_1={} +lon_2={} +lat_2={}".format(lon_1, lat_1, lon_2, lat_2)
    tpeqd = CRS.from_proj4(proj_str)
    transformer = Transformer.from_crs(CRS.from_proj4("+proj=latlon"), tpeqd)

    # Transfor to tpeqd coordinates
    point_1 = transformer.transform(lon_1, lat_1)
    point_2 = transformer.transform(lon_2, lat_2)

    # Create an bounding box (minx, miny, maxx, maxy) in tpeqd coordinates
    bbox = (point_1[0], -(width*0.5), point_2[0], (width*0.5))

    # Calculate the number of samples in our profile.
    num_samples = int((point_2[0] - point_1[0]) / dist)
    print("Reading and warping data into profile swath...")

    # Warp it into dataset in tpeqd projection. If tif is empty GDAL will
    # interpret it as an in-memory dataset.
    format = 'GTiff' if tif else 'VRT'
    profile = gdal.Warp(tif, ds, dstSRS=proj_str, outputBounds=bbox, 
                        height=1, width=num_samples, resampleAlg=resample, 
                        format=format)

    # Extract the pixel values and write to an output file
    data = profile.GetRasterBand(1).ReadAsArray()
    print("Created {}m profile with {} samples.".format(dist*num_samples, num_samples))

    # Write csv output
    if csv:
        with open(csv, 'w') as f:
            f.write("dist,value\n")
            for (d, value) in enumerate(data[0,:]):
                f.write("{},{}\n".format(d*dist, value))
        print("Saved as {}".format(csv))

    # Clean up
    profile = None
    ds = None