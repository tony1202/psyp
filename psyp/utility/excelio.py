import pandas as pd
import os

def read_data_from_table(data_dir,
                         file, sheet=0): 
    '''reads curves or other tables from excel or csv'''
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.xlsx' or file_extension == '.xls':
        data_df = pd.read_excel(os.path.join(data_dir,file),sheet_name = sheet)
    if file_extension == '.csv':
        data_df = pd.read_csv(os.path.join(data_dir,file))
    return data_df