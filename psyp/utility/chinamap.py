#import the nessarial libary:geopandas,cartopy,gma
import geopandas as gpd 
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import gma 
#import the metadata 
china_land_district= gpd.read_file("./China map/中国省级地图GS（2019）1719号.geojson")
china_boundary = gpd.read_file("./China map/九段线GS（2019）1719号.geojson") 
import gma.extend.mapplottools as mpt
import gma.extend.arrayenhancement as aec
projn = ccrs.LambertConformal(central_longitude=105, 
                              central_latitude=40,
                              standard_parallels=(25.0, 47.0))

fig = plt.figure(figsize=(4,3.5),dpi=120,facecolor="w")
ax = fig.add_subplot(projection=projn)
ax.set_global()
#ax.stock_img()
ax.set_global()
#ax.stock_img()
ax.set_facecolor('#BEE8FF')
ax.add_feature(cfeature.LAND, facecolor='white')
ax.add_feature(cfeature.LAKES.with_scale('110m'), facecolor='#BEE8FF')
ax.set_extent([80, 130, 16, 55], crs=ccrs.PlateCarree())
#添加geopandas 读取的地理文件
ax.add_geometries(china_land_district["geometry"],crs=ccrs.PlateCarree(),
                  fc="None",ec="black",linewidth=.2)
ax.add_geometries(china_boundary["geometry"],crs=ccrs.PlateCarree(),
                  fc="None",ec="black",linewidth=.3)
#ax.coastlines(linewidth=0.3,zorder=20)
#ax.add_feature(cfeature.LAND, facecolor='gainsboro')
#ax.coastlines(linewidth=0.3,zorder=20)

gls = ax.gridlines(draw_labels=True, crs=ccrs.PlateCarree(), 
                   color='k', linestyle='dashed', linewidth=0.3, 
                   y_inline=False, x_inline=False,
                   rotate_labels=0,xpadding=5,
                   xlocs=range(-180,180,10), ylocs=range(-90,90,10),
                   xlabel_style={"size":8,"weight":"bold"},
                   ylabel_style={"size":8,"weight":"bold"}
                  )
gls.top_labels= False                      
gls.right_labels=False
ax2 = fig.add_axes([0.83, 0.09, 0.13, 0.25], projection = projn)
ax2.set_extent([104.5, 125, 0, 26])
ax2.set_facecolor('#BEE8FF')
ax2.spines['geo'].set_linewidth(.2)
# 设置网格点
lb=ax2.gridlines(draw_labels=False,x_inline=False, y_inline=False,
                 linewidth=0.1, color='gray', alpha=0.8, 
                 linestyle='--' )
ax2.add_geometries(china_land_district["geometry"],crs=ccrs.PlateCarree(),
                  fc="None",ec="black",linewidth=.3)
ax2.add_geometries(china_boundary["geometry"],crs=ccrs.PlateCarree(),
                  fc="None",ec="black",linewidth=.5)
ax2.add_feature(cfeature.LAND, facecolor='w')

## n.1 添加指北针
#mpt.AddCompass(ax, LOC = (0.07, 0.86), SCA = 0.04, FontSize = 8)
## n.2 添加比例尺
mpt.AddScaleBar(ax, LOC = (0.2, 0.05), SCA = 0.12, FontSize = 6, 
                UnitPad = 0.2, BarWidth = 0.6)
plt.tight_layout()
plt.savefig('china map.png',dpi=300)
plt.show()