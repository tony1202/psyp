import numpy as np
import time  
import matplotlib.pyplot as plt  
from matplotlib.animation import FuncAnimation, PillowWriter
from scipy.interpolate import make_interp_spline

def generateAnimateLine(x,y,t,file):
    fig, ax = plt.subplots()    
    ln, = plt.plot(x, y[0], '-r') 
    # 设置初始绘图
    def init():
        ymax = max(list(map(max,y)))
        ax.set_xlim(np.min(x), np.max(x))  
        ax.set_ylim(np.min(y[0]), ymax+ymax*0.3)
        return ln,
    # 动态更新
    def update(frame):        
        ln.set_data(x, y[frame]) 
        return ln,
    ani = FuncAnimation(fig=fig, func=update, frames=np.arange(1,len(t)),
    init_func=init, blit=True)  
    # plt.show()
    writer = PillowWriter(fps=1)  
    ani.save(file, writer=writer)  

def generateAnimateLines(x,y,t,file):
    fig, ax = plt.subplots()

    fig.set_figheight(3)
    fig.set_figwidth(5)

    lines = []
    # 设置初始绘图
    def init():
        ymax = max(list(map(max,y)))
        # x_ymax = np.argmax(y[0])
        # show_max='['+str(x_ymax)+' '+ str(y[0][x_ymax])+']'
        
        ax.set_xlim(np.min(x), np.max(x))  
        ax.set_ylim(np.min(y[0]), ymax+ymax*0.3)
        
        x_smooth = np.linspace(np.min(x), np.max(x), 300)
        y_smooth = make_interp_spline(x, y[0])(x_smooth)
        
        # plt.plot(x_ymax,y[0][x_ymax],'ko') 
        # plt.annotate(show_max,xy=(x_ymax,y[0][x_ymax]),xytext=(x_ymax,y[0][x_ymax]))
        
        ln, = plt.plot(x_smooth, y_smooth,'-r')
        lines.append(ln)
        ax.set_title('t= '+str(t[0])+' s', fontsize=12, color='r')
        return lines
    # 动态更新
    def update(frame): 
        if frame == 2:
            ax.set_ylim(np.min(y[frame]), np.max(y[frame])+np.max(y[frame]*0.3))
        for index, item in enumerate(lines):
            item.set_color('0.5')
            # item.set_linestyle('dashdot')
            item.set_linewidth('0.5')
        
        ymax = max(y[frame])
        # x_ymax = np.argmax(y[frame])
        # show_max='['+str(x_ymax)+' '+ str(y[frame][x_ymax])+']'
        
        x_smooth = np.linspace(np.min(x), np.max(x), 300)
        y_smooth = make_interp_spline(x, y[frame])(x_smooth)
        
        # plt.plot(x_ymax,y[frame][x_ymax],'ko') 
        # plt.annotate(show_max,xy=(x_ymax,y[frame][x_ymax]),xytext=(x_ymax,y[frame][x_ymax]))
        
        ln, = plt.plot(x_smooth, y_smooth,'-r')
        lines.append(ln)
        ax.set_title('t= '+str(t[frame])+' s', fontsize=12, color='r')
        if frame == 1:
            time.sleep(200)
        return lines
    # 动画
    ani = FuncAnimation(fig=fig, func=update, frames=np.arange(1,len(t)),
    init_func=init, blit=True)  
    # plt.show()
    # 输出为gif
    writer = PillowWriter(fps=1)  
    ani.save(file, writer=writer)  