import graphviz

# 创建有向图
dot = graphviz.Digraph(comment='Water Pollution Source Emission Inventory Construction Software')

# 添加节点及其标签
dot.node('A', 'Overall Design and Development')
dot.node('B', 'Database Design and Development')
dot.node('C', 'Interface Design and Development')
dot.node('D', 'User Interface Design and Development')

# 添加边及其标签
dot.edge('A', 'B', 'Design and development based on the results of the previous research')
dot.edge('A', 'C', 'Exploratory design and development from the perspective of computer-aided tools')
dot.edge('A', 'D', 'Design and development of overall software framework')
dot.edge('B', 'C', 'Determine the structure of various database tables and establish logical relationships between them')
dot.edge('B', 'D', 'Complete the database backend programming of the designed forms')
dot.edge('C', 'D', 'Design and development of user interface based on the needs, preferences and behaviors of users')

# 显示图形
dot.render('water_pollution_source_emission_inventory_construction_software', view=True)
