import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

data = np.loadtxt(r'tests\testdata\utility\data.txt')
x = data[:,0]
y = data[:,1]

f = interp1d(x, y)
x_new = np.linspace(np.min(x), np.max(x), int(np.max(x)/20))
y_new = f(x_new)

with open(r'tests\testdata\utility\data-new.csv','w+') as f:
    for index, val in enumerate(x_new):
        f.write(str(val)+','+str(y_new[index])+'\n')
    f.close
