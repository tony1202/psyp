from windrose import WindroseAxes
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import numpy as np

# Create wind speed and direction variables

def draw_wndrose(ws,wd,imgfile):
    # ws = np.random.random(500) * 6
    # wd = np.random.random(500) * 360
    wd = wind_direction_change(wd)
    ax = WindroseAxes.from_ax()
    # ax.bar(wd, ws, normed=True, opening=0.8, edgecolor='white')
    ax.contourf(wd, ws, cmap=cm.hot)
    ax.contour(wd, ws, colors='black')
    ax.set_theta_zero_location('N')#改变0°位置
    ax.set_theta_direction(-1)
    ax.set_legend()
    plt.savefig(imgfile)

#角度预处理函数，用来画准确的风向
def wind_direction_change(wd):
    wind_direction = np.array(wd)
    wind_direction[np.where((wind_direction >= 0)&(wind_direction <= 135))[0]] = 90 - wind_direction[np.where((wind_direction >= 0)&(wind_direction <= 135))[0]]
    wind_direction[np.where((wind_direction >= 315)&(wind_direction < 360))[0]] = 90 - wind_direction[np.where((wind_direction >= 315)&(wind_direction < 360))[0]]
    wind_direction[np.where((wind_direction > 135)&(wind_direction < 315))[0]] = 450 - wind_direction[np.where((wind_direction > 135)&(wind_direction < 315))[0]]
    wind_direction[np.where(wind_direction < 0)[0]] = 360 + wind_direction[np.where(wind_direction < 0)[0]]
    return wind_direction
