import numpy as np
import matplotlib.pyplot as plt
from deap import algorithms, base, creator, tools

# Define the fitness function (f)
def f(individual):
    x1, x2, x3, x4, x5, x6, x7, x8 = individual
    # Define the objectives (f1, f2, f3) using the equations from the MATLAB code
    f1 = x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8
    RP = 3.33 * f1 - 18667
    S = 4.33 * f1 - 24267
    f2 = 0.9363 * f1 - 0.000488 * (RP ** 2) + 5.936 * RP - 0.0003 * (S ** 2) + 3.33 * S + 68517
    f3 = 0.436 * f1 - 0.000237 * (RP ** 2) + 2.916 * RP - 0.000146 * (S ** 2) + 1.82 * S + 8104
    return f1, f2, f3

# Define the problem
creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0, -1.0))
creator.create("Individual", list, fitness=creator.FitnessMin)

# Initialize the Toolbox
toolbox = base.Toolbox()

# Define the decision variables range
nvars = 8
lower_bound = [0, 0, 0, 0, 0, 0, 0, 0]
upper_bound = [36300, 14700, 268, 150, 94, 65, 34, 891]
toolbox.register("attr_float", np.random.uniform, lower_bound, upper_bound)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

# Register the evaluation function (fitness function)
toolbox.register("evaluate", f)

# Register the mating and mutation operators
toolbox.register("mate", tools.cxSimulatedBinaryBounded, eta=1.0)
toolbox.register("mutate", tools.mutPolynomialBounded, eta=1.0, indpb=0.1, low=lower_bound, up=upper_bound)
toolbox.register("select", tools.selNSGA2)

# Define the main function to perform optimization
def main():
    # Set the parameters for the optimization
    pop_size = 300
    ngen = 200
    pareto_fraction = 0.3

    # Create the initial population
    pop = toolbox.population(n=pop_size)

    # Perform the optimization using NSGA-II algorithm
    algorithms.eaMuPlusLambda(pop, toolbox, mu=pop_size, lambda_=pop_size, cxpb=0.9, mutpb=0.1, ngen=ngen)

    # Extract the results of the optimization
    front = np.array([ind.fitness.values for ind in pop])

    # Plot the Pareto front
    plt.figure()
    plt.scatter(front[:, 0], front[:, 1], marker='o')
    plt.xlabel('Objective 1')
    plt.ylabel('Objective 2')
    plt.title('Pareto Front')
    plt.show()

if __name__ == "__main__":
    main()
