from datetime import datetime, date
import pandas as pd
import numpy as np
import os

def generate_swmm_inp_file(project_dir,prjName):
        inp_file_name = os.path.join(project_dir,prjName)
        swmm_data_dir = project_dir #'psyp/modelbase/swmm/data/'

        inp_dict = dict()
        inp_dict['junctions_df'] = pd.DataFrame()
        inp_dict['conduits_df'] = pd.DataFrame()
        inp_dict['storage_df'] = pd.DataFrame()
        inp_dict['vertices_dict'] = {}
        inp_dict['options_dict'] = {}

        # reading Data
        from .g_s_read_data import read_shapefiles
        file_outfalls = None # 'SWMM_outfalls.shp'
        file_storages = None # 'SWMM_storages.shp'
        file_subcatchments = 'SWMM_subcatchments.shp'
        file_conduits = None #'SWMM_conduits.shp'
        file_junctions = 'SWMM_junctions.shp'
        file_pumps = None # 'SWMM_pumps.shp'
        file_weirs = None # 'SWMM_weirs.shp'
        file_outlets = None # 'SWMM_outlets.shp'
        raw_data_dict = read_shapefiles(swmm_data_dir,
                                           file_outfalls,
                                           file_storages,
                                           file_subcatchments,
                                           file_conduits,
                                           file_junctions,
                                           file_pumps,
                                           file_weirs,
                                           file_outlets)
        # readling table
        """data in tables (curves, patterns, inflows ...)"""
        file_curves = 'gisswmm_curves.xlsx'
        file_patterns = 'gisswmm_patterns.xlsx'
        file_options = 'gisswmm_options.xlsx'
        file_timeseries = 'gisswmm_timeseries.xlsx'
        file_inflows = 'gisswmm_inflows.xlsx'
        file_quality = 'gisswmm_quality.xlsx'

        from .g_s_read_data import read_data_from_table

        if file_options is not None:
            raw_data_dict['options_df'] = read_data_from_table(swmm_data_dir,file_options)
        
        if file_timeseries is not None:
            raw_data_dict['timeseries'] = read_data_from_table(swmm_data_dir, file_timeseries)   
        
        if file_quality is not None:
            raw_data_dict['quality']={}
            for quality_param in['POLLUTANTS', 'LANDUSES', 'COVERAGES','LOADINGS']:
                raw_data_dict['quality'][quality_param] = read_data_from_table(swmm_data_dir,
                             file_quality,
                             sheet = quality_param)

        """options"""
        from .g_s_various_functions import get_options_from_table
        inp_dict['options_dict'] = get_options_from_table(raw_data_dict['options_df'].copy())

        """subcatchments"""
        if 'subcatchments_raw' in raw_data_dict.keys():
            from .g_s_subcatchments import get_subcatchments_from_shapefile
            from .g_s_various_functions import get_coords_from_geometry
            subcatchments_df = get_subcatchments_from_shapefile(raw_data_dict['subcatchments_raw'])
            inp_dict['polygons_dict'] = get_coords_from_geometry(subcatchments_df)
            inp_dict['subcatchments_df'] = subcatchments_df
        
        from .g_s_various_functions import get_coords_from_geometry
        if 'junctions_raw' in raw_data_dict.keys():
            junctions_df = raw_data_dict['junctions_raw'].copy()
            junctions_df['X_Coord'],junctions_df['Y_Coord'] = get_coords_from_geometry(junctions_df)
            inp_dict['junctions_df'] = junctions_df
        
        """time series"""
        if 'timeseries' in raw_data_dict.keys():
            from .g_s_various_functions import get_timeseries_from_table, get_raingages_from_timeseries
            inp_dict['timeseries_dict'] = get_timeseries_from_table(raw_data_dict['timeseries'],
                                                                 name_col='Name')
            """rain gages"""
            inp_dict['raingages_dict'] = get_raingages_from_timeseries(inp_dict['timeseries_dict'])
        
        """quality"""
        if 'quality' in raw_data_dict.keys():
            from .g_s_quality import get_quality_params_from_table
            if 'subcatchments_df' in inp_dict.keys():
                inp_dict['quality_dict'] = get_quality_params_from_table(raw_data_dict['quality'], inp_dict['subcatchments_df'].copy())
            else: 
                inp_dict['quality_dict'] = get_quality_params_from_table(raw_data_dict['quality'])
        
        """writing inp"""
        # from .g_s_write_inp import write_inp
        from .g_s_write_inp_default import write_inp
        write_inp(inp_file_name,
                  project_dir,
                  inp_dict)

if __name__ == '__main__':
    generate_swmm_inp_file('d:\\','test.inp')


