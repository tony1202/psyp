import os
from random import random
import random as rd
import subprocess
from pyswmm import Simulation, Nodes, Subcatchments
from pyswmm.swmm5 import PySWMM
import pyswmm.toolkitapi as tka
import swmmio
import tempfile
import shutil
from mikeio.dfs0 import Dfs0
from mikeio.eum import TimeStepUnit, EUMType, EUMUnit, ItemInfo
import numpy as np
from datetime import date, datetime, time, timedelta
import pandas as pd
from .generate_swmm_inp_file import generate_swmm_inp_file
from psyp.utility.threadio import BaseThread
from psyp.utility.gisutility import write_geojson


#region 前处理
def inp2geojson(sdir, prjname, crs_in, crs_out):
    '''
    crs_out: WGS84
    '''
    simfile = os.path.join(sdir, prjname)
    swmmobject = swmmio.Model(simfile)
    catchments = swmmobject.subcatchments
    # 坐标转换
    # origin_coords = catchments.dataframe['coords']
    # new_coords =change_crs(origin_coords,\
    #     crs_in,crs_out)
    # catchments.dataframe.drop(['coords'], axis=1)
    # catchments.dataframe['coords']=new_coords
    geojson = write_geojson(catchments.dataframe, crs_in, crs_out,
                            geomtype='polygon')
    return geojson

#endregion

def generateInp(sdir, prjname):
    generate_swmm_inp_file(sdir, prjname)

def updateInp(sdir, prjname):
    with tempfile.TemporaryDirectory() as tempdir:
        cp_path = os.path.join(tempdir, prjname)
        inp_file_name = os.path.join(sdir, prjname)
        # newInp_file_name = os.path.join(sdir,'new-'+prjname)
        # swmmobject = PySWMM(inp_file_name)
        # swmmobject.swmm_open()
        # swmmobject.setSubcatchParam('S_0',tka.SubcParams.area,950.5)
        # swmmobject.swmm_close()
        swmmobject = swmmio.Model(inp_file_name)
        # swmmobject.inp.subcatchments['Area']['S_1'] = 164.3
        swmmobject.inp.junctions[0]
        swmmobject.inp.save(cp_path)
        os.remove(inp_file_name)
        shutil.copy2(cp_path, inp_file_name)

def update_cat_params(sdir, prjname, params):
    '''
    params = [
        {
            'cat_name': 'S1',
            'Width': 100
        }
    ]
    '''
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            # newInp_file_name = os.path.join(sdir,'new-'+prjname)
            # swmmobject = PySWMM(inp_file_name)
            # swmmobject.swmm_open()
            # swmmobject.setSubcatchParam('S_0',tka.SubcParams.area,950.5)
            # swmmobject.swmm_close()
            swmmobject = swmmio.Model(inp_file_name)
            # swmmobject.inp.subcatchments['Area']['S_1'] = 164.3
            cats = list(swmmobject.inp.subcatchments.index)

            for cat in cats:
                # swmmobject.inp.subcatchments['Width'][cat] = swmmobject.inp.subcatchments['Width'][cat]*0.5
                swmmobject.inp.subcatchments['PercImperv'][cat] = swmmobject.inp.subcatchments['PercImperv'][cat]*1.15
                # swmmobject.inp.subareas['S-Imperv'][cat] = swmmobject.inp.subareas['S-Imperv'][cat]*2
                # swmmobject.inp.infiltration['MaxRate'][cat]=4*25.4
                # swmmobject.inp.infiltration['MinRate'][cat]=0.0065*25.4
                # swmmobject.inp.infiltration['Decay'][cat]=120
                # swmmobject.inp.infiltration['DryTime'][cat]=0.05
                # swmmobject.inp.infiltration['MaxInfil'][cat]=0

            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)

    except Exception as inst:
        return inst.args[0]

def update_ldu_buildup_params(sdir, prjname, params):
    '''
    params = [
        {
            'ldu': 'name',
            'data': [
                {
                    'pollutant': 'name',
                    'washoff': {
                        'coeficient': 0.1,
                        'exponent': 1,
                        'cleaning_coffic': 0,
                        'bmp_coffic': 0
                    },
                    'build_up': {
                        'max_buildup':20,
                        'rate_constant': 0.79,
                        'pow_constant': 1,
                    }
                }
            ]
        }
    ]
    params = {
        'LU_20':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_30':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_40':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_50':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_60':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_80':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_90':{
            'NH3N':{
                'Coeff1':10
            }
        }
    }
    '''
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            swmmobject = swmmio.Model(inp_file_name)
            cols_idx = list(set(list(swmmobject.inp.buildup.index)))
            # col_name = list(swmmobject.inp.buildup.columns.values)
            for idx in cols_idx:
                v_idx = 0
                for pollutant in swmmobject.inp.buildup['Pollutant'][idx]:
                    swmmobject.inp.buildup['Coeff1'][idx][v_idx] = swmmobject.inp.buildup['Coeff1'][idx][v_idx] * \
                        params[idx][pollutant]['Coeff1']
                    # swmmobject.inp.buildup['Coeff2'][idx][v_idx] = swmmobject.inp.buildup['Coeff2'][idx][v_idx] * params[idx][pollutant]['Coeff2']
                    # swmmobject.inp.buildup['Coeff2'][idx][v_idx] = params[idx][pollutant]['Coeff2']
                    # swmmobject.inp.buildup['Coeff3'][idx][v_idx] = params[idx][pollutant]['Coeff3']
                    # swmmobject.inp.buildup['Coeff3'][idx][v_idx] = swmmobject.inp.buildup['Coeff3'][idx][v_idx] * params[idx][pollutant]['Coeff3']
                    v_idx += 1
            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)

    except Exception as inst:
        return inst.args[0]

def update_ldu_washoff_params(sdir, prjname, params):
    '''
    params = [
        {
            'ldu': 'name',
            'data': [
                {
                    'pollutant': 'name',
                    'washoff': {
                        'coeficient': 0.1,
                        'exponent': 1,
                        'cleaning_coffic': 0,
                        'bmp_coffic': 0
                    },
                    'build_up': {
                        'max_buildup':20,
                        'rate_constant': 0.79,
                        'pow_constant': 1,
                    }
                }
            ]
        }
    ]
    params = {
        'LU_20':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_30':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_40':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_50':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_60':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_80':{
            'NH3N':{
                'Coeff1':10
            }
        },
        'LU_90':{
            'NH3N':{
                'Coeff1':10
            }
        }
    }
    '''
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            swmmobject = swmmio.Model(inp_file_name)
            cols_idx = list(set(list(swmmobject.inp.washoff.index)))
            # col_name = list(swmmobject.inp.washoff.columns.values)
            for idx in cols_idx:
                v_idx = 0
                for pollutant in swmmobject.inp.washoff['Pollutant'][idx]:
                    swmmobject.inp.washoff['Coeff1'][idx][v_idx] = params[idx][pollutant]['Coeff1']
                    swmmobject.inp.washoff['Coeff2'][idx][v_idx] = params[idx][pollutant]['Coeff2']
                    v_idx += 1
            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)

    except Exception as inst:
        return inst.args[0]

def update_raingage_tsdata(sdir, prjname, startTime, forecastngTime, endTime, forecastRain):
    '''
    认为模板模型的时间总是小于本次预报方案的时间，将模板边界的时间统一增加时间差
    startTime: 模型计算开始时间
    forecastngTime：起报时间
    endTime：模型计算结束时间
    forecastRain：预报的降雨，按下列标准
    下雨分6个等级。分别是小雨、中雨、大雨、暴雨、大暴雨、特大暴雨。
    0、无雨
    1、小雨：降雨量小于10mm者。
    2、中雨：降雨量10～25mm者。
    3、大雨：降雨量25～50mm者。
    4、暴雨：降雨量50～100mm者。
    5、大暴雨：降雨量100～250mm者。
    6、特大暴雨：1d（或24h）降雨量在250mm以上者。
    '''
    rain_dict = {
        '0': 0,
        '1': rd.randrange(0, 10)/24,
        '2': rd.randrange(10, 25)/24,
        '3': rd.randrange(25, 50)/24,
        '4': rd.randrange(50, 100)/24,
        '5': rd.randrange(100, 250)/24,
        '6': rd.randrange(250, 500)/24
    }
    try:
        forecastRain = str(forecastRain)
        print(rain_dict[forecastRain])
        rainfall_df = getSwmmModelRainData(sdir, prjname)
        for raindata in rainfall_df:
            # 查缺补齐
            maxt = datetime.strptime(
                max([t['time'] for t in raindata['data']]), '%Y-%m-%d %H:%M')
            mint = datetime.strptime(
                min([t['time'] for t in raindata['data']]), '%Y-%m-%d %H:%M')
            startTime = datetime.strptime(startTime, '%Y-%m-%d %H:%M:%S')
            endTime = datetime.strptime(endTime, '%Y-%m-%d %H:%M:%S')
            forecastngTime = datetime.strptime(
                forecastngTime, '%Y-%m-%d %H:%M:%S')
            dv_s = (startTime-mint).total_seconds()
            dv_e = (endTime-maxt).total_seconds()
            if dv_e > 0:  # 暂时假定后报期间降雨为0，可不设置降雨数据
                # 删除原预报期的降雨数据
                skipNo = -(datetime.strptime(raindata['data'][-1]['time'],'%Y-%m-%d %H:%M')-forecastngTime).total_seconds/3600
                raindata['data'] = raindata['data'][:skipNo]
                # 追加预报期的降雨估测值
                period = int((endTime-forecastngTime).total_seconds()/3600)
                for i in range(0, period):
                    raindata['data'].append({
                        'time': datetime.strftime(forecastngTime+timedelta(hours=i), '%Y-%m-%d %H:%M'),
                        'data': rain_dict[forecastRain]
                    })

        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            swmmobject = swmmio.Model(inp_file_name)
            raingages = swmmobject.inp.raingages
            tsdata = swmmobject.inp.timeseries

            new_tsdata = pd.DataFrame(
                columns=['Name', 'Date', 'time', 'Value'])
            
            ts_date_0=[]
            ts_time_0=[]
            ts_Value_0=[]
            ts_name_0 = []
            for raingage in list(raingages.index):
                tsname = raingages['DataSourceName'][raingage]
                for ts in list(tsdata.index.unique()):
                    if tsname == ts:
                        ts_data = [t['data']
                                   for t in rainfall_df if t['tsname'] == tsname][0]
                        ts_date = [datetime.strftime(datetime.strptime(
                            t['time'], '%Y-%m-%d %H:%M'), '%m/%d/%Y') for t in ts_data]
                        # tsdata['Date'][ts] = ts_date # datetime.strftime(['time'],'%m/%d/%Y')
                        ts_time = [datetime.strftime(datetime.strptime(
                            t['time'], '%Y-%m-%d %H:%M'), '%H:%M') for t in ts_data]
                        ts_Value = [t['data'] for t in ts_data]
                ts_date_0.extend(ts_date)
                ts_time_0.extend(ts_time)
                ts_Value_0.extend(ts_Value)
                ts_name_0.extend(np.full([1, len(ts_time)], tsname)[0])

            new_tsdata['Name'] = ts_name_0 
            new_tsdata['Date'] = ts_date_0
            new_tsdata['time'] = ts_time_0
            new_tsdata['Value'] = ts_Value_0
            new_tsdata = new_tsdata.set_index(['Name'])
            # new_tsdata = new_tsdata.set_index(['Date','time','Value'])

            swmmobject.inp.timeseries = new_tsdata

            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)

    except Exception as inst:
        return inst.args[0]

def update_raingage_tsdata2(sdir, prjname, rainData_time,rainData_value):
    '''
    rainData_time: ["",""],
    rainData_value: [0,1]
    '''
    try: 
        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            swmmobject = swmmio.Model(inp_file_name)
            raingages = swmmobject.inp.raingages
            tsdata = swmmobject.inp.timeseries

            new_tsdata = pd.DataFrame(
                columns=['Name', 'Date', 'time', 'Value'])
            
            ts_date_0=[]
            ts_time_0=[]
            ts_Value_0=[]
            ts_name_0 = []
            for raingage in list(raingages.index):
                tsname = raingages['DataSourceName'][raingage]
                for ts in list(tsdata.index.unique()):
                    if tsname == ts:
                        ts_Value = rainData_value
                        ts_date = [datetime.strftime(datetime.strptime(
                            t, '%Y-%m-%d %H:%M:%S'), '%m/%d/%Y') for t in rainData_time]
                        ts_time = [datetime.strftime(datetime.strptime(
                            t, '%Y-%m-%d %H:%M:%S'), '%H:%M') for t in rainData_time]
                ts_date_0.extend(ts_date)
                ts_time_0.extend(ts_time)
                ts_Value_0.extend(ts_Value)
                ts_name_0.extend(np.full([1, len(ts_time)], tsname)[0])

            new_tsdata['Name'] = ts_name_0 
            new_tsdata['Date'] = ts_date_0
            new_tsdata['time'] = ts_time_0
            new_tsdata['Value'] = ts_Value_0
            new_tsdata = new_tsdata.set_index(['Name'])
            # new_tsdata = new_tsdata.set_index(['Date','time','Value'])

            swmmobject.inp.timeseries = new_tsdata

            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)

    except Exception as inst:
        return inst.args[0]

def update_options_simulationTime(sdir,prjname,startTime,endTime):
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            cp_path = os.path.join(tempdir, prjname)
            inp_file_name = os.path.join(sdir, prjname)
            swmmobject = swmmio.Model(inp_file_name)
            inp_options = swmmobject.inp.options
            inp_options.loc['START_DATE']=datetime.strftime(datetime.strptime(
                            startTime, '%Y-%m-%d %H:%M:%S'), '%m/%d/%Y')
            inp_options.loc['START_TIME']=datetime.strftime(datetime.strptime(
                            startTime, '%Y-%m-%d %H:%M:%S'), '%H:%M:%S')
            inp_options.loc['REPORT_START_DATE']=datetime.strftime(datetime.strptime(
                            startTime, '%Y-%m-%d %H:%M:%S'), '%m/%d/%Y')
            inp_options.loc['REPORT_START_TIME']=datetime.strftime(datetime.strptime(
                            startTime, '%Y-%m-%d %H:%M:%S'), '%H:%M:%S')
            inp_options.loc['END_DATE']=datetime.strftime(datetime.strptime(
                            endTime, '%Y-%m-%d %H:%M:%S'), '%m/%d/%Y')
            inp_options.loc['END_TIME']=datetime.strftime(datetime.strptime(
                            endTime, '%Y-%m-%d %H:%M:%S'), '%H:%M:%S')                
            swmmobject.inp.save(cp_path)
            os.remove(inp_file_name)
            shutil.copy2(cp_path, inp_file_name)
    except Exception as inst:
        return inst.args[0]

# 该统计结果与swmm report file结果存在较大出入，待验证


def runAndGetStatistic(sdir, prjname):
    try:
        simfile = os.path.join(sdir, prjname)

        # get report time step
        swmmobject = swmmio.Model(simfile)
        tsfmt = '%m/%d/%Y %H:%M:%S'

        report_timestep = swmmobject.inp.options.loc['REPORT_STEP'].Value
        runoff_timestep = swmmobject.inp.options.loc['WET_STEP'].Value

        start_date = swmmobject.inp.options.loc['START_DATE'].Value
        start_time = swmmobject.inp.options.loc['START_TIME'].Value
        stime = datetime.strptime(start_date+' '+start_time, tsfmt)
        # steptime = datetime.strptime(start_date+' '+routing_timestep,tsfmt)
        if (runoff_timestep[:2]=="24"):
            steptime_sec = 86400
        else:
            steptime = datetime.strptime(runoff_timestep, "%H:%M:%S")
            steptime_sec = steptime.hour*3600+steptime.minute*60+steptime.second
        # steptime = stime+ datetime.timedelta(seconds=steptime_sec)
        # reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
        # reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second
        if report_timestep[:2] == "24":
            reporttime_sec = 86400
        else:
            reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
            reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second

        # reporttime = stime+ datetime.timedelta(seconds=reporttime_sec)
        timespan1 = steptime_sec  # (steptime-stime).seconds
        timespan = reporttime_sec  # (reporttime - stime).seconds

        outfrequency = int(timespan/timespan1)

        nodes = swmmobject.nodes.dataframe
        junction_names = list(nodes.index)

        dv = {}

        with Simulation(simfile) as sim:
            # 遍历swmm计算
            rec_step = 1
            for step in sim:
                if rec_step % outfrequency != 0:
                    rec_step += 1
                    continue
                rec_step += 1
                # 获取每一个节点的流量和污染物浓度
                for junction_name in junction_names:
                    junction_object = Nodes(sim)
                    ndobj = junction_object[junction_name]
                    q = ndobj.total_inflow
                    if 'Q' in dv:
                        dv['Q'] += q*24*3600
                    else:
                        dv['Q'] = q*24*3600
                    for key, value in ndobj.pollut_quality.items():
                        if key in dv:
                            dv[key] += q*value * reporttime_sec * 0.000001
                        else:
                            dv[key] = q*value * reporttime_sec * 0.000001

        out_file = simfile[:-3]+'csv'
        with open(out_file, 'w') as f:
            for key, value in dv.items():
                f.write(key+','+str(value)+'\n')
    except Exception as inst:
        return inst.args[0]

# 将swmm汇水区计算的径流和面源水质因子浓度写为dfs0数据，供mike11作为点源边界


def runAndExtractSubcatchmentResults(sdir, prjname, m11tsFolder,
                                     swmm_m11_mapfile):
    try:
        simfile = os.path.join(sdir, prjname)
        # mapfile = os.path.join(sdir,swmm_m11_mapfile)
        # get report time step
        swmmobject = swmmio.Model(simfile)
        tsfmt = '%m/%d/%Y %H:%M:%S'

        report_timestep = swmmobject.inp.options.loc['REPORT_STEP'].Value
        runoff_timestep = swmmobject.inp.options.loc['WET_STEP'].Value

        start_date = swmmobject.inp.options.loc['START_DATE'].Value
        start_time = swmmobject.inp.options.loc['START_TIME'].Value
        stime = datetime.strptime(start_date+' '+start_time, tsfmt)
        # steptime = datetime.strptime(start_date+' '+routing_timestep,tsfmt)
        if (runoff_timestep[:2]=="24"):
            steptime_sec = 86400
        else:
            steptime = datetime.strptime(runoff_timestep, "%H:%M:%S")
            steptime_sec = steptime.hour*3600+steptime.minute*60+steptime.second
        # steptime = stime+ datetime.timedelta(seconds=steptime_sec)
        # reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
        # reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second
        if report_timestep[:2] == "24":
            reporttime_sec = 86400
        else:
            reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
            reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second

        # reporttime = stime+ datetime.timedelta(seconds=reporttime_sec)
        timespan1 = steptime_sec  # (steptime-stime).seconds
        timespan = reporttime_sec  # (reporttime - stime).seconds

        outfrequency = int(timespan/timespan1)

        mapobj = {'pollutants': ['TP'],
                'links': [{
                    'cathmentId': 'S-0',
                    'm11BdId': 'ysk-1',  # mike11边界的BdID作为所引用的时间序列的文件名
                }, ],
                }
        # !!! 不支持中文路径
        with Simulation(simfile) as sim:
            subcatch_object = Subcatchments(sim)
            dv = dict()
            ts = []
            # 遍历swmm计算
            rec_step = 1
            for step in sim:
                if rec_step % outfrequency != 0:
                    rec_step += 1
                    continue
                rec_step += 1
                ts.append(sim.current_time)
                # 遍历输出的子汇水区
                for sc in mapobj['links']:
                    catobj = subcatch_object[sc['cathmentId']]
                    if sc['cathmentId'] not in dv.keys():
                        tmpdv = {}
                        tmpdv['outflow'] = [catobj.runoff]
                        for p in mapobj['pollutants']:
                            tmpdv[p] = [catobj.pollut_quality[p]]
                        dv[sc['cathmentId']] = tmpdv
                    else:
                        dv[sc['cathmentId']]['outflow'].append(catobj.runoff)
                        for p in mapobj['pollutants']:
                            dv[sc['cathmentId']][p].append(
                                catobj.pollut_quality[p])

                    # print(str(sim.current_time) + " " + str(catobj.pollut_quality))
                    # print(str(sim.current_time) + "," + str(catobj.runoff))
            # 将最后一步结果也导出
            ts.append(sim.current_time)
            # 遍历输出的子汇水区
            for sc in mapobj['links']:
                catobj = subcatch_object[sc['cathmentId']]
                dv[sc['cathmentId']]['outflow'].append(catobj.runoff)
                for p in mapobj['pollutants']:
                    dv[sc['cathmentId']][p].append(
                        catobj.pollut_quality[p])
        # 生成dfs0文件
        dfs = Dfs0()
        items = []
        for key in dv[mapobj['links'][0]['cathmentId']]:
            if key == 'outflow':
                items.append(
                    ItemInfo('Q', EUMType.Discharge, EUMUnit.meter_pow_3_per_sec))
            else:
                items.append(
                    ItemInfo(key, EUMType.Concentration, EUMUnit.mg_per_liter))
        for sc in mapobj['links']:
            dfsfile = os.path.join(m11tsFolder, sc['m11BdId']+'.dfs0')
            data = []
            for key, value in dv[sc['cathmentId']].items():
                data.append(value)
            dfs.write(
                filename=dfsfile,
                data=data,
                datetimes=ts,
                items=items,)
        return True
    except Exception as inst:
        return inst.args[0]

# 将swmm汇水区计算的径流和面源水质因子浓度写为dfs0数据，供mike11作为点源边界


def runAndExtractJuncionResults(sdir, prjname, m11tsFolder,
                                swmm_m11_mapfile):
    try:
        simfile = os.path.join(sdir, prjname)
        # read map file
        data_df = pd.read_excel(swmm_m11_mapfile, sheet_name='map')
        data_df2 = pd.read_excel(swmm_m11_mapfile, sheet_name='pollutants')
        jc_ids = data_df['Junction_Id']
        m11_ids = data_df['M11bd_Id']
        links = []
        for idx, row in data_df.iterrows():
            rl = {
                'juctionId': row['Junction_Id'],
                'm11BdId': row['M11bd_Id'],
                'Q': row['Q'],
                'NH3N': row['NH3N'],
                'TP': row['TP'],
                'COD': row['COD'],
                'DiversionScale': row['DiversionScale'],
                'DiversionName': row['DiversionName']
            }
            links.append(rl)
        pollutants = []
        for idx, row in data_df2.iterrows():
            pollutants.append(row['Pollutants'])

        mapobj = {'pollutants': pollutants,
                  'links': links
                  }

        # get report time step
        swmmobject = swmmio.Model(simfile)
        tsfmt = '%m/%d/%Y %H:%M:%S'

        report_timestep = swmmobject.inp.options.loc['REPORT_STEP'].Value
        runoff_timestep = swmmobject.inp.options.loc['WET_STEP'].Value

        start_date = swmmobject.inp.options.loc['START_DATE'].Value
        start_time = swmmobject.inp.options.loc['START_TIME'].Value
        stime = datetime.strptime(start_date+' '+start_time, tsfmt)
        # steptime = datetime.strptime(start_date+' '+routing_timestep,tsfmt)
        if (runoff_timestep[:2]=="24"):
            steptime_sec = 86400
        else:
            steptime = datetime.strptime(runoff_timestep, "%H:%M:%S")
            steptime_sec = steptime.hour*3600+steptime.minute*60+steptime.second
        # steptime = stime+ datetime.timedelta(seconds=steptime_sec)
        # reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
        # reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second
        if report_timestep[:2] == "24":
            reporttime_sec = 86400
        else:
            reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
            reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second

        # reporttime = stime+ datetime.timedelta(seconds=reporttime_sec)
        timespan1 = steptime_sec  # (steptime-stime).seconds
        timespan = reporttime_sec  # (reporttime - stime).seconds

        outfrequency = int(timespan/timespan1)

        # !!! 不支持中文路径
        with Simulation(simfile) as sim:
            junction_object = Nodes(sim)
            dv = dict()
            ts = []
            # 添加开始时间的值
            ts.append(stime)
            # 遍历输出的子汇水区
            for sc in mapobj['links']:
                ndobj = junction_object[sc['juctionId']]
                if sc['juctionId'] not in dv.keys():
                    tmpdv = {}
                    tmpdv['outflow'] = [0.0]
                    for p in mapobj['pollutants']:
                        tmpdv[p] = [0.0]
                    dv[sc['juctionId']] = tmpdv
            # 遍历swmm计算
            rec_step = 1
            for step in sim:
                if rec_step % outfrequency != 0:
                    rec_step += 1
                    continue
                rec_step += 1
                ts.append(sim.current_time)
                # 遍历输出的junction
                for sc in mapobj['links']:
                    ndobj = junction_object[sc['juctionId']]
                    # 检查是否需要对流量削减
                    q_factor = sc['Q']
                    q_out = ndobj.total_inflow
                    if str(q_factor) != 'nan' and len(str(q_factor)) > 0:
                        q_out = q_out * float(str(q_factor))
                    dv[sc['juctionId']]['outflow'].append(q_out)
                    for p in mapobj['pollutants']:
                        dv[sc['juctionId']][p].append(
                            ndobj.pollut_quality[p] * sc[p])
            # 将最后一步结果也导出
            ts.append(sim.current_time)
            # 遍历输出的子汇水区
            for sc in mapobj['links']:
                ndobj = junction_object[sc['juctionId']]
                # 检查是否需要对流量削减
                q_factor = sc['Q']
                q_out = ndobj.total_inflow
                if str(q_factor) != 'nan' and len(str(q_factor)) > 0:
                    q_out = q_out * float(str(q_factor))
                dv[sc['juctionId']]['outflow'].append(q_out)
                for p in mapobj['pollutants']:
                    dv[sc['juctionId']][p].append(
                        ndobj.pollut_quality[p] * sc[p])
        # 生成dfs0文件
        dfs = Dfs0()
        items = []
        for key in dv[mapobj['links'][0]['juctionId']]:
            if key == 'outflow':
                items.append(
                    ItemInfo('Q', EUMType.Discharge, EUMUnit.meter_pow_3_per_sec))
            else:
                items.append(
                    ItemInfo(key, EUMType.Concentration, EUMUnit.mg_per_liter))

        for sc in mapobj['links']:
            # 判断是否节点有分流
            division = sc['DiversionScale']
            if str(division) != 'nan' and len(division) > 0:
                scales = division.split(';')
                bdids = sc['DiversionName'].split(';')
                for idx, val in enumerate(scales):
                    scale = val
                    dfsfile = os.path.join(
                        m11tsFolder, sc['m11BdId']+'-'+bdids[idx]+'.dfs0')
                    data = []
                    for key, value in dv[sc['juctionId']].items():
                        for idxx, valx in enumerate(value):
                            value[idxx] = value[idxx]*float(scale)
                        data.append(value)
                    dfs.write(
                        filename=dfsfile,
                        data=data,
                        datetimes=ts,
                        items=items,)
                # 将母节点流量置为0
                dfsfile = os.path.join(m11tsFolder, sc['m11BdId']+'.dfs0')
                data = []
                for key, value in dv[sc['juctionId']].items():
                    for idxx, valx in enumerate(value):
                        value[idxx] = 0
                    data.append(value)

                dfs.write(
                    filename=dfsfile,
                    data=data,
                    datetimes=ts,
                    items=items,)
            else:
                dfsfile = os.path.join(m11tsFolder, sc['m11BdId']+'.dfs0')
                data = []
                for key, value in dv[sc['juctionId']].items():
                    data.append(value)

                dfs.write(
                    filename=dfsfile,
                    data=data,
                    datetimes=ts,
                    items=items,)
        return True
    except Exception as inst:
        return inst.args[0]

# 将swmm汇水区计算的径流和面源水质因子浓度写成hecras的边界


def runAndExtractCatchmentsResults4HecRas(sdir, prjname, tsFolder,
                                          mapfile):
    res = []
    try:
        simfile = os.path.join(sdir, prjname)
        # read map file
        data_df = pd.read_excel(mapfile, sheet_name='map')
        data_df2 = pd.read_excel(mapfile, sheet_name='pollutants')
        links = []
        for idx, row in data_df.iterrows():
            rl = {
                'catchment_Id': row['catchment_Id'],
                'hec_rvn': row['hecras_rivername'],
                'hec_rcn': row['hecras_reachname'],
                'station': row['start_station'],
                'Q': row['Q'],
                'NH3N': row['NH3N'],
                'TP': row['TP'],
                'COD': row['COD'],
                'DiversionScale': row['DiversionScale'],
                'DiversionName': row['DiversionName']
            }
            links.append(rl)

        pollutants = []
        for idx, row in data_df2.iterrows():
            pollutants.append(row['Pollutants'])

        mapobj = {'pollutants': pollutants,
                  'links': links
                  }

        # get report time step
        swmmobject = swmmio.Model(simfile)
        tsfmt = '%m/%d/%Y %H:%M:%S'

        report_timestep = swmmobject.inp.options.loc['REPORT_STEP'].Value
        runoff_timestep = swmmobject.inp.options.loc['WET_STEP'].Value

        start_date = swmmobject.inp.options.loc['START_DATE'].Value
        start_time = swmmobject.inp.options.loc['START_TIME'].Value
        stime = datetime.strptime(start_date+' '+start_time, tsfmt)
        # steptime = datetime.strptime(start_date+' '+routing_timestep,tsfmt)
        if (runoff_timestep[:2]=="24"):
            steptime_sec = 86400
        else:
            steptime = datetime.strptime(runoff_timestep, "%H:%M:%S")
            steptime_sec = steptime.hour*3600+steptime.minute*60+steptime.second
        # steptime = stime+ datetime.timedelta(seconds=steptime_sec)
        # reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
        # reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second
        if report_timestep[:2] == "24":
            reporttime_sec = 86400
        else:
            reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
            reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second

        # reporttime = stime+ datetime.timedelta(seconds=reporttime_sec)
        timespan1 = steptime_sec  # (steptime-stime).seconds
        timespan = reporttime_sec  # (reporttime - stime).seconds

        outfrequency = int(timespan/timespan1)

        # !!! 不支持中文路径
        with Simulation(simfile) as sim:
            cat_object = Subcatchments(sim)
            dv = dict()
            ts = []
            # 添加开始时间的值
            ts.append(stime)
            # 遍历输出的子汇水区
            for sc in mapobj['links']:
                ndobj = cat_object[sc['catchment_Id']]
                if sc['catchment_Id'] not in dv.keys():
                    tmpdv = {}
                    tmpdv['outflow'] = [0.0]
                    for p in mapobj['pollutants']:
                        tmpdv[p] = [0.0]
                    dv[sc['catchment_Id']] = tmpdv
            # 遍历swmm计算
            rec_step = 1
            for step in sim:
                if rec_step % outfrequency != 0:
                    rec_step += 1
                    current_time = sim.current_time
                    continue
                rec_step += 1
                ts.append(sim.current_time)
                # 遍历输出的junction
                for sc in mapobj['links']:
                    ndobj = cat_object[sc['catchment_Id']]
                    # 检查是否需要对流量削减
                    q_factor = sc['Q']
                    q_out = ndobj.runoff
                    if str(q_factor) != 'nan' and len(str(q_factor)) > 0:
                        q_out = q_out * float(str(q_factor))
                    dv[sc['catchment_Id']]['outflow'].append(q_out)
                    for p in mapobj['pollutants']:
                        dv[sc['catchment_Id']][p].append(
                            ndobj.pollut_quality[p] * sc[p])
            # 将最后一步结果也导出
            ts.append(sim.current_time)
            # 遍历输出的子汇水区
            for sc in mapobj['links']:
                ndobj = cat_object[sc['catchment_Id']]
                # 检查是否需要对流量削减
                q_factor = sc['Q']
                q_out = ndobj.runoff
                if str(q_factor) != 'nan' and len(str(q_factor)) > 0:
                    q_out = q_out * float(str(q_factor))
                dv[sc['catchment_Id']]['outflow'].append(q_out)
                for p in mapobj['pollutants']:
                    dv[sc['catchment_Id']][p].append(
                        ndobj.pollut_quality[p] * sc[p])
        # # 生成dfs0文件
        # dfs = Dfs0()
        # items = []
        # for key in dv[mapobj['links'][0]['catchment_Id']]:
        #     if key == 'outflow':
        #         items.append(
        #             ItemInfo('Q', EUMType.Discharge, EUMUnit.meter_pow_3_per_sec))
        #     else:
        #         items.append(
        #             ItemInfo(key, EUMType.Concentration, EUMUnit.mg_per_liter))

        # for sc in mapobj['links']:
        #     # 判断是否节点有分流
        #     division = sc['DiversionScale']
        #     if str(division) != 'nan' and len(division) > 0:
        #         scales = division.split(';')
        #         bdids = sc['DiversionName'].split(';')
        #         for idx, val in enumerate(scales):
        #             scale = val
        #             dfsfile = os.path.join(
        #                 tsFolder, sc['hec_rvn']+'-'+sc['hec_rcn']+'-' + str(sc['station'])+'-'+bdids[idx]+'.dfs0')
        #             data = []
        #             for key, value in dv[sc['catchment_Id']].items():
        #                 for idxx, valx in enumerate(value):
        #                     value[idxx] = value[idxx]*float(scale)
        #                 data.append(value)
        #             dfs.write(
        #                 filename=dfsfile,
        #                 data=data,
        #                 datetimes=ts,
        #                 items=items,)
        #         # 将母节点流量置为0
        #         dfsfile = os.path.join(
        #             tsFolder, sc['hec_rvn']+'-'+sc['hec_rcn']+'-' + str(sc['station'])+'.dfs0')
        #         data = []
        #         for key, value in dv[sc['catchment_Id']].items():
        #             for idxx, valx in enumerate(value):
        #                 value[idxx] = 0
        #             data.append(value)

        #         dfs.write(
        #             filename=dfsfile,
        #             data=data,
        #             datetimes=ts,
        #             items=items,)
        #     else:
        #         dfsfile = os.path.join(
        #             tsFolder, sc['hec_rvn']+'-'+sc['hec_rcn']+'-' + str(sc['station'])+'.dfs0')
        #         data = []
        #         for key, value in dv[sc['catchment_Id']].items():
        #             data.append(value)

        #         dfs.write(
        #             filename=dfsfile,
        #             data=data,
        #             datetimes=ts,
        #             items=items,)
        res.append( {
            'success': True,
            'map': mapobj,
            'data': dv,
            'time': [datetime.strftime(t,'%Y-%m-%d %H:%M:%S') for t in ts]
        })
        return res
    except Exception as inst:
        res.append( { 
            'success': False,
            'msg': inst.args[0]})
        return res


def runAndExtractResults(sdir, prjname, objectId, dataType, out2file=False):
    try:
        simfile = os.path.join(sdir, prjname)

        # get report time step
        swmmobject = swmmio.Model(simfile)
        tsfmt = '%m/%d/%Y %H:%M:%S'

        report_timestep = swmmobject.inp.options.loc['REPORT_STEP'].Value
        runoff_timestep = swmmobject.inp.options.loc['WET_STEP'].Value

        start_date = swmmobject.inp.options.loc['START_DATE'].Value
        start_time = swmmobject.inp.options.loc['START_TIME'].Value
        stime = datetime.strptime(start_date+' '+start_time, tsfmt)
        # steptime = datetime.strptime(start_date+' '+routing_timestep,tsfmt)
        if (runoff_timestep[:2]=="24"):
            steptime_sec = 86400
        else:
            steptime = datetime.strptime(runoff_timestep, "%H:%M:%S")
            steptime_sec = steptime.hour*3600+steptime.minute*60+steptime.second
        # steptime = stime+ datetime.timedelta(seconds=steptime_sec)
        # reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
        # reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second
        if report_timestep[:2] == "24":
            reporttime_sec = 86400
        else:
            reporttime = datetime.strptime(report_timestep, "%H:%M:%S")
            reporttime_sec = reporttime.hour*3600+reporttime.minute*60+reporttime.second

        # reporttime = stime+ datetime.timedelta(seconds=reporttime_sec)
        timespan1 = steptime_sec  # (steptime-stime).seconds
        timespan = reporttime_sec  # (reporttime - stime).seconds

        outfrequency = int(timespan/timespan1)
        
        objectId = str(objectId)
        # !!! 不支持中文路径
        with Simulation(simfile) as sim:
            dv = dict()
            ts = []
            # 添加开始时间的值
            ts.append(stime)
            junction_object = Nodes(sim)

            ndobj = junction_object[objectId]
            if(dataType == 'total_inflow'):
                tmpdv = {}
                tmpdv[dataType] = [ndobj.total_inflow]
                dv[objectId] = tmpdv
            # 遍历swmm计算
            rec_step = 1
            for step in sim:
                if rec_step % outfrequency != 0:
                    rec_step += 1
                    continue
                rec_step += 1
                ts.append(sim.current_time)
                ndobj = junction_object[objectId]
                if(dataType == 'total_inflow'):
                    dv[objectId][dataType].append(ndobj.total_inflow)
            # 将最后一步结果也导出
            ts.append(sim.current_time)
            ndobj = junction_object[objectId]
            if(dataType == 'total_inflow'):
                dv[objectId][dataType].append(ndobj.total_inflow)
        if(out2file):
            out_file = simfile[:-3]+"dat"
            with open(out_file, 'w') as f:
                f.write('-------\n')  # --- 为pest instruction文件规定的标识
                for d in dv[objectId][dataType]:
                    f.write(str(d)+"\n")
        return dv
    except Exception as inst:
        return inst.args[0]

# 获取降雨数据
def getSwmmModelRainData(sdir, prjname):
    simfile = os.path.join(sdir, prjname)
    swmmobject = swmmio.Model(simfile)
    raingages = swmmobject.inp.raingages
    tsdata = swmmobject.inp.timeseries
    rainfalldata = []

    for raingage in list(raingages.index):
        tsname = raingages['DataSourceName'][raingage]
        tv = []
        for ts in list(tsdata.index.unique()):
            if tsname == ts:
                idx = 0
                for r in tsdata['Date'][ts]:
                    date = r
                    time = tsdata['Time'][ts][idx]
                    value = tsdata['Value'][ts][idx]
                    tv.append({
                        'time': datetime.strftime(datetime.strptime(date + ' ' + time, '%m/%d/%Y %H:%M'), '%Y-%m-%d %H:%M'),
                        'data': value
                    })
                    idx += 1
        rainfalldata.append({
            'station': raingage,
            'tsname': tsname,
            'data': tv
        })

    return rainfalldata

# 获取模拟时段
def getSwmmSimulationTime(sdir,prjname):
    tsfmt = '%m/%d/%Y %H:%M:%S'
    simfile = os.path.join(sdir, prjname)
    swmmobject = swmmio.Model(simfile)
    start_date = swmmobject.inp.options.loc['START_DATE'].Value
    start_time = swmmobject.inp.options.loc['START_TIME'].Value
    stime = datetime.strptime(start_date+' '+start_time, tsfmt)
    end_date = swmmobject.inp.options.loc['END_DATE'].Value
    end_time = swmmobject.inp.options.loc['END_TIME'].Value
    etime = datetime.strptime(end_date+' '+end_time, tsfmt)
    return {
        'start_time': datetime.strftime(stime,'%Y-%m-%d %H:%M:%S'),
        'end_time':datetime.strftime(etime,'%Y-%m-%d %H:%M:%S'),
    }


def run_pest(enginefile, configFile):
    try:
        subprocess.run([enginefile, configFile])
    except Exception as inst:
        return inst.args[0]


def run_pest_finieshed(obs_mapdata, data_before_calibration, swmm_out_file):
    try:
        with open(swmm_out_file, 'r'):
            i = 1
    except Exception as inst:
        return inst.args[0]

# 利用pest自动率定swmm汇水区水文水质参数


def run_swmm_pest(pest_engine, python_engine, method_file,
                  swmm_dir, prjname, cal_params, obs_mapdata):
    '''
    # 需要率定参数的数据结构
    cal_params = [{
        subc_name: "S_0",
        params: [
            {
                name:"Area",
                value: 20f,
                low_limit: 10f,
                up_limit: 100f
            }
        ]
        }
    ]

    # 实测数据结构
    obs_mapdata = [
        {
            type: "total_inflow",
            name: "18",
            data: [
                time: "2021-1-1 00:00:00",
                data: 0f
            ]
        }
    ]
    '''
    try:
        simfile = os.path.join(swmm_dir, prjname)
        # 先计算swmm，输出ascii结果文件
        object_id = obs_mapdata[0]['name']
        data_type = obs_mapdata[0]['type']
        data_before_calibration = runAndExtractResults(
            swmm_dir, prjname, object_id, data_type, True)
        swmm_out_file = simfile[:-3]+"dat"
        # with open(swmm_out_file,'w') as out_file:
        #     for data in res_data[object_id][data_type]:
        #         out_file.write(str(data)+'\r')

        pst_params = []
        pst_obs_data = []
        # 写tpl文件
        tplfile = simfile[:-3] + "tpl.inp"
        swmmobject = swmmio.Model(simfile)
        for sct in cal_params:
            for para in sct['params']:
                pst_param_name = "# " + \
                    sct['subc_name'] + "_" + para['name'] + " #"
                pst_params.append({
                    'name': sct['subc_name'] + "_" + para['name'],
                    'low_limit': para['low_limit'],
                    'up_limit': para['up_limit'],
                    'value': para['value']
                })
                swmmobject.inp.subcatchments[para['name']
                                             ][sct['subc_name']] = pst_param_name

        swmmobject.inp.save(tplfile)
        newtplfile = tplfile[:-4]
        shutil.copy2(tplfile, newtplfile)
        # 加入头文件
        with open(newtplfile, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write('ptf #\n'+content)

        # 写ins文件 0:30 为某个输出变量结果数据在结果文件中对应的起始列和终止列
        ins_file = simfile[:-3] + "ins"
        with open(ins_file, 'w') as f:
            f.write("pif #\n#-------#\n")  # --- 为结果文件对应的标识
            # for index,data in enumerate(res_data[object_id][data_type]):
            for index, data in enumerate(obs_mapdata[0]['data']):
                pst_obs_name = object_id+"_"+data_type+"_" + str(index)
                pst_obs_data.append({
                    'name': pst_obs_name,
                    'data': data['data']
                })
                f.write("l1 "+"["+pst_obs_name + "]1:30\n")

        # 写pst文件
        num_of_pars = len(cal_params[0]['params'])
        num_of_obs = len(obs_mapdata[0]['data'])
        pst_file = simfile[:-3] + "pst"
        control_file_data = "pcf\n* control data\nrestart estimation\n"
        control_file_data += "   " + \
            str(num_of_pars) + "    " + \
            str(num_of_obs) + "    " + "1    0    1\n"
        control_file_data += "    1     1 single point 1 0 0\n"
        control_file_data += "  5.0   2.0   0.3    0.01  10\n"
        control_file_data += "  5.0   5.0   0.001\n"
        control_file_data += "  0.1\n"
        control_file_data += "   30  0.01     4     3  0.01     3\n"
        control_file_data += "    1     1     1\n"

        control_file_data += "* parameter groups\nparagroup  relative 0.01  0.0  switch  2.0 parabolic\n\n"
        control_file_data += "* parameter data\n"

        for param in pst_params:
            control_file_data += param['name'] + "           none"
            control_file_data += "  factor    " + str(param['value']) + "    " + str(param['low_limit']) + "    " + \
                str(param['up_limit']) + "    paragroup  1.0  0.0  1\n"

        '''
        Observation data
        '''
        control_file_data += "* observation groups\nobsgroup\n\n"
        control_file_data += "* observation data\n"

        for obs in pst_obs_data:
            control_file_data += (str(obs['name'])+"               " + str(obs['data']) +
                                  "    1.0  obsgroup" "\n")
        '''
        Model Command Line Section
        '''
        control_file_data += "* model command line\n"
        control_file_data += (python_engine+" "+method_file+" runAndExtractResults appid " +
                              "\""+swmm_dir+"\""+" "+prjname+" "+object_id+" "+data_type+" "+'0'+'\n\n'
                              )

        '''
        Model Input Output Data Section
        '''
        control_file_data += "* model input/output\n"
        control_file_data += "\"" + newtplfile+"\""+" " + "\""+simfile+"\""+"\n"
        control_file_data += "\""+ins_file+"\""+" " + "\""+swmm_out_file+"\""+"\n\n"
        control_file_data += "* prior information\n\n"

        with open(pst_file, 'w') as f:
            f.write(control_file_data)

        # 运行pest
        # cmd_line = pest_engine +" "+ pst_file  # Set Command line
        thread = BaseThread(
            name='test',
            target=run_pest,
            target_args=(pest_engine, pst_file),
            callback=run_pest_finieshed,
            callback_args=(obs_mapdata, data_before_calibration, swmm_out_file)
        )
        thread.start()

    except Exception as inst:
        return inst.args[0]


if __name__ == '__main__':
    sdir = 'C:\\Users\\jip\\Documents\\EPA SWMM Projects\\Examples'
    # generateInp('d:\\','test.inp')
    # updateInp('d:\\','test.inp')
    # runAndExtractSubcatchmentResults(sdir, 'Example1.inp', '', '')
    update_ldu_buildup_params(
        r'C:\sypcloud_git\sypcloud\trunk\projects\doc\91_nwh_mzh\m11\scenario_results\0-2015\swmm',
        'mzh.inp', '')
