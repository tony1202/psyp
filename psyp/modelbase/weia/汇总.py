#E.1 混合过程过程长度估算公式
'''
B——水面宽度，m
a——排放口到岸边的距离,m
u——断面流速，m/s  
Ey——污染物横向扩散指数，㎡/s
'''
def Lenth_of_mix(B,a,u,Ey):
  return (0.11+0.7*(0.5-a/B-1.1*(0.5-a/B)**2)**0.5)*u*B*B/Ey
# print(Lenth_of_mix(30,5,0.5,0.05))


#E.2 零维数学模型
#E.2.1 河流均匀混合模型
'''
Cp——污染物排放浓度，mg/L
Qp——污水排放量，m³/s
Ch——河流上游污染物浓度，mg/L
Qh——河流流量，m³/s
'''
def River_Uniform_mixing(Cp,Qp,Ch,Qh):
  return (Cp*Qp+Ch*Qh)/(Qp+Qh)
# print(River_Uniform_mixing(1000,100,500,10))


#E.2.2 湖库均匀混合模型
'''
W——单位时间污染物排放量，g/s
Q——水量平衡时流入与流出湖（库）的流量，m³/s
k——污染物综合衰减系数，s^(-1)
V——水体体积，m³
'''
def Lake_Uniform_mixing(W,Q,k,V):
    return W/(Q+k*V)
# print(Lake_Uniform_mixing(300,20,1,100000))


#E.2.3 狄龙模型
'''
Ip——单位时间进入湖（库）中的氮、磷的平均浓度，mg/L
Lp——单位时间、单位面积进入湖（库）的氮、磷负荷量，g/（m³.a）
H——平均水深，m
Rp——氮、磷在湖（库）中的滞留率，量纲一
qa——年出流的水量，m³/a
qi——年入流的水量，m³/a
[P]a——年出流的氮（磷）平均浓度，mg/L
[P]i——年入流的氮（磷）平均浓度，mg/L
Q——湖（库）的年出水流量，m³/a
V——水体体积，m³
'''
def Dillon_model_Ip_V(Ip,V,Q,listqa,listqi,listPa,listPi):
  n=len(listqa)
  M=0
  for i in range(1,n+1):
    M+=listqa[i]*listPa[i]/listqi[i]/listPi[i]
  Rp=1-M
  r=Q/V
  return Ip*(1-Rp)/r/V
print(def Dillon_model_Ip_V())
  


#E.3 纵向一维数学模型
#E.3.2.1 连续稳定排放
import math
import numpy as np
def Portrait_1d_stable(k,Ex,u,B,A,Cp,Qp,Ch,Qh,x1,x2,Δx):
    '''
    α——O’Connor数，量纲一，表征物质离散降解通量与移流通量的比值
    Pe——贝克来数，量纲一，表征物质移流通量与离散通量的比值
    C0——河流排放口初始断面混合浓度，mg/L
    x——河流沿程坐标，m，x=0指排放口处，x>0指排放口下游段，x<0,指排放口上游段
    Cp——污染物排放浓度，mg/L
    Qp——污水排放量，m³/s
    Ch——河流上游污染物浓度，mg/L
    Qh——河流流量，m³/s
    B——水面宽度，m
    A——断面面积，㎡
    k——污染物综合衰减系数，s^(-1)
    u——断面流速，m/s 
    Ex——污染物纵向扩散指数，㎡/s
    '''
    α=k*Ex/u**2
    Pe=u*B/Ex
    n1=int(x1/Δx)
    n2=int(x2/Δx)
    Δx1=x1/n1
    Δx2=x2/n2
    C01=(Cp*Qp+Ch*Qh)/(Qp+Qh)
    C02=(Cp*Qp+Ch*Qh)/((Qp+Qh)*math.sqrt(1+4*α))
    C03=(Cp*Qp+Ch*Qh)/(2*A*math.sqrt(k*Ex))
    result=[]
    data=np.random.random([n1+n2+1,2])
    idx=0
    for i in range(n1):
        x=-x1+i*Δx1
        if α<=0.027 and Pe<1:
            data[idx][0]=x
            data[idx][1]=C01*math.exp(u*x/Ex)
            idx=idx+1
        elif α<=0.027 and Pe>=1:
            data[idx][0]=x
            data[idx][1]=0
            idx=idx+1
        elif 0.027<α<=380:
            data[idx][0]=x
            data[idx][1]=C02*math.exp(u*x/2/Ex*(1+math.sqrt(1+4*α)))
            idx=idx+1
        elif α>380:
            data[idx][0]=x
            data[idx][1]=C03*math.exp(x*math.sqrt(k/Ex))
            idx=idx+1
    for i in range(n2+1):
        x=i*Δx2
        if α<=0.027:
            data[idx][0]=x
            data[idx][1]=C01*math.exp(-k*x/u)
            idx=idx+1
        elif 0.027<α<=380:
            data[idx][0]=x
            data[idx][1]=C02*math.exp(u*x/2/Ex*(1-math.sqrt(1+4*α)))
            idx=idx+1
        elif α>380:
            data[idx][0]=x
            data[idx][1]=C03*math.exp(-x*math.sqrt(k/Ex))
            idx=idx+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Portrait_1d_stable(0.0001,0.5,0.5,30,30,200,10,50,50,233,1000,45)


# E.3.2.2 瞬时排放
# 瞬时排放源河流一维对流扩散方程的浓度分布公式
import math
import numpy as np
def Portrait_1d_instant(M,A,Ex,k,u,x,t,Δx,Δt):
    '''
    M——污染物的瞬时排放总质量，g
    A——断面面积，㎡
    Ex——污染物纵向扩散指数，㎡/s
    k——污染物综合衰减系数，s^(-1)
    u——断面流速，m/s
    x——离排放口距离，m
    t——排放发生后的扩散历时，s
    '''
    n1=int(x/Δx) 
    Δx=x/n1
    n2=int(t/Δt)
    Δt=t/n2
    result=[]
    data=np.random.random([n2,n1+1,3])
    idt=0
    for i in range(1,n2+1):
        t=i*Δt       
        idx=0
        for i in range(n1+1):
            x=i*Δx
            C=M/(A*math.sqrt(4*math.pi*Ex*t))*math.exp(-k*t)*math.exp(-(x-u*t)**2/(4*Ex*t))
            data[idt][idx][0]=t
            data[idt][idx][1]=x
            data[idt][idx][2]=C
            idx+=1
        idt+=1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Portrait_1d_instant(10,20,10,1,0.5,20,6,5,2)

# 在t时刻、距离污染源下游x=u*t处的污染物浓度峰值
import math
import numpy as np
def Max_portrait_1d_instant(M,A,Ex,k,u,x,Δx):
    '''
    M——污染物的瞬时排放总质量，g
    A——断面面积，㎡
    Ex——污染物纵向扩散指数，㎡/s
    k——污染物综合衰减系数，s^(-1)
    u——断面流速，m/s
    x——离排放口距离，m
    '''
    n1=int(x/Δx) 
    Δx=x/n1
    result=[]
    data=np.random.random([n1,2])
    idx=0
    x=0
    for i in range(1,n1+1):
        x=i*Δx
        C=M/(A*math.sqrt(4*math.pi*Ex*x/u))*math.exp(-k*x/u)
        data[idx][0]=x
        data[idx][1]=C
        idx+=1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Max_portrait_1d_instant(10,20,1,0.0001,0.5,20,3)


# E.3.2.3 有限时段排放
import math
import numpy as np
def Portrait_1d_limited(A,Ex,k,u,x,t0,t1,Δt,Δx,listWi):
    '''
    A——断面面积，㎡
    Ex——污染物纵向扩散指数，㎡/s
    k——污染物综合衰减系数，s^(-1)
    u——断面流速，m/s 
    x——河流沿程坐标，m，x=0指排放口处，x>0指排放口下游段，x<0,指排放口上游段
    t0——污染源的排放持续时间，s
    Δt——计算时间步长，s
    n——计算分段数，n=t0/Δt
    t(i-0.5)——污染源排放的时间变量，t(i-0.5)=(i-0.5)*Δt<t0,s
    i——最大为n的自然数
    j——自然数
    Wi——t(i-1)到ti时间段内，单位时间污染物的排放质量，g/s
    '''
    n=int(t0/Δt)
    Δt1=t0/n
    n1=int((t1-t0)/Δt)
    Δt2=(t1-t0)/n1
    n2=int(x/Δx)
    Δx=x/n2
    result=[]
    data=np.random.random([n+n1,n2+1,3])
    idx=0
    for i in range(n2+1):
        x=i*Δx
        j=1
        idt=0
        while j<=n:
            tj=j*Δt
            M1=0
            for i in range(1,j+1):
                th=(i-0.5)*Δt1
                M1+=listWi[i-1]/(math.sqrt(tj-th))*math.exp(-k*(tj-th))*math.exp(-(x-u*(tj-th))**2/(4*Ex*(tj-th)))
            C=Δt/(A*math.sqrt(4*math.pi*Ex))*M1
            data[idt][idx][0]=tj
            data[idt][idx][1]=x
            data[idt][idx][2]=C
            idt = idt+1
            j+=1
        j=1        
        while j<=n1:
            tj=t0+j*Δt2
            M1=0
            for i in range(1,n+1):
                th=(i-0.5)*Δt1
                M1+=listWi[i-1]/(math.sqrt(tj-th))*math.exp(-k*(tj-th))*math.exp(-(x-u*(tj-th))**2/(4*Ex*(tj-th)))
            C=Δt/(A*math.sqrt(4*math.pi*Ex))*M1
            data[idt][idx][0]=tj
            data[idt][idx][1]=x
            data[idt][idx][2]=C
            idt = idt+1
            j+=1
        idx+=1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Portrait_1d_limited(30,1,1,1,15,10,20,2,4,[2,2,2,2,2])


# E.6 平面二维数学模型
# E.6.2.1 连续稳定排放
# 不考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
import math
import numpy as np
def Plane_2d_stable_E1(Ch,m,h,Ey,u,k,x,y,Δx,Δy):     
    '''
    Ch——河流上游污染物浓度，mg/L
    m——污染物排放速率，g/s
    h——断面水深，m
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    x——纵向距离，m
    y——横向距离，m
    '''
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    result=[]
    data=np.random.random([n1,n2+1,3])
    idx=0
    for i in range(1,n1+1):
        x=i*Δx
        idy=0
        for i in range(n2+1):
            y=i*Δy
            C=Ch+m/(h*math.sqrt(math.pi*Ey*u*x))*math.exp(-u*y**2/(4*Ey*x))*math.exp(-k*x/u)
            data[idx][idy][0]=x
            data[idx][idy][1]=y
            data[idx][idy][2]=C
            idy=idy+1
        idx=idx+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Plane_2d_stable_E1(1,1,1,1,1,1,20,10,3,3)

# 污染混合区外边界等浓度线方程
import math
import numpy as np
def Boundary_of_mix(m,u,Ey,h,Cs,Ch,x,Δx):
    '''
    m——污染物排放速率，g/s
    h——断面水深，m
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    Ch——河流上游污染物浓度，mg/L
    Cs——水功能区所执行的污染物浓度标准限值，mg/L
    Ca——允许升高浓度，Ca=Cs-Ch，mg/L
    x——纵向距离，m
    '''
    Ca=Cs-Ch
    Ls=(m/h/Ca)**2/(math.pi*u*Ey)            # 污染混合区纵向最大长度
    bs=math.sqrt(2*Ey*Ls/math.exp(1)/u)      # 污染混合区横向最大宽度
    n=int(x/Δx)
    Δx=x/n
    result=[]
    data=np.random.random([n,2])
    idx=0
    for i in range(1,n+1):
        x=i*Δx
        y=bs*math.sqrt(-math.exp(1)*x/Ls*math.log(x/Ls))
        data[idx][0]=x
        data[idx][1]=y
        idx+=1
        x+=Δx
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Boundary_of_mix(30,1,1,1,5,2,30,5)

# 考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
import math
import numpy as np
def Plane_2d_stable_E3(Ch,m,h,B,Ey,u,k,x,y,Δx,Δy):
    '''
    Ch——河流上游污染物浓度，mg/L
    m——污染物排放速率，g/s
    h——断面水深，m
    B——水面宽度—，m
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    x——纵向距离，m
    y——横向距离，m
    '''
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    result=[]
    data=np.random.random([n1,n2+1,3])
    idx=0
    for i in range(1,n1+1):
        x=i*Δx
        idy=0
        for i in range(n2+1):
            y=i*Δy
            x1=0
            for n in range(-1,2):
                x1+=math.exp(-u*(y-2*n*B)**2/(4*Ey*x))
            C=Ch+m/(h*math.sqrt(math.pi*Ey*u*x))*math.exp(-k*x*u)*x1
            data[idx][idy][0]=x
            data[idx][idy][1]=y
            data[idx][idy][2]=C
            idy=idy+1
        idx=idx+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Plane_2d_stable_E3(1,1,1,3,1,1,1,20,6,5,2)


# E.6.2.2 瞬时排放
# 不考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源排放
import math
import numpy as np
def Plane_2d_instant_E5(Ch,M,h,Ex,Ey,u,k,x,y,t,Δx,Δy,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    M——污染物的瞬时排放总质量，g
    h——断面水深，m
    Ex——污染物纵向扩散指数，㎡/s
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    t——排放发生后的扩散历时，s
    x——纵向距离，m
    y——横向距离，m
    '''
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    n3=int(t/Δt)
    Δt=t/n3
    result=[]
    data=np.random.random([n3,n1+1,n2+1,4])
    idt=0
    for i in range(1,n3+1):
        t=i*Δt
        idx=0
        for i in range(n1+1):
            x=i*Δx
            idy=0
            for i in range(n2+1):
                y=i*Δy
                C=Ch+M/(2*math.pi*h*t*math.sqrt(Ex*Ey))*math.exp(-(x-u*t)**2/(4*Ex*t)-y**2/(4*Ey*t))*math.exp(-k*t)
                data[idt][idx][idy][0]=t
                data[idt][idx][idy][1]=x
                data[idt][idx][idy][2]=y
                data[idt][idx][idy][3]=C
                idy=idy+1
            idx=idx+1
        idt=idt+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Plane_2d_instant_E5(1,10,1,1,1,1,1,20,10,6,5,2,2)

# 考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源瞬时排放
import math
import numpy as np
def Plane_2d_instant_E6(Ch,M,h,B,Ex,Ey,u,k,x,y,t,Δx,Δy,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    M——污染物的瞬时排放总质量，g
    h——断面水深，m
    B——水面宽度—，m
    Ex——污染物纵向扩散指数，㎡/s
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    t——排放发生后的扩散历时，s
    x——纵向距离，m
    y——横向距离，m
    '''
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    n3=int(t/Δt)
    Δt=t/n3   
    result=[]
    data=np.random.random([n3,n1+1,n2+1,4])
    idt=0
    for i in range(1,n3+1):
        t=i*Δt
        idx=0
        for i in range(n1+1):
            x=i*Δx
            idy=0
            for i in range(n2+1):
                y=i*Δy
                x1=0
                for n in range(-1,2):
                    x1+=math.exp(-(y-2*n*B)**2/(4*Ey*t))
                C=Ch+M/(2*math.pi*h*t*math.sqrt(Ex*Ey))*math.exp(-(x-u*t)**2/(4*Ex*t)-k*t)*x1
                data[idt][idx][idy][0]=t
                data[idt][idx][idy][1]=x
                data[idt][idx][idy][2]=y
                data[idt][idx][idy][3]=C
                idy=idy+1
            idx=idx+1
        idt=idt+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Plane_2d_instant_E6(1,10,1,3,1,1,1,1,20,6,6,5,2,2)

# 宽浅型平直均匀河流，离岸点源排放
import math
import numpy as np
def Plane_2d_instant_E7(Ch,M,h,B,a,Ex,Ey,u,k,x,y,t,Δx,Δy,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    M——污染物的瞬时排放总质量，g
    h——断面水深，m
    B——水面宽度，m
    a——排放口到岸边的距离，m
    Ex——污染物纵向扩散指数，㎡/s
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    t——排放发生后的扩散历时，s
    x——纵向距离，m
    y——横向距离，m
    '''
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    n3=int(t/Δt)
    Δt=t/n3
    result=[]
    data=np.random.random([n3,n1+1,n2+1,4])
    idt=0
    for i in range(1,n3+1):
        t=i*Δt
        idx=0
        for i in range(n1+1):
            x=i*Δx
            idy=0
            for i in range(n2+1):
                y=i*Δy
                x1=0
                for n in range(-1,2):
                    x1+=math.exp(-(y-2*n*B)**2/(4*Ey*t))+math.exp(-(y-2*n*B+2*a)**2/(4*Ey*t))
                C=Ch+M/(4*math.pi*h*t*math.sqrt(Ex*Ey))*math.exp(-(x-u*t)**2/(4*Ex*t)-k*t)*x1
                data[idt][idx][idy][0]=t
                data[idt][idx][idy][1]=x
                data[idt][idx][idy][2]=y
                data[idt][idx][idy][3]=C
                idy=idy+1
            idx=idx+1
        idt=idt+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Plane_2d_instant_E7(1,100,1,3,1,1,1,1,0.01,20,6,6,5,2,2)

# E.9.2 化学需氧量(COD)
def COD(C,Kcod):
    '''
    C——COD浓度，mg/L
    Kcod——COD降解系数，s^(-1)
    '''
    return -Kcod*C
# print(COD(30,0.05))

# E.9.3 五日生化需氧量(BOD5)
def BOD5(C,k1):
    '''
    C——BOD5浓度，mg/L
    K1——耗氧系数，s^(-1)
    '''
    return -k1*C
# print(BOD5(11,0.04))

# E.9.4 溶解氧(DO)
def DO(C,k1,k2,Cb,Cs,So,h):
    '''
    C——DO浓度，mg/L
    k1——耗氧系数，s^(-1)
    k2——复氧系数，s^(-1)
    Cb——BOD的浓度，mg/L
    Cs——饱和溶解氧的浓度，mg/L
    So——底泥耗氧系数，g/(㎡.s)
    h——断面水深，m
    '''
    return -k1*Cb+k2*(Cs-C)-So/h
# print(DO(20,0.1,0.2,30,40,0.01,1))

# E.9.5 氮循环
# 水体中的氮包括氨氮、亚硝酸盐氮、硝酸盐氮三种形态，三种形态之间的转换关系可以表示为
def NC(Nnh,Nno2,b1,b2,h,Snh):
    '''
    Nnh、Nno2、Nno2——分别为氨氮、亚硝酸盐氮、硝酸盐氮浓度，mg/L
    b1、b2——分别为氨氮氧化成亚硝酸盐氮、亚硝酸盐氮氧化成硝酸盐氮的反应速率，s^(-1)
    h——断面水深，m
    Snh——氨氮的底泥(沉积)释放率，g/(㎡.s)
    '''
    NH=-b1*Nnh+Snh/h         # f(Nnh)
    NO2=b1*Nnh-b2*Nno2       # f(Nno2)
    NO3=b2*Nno2              # f(Nno3)
    return NH,NO2,NO3
# print(NC(5,10,2,3,2,5))

# E.9.6 总氮(TN)
def NT(C,kTN,STN,h):
    '''
    C——TN浓度，mg/L
    kTN——总氮的综合沉降系数，s^(-1)
    STN——总氮的底泥释放(沉积)系数，g/(㎡.s)
    h——断面水深，m
    '''
    return -kTN*C+STN/h
# print(NT(4,3,5,2))

# E.9.7 磷循环
# 水体中的磷可以分为无机磷和有机磷两种形态，两种形态之间的转换关系可以表示为：
def PC(Cps,Cpd,Gp,Ap,Cp,Dp,Sps,Spd,h):
    '''
    Cps——无机磷浓度，mg/L
    Cpd——有机磷浓度，mg/L
    Gp——浮游植物生长速率，s-1
    Ap——浮游植物磷含量系数，量纲一
    Cp——有机磷氧化成无机磷的反应速率，s-1
    Dp——浮游植物死亡速率，s-1
    Sps——无机磷的底泥释放(沉积)系数，g/(㎡·s)
    Spd——有机磷的底泥释放(沉积)系数，g/(㎡·s)
    h——断面水深，m
    '''
    CPS=-Gp*Cps*Ap+Cp*Cpd+Sps/h     # f(Cps)
    CPD=Dp*Cpd*Ap-Cp*Cpd+Spd/h      # f(Cpd)
    return CPS,CPD
# print(PC(5,10,2,3,4,5,6,7,1))

# E.9.8 总磷(TP)
def TP(C,kTP,STP,h):
    '''
    C——TP浓度，mg/L
    kTP——总磷的综合沉降系数，s^(-1)
    STP——总磷的底泥释放(沉积)系数，g/(㎡·s)
    h——断面水深，m
    '''
    return -kTP*C+STP/h
# print(TP(4,5,6,2))

# E.9.9 叶绿素a(Chl-a)
def Chl_a(μMAX,T,L,TP,TN,Dp,C):
    '''
    C——叶绿素a浓度，mg/L；
    Gp——浮游植物生长速率，s-1；
    Dp——浮游植物死亡速率，s-1；
    μMAX——浮游植物最大生长速率，s-1；
    T、L、TP、TN——分别为水温、光照、TP、TN的影响函数，可以根据评价水域的实际情况以及基础资料条件选择适合的函数形式
    '''
    Gp=μMAX*T*L*TP*TN
    return (Gp-Dp)*C
# print(Chl_a(1,2,3,4,5,6,7))

# E.9.11 热排放
def Heat_emission(C,kT,Cp,q,To,ρ):
    '''
    C——水体温升，℃；
    kT——水面综合散热系数，J/(S·㎡·℃)；
    Cp—水的比热，J/(kg·℃)；
    q——温排水的源强，m/s；
    To——温排水的温升,℃
    ρ——水的密度，kg/m3
    '''
    return -kT*C/(ρ*Cp)+q*To
# print(Heat_emission(2,3,4,5,6,7))

# E.9.12 余氯
def Residual_chlorine(C,kCl):
    '''
    C——余氯浓度，mg/L；
    kCl——余氯衰减系数，s^(-1)
    '''
    return -kCl*C
# print(Residual_chlorine(4,7))

# E.9.19 泥沙
# 挟沙力法
def Sand_C(α,w,S1,S):
    '''
    α——恢复饱和系数
    w——泥沙颗粒沉速，m/s
    S1——水流挟沙能力，kg/m³
    S——泥沙含量，kg/m³
    '''
    return α*w*(S1-S)
# print(Sand_C(2,3,5,2))

# 切应力方法
def Sand_shear_stress(α,w,S,M,τ,τd,τe):
    '''
    α——恢复饱和系数
    w——泥沙颗粒沉速，m/s
    S——泥沙含量，kg/m³
    τd——临界淤积切应力，可由实验确定，也可由验证计算确定；
    τe——临界冲刷切应力，可由实验确定，也可由验证计算确定；
    M——冲刷系数，由实验确定，也可由验证计算确定。
    ''' 
    if τ<=τd:
        C=α*w*S*(1-τ/τd)
        print(C)
    elif τd<τ<=τe:
        print('泥沙含量不变')
    elif τ>=τe:
        C=-M*(τ/τe-1)
        print(C)
if __name__ == '__main__':
    Sand_shear_stress(1,2,3,4,6.5,6,7)

# F.2 河口解析模式
# F.2.1 充分混合段
# 河口—1 适用于狭长、均匀河口连续点源稳定排放的情况
import math 
import numpy as np
def Well_mixed_stable(Cp,Qp,Qh,Ch,u,k,Ex,x1,x2,Δx):
    '''
    Cp——污染物排放浓度，mg/L
    Qp——污水排放量，m³/s
    Qh——河流流量，m³/s
    Ch——河流上游污染物浓度，mg/L
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    Ex——污染物纵向扩散指数，㎡/s
    x——河流沿程坐标，m，x=0指排放口处，x>0指排放口下游段，x<0,指排放口上游段
    '''
    n1=int(x1/Δx)
    n2=int(x2/Δx)
    Δx1=x1/n1
    Δx2=x2/n2
    M=(1+4*k*Ex/u**2)**0.5
    result=[]
    data=np.random.random([n1+n2+1,2])
    idx=0
    for i in range(n1+1):
        x=-x1+i*Δx1
        C=Cp*Qp/((Qh+Qp)*M)*math.exp(u*x*(1+M)/2/Ex)+Ch
        data[idx][0]=x
        data[idx][1]=C
        idx+=1
    for i in range(1,n2+1):
        x=i*Δx2
        C=Cp*Qp/((Qh+Qp)*M)*math.exp(u*x*(1-M)/2/Ex)+Ch
        data[idx][0]=x
        data[idx][1]=C
        idx+=1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Well_mixed_stable(100,1,5,10,0.5,1,1,20,30,5)

# 河口—2 适用于狭长、均匀河口点源瞬时排放
import math
import numpy as np
def Well_mixed_instant(Ch,W,A0,Ex,u,k,x,t,Δx,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    W——在x=0、t=0时污染物的排放量，g
    A0——河流断面面积，㎡
    Ex——污染物纵向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    x——离排放口距离，m
    t——排放发生后的扩散历时，s
    '''
    n1=int(t/Δt)
    n2=int(x/Δx)
    Δt=t/n1
    Δx=x/n2
    result=[]
    data=np.random.random([n1,n2+1,3])
    idt=0
    for i in range(1,n1+1):
        t=i*Δt
        idx=0
        x=0
        for j in range(n2+1):
            x=j*Δx
            C=W/(A0*math.sqrt(4*math.pi*Ex*t))*math.exp(-((x-u*t)**2/(4*Ex*t)+k*t))+Ch
            data[idt][idx][0]=t
            data[idt][idx][1]=x
            data[idt][idx][2]=C
            idx+=1
        idt+=1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    Well_mixed_instant(10,100,20,10,2,1,20,6,5,2)

# F.2.2 混合过程段
# 河口—3 适用于狭长、均匀河口，点源江心稳定排放
import math
import numpy as np
def F9_mix(Qp,Cp,Ch,u,h,Ey,k,x,y,Δx,Δy):
    '''
    Qp——污水排放量，m³/s
    Cp——污染物排放浓度，mg/L
    Ch——河流上游污染物浓度，mg/L
    u——断面流速，m/s 
    h——断面水深，m
    Ey——污染物横向扩散指数，㎡/s
    k——污染物综合衰减系数，s^(-1)
    x——河流沿程坐标，m
    y——河道宽度，m
    '''
    m=int(x/Δx)
    n=2*int(y/(2*Δy))
    Δx=x/m
    Δy=y/n
    y2=-y/2
    y1=y/2
    result=[]
    data=np.random.random([m,n+1,3])
    idx=0
    for i in range(1,m+1):
        x=i*Δx
        idy=0
        for i in range(n+1):
            y=y2+i*Δy
            if y==0:
                y=Δy/2
                C=Qp*Cp/(u*h*2*math.sqrt(math.pi*Ey*x/u))*math.exp(-u*y**2/(4*Ey*x)-k*x/u)+Ch
                data[idx][idy][0]=x
                data[idx][idy][1]=0
                data[idx][idy][2]=C
                idy =idy+1 
            else:
                C=Qp*Cp/(u*h*2*math.sqrt(math.pi*Ey*x/u))*math.exp(-u*y**2/(4*Ey*x)-k*x/u)+Ch
                data[idx][idy][0]=x
                data[idx][idy][1]=y
                data[idx][idy][2]=C
                idy =idy+1  
        idx =idx+1
    result.append(data)
    print(result)
    return result
if __name__ == '__main__':
    F9_mix(10,50,30,0.3,1,0.2,0.0001,1000,20,5,5)