import math
import numpy as np

# 计算河口断面中心连续稳定排放浓度场
def cal_c_estuary_2d_center(Qp,Cp,Ch,u,h,Ey,k,x,y,Δx,Δy):
    '''
    计算河口断面中心连续稳定排放浓度场
    Qp——污水排放量，m³/s
    Cp——污染物排放浓度，mg/L
    Ch——河流上游污染物浓度，mg/L
    u——断面流速，m/s 
    h——断面水深，m
    Ey——污染物横向扩散指数，㎡/s
    k——污染物综合衰减系数，d^(-1)
    x——河流沿程坐标，m
    y——河道宽度，m
    '''
    k=k/86400
    m=int(x/Δx)
    n=2*int(y/(2*Δy))
    Δx1=x/m
    Δy1=y/n
    y2=-y/2
    y1=y/2
    data=np.random.random([m,n+1,3])
    idx=0
    for i in range(1,m+1):
        x=i*Δx1
        y=y2
        idy=0
        while y-y1< 0.00001:
            if y==0:
                y= Δy1/2
                C=Qp*Cp/(u*h*2*math.sqrt(math.pi*Ey*x/u))*math.exp(-u*y**2/(4*Ey*x)-k*x/u)+Ch
                y=0
            else:
                C=Qp*Cp/(u*h*2*math.sqrt(math.pi*Ey*x/u))*math.exp(-u*y**2/(4*Ey*x)-k*x/u)+Ch
            data[idx][idy][0]=x
            data[idx][idy][1]=y
            data[idx][idy][2]=C
            y+=Δy1
            idy =idy+1
        idx =idx+1
    return data

# 不考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
def cal_c_river_2d_side(Ch,m,h,Ey,u,k,x,y,Δx,Δy):     
    '''
    # E.6 平面二维数学模型
    # E.6.2.1 连续稳定排放
    # 不考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
    Ch——河流上游污染物浓度，mg/L
    m——污染物排放速率，g/s
    h——断面水深，m
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，d^(-1)
    x——纵向距离，m
    y——横向距离，m
    '''
    k=k/86400
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    data=np.random.random([n1,n2+1,3])
    idx=0
    for i in range(1,n1+1):
        x=i*Δx
        idy=0
        for i in range(n2+1):
            y=i*Δy
            C=Ch+m/(h*math.sqrt(math.pi*Ey*u*x))*math.exp(-u*y**2/(4*Ey*x))*math.exp(-k*x/u)
            data[idx][idy][0]=x
            data[idx][idy][1]=y
            data[idx][idy][2]=C
            idy=idy+1
        idx=idx+1
    return data

# 考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
def cal_c_river_2d_side_reflex(Ch,m,h,B,Ey,u,k,x,y,Δx,Δy):
    '''
    # E.6 平面二维数学模型
    # E.6.2.1 连续稳定排放
    # 考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源稳定排放
    Ch——河流上游污染物浓度，mg/L
    m——污染物排放速率，g/s
    h——断面水深，m
    B——水面宽度—，m
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，d^(-1)
    x——纵向距离，m
    y——横向距离，m
    '''
    k=k/86400
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    data=np.random.random([n1,n2+1,3])
    idx=0
    for i in range(1,n1+1):
        x=i*Δx
        idy=0
        for i in range(n2+1):
            y=i*Δy
            x1=0
            for n in range(-1,2):
                x1+=math.exp(-u*(y-2*n*B)**2/(4*Ey*x))
            C=Ch+m/(h*math.sqrt(math.pi*Ey*u*x))*math.exp(-k*x*u)*x1
            data[idx][idy][0]=x
            data[idx][idy][1]=y
            data[idx][idy][2]=C
            idy=idy+1
        idx=idx+1
    return data

#E.3.2.1 连续稳定排放
def cal_c_river_1d(k,Ex,u,B,A,Cp,Qp,Ch,Qh,x1,x2,Δx):
    '''
    # 河流纵向一维连续稳定排放
    α——O’Connor数，量纲一，表征物质离散降解通量与移流通量的比值
    Pe——贝克来数，量纲一，表征物质移流通量与离散通量的比值
    C0——河流排放口初始断面混合浓度，mg/L
    x——河流沿程坐标，m，x=0指排放口处，x>0指排放口下游段，x<0,指排放口上游段
    Cp——污染物排放浓度，mg/L
    Qp——污水排放量，m³/s
    Ch——河流上游污染物浓度，mg/L
    Qh——河流流量，m³/s
    B——水面宽度，m
    A——断面面积，㎡
    k——污染物综合衰减系数，d^(-1)
    u——断面流速，m/s 
    Ex——污染物纵向扩散指数，㎡/s
    '''
    k=k/86400
    α=k*Ex/u**2
    Pe=u*B/Ex
    n1=int(x1/Δx)
    n2=int(x2/Δx)
    Δx1=x1/n1
    Δx2=x2/n2
    C01=(Cp*Qp+Ch*Qh)/(Qp+Qh)
    C02=(Cp*Qp+Ch*Qh)/((Qp+Qh)*math.sqrt(1+4*α))
    C03=(Cp*Qp+Ch*Qh)/(2*A*math.sqrt(k*Ex))
    data=np.random.random([n1+n2+1,2])
    idx=0
    for i in range(n1):
        x=-x1+i*Δx1
        if α<=0.027 and Pe<1:
            data[idx][0]=x
            data[idx][1]=C01*math.exp(u*x/Ex)
            idx=idx+1
        elif α<=0.027 and Pe>=1:
            data[idx][0]=x
            data[idx][1]=0
            idx=idx+1
        elif 0.027<α<=380:
            data[idx][0]=x
            data[idx][1]=C02*math.exp(u*x/2/Ex*(1+math.sqrt(1+4*α)))
            idx=idx+1
        elif α>380:
            data[idx][0]=x
            data[idx][1]=C03*math.exp(x*math.sqrt(k/Ex))
            idx=idx+1
    for i in range(n2+1):
        x=i*Δx2
        if α<=0.027:
            data[idx][0]=x
            data[idx][1]=C01*math.exp(-k*x/u)
            idx=idx+1
        elif 0.027<α<=380:
            data[idx][0]=x
            data[idx][1]=C02*math.exp(u*x/2/Ex*(1-math.sqrt(1+4*α)))
            idx=idx+1
        elif α>380:
            data[idx][0]=x
            data[idx][1]=C03*math.exp(-x*math.sqrt(k/Ex))
            idx=idx+1
    return data

# 瞬时排放源河流一维对流扩散方程的浓度分布公式
def cal_c_river_1d_instant(k,Ex,u,A,M,Ch,Qh,x,Δx,t,Δt):
    '''
    # E.3.2.2 瞬时排放
    # 瞬时排放源河流一维对流扩散方程的浓度分布公式
    M——污染物的瞬时排放总质量，g
    A——断面面积，㎡
    Ex——污染物纵向扩散指数，㎡/s
    k——污染物综合衰减系数，d^(-1)
    u——断面流速，m/s
    x——离排放口距离，m
    t——排放发生后的扩散历时，s
    '''
    k=k/86400
    n1=int(x/Δx) 
    Δx=x/n1
    n2=int(t/Δt)
    Δt=t/n2
    data=np.random.random([n2+1,n1+1,3])
    idt=0
    for i in range(n2+1):
        t=i*Δt     
        idx=0
        for i in range(n1+1):
            x=i*Δx
            C=M/(A*math.sqrt(4*math.pi*Ex*(t if t>0 else 0.1)))*math.exp(-k*(t if t>0 else 0.1))*math.exp(-(x-u*(t if t>0 else 0.1))**2/(4*Ex*(t if t>0 else 0.1)))
            data[idt][idx][0]=t
            data[idt][idx][1]=x
            data[idt][idx][2]=C
            idx+=1
        idt+=1
    return data
    
# 不考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源瞬时排放
def cal_c_river_2d_instant_side(Ch,M,h,Ex,Ey,u,k,x,y,t,Δx,Δy,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    M——污染物的瞬时排放总质量，g
    h——断面水深，m
    Ex——污染物纵向扩散指数，㎡/s
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    t——排放发生后的扩散历时，s
    x——纵向距离，m
    y——横向距离，m
    '''
    k=k/86400
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    n3=int(t/Δt)
    Δt=t/n3
    data=np.random.random([n3,n1+1,n2+1,4])
    idt=0
    for i in range(1,n3+1):
        t=i*Δt
        idx=0
        for i in range(n1+1):
            x=i*Δx
            idy=0
            for i in range(n2+1):
                y=i*Δy
                C=Ch+M/(2*math.pi*h*t*math.sqrt(Ex*Ey))*math.exp(-(x-u*t)**2/(4*Ex*t)-y**2/(4*Ey*t))*math.exp(-k*t)
                data[idt][idx][idy][0]=t
                data[idt][idx][idy][1]=x
                data[idt][idx][idy][2]=y
                data[idt][idx][idy][3]=C
                idy=idy+1
            idx=idx+1
        idt=idt+1
    return data

# 考虑岸边反射影响的宽浅型平直恒定均匀河流，岸边点源瞬时排放
def cal_c_river_2d_instant_side_reflex(Ch,M,h,Ex,Ey,u,k,x,y,t,Δx,Δy,Δt):
    '''
    Ch——河流上游污染物浓度，mg/L
    M——污染物的瞬时排放总质量，g
    h——断面水深，m
    B——断面宽度, m
    Ex——污染物纵向扩散指数，㎡/s
    Ey——污染物横向扩散指数，㎡/s
    u——断面流速，m/s
    k——污染物综合衰减系数，s^(-1)
    t——排放发生后的扩散历时，s
    x——纵向距离，m
    y——横向距离，m
    '''
    k=k/86400
    B=y
    n1=int(x/Δx)
    Δx=x/n1
    n2=int(y/Δy)
    Δy=y/n2
    n3=int(t/Δt)
    Δt=t/n3  
    data=np.random.random([n3,n1+1,n2+1,4])
    idt=0
    for i in range(1,n3+1):
        t=i*Δt
        idx=0
        for i in range(n1+1):
            x=i*Δx
            idy=0
            for i in range(n2+1):
                y=i*Δy
                x1=0
                for n in range(-1,2):
                    x1+=math.exp(-(y-2*n*B)**2/(4*Ey*t))
                C=Ch+M/(2*math.pi*h*t*math.sqrt(Ex*Ey))*math.exp(-(x-u*t)**2/(4*Ex*t)-k*t)*x1
                data[idt][idx][idy][0]=t
                data[idt][idx][idy][1]=x
                data[idt][idx][idy][2]=y
                data[idt][idx][idy][3]=C
                idy=idy+1
            idx=idx+1
        idt=idt+1
    return data
