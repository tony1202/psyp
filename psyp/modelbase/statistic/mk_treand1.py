# -*- encoding: utf-8 -*-
'''
@File    :   mk_trend.py
@Time    :   2021/08/15 20:31:49
@Author  :   HMX 
@Version :   1.0
@Contact :   kzdhb8023@163.com
'''

# here put the import lib
from scipy.stats import norm
import numpy as np
from sklearn.linear_model import LinearRegression
 
 
def mk(x, alpha=0.1):
    n = len(x)

    # 计算趋势slope
    model = LinearRegression()
    model.fit(np.arange(1,n+1).reshape(-1,1),x)
    slope = model.coef_[0]
 
    # 计算S的值
    s = 0
    for j in range(n - 1):
        for i in range(j + 1, n):
            s += np.sign(x[i] - x[j])
 
    # 判断x里面是否存在重复的数，输出唯一数队列unique_x,重复数数量队列tp
    unique_x, tp = np.unique(x, return_counts=True)
    g = len(unique_x)
 
    # 计算方差VAR(S)
    if n == g:  # 如果不存在重复点
        var_s = (n * (n - 1) * (2 * n + 5)) / 18
    else:
        var_s = (n * (n - 1) * (2 * n + 5) - np.sum(tp * (tp - 1) * (2 * tp + 5))) / 18
 
    # 计算z_value
    if n <= 10:  # n<=10属于特例
        z = s / (n * (n - 1) / 2)
    else:
        if s > 0:
            z = (s - 1) / np.sqrt(var_s)
        elif s < 0:
            z = (s + 1) / np.sqrt(var_s)
        else:
            z = 0
 
    # 计算p_value，可以选择性先对p_value进行验证
    p = 2 * (1 - norm.cdf(abs(z)))
 
    # 计算Z(1-alpha/2)
    h = abs(z) > norm.ppf(1 - alpha / 2)
 
    # 趋势判断
    if (z < 0) and h:
        trend = -1#'decreasing'
    elif (z > 0) and h:
        trend = 1#'increasing'
    else:
        trend = 0#'no trend'
 
    return trend,p,slope


if __name__ == '__main__':
    data = [3.67,3.65,3.63,3.6,3.66,3.73,3.89,3.94,3.81,3.78,3.75,3.73,3.7,3.7,3.7,3.65,3.66,3.65,3.63,3.59,3.54,3.51,3.46,3.49,3.51,3.53,3.51,3.48,3.47,3.45,3.44,3.43,3.39,3.38,3.4,3.4,3.38,3.38,3.39,3.39,3.38,3.29,3.31,3.37,3.37,3.36,3.42,3.37,3.38,3.38,3.32,3.3,3.38,3.39,3.35,3.35,3.36,3.38,3.39,3.42,3.44,3.45,3.38,3.36,3.34,3.31,3.28,3.28,3.27,3.3,3.36,3.42,3.43,3.43,3.39,3.49,3.48,3.46,3.4,3.34,3.38,3.36,3.38,3.43,3.42,3.44,3.47,3.55,3.56,3.63,3.65,3.6,3.57,3.47,3.49,3.45,3.53,3.52,3.75,3.81,3.84,3.78,3.75,3.7,3.65,3.59,3.7,3.7,3.63,3.56,3.53,3.46,3.44,3.47,3.5,3.62,3.68,3.65,3.67,3.64,3.65,3.67,3.62,3.66,3.54,3.49,3.49,3.49,3.59,3.6,3.6,3.65,3.64,3.62,3.62,3.66,3.77,3.7,3.6,3.63,3.63,3.6,3.56,3.62,3.67,3.71,3.75,3.69,3.7,3.7,3.74,3.61,3.63,3.63,3.59,3.56,3.72,3.62,3.6,3.61,3.72,4.82,4.01,3.72,3.68,3.66,3.65,3.65,3.54,3.6,3.66,3.57,3.65,3.53,3.45,3.49,3.53,3.6,3.7,3.67,3.69,3.72,3.72,3.78,3.79,3.81,3.82,3.78,3.77,3.73,3.72,3.76,3.75,3.79,3.75,3.79,3.81,3.78,3.76,3.79,3.81,3.83,3.83,3.74,3.81,3.79,3.82,3.81,3.79,3.76,3.79,3.64,3.58,3.62,3.64,3.65,3.7,3.73,3.73,3.73,4.04,3.87,3.72,3.72,3.75,3.88,3.78,4.05,3.79,3.77,3.77,3.89,3.95,3.82,3.88,3.85,3.74,3.9,3.82,3.77,3.68,3.72,3.65,3.61,3.57,3.59,3.69,3.73,3.77,3.69,3.73,3.74,3.73,3.9,3.86,3.78,3.71,3.71,3.67,3.7,3.72,3.75,3.81,3.81,3.8,3.74,3.85,4.47,5.04,4.15,3.85,3.85,3.78,3.73,3.66,3.88,3.86,3.86,3.84,3.73,3.79,3.79,3.79,3.78,3.75,3.7,3.62,3.66,3.77,3.77,3.79,3.79,3.79,3.78,3.78,3.78,3.82,3.81,3.88,3.76,3.72,3.7,3.62,3.63,3.73,3.79,3.86,3.85,3.86,3.85,3.89,3.89,3.77,3.69,3.66,3.62,3.63,3.58,3.54,3.61,3.67,3.66,3.62,3.66,3.7,3.67,3.6,3.55,3.5,3.44,3.46,3.46,3.43,3.49,3.54,3.54,3.56,3.56,3.6,3.62,3.58,3.57,3.49,3.43,3.38,3.36,3.38,3.39,3.43,3.39,3.38,3.36,3.4,3.4,3.38,3.33,3.34,3.33,3.3,3.31,3.3,3.31,3.31,3.31,3.37,3.38,3.52,3.64,3.75,3.66,3.61,3.65,3.6,3.55,3.51,3.45,3.43,3.36,3.41,3.43,3.43,3.45,3.49,3.46,3.47,3.49,3.46,3.47,3.41,3.41,3.42,3.41,3.46,3.46,3.5,3.56,3.6,3.65,3.63,3.55,3.51,3.51,3.49,3.46,3.45,3.42,3.37,3.35,3.33,3.31,3.34,3.35,3.37,3.41,3.46,3.53,3.49,3.45,3.4,3.37,3.37,3.4,3.53,3.51,3.55,3.57,3.65,3.9,3.86,3.77,3.7,3.59,3.57,3.54,3.54,3.53,3.53,3.54,3.55,3.61,3.62,3.68,3.66,3.74,3.75,3.66,3.55,3.54,3.53,3.5,3.49,3.5,3.52,3.55,3.62,3.66,3.7,3.69,3.64,3.65,3.53,3.49,3.44,3.41,3.37,3.35,3.37,3.37,3.38,3.39,3.5,3.57,3.59,3.58,3.54,3.51,3.41,3.62,3.52,3.51,3.5,3.49,3.56,3.63,3.73,3.68,3.66,3.49,3.41,3.37,3.34,3.36,3.42,3.46,3.46,3.44,3.53,3.6,3.69,3.68,3.67,3.62,3.54,3.47,3.54,3.49,3.5,3.47,3.68,3.57,3.56,3.62,3.72,3.73,3.72,3.75,3.76,3.75,3.78,3.66,3.57,3.56,3.49,3.46,3.51,3.6,3.64,3.72,3.75,3.72,3.7,3.66,3.59,3.44,3.42,3.49,3.51,3.53,3.52,3.52,3.54,3.55,3.49,3.42,3.5,3.56,3.8,3.73,3.85,3.8,3.69,3.72,3.67,3.75,3.82,3.87,3.81,3.81,3.83,3.82,3.78,3.78,3.77,3.7,3.56,3.52,3.57,3.57,3.67,3.73,3.81,3.9,3.94,3.9,3.89,3.84,3.69,3.57,3.59,3.65,3.68,3.73,3.67,3.73,3.79,3.8,3.65,3.67,3.71,3.76,3.72,3.74,4.2,3.98,3.85,3.83,3.95,3.88,3.92,3.89,3.88,3.88,3.91,3.84,3.87,3.89,3.96,3.89,3.82,3.89,3.85,3.74,3.72,3.78,3.79,3.83,3.84,3.86,3.86,3.88,3.83,3.75,3.95,3.73,3.73,3.68,3.76,3.7,3.68,3.68,3.74,3.8,3.84,3.86,3.91,3.89,3.83,3.67,3.63,3.59,3.54,3.6,3.61,3.65,3.74,3.75,3.81,3.82,3.78,3.7,3.69,3.6,3.59,3.51,3.46,3.45,3.49,3.53,3.53,3.64,3.67,3.75,3.79,3.77,3.72,3.7,3.62,3.62,3.6,3.6,3.55,3.56,3.57,3.65,3.69,3.65,3.64,3.63,3.62,3.59,3.51,3.51,3.45,3.43,3.4,3.44,3.43,3.63,3.72,3.7,3.74,3.79,3.75,3.75,3.78,3.69,3.69,3.62,3.56,3.56,3.6,3.67,3.72,3.88,3.81,3.67,3.73,3.91,3.86,3.78,3.73,3.69,3.67,3.62,3.63,3.63,3.62,3.65,3.62,3.66,3.71,3.75,3.81,3.97,3.85,3.73,3.67,3.6,3.57,3.58,3.59,3.59,3.63,3.65,3.6,3.65,3.67,3.7,3.72,3.72,3.7,3.66,3.65,3.63,3.63,3.6,3.62,3.59,3.6,3.62,3.72,3.7,3.72,3.69,3.65,3.58,3.56,3.54,3.56,3.58,3.57,3.52,3.49,3.47,3.48,3.47,3.56,3.55,3.58,3.56,3.57,3.78,3.86,3.88,3.83,3.73,3.68,3.83,3.86,3.87,3.96,3.86,3.78,3.75,3.67,3.66,3.66,3.63,3.62,3.63,3.63,3.67,3.73,3.79,3.81,3.82,3.79,3.84,3.78,3.76,3.66,3.68,3.62,3.6,3.57,3.62,3.69,3.74,3.72,3.73,3.72,3.73,3.72,3.67,3.62,3.57,3.58,3.55,3.57,3.55,3.57,3.62,3.65,3.65,3.66,3.68,3.66,3.7,3.69,3.63,3.58,3.52,3.46,3.47,3.52,3.59,3.62,3.66,3.67,3.66,3.71,3.64,3.69,3.62,3.56,3.49,3.47,3.52,3.51,3.51,3.57,3.65,3.72,3.71,3.84,3.75,3.79,3.74,3.74,3.56,3.56,3.53,3.52,3.57,3.61,3.69,3.71,3.78,3.82,3.71,3.72,3.7,3.66,3.63,3.54,3.62,3.54,3.6,3.65,3.73,3.76,3.83,3.86,3.79,3.77,3.69,3.7,3.65,3.62,3.58,3.62,3.6,3.67,3.72,3.69,3.6,3.77,3.66,3.62,3.62,3.65,3.65,3.7,3.72,3.7,3.67,3.69,3.81 ,3.74 ,3.73 ,3.72 ,3.70 ,3.68 ,3.89 ,3.77 ,3.71 ,3.71 ,3.71 ,3.73 ,4.16 ,3.98 ,3.94 ,3.87 ,3.83 ,4.05 ,3.74 ,3.81 ,3.85 ,3.90 ,3.89 ,3.86 ,3.89 ,3.82 ,3.82 ,3.83 ,3.84 ,3.85 ,3.88 ,3.83 ,3.86 ,3.92 ,3.82 ,3.80 ,3.78 ,3.75 ,3.70 ,3.60 ,3.78 ,4.47 ,3.93 ,3.93 ,3.95 ,3.94 ,4.00 ,3.89 ,3.92 ,3.85 ,3.91 ,3.84 ,3.83 ,3.84 ,3.68 ,3.65 ,3.68 ,3.64 ,3.86 ,3.83 ,3.80 ,3.85 ,3.84 ,4.10 ,3.98 ,3.95 ,3.94 ,3.94 ,3.91 ,3.90 ,3.82 ,3.86 ,3.84 ,3.81 ,3.81 ,3.84 ,3.82 ,3.79 ,3.80 ,3.82 ,3.73 ,3.78 ,3.76 ,3.71 ,3.69 ,3.62 ,3.60 ,3.55 ,3.57 ,3.67 ,3.72 ,3.75 ,3.67 ,3.69 ,3.67 ,3.71 ,3.64 ,3.65 ,3.54 ,3.49 ,3.47 ,3.49 ,3.47 ,3.48 ,3.63 ,3.69 ,3.76 ,3.73 ,3.75 ,3.73 ,3.70 ,3.62 ,3.51 ,3.54 ,3.47 ,3.45 ,3.47 ,3.51 ,3.54 ,3.55 ,3.61 ,3.68 ,3.70 ,3.70 ,3.69 ,3.61 ,3.62 ,3.58 ,3.53 ,3.51 ,3.54 ,3.51 ,3.51 ,3.50 ,3.48 ,3.57 ,3.65 ,3.61 ,3.65 ,3.65 ,3.57 ,3.58 ,3.50 ,3.43 ,3.43 ,3.42 ,3.45 ,3.45 ,3.57 ,3.60 ,3.75 ,3.69 ,3.68 ,3.63 ,3.54 ,3.55 ,3.54 ,3.49 ,3.48 ,3.45 ,3.44 ,3.41 ,3.40 ,3.41 ,3.39 ,3.47 ,3.46 ,3.51 ,3.53 ,3.47 ,3.53 ,3.54 ,3.44 ,3.42 ,3.51 ,3.48 ,3.52 ,3.55 ,3.63 ,3.63 ,3.58 ,3.63 ,3.65 ,3.63 ,3.58 
]
    data2 = np.arange(100)
    data3 = np.arange(100,0,-1)
    for x in [data,data2,data3]:
        trend,p,slope = mk(x,alpha = 0.05)
        print(f'trend:{trend}','p_value:{:.2f}'.format(p),'slope:{:.2f}'.format(slope),sep = ',')

