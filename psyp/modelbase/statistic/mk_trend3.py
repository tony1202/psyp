import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
import datetime as dt


class Mk:

    def __init__(self, filepath: str, int_x: int, int_y: int):
        """
        :param filepath: 文件路径 文件格式为.xls、.xlsx
        :param da_x: x轴数据列号
        :param da_y: y周数据列号
        """
        self.__filepath = filepath
        self.__da_x = int_x
        self.__da_y = int_y
        self.__da_x_content = None
        self.__da_y_content = None
        self.__data = None

    @property
    def da_x_content(self):
        return self.__da_x_content

    @property
    def da_y_content(self):
        return self.__da_y_content

    @property
    def data(self):
        return self.__data

    def read_data(self):
        data = pd.read_excel(self.__filepath,converters= {'Date': pd.to_datetime})
        self.__da_x_content = data.iloc[:, self.__da_x]
        self.__da_y_content = data.iloc[:, self.__da_y]
        self.__data = data

    @staticmethod
    def __cal_mk_process(y):
        n = len(y)

        # 正序计算
        # 定义累计量序列Sk，长度n，初始值为0
        Sk = np.zeros(n)
        UFk = np.zeros(n)

        # 定义Sk序列元素s
        s = 0

        for i in range(1, n):
            for j in range(0, i):
                if y.iloc[i] > y.iloc[j]:
                    s += 1
            Sk[i] = s
            E = (i + 1) * (i / 4)
            Var = (i + 1) * i * (2 * (i + 1) + 5) / 72
            UFk[i] = (Sk[i] - E) / np.sqrt(Var)

        # 逆序计算
        # 定义逆累计量序列Sk2
        # 定义逆统计量序列Sk2
        y2 = np.zeros(n)
        Sk2 = np.zeros(n)
        UBk = np.zeros(n)

        s = 0
        y2 = y[::-1]

        for i in range(1, n):
            for j in range(0, i):
                if y2.iloc[i] > y2.iloc[j]:
                    s += 1
            Sk2[i] = s
            E = (i + 1) * (i / 4)
            Var = (i + 1) * i * (2 * (i + 1) + 5) / 72
            UBk[i] = -(Sk2[i] - E) / np.sqrt(Var)

        UBk2 = UBk[::-1]
        return UFk, UBk2

    @staticmethod
    def make_img(data, UFk, UBk2, x_label = '年份', y_label = 'Mann-Kendall检验值', lr = 0.75, tb = 0.2,):
        plt.rcParams['font.family'] = ['SimHei']
        plt.rcParams['axes.unicode_minus'] = False
        # 画图
        # plt.figure(figsize=(7, 6), dpi=350)
        plt.figure()
        
        plt.plot(data['Date'], UFk, label='UF', color='orange', marker='s',markersize=1)
        plt.plot(data['Date'], UBk2, label='UB', color='cornflowerblue', linestyle='--', marker='o',markersize=1)
        
        plt.xticks(data['Date'])

        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        plt.gca().xaxis.set_major_locator(mdates.YearLocator())
        plt.tick_params(axis='x')
               
        plt.ylabel(y_label)
        plt.xlabel(x_label)

        ax = plt.gca()
        # # # center lable
        # ax.set_xticks([float(n)+0.5 for n in ax.get_xticks()])          

        # 添加辅助线
        x_lim = plt.xlim()        
        # 添加显著水平线和y=0
        plt.plot(x_lim, [-1.96, -1.96], ':', color='black', label='5%显著水平')
        plt.plot(x_lim, [0, 0], '--', color='black')
        plt.plot(x_lim, [1.96, 1.96], ':', color='black')
        
        # 设置图例位置，第一个参数调整左右位置，第二个参数调整上下位置
        plt.legend(bbox_to_anchor=(lr, tb), facecolor='w', frameon=False)
        
        # 添加文本注释
        xs = mdates.date2num(data['Date'][0])
        plt.text(xs, -1.6, '突变点检验')
        # plt.show()
        plt.savefig(r'D:\Temp\dz\甘露.png')

    def cal_mk(self):
        return self.__cal_mk_process(self.da_y_content)


if __name__ == '__main__':
    mk = Mk(r"D:\Temp\dz\甘露.xlsx", 0, 1) # 列数据
    mk.read_data()
    UFk, UBk2 = mk.cal_mk()
    mk.make_img(mk.data, UFk, UBk2) # 可通过x_label, y_label控制x，y轴标签
    # mk.make_img(mk.da_x_content, UFk, UBk2, "x", "y")
    # mk.make_img(mk.da_x_content, UFk, UBk2, "x", "y", 0.9, 0.9)# 通过lr，tb控制图例位置


