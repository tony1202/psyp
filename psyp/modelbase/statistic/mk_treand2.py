# -*- encoding: utf-8 -*-
'''
@File    :   mk_demo.py
@Time    :   2021/11/27 12:01:23
@Author  :   HMX 
@Version :   1.0
@Contact :   kzdhb8023@163.com
'''
# here put the import lib
import pymannkendall as mk
import numpy as np

# 生成3组数据
data = np.random.random(100)
data2 = np.arange(100)
data3 = np.arange(100,0,-1)

for x in [data,data2,data3]:
    result = mk.original_test(x,alpha=0.05)#alpha默认为0.05
    print(result)
    
Mann_Kendall_Test(trend='no trend', h=False, p=0.823256299016955, z=-0.22335875907380243, Tau=-0.015353535353535354, s=-76.0, var_s=112750.0, slope=-0.0002038833935962383, intercept=0.4745895001589582)
Mann_Kendall_Test(trend='increasing', h=True, p=0.0, z=14.73869998208331, Tau=1.0, s=4950.0, var_s=112750.0, slope=1.0, intercept=0.0)
Mann_Kendall_Test(trend='decreasing', h=True, p=0.0, z=-14.73869998208331, Tau=-1.0, s=-4950.0, var_s=112750.0, slope=-1.0, intercept=100.0)

print(f'trend:{res.trend}','p_value:{:.2f}'.format(res.p),'slope:{:.2f}'.format(res.slope),sep = ',')
