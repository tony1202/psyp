import os
import numpy as np
import re
from pickle import dump,load
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from tensorflow.keras.utils import to_categorical


# load doc into memory
def load_doc(filename):
	# open the file as read only
	file = open(filename, 'r',encoding='utf-8')
	# read all text
	text = file.read()
	# close the file
	file.close()
	return text

# save tokens to file, one dialog per line
def save_doc(lines, filename):
	data = '\n'.join(lines)
	file = open(filename, 'w',  encoding='utf-8')
	file.write(data)
	file.close()

# 由领域语料创建领域语言模型
def generate_nlp_model(filename):
    model_dir = os.path.dirname(filename)
    # load text
    raw_text = load_doc(filename)
    print(raw_text)

    # clean
    tokens = re.sub(r'[0-9]+|\\|—|/|-|^\s*$|[a-zA-Z]|一|～|[.|~|%|+|±|\r\n|]|\n[\s| ]*\n|^\s+|[\t+]', '', raw_text)
    tokens = tokens.split()
    raw_text = ' '.join(tokens)
    print(raw_text)

    # organize into sequences of characters
    length = 10
    sequences = list()
    for i in range(length, len(raw_text)):
        # select sequence of tokens
        seq = raw_text[i-length:i+1]
        # store
        sequences.append(seq)
        print(seq)
    print('Total Sequences: %d' % len(sequences))

    # save sequences to file
    out_filename =os.path.join(model_dir, 'char_sequences.txt')
    save_doc(sequences, out_filename)

    # Train Language Model
    # develop a neural language model for the prepared sequence data.
    # The model will read encoded characters and predict the next character
    #  in the sequence. A Long Short-Term Memory recurrent neural network 
    # hidden layer will be used to learn the context from the input sequence
    #  in order to make the predictions

    # load
    in_filename = out_filename
    raw_text = load_doc(in_filename)
    lines = raw_text.split('\n')

    # Encode Sequences
    chars = sorted(list(set(raw_text)))
    print(chars)
    mapping = dict((c, i) for i, c in enumerate(chars))
    print(mapping)

    sequences = list()
    for line in lines:
        # integer encode line
        encoded_seq = [mapping[char] for char in line]
        # store
        sequences.append(encoded_seq)
        print(encoded_seq)
    
    # vocabulary size
    vocab_size = len(mapping)
    print('Vocabulary Size: %d' % vocab_size)

    # Split Inputs and Output
    sequences = np.array(sequences)
    X, y = sequences[:,:-1], sequences[:,-1]
    print(X)
    print(y)

    # one hot encode each character
    sequences = [to_categorical(x, num_classes=vocab_size) for x in X]
    print(sequences)
    X = np.array(sequences)
    print(X)
    y = to_categorical(y, num_classes=vocab_size)
    print(y)

    # Fit Model
    # define model
    model = Sequential()
    model.add(LSTM(75, input_shape=(X.shape[1], X.shape[2])))
    model.add(Dense(vocab_size, activation='softmax'))
    print(model.summary())

    # compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # fit model
    model.fit(X, y, epochs=100, verbose=2)

    # save the model to file
    model.save(os.path.join(model_dir, 'model.h5'))

    # save the mapping
    dump(mapping, open(os.path.join(model_dir, 'mapping.pkl'), 'wb'))

# 预测
def generate_seq(modelfile,mappingfile,text,seq_length):
    # generate a sequence of characters with a language model
    def generate_seq(model, mapping, seq_length, seed_text, n_chars):
        in_text = seed_text
        # generate a fixed number of characters
        for _ in range(n_chars):
            # encode the characters as integers
            encoded = [mapping[char] for char in in_text]
            # truncate sequences to a fixed length
            encoded = pad_sequences([encoded], maxlen=seq_length, truncating='pre')
            # one hot encode
            encoded = to_categorical(encoded, num_classes=len(mapping))
            # predict character
            # yhat = model.predict_classes(encoded, verbose=0)
            # yhat = (model.predict(encoded) > 0.5).astype("int32")
            yhat=model.predict(encoded)
            yhat= np.argmax(yhat,axis=1)
            # reverse map integer to character
            out_char = ''
            for char, index in mapping.items():
                if index == yhat:
                    out_char = char
                    break
            # append to input
            in_text += char
        return in_text
    
    # load the model
    model = load_model(modelfile)
    # load the mapping
    mapping = load(open(mappingfile, 'rb'))
    
    # test start of rhyme
    # print(generate_seq(model, mapping, 10, 'Sing a son', 20))
    # test mid-line
    return generate_seq(model, mapping, 10, text, seq_length)
    # test not in original
    # print(generate_seq(model, mapping, 10, 'hello worl', 20))

def train_predict(dir,int_text_filename,keywords):
    outtext = os.path.join(dir,keywords+'.txt')
    generate_nlp_model(os.path.join(dir,int_text_filename))
    res =generate_seq(os.path.join(dir,'model.h5'),\
                 os.path.join(dir, 'mapping.pkl'),\
                 keywords)
    f = open(outtext,'w')
    f.write(res)
    f.close()

if __name__ == '__main__':
    # generate_nlp_model(r'tests\testdata\nlp\keras\emd\2022年公布典型案例.txt')
    # generate_seq(r'tests\testdata\nlp\keras\emd\model.h5',\
    #              r'tests\testdata\nlp\keras\emd\mapping.pkl',\
    #              '监管缺失')

    # dir = r'tests\testdata\nlp\keras\六中全会'
    # outtext = os.path.join(dir,'generatedText.txt')
    # generate_nlp_model(os.path.join(dir, '十九届六中全会.txt'))
    # res =generate_seq(os.path.join(dir,'model.h5'),\
    #              os.path.join(dir, 'mapping.pkl'),\
    #              '学习六中全会')
    # f = open(outtext,'w')
    # f.write(res)
    # f.close()

    dir=r'tests\testdata\nlp\keras\环评文件'
    # txtfile = os.path.join(dir,'report_245022.txt')
    # generate_nlp_model(txtfile)

    model_file = os.path.join(dir,'model.h5')
    mapping_file = os.path.join(dir,'mapping.pkl')
    outtext_file = os.path.join(dir,'generatedText.txt')

    res =generate_seq(model_file,mapping_file,\
                 '接管污水量为',100)
    # f = open(outtext_file,'w')
    # f.write(res)
    # f.close()
    print(res)