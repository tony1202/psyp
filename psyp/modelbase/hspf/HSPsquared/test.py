from HSP2.main import main
from HSP2IO.hdf import HDF5
from HSP2IO.io import IOManager
from HSP2tools import readUCI,readWDM
import os

dir = r'C:\sypcloud_git\projects\114_yn_jsj\model\basins'
prj_name='jsj'

uci_file = os.path.join(dir,prj_name+'.uci')
wdm_file = os.path.join(dir,prj_name+'.wdm')
output_hdf5_path = os.path.join(dir,prj_name+'.h5')
    
readUCI(uci_file,output_hdf5_path)
readWDM(wdm_file,output_hdf5_path)

try:        
    with HDF5(output_hdf5_path) as hdf5_instance:
        io_manager = IOManager(hdf5_instance)
        main(io_manager, saveall=False)

except Exception as ex:
    print('\n'+ ex.args[0]+'\n')