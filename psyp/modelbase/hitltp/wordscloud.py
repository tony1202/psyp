from psyp.modelbase.hitltp.sentence_parser import *
from wordcloud import WordCloud, get_single_color_func, ImageColorGenerator
import numpy as np
import matplotlib.pyplot as plt

def eventMining2wordscloud(content):
    if not content:
        return []
    ltp_p = LtpParser()
    
    # 对文章进行去噪处理
    content = ltp_p.remove_noisy(content)
    # 对文章进行长句切分处理
    sents = ltp_p.seg_content(content)  
    # 对文章进行短句切分处理
    subsents = ltp_p.seg_short_content(content)
    subsents_seg = []
    # words_list存储整篇文章的词频信息
    words_list = []
    text=''
    for sent in subsents:
        words, hidden, postags = ltp_p.process_sent([sent])
        words_list += [[i[0], i[1]] for i in zip(words, postags)]
    # 获取文章关键词, 并图谱组织, 这个可以做
    text = ' '.join(i[0] for i in ltp_p.extract_keywords(words_list))
    # mask
    x, y = np.ogrid[:600, :600]

    mask = (x - 300) ** 2 + (y - 300) ** 2 > 270 ** 2
    mask = 255 * mask.astype(int)

    wc = WordCloud(font_path='C:\Windows\Fonts\STSONG.TTF',
                   background_color="white", repeat=True, mask=mask)
    wc.generate(text)

    # plt.axis("off")
    # plt.imshow(wc, interpolation="bilinear")
    # plt.savefig('wordscloud.png')

    wc.to_file('wordscloud.png')

