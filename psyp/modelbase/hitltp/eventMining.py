from psyp.modelbase.hitltp.GraphShow import *
from psyp.modelbase.hitltp.sentence_parser import *
from ltp import LTP

from collections import Counter
# from keywords_textrank import *
# from GraphShow import *
# from sentence_parser import *


class eventMining:
    def __init__(self):
        self.parser = LtpParser()
        self.ner_dict = {
        'nh':'人物',
        'ni':'机构',
        'ns':'地名'
        }
        self.graph_shower = GraphShow()

    '''test'''
    def test(self):
        ltp = LTP()     # 默认加载 Small 模型
                        # ltp = LTP(path="small")
                        #     其中 path 可接受的路径为下载下来的模型或者解压后的文件夹路径
                        #     另外也可以接受一些已注册可自动下载的模型名(可使用 ltp.available_models() 查看): 
                        #     base/base1/base2/small/tiny/GSD/GSD+CRF/GSDSimp/GSDSimp+CRF
        seg, hidden = ltp.seg(["他叫汤姆去拿外衣。"])
        pos = ltp.pos(hidden)
        ner = ltp.ner(hidden)
        srl = ltp.srl(hidden)
        dep = ltp.dep(hidden)
        sdp = ltp.sdp(hidden)

    '''列表全排列'''
    def combination(self, a):
        combines = []
        if len(a) == 0:
            return []
        for i in a:
            for j in a:
                if i == j:
                    continue
                combines.append('@'.join([i, j]))
        return combines
    '''构建实体之间的共现关系'''
    def collect_coexist(self, ner_sents, ners):
        co_list = []
        for sent in ner_sents:
            words = [i[0] + '/' + i[1] for i in zip(sent[0], sent[1])]
            co_ners = set(ners).intersection(set(words))
            co_info = self.combination(list(co_ners))
            co_list += co_info
        if not co_list:
            return []
        return {i[0]:i[1] for i in Counter(co_list).most_common()}
    '''基于文章关键词，建立起实体与关键词之间的关系'''
    def rel_entity_keyword(self, ners, keyword, subsent):
        events = []
        rels = []
        sents = []
        ners = [i.split('/')[0] for i in set(ners)]
        keyword = [i[0] for i in keyword]
        for sent in subsent:
            tmp = []
            for wd in sent:
                if wd in ners + keyword:
                    tmp.append(wd)
            if len(tmp) > 1:
                sents.append(tmp)
        for ner in ners:
            for sent in sents:
                if ner in sent:
                    tmp = ['->'.join([ner, wd]) for wd in sent if wd in keyword and wd != ner and len(wd) > 1]
                    if tmp:
                        rels += tmp
        for e in set(rels):
            events.append([e.split('->')[0], e.split('->')[1]])
        return events

    def main(self, content):
        if not content:
            return []
        # 对文章进行去噪处理
        content = self.parser.remove_noisy(content)
        # 对文章进行长句切分处理
        sents = self.parser.seg_content(content)  
        # 对文章进行短句切分处理
        subsents = self.parser.seg_short_content(content)
        subsents_seg = []
        # words_list存储整篇文章的词频信息
        words_list = []
        # ner_sents保存具有命名实体的句子
        ner_sents = []
        # ners保存命名实体
        ners = []
        # triples保存主谓宾短语
        triples = []
        # 存储文章事件
        events = []
        for sent in subsents:
            words, hidden, postags = self.parser.process_sent([sent])
            words_list += [[i[0], i[1]] for i in zip(words, postags)]
            subsents_seg.append([i[0] for i in zip(words, postags)])
            ner = self.parser.collect_ners(words,postags)
            if ner:
                triple = self.parser.extract_triples(words, hidden, postags)
                if not triple:
                    continue
                triples += triple
                ners += ner
                ner_sents.append([words, postags])
        # 获取文章关键词, 并图谱组织, 这个可以做
        keywords = [i[0] for i in self.parser.extract_keywords(words_list)]
        for keyword in keywords:
            name = keyword
            cate = '关键词'
            events.append([name, cate])
        # 对三元组进行event构建，这个可以做
        for t in triples:
            if (t[0] in keywords or t[1] in keywords) and len(t[0]) > 1 and len(t[1]) > 1:
                events.append([t[0], t[1]])
        # 获取文章词频信息话，并图谱组织，这个可以做
        word_dict = [i for i in Counter([i[0] for i in words_list if i[1][0] in ['n', 'v'] and len(i[0]) > 1]).most_common()][:10]
        for wd in word_dict:
            name = wd[0]
            cate = '高频词'
            events.append([name, cate])
        #　获取全文命名实体，这个可以做
        ner_dict = {i[0]:i[1] for i in Counter(ners).most_common()}
        for ner in ner_dict:
            name = ner.split('/')[0]
            cate = self.ner_dict[ner.split('/')[1]]
            events.append([name, cate])
        # 获取全文命名实体共现信息,构建事件共现网络
        co_dict = self.collect_coexist(ner_sents, list(ner_dict.keys()))
        co_events = [[i.split('@')[0].split('/')[0], i.split('@')[1].split('/')[0]] for i in co_dict]
        events += co_events
        #将关键词与实体进行关系抽取
        events_entity_keyword = self.rel_entity_keyword(ners, keywords, subsents_seg)
        events += events_entity_keyword
        #对事件网络进行图谱化展示
        self.graph_shower.create_page(events)

def eventMining2network(content):
    handler = eventMining()
    handler.main(content)

if __name__ == '__main__':
    content = '''
    （原标题：中科院研究生遇害案：凶手系同乡学霸，老师同学已为死者发起捐款）

    6月14日下午6点多，中科院信息工程研究所硕士研究生谢雕在饭馆招待自重庆远道而来的高中同学周凯旋时，被周凯旋用匕首杀害。随后，周凯旋被北京警方抓获。

    周凯旋被抓后，他的家人向被警方递交了精神鉴定材料，称周凯旋患有精神性疾病。

    谢雕的家人罗发明告诉南都记者，谢雕被害后，他的研究生老师和同学发起了捐款。并说，谢雕的遗体已经进行尸检，等尸检结果出来后，家人将会把火化后的骨灰带回老家安葬，之后，他们将等待北京检察机关的公诉。

    高中同学千里赴京去杀人

    今年25岁的谢雕生长于重庆垫江县的一个小山村，谢雕和周凯旋同在垫江中学读高中，两人学习成绩名列前茅，周凯旋经常考年级第一，两人都是垫江中学的优秀毕业生，谢雕考上了西安电子科技大学，周凯旋考取了四川大学。

    微信图片_20180627174901_副本.jpg案发现场的行凶者周凯旋（受访者提供）。

    学习优秀的周凯旋认为自己应该能考上北大清华等名校，于是在入读四川大学两三个月后，选择了退学复读。经过半年多的苦读，周凯旋以优异成绩考取了西安交通大学，来到了谢雕所在的城市，且是硕博连读。

    但周凯旋因大学本科期间因沉迷游戏，考试不及格，最终失掉了硕博连读的机会，本科毕业后就回到重庆寻找就业机会。谢雕自西安电子科技大学毕业后，在2016年考取了中国科学院大学的硕士研究生，所读专业隶属于中科院信息工程研究所。

    谢雕的家人告诉南都记者，6月14日下午6点，谢雕在西五环外的中科院信息工程研究所门口见到了久未见面的高中同学周凯旋。把他带到旁边的饭馆吃饭，两人还合影发到了高中同学微信群。这时，谢雕还没意识到周凯旋即将对他带来致命伤害。

    南都记者在谢雕遇害现场视频中看到，在谢雕点菜时，周凯旋用匕首刺向他胸部，谢雕中刀站起后退时，周凯旋用匕首又刺向他颈部，谢雕倒地后，周凯旋又从背部向他连刺几刀。之后，又持刀割断了谢雕的颈部动脉。这时，有食客拿起椅子砸向正在行凶的周凯旋。刺死谢雕后，周凯旋举起双手挥舞，随后扬长而去。后来，周凯旋被北京警方抓获。

    同学聚会时自己觉得受伤害起杀心

    罗发明告诉南都记者，作为被害人家属，他们向北京警方了解到，凶案原因来自两年前的一场同学聚会，谢雕的一些话对周凯旋带来很大心理压力，让他不能释怀。

    两年前的一次高中同学聚会中，大家聊的话题很多，也聊到了周凯旋喜欢打游戏的事情，谢雕说了一些激励周凯旋的话，让他不要再打游戏，要振作起来。在参与聚会的同学们看来，这些话是常理之中的，但在周凯旋看来，对他带来很大伤害，两年来给他带来很大心理压力。

    参与那次聚会的同学后来回忆，在一起玩“狼人杀”游戏时，谢雕、周凯旋发生了争执，但不愉快的瞬间很快就过去了，大家也都没当回事。

    那次聚会之后的春节，不少同学发现被周凯旋拉黑，中断了联系。直至一年之后，周凯旋才加入了高中同学微信群。

    谢雕的家人说，周凯旋在网上购买了杀人凶器匕首，收货地址填写了北京，他在北京拿到网购的匕首后，才暗藏在身前来面见谢雕。

    师生捐款助他家人渡难关

    周凯旋被北京警方抓获后，他的家人向警方称周凯旋患有精神病，并提供了一些证明材料，希望得到从轻处置。


    谢雕遇害后，他的学校为失去这么优秀的学生感到惋惜。谢雕的老师说，“谢雕家境并不富裕，本科尚有2.5万助学贷款未偿还，前不久还向同学借款1万，父亲也患有鼻咽癌。”

    谢雕的老师和同学发起了捐款，希望能帮助谢雕的家人暂时渡过难关。

    谢雕的家人告诉南都记者，他们向谢雕的学校提出要求，希望案件能尽快解决。

    罗发明对南都记者说，谢雕的遗体已经进行尸检，尸检后十天至十五天出来结果，等拿到尸检报告后，他们会尽快火化谢雕的遗体，把他的骨灰带回重庆老家安葬。

    对于这一案件，谢雕的家人告诉南都记者，他们将等待北京的检察机关提起公诉。


    '''
    eventMining2network(content)
    