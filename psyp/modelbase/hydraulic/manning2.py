from cProfile import label
from typing import Sequence
from shapely.geometry import Polygon,LineString
import numpy as np
import matplotlib.pyplot as plt
import os

def get_cross_pt(xc,yc,y):
    # 计算交点前后点序号
    coords = list(zip(xc, yc))
    segment = []
    for i, val in enumerate(coords):
        xi, yi = val
        if yi < y:
            segment.append(i)

    if segment[0]==0:
        xy1_left=coords[segment[0]]
    else:
        xy1_left = coords[segment[0]-1]
    xy1_right = coords[segment[0]]

    xy2_left = coords[segment[-1]]
    if segment[-1]==len(yc)-1:
        xy2_right = coords[segment[-1]]
    else:
        xy2_right = coords[segment[-1]+1]

    # 左岸交点x值
    x1_left, y1_left = xy1_left
    x1_right, y1_right = xy1_right

    if y1_left==y1_right:
        x0_1=x1_left
    else:
        x0_1 = x1_left - (x1_left - x1_right) * (y1_left - y) / (y1_left - y1_right)
    
    # 右岸交点x值
    x2_left, y2_left = xy2_left
    x2_right, y2_right = xy2_right

    if y2_left==y2_right:
        x0_2=x2_left
    else:
        x0_2 = x2_left - (x2_left - x2_right) * (y2_left - y) / (y2_left - y2_right)
    return x0_1,x0_2,segment

def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

def CalDepthFromQ(sdir,id,x,y,q,s,nc,ntl,ntr,tlidx,dcidx,tridx):
    '''
    sdir: 结果图保存路径
    id：标识
    x = [0,1,2,3,4,5,6,7,8]
    y = [10,3,3,0,0,0,1,1,2]
    q=20
    s=0.001 # 河床水力坡度

    #---参数---#
    nc = 0.03 # 主槽糙率系数
    ntl = 0.1 # 左滩区糙率
    ntr = 0.06 # 右滩区糙率

    tlidx=2 # 左岸滩分界点序号
    tridx=6 # 右岸滩分界点序号
    dcidx=4 # 主槽最低点序号

    '''
    #---待求参数---#
    yn = 0 # 待求水深
    velocity = 0 # 待求流速
    qtl,area_tl,w_tl,p_tl,dp_tl,qtr,area_tr,w_tr,p_tr,dp_tr,qc,area_c,w_c,p_c,dp_c=np.linspace(0,0,15)
    #---左岸滩区坐标---#
    xtl=x[:tlidx+1]
    ytl=y[:tlidx+1]
    dl = min(ytl)

    #---主槽坐标---#
    xc=x[tlidx:tridx+1]
    yc=y[tlidx:tridx+1]
    xcl=x[tlidx:dcidx+1]
    ycl=y[tlidx:dcidx+1]
    xcr=x[dcidx:tridx+1]
    ycr=y[dcidx:tridx+1]
    dc=y[dcidx]

    #---右岸滩区坐标---#
    xtr=x[tridx:]
    ytr=y[tridx:]
    dr = min(ytr)

    # 左滩湿周点集
    xl_tmp=[]
    yl_tmp=[]
    # 右滩湿周点集
    xr_tmp=[]
    yr_tmp=[]
    # 主槽湿周点集
    xc_tmp=[]
    yc_tmp=[]
    # 全断面湿周点集
    x_tmp=[]
    y_tmp=[]

    #---迭代水深，直至计算的流量大于等于给定流量---#
    dcc = dc
    tag = 0
    delt = 0.01
    while(True):
        yn=yn+delt   
        d=dcc+yn #水深换算为水位
        #---计算过水面积---#
        # 水深低于两岸堤防：只计算主槽过水面积
        if d <= yc[0] and d <= yc[-1] and tag==0:
            # 插值生成该水深下两岸坐标
            xl0,xr0,segment = get_cross_pt(xc,yc,d)
            # 拼接成过水断面点集合
            xc_tmp = np.append(np.append([xl0],xc[segment[0]:segment[-1]+1]),[xr0])
            yc_tmp = np.append(np.append([d],yc[segment[0]:segment[-1]+1]),[d])            
            polygon_geom = Polygon(zip(xc_tmp, yc_tmp))
            polyline_geom = LineString(zip(xc_tmp, yc_tmp))
            area_c = polygon_geom.area
            p_c = polyline_geom.length
            w_c = polygon_geom.length-p_c            
            qc= 1/nc * area_c * (area_c/p_c)**(2/3) * s**0.5

            if d+delt>yc[0] or d+delt>yc[-1]:
                if yc[0]==yc[-1]:
                    tag =1 if dl<dr else 2                    
                else:
                    tag =1 if yc[0]<yc[-1] else 2
                yn=0
        # 水深高于左岸堤防小于右岸堤防：过水面积=主槽过水面积+左岸滩区过水面积，主槽左岸假定为竖直
        elif tag==1:        
            # 计算左岸滩区部分,右侧衔接主槽竖壁
            # 考虑滩区低于堤防的情况
            if len(ytl)>1:
                dcc = dl   
                d=dcc+yn #水深换算为水位
                # 插值生成该水深下两岸坐标
                xl0,xr0,segment = get_cross_pt(xtl,ytl,d)
                # 拼接成过水断面点集合
                xl_tmp = np.append(np.append([xl0],xtl[segment[0]:segment[-1]+1]),[xr0])
                yl_tmp = np.append(np.append([d],ytl[segment[0]:segment[-1]+1]),[d])  
                polygon_geom = Polygon(zip(xl_tmp, yl_tmp))
                polyline_geom = LineString(zip(xl_tmp, yl_tmp))
                area_tl = polygon_geom.area
                p_tl = polyline_geom.length        
                w_tl = polygon_geom.length-p_tl
                qtl= 1/ntl * area_tl * (area_tl/p_tl) **(2/3) * s ** 0.5
                dp_tl = d                
            # 如果左滩水位计算到了主槽水位，则继续计算主槽部分
            if d> yc[0]:
                # 计算主槽部分
                # 插值生成该水深下两岸坐标
                xl0,xr0,segment = get_cross_pt(xc,yc,d)
                xc_tmp = np.append(np.append(xl0,xc[:segment[-1]+1]),xr0)
                yc_tmp = np.append(np.append([d],yc[:segment[-1]+1]),[d])
                polygon_geom = Polygon(zip(xc_tmp, yc_tmp))
                polyline_geom = LineString(zip(xc_tmp, yc_tmp))        
                area_c = polygon_geom.area
                p_c = polyline_geom.length
                w_c = polygon_geom.length-p_c
                qc= 1/nc * area_c * (area_c/p_c)**(2/3) * s** 0.5            

                # 计算右滩部分
                if d> yc[-1]:
                    if len(ytr)>1:                
                        # 插值生成该水深下两岸坐标
                        xl0,xr0,segment = get_cross_pt(xtr,ytr,d)
                        # 拼接成过水断面点集合
                        xr_tmp = np.append(np.append([xl0],xtr[segment[0]:segment[-1]+1]),[xr0])
                        yr_tmp = np.append(np.append([d],ytr[segment[0]:segment[-1]+1]),[d])  
                        polygon_geom = Polygon(zip(xr_tmp, yr_tmp))
                        polyline_geom = LineString(zip(xr_tmp, yr_tmp))
                        area_tr = polygon_geom.area
                        p_tr = polyline_geom.length        
                        w_tr = polygon_geom.length-p_tr
                        qtr= 1/ntr * area_tr * area_tr/p_tr **(2/3) * s ** 0.5
                        dp_tr = d  
        # 水深高于右岸堤防小于左岸堤防：过水面积=主槽过水面积+右岸滩区过水面积，主槽右岸假定为竖直
        elif tag==2:
            # 计算右岸滩区部分,左侧衔接主槽竖壁
            # 考虑滩区低于堤防的情况
            if len(ytr)>1:
                dcc = dr   
                d=dcc+yn #水深换算为水位
                # 插值生成该水深下两岸坐标
                xl0,xr0,segment = get_cross_pt(xtr,ytr,d)
                # 拼接成过水断面点集合
                xr_tmp = np.append(np.append([xl0],xtr[segment[0]:segment[-1]+1]),[xr0])
                yr_tmp = np.append(np.append([d],ytr[segment[0]:segment[-1]+1]),[d])  
                polygon_geom = Polygon(zip(xr_tmp, yr_tmp))
                polyline_geom = LineString(zip(xr_tmp, yr_tmp))
                area_tr = polygon_geom.area
                p_tr = polyline_geom.length        
                w_tr = polygon_geom.length-p_tr
                qtr= 1/ntr * area_tr * area_tr/p_tr **(2/3) * s ** 0.5 
                dp_tr = d
            if d>yc[-1]:
                # 计算主槽部分
                # 插值生成该水深下两岸坐标
                xl0,xr0,segment = get_cross_pt(xc,yc,d)
                xc_tmp = np.append(np.append(xl0,xc[segment[0]:]),xr0)
                yc_tmp = np.append(np.append([d],yc[segment[0]:]),[d])
                polygon_geom = Polygon(zip(xc_tmp, yc_tmp))
                polyline_geom = LineString(zip(xc_tmp, yc_tmp))
                area_c = polygon_geom.area
                p_c = polyline_geom.length
                w_c = polygon_geom.length-p_c
                qc= 1/nc * area_c * (area_c/p_c) **(2/3) * s ** 0.5

                # 计算左滩部分
                if d>yc[0]:
                    if len(ytl)>1:                
                        # 插值生成该水深下两岸坐标
                        xl0,xr0,segment = get_cross_pt(xtl,ytl,d)
                        # 拼接成过水断面点集合
                        xl_tmp = np.append(np.append([xl0],xtl[segment[0]:segment[-1]+1]),[xr0])
                        yl_tmp = np.append(np.append([d],ytl[segment[0]:segment[-1]+1]),[d])  
                        polygon_geom = Polygon(zip(xl_tmp, yl_tmp))
                        polyline_geom = LineString(zip(xl_tmp, yl_tmp))
                        area_tl = polygon_geom.area
                        p_tl = polyline_geom.length        
                        w_tl = polygon_geom.length-p_tl
                        qtl= 1/ntl * area_tl * (area_tl/p_tl) **(2/3) * s ** 0.5              
                        dp_tl = d
        #region
        # # 水深高于左右岸堤防：过水面积=主槽过水面积+两岸滩区过水面积
        # else:
        #     # 计算主槽部分
        #     xc_tmp = np.append(np.append(xc[0],xc),xc[-1])
        #     yc_tmp = np.append(np.append([d],yc),[d])
        #     polygon_geom = Polygon(zip(xc_tmp, yc_tmp))
        #     polyline_geom = LineString(zip(xc_tmp, yc_tmp))
        #     area_c = polygon_geom.area
        #     p_c = polyline_geom.length
        #     w_c = polygon_geom.length-p_c
        #     qc= 1/nc * area_c * (area_c/p_c) **(2/3) * s ** 0.5

        #     # 计算左岸滩区部分,右侧衔接主槽竖壁
        #     if len(ytl)>1:
        #         # 插值生成该水深下两岸坐标
        #         xl0,xr0,segment = get_cross_pt(xtl,ytl,d)
        #         # 拼接成过水断面点集合
        #         xl_tmp = np.append(np.append([xl0],xtl[segment[0]:segment[-1]+1]),[xr0])
        #         yl_tmp = np.append(np.append([d],ytl[segment[0]:segment[-1]+1]),[d])                 
        #         polygon_geom = Polygon(zip(xl_tmp, yl_tmp))
        #         polyline_geom = LineString(zip(xl_tmp, yl_tmp))
        #         area_tl = polygon_geom.area
        #         p_tl = polyline_geom.length        
        #         w_tl = polygon_geom.length-p_tl
        #         qtl= 1/ntl * area_tl * (area_tl/p_tl) **(2/3) * s ** 0.5 

        #     # 计算右岸滩区部分,左侧衔接主槽竖壁
        #     if len(ytr)>1:
        #         # 插值生成该水深下两岸坐标
        #         xl0,xr0,segment = get_cross_pt(xtr,ytr,d)
        #         # 拼接成过水断面点集合
        #         xr_tmp = np.append(np.append([xl0],xtr[segment[0]:segment[-1]+1]),[xr0])
        #         yr_tmp = np.append(np.append([d],ytr[segment[0]:segment[-1]+1]),[d])  
        #         polygon_geom = Polygon(zip(xr_tmp, yr_tmp))
        #         polyline_geom = LineString(zip(xr_tmp, yr_tmp))
        #         area_tr = polygon_geom.area
        #         p_tr = polyline_geom.length
        #         w_tr = polygon_geom.length-p_tr
        #         qtr= 1/ntr * area_tr * (area_tr/p_tr) **(2/3) * s ** 0.5
        #endregion
        if (qtl+qc+qtr).real >= q:
            area = (area_tl+area_c+area_tr)
            w = w_tl + w_c + w_tr
            velocity = q/area                
            break  
        
        dp_c = max(dp_c,d)
    # 计算湿周
    # - 用几何算
    if dp_c > yc[0]:
        xl_tmp = xl_tmp[:-1] if len(xl_tmp)>0 else xl_tmp
        yl_tmp = yl_tmp[:-1] if len(yl_tmp)>0 else yl_tmp
        xc_tmp = xc_tmp[1:]
        yc_tmp = yc_tmp[1:]
    if dp_c > yc[-1]:
        xr_tmp = xr_tmp[1:] if len(xr_tmp)>0 else xr_tmp
        yr_tmp = yr_tmp[1:] if len(yr_tmp)>0 else yr_tmp
        xc_tmp = xc_tmp[:-1]
        yc_tmp = yc_tmp[:-1]
    
    x_tmp = np.concatenate((xl_tmp,xc_tmp,xr_tmp))
    y_tmp = np.concatenate((yl_tmp,yc_tmp,yr_tmp))         
    polyline_geom = LineString(zip(x_tmp,y_tmp))
    p = polyline_geom.length    

    plt.style.use('_mpl-gallery')
    
    img=os.path.join(sdir,str(id)+'-res-0.png')
    plt.plot(x, y, 'k')
    plt.plot(xl_tmp,yl_tmp,'ro-',label='Left Wetted Perimeter')
    plt.plot(xr_tmp,yr_tmp,'ro-',label='Rgith Wetted Perimeter')
    plt.plot(xc_tmp,yc_tmp,'go-',label='Channel Wetted Perimeter')
    plt.savefig(img)

    img=os.path.join(sdir,str(id)+'-res-1.png')
    plt.plot(x, y, 'k')
    plt.plot(x_tmp,y_tmp,'ro-',label='Real Wetted Perimeter')
    plt.plot([x_tmp[0],x_tmp[-1]],[d,d])
    plt.savefig(img)

    return (dp_c-dc),velocity,q,area,w,p,qtl,area_tl,w_tl, \
    p_tl,qc,area_c,w_c,p_c,qtr,area_tr,w_tr,p_tr, \
        x_tmp,y_tmp, \
        xl_tmp,yl_tmp,dp_tl,(dp_tl-dl) if dp_tl>0 else 0, \
        xc_tmp,yc_tmp,dp_c,(dp_c-dc), \
        xr_tmp,yr_tmp,dp_tr,(dp_tr-dr) if dp_tr>0 else 0,
