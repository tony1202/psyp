import pandas as pd
import pyodbc
from wdmtoolbox import wdmtoolbox
import numpy as np
import csv
import os

class MetroDataIO(object):
    def __init__(self, h2_number):
        self.h2_number = h2_number
        self.long_term_avg = None
        self.alternative_gages = []
        self.raw_data = None
        self.filled_data = None

    def fill_precipitation_data(self):
        self.filled_data = self.raw_data.copy()
        for alternative_gage in self.alternative_gages:
            scaling_factor = self.long_term_avg/alternative_gage.long_term_avg
            data_for_filling = alternative_gage.raw_data.copy()
            data_for_filling.loc[:, 'rainfall_amount_inches'] *= scaling_factor
            self.filled_data['rainfall_amount_inches'].fillna(data_for_filling['rainfall_amount_inches'], inplace=True)
            pass

    def get_raw_precipitation_data(self, start_date, end_date, interval, daypart):
        DB = {'servername': 'BESDBPROD1',
              'database': 'NEPTUNE'}

        sql = "EXEC USP_MODEL_RAIN " +\
            " @start_date = " + "'" + start_date + "'" + "," +\
            " @end_date = " + "'" + end_date + "'" + "," +\
            " @interval = " + str(interval) + "," +\
            " @daypart = " + daypart + "," +\
            " @h2_number = " + str(self.h2_number) + "," + \
            " @adjust_for_dst = false" + "," + \
            " @limit_rows = -1"

        # create the connection
        conn = pyodbc.connect('DRIVER={SQL Server};SERVER=' + DB['servername'] + ';DATABASE=' + DB['database'] + ';Trusted_Connection=yes')
        with conn:
            self.raw_data = pd.read_sql(sql, conn, index_col="date_time")
            #self.raw_data = self.raw_data[self.raw_data['sensor_present'] == 'Y']
            #self.raw_data = self.raw_data[self.raw_data['downtime'] == 'N']
            #self.raw_data = self.raw_data.dropna(subset=['rainfall_amount_inches'])
            self.raw_data.drop(['sensor_present', 'downtime', 'h2_number', 'time_zone'], axis=1, inplace=True)
            pass

    def write_raw_data_to_wdm(self, wdm, tcode, tstep):
        tstype = 'PREC'
        base_year = 1948
        # tcode -> 2 minute 3 hour
        # tstep = 1
        statid = str(self.h2_number)
        scenario = "OBSERVED"
        location = str(self.h2_number)
        description = str(self.h2_number)
        constituent = 'PREC'
        tsfill = -999.0
        dsn = self.h2_number + 1000
        wdmtoolbox.createnewdsn(wdm, dsn, tstype, base_year, tcode, tstep, statid, scenario, location, description, constituent, tsfill)
        #self.raw_data = self.raw_data.tshift()
        wdmtoolbox._writetodsn(wdm, dsn, self.raw_data)

    def write_filled_data_to_wdm(self, wdm, tcode, tstep):
        tstype = 'PREC'
        base_year = 1948
        # tcode -> 2 minute 3 hour
        # tstep = 1
        statid = str(self.h2_number)
        scenario = "OBSERVED"
        location = str(self.h2_number)
        description = str(self.h2_number)
        constituent = 'PREC'
        tsfill = 0.0  #TODO need to provide feedback on fill
        dsn = self.h2_number
        wdmtoolbox.createnewdsn(wdm, dsn, tstype, base_year, tcode, tstep, statid, scenario, location, description, constituent, tsfill)
#        self.filled_data = self.filled_data.tshift()
        wdmtoolbox._writetodsn(wdm, dsn, self.filled_data)

    @staticmethod
    def write_design_storm_to_wdm(wdm, dsn, storm, tcode, tstep, design_storm_data):
        tstype = 'PREC'
        base_year = 1948
        # tcode -> 2 minute 3 hour
        # tstep = 1
        statid = storm
        location = "PDX"
        description = storm + "5 min Design Storm"
        scenario = "OBSERVED"
        constituent = 'PREC'
        tsfill = 0.0  #TODO need to provide feedback on fill
        wdmtoolbox.createnewdsn(wdm, dsn, tstype, base_year, tcode, tstep, statid, scenario, location, description, constituent, tsfill)
        design_storm_data = design_storm_data.shift()  # tshift deprecated
        wdmtoolbox._writetodsn(wdm, dsn,  design_storm_data)
    
    @staticmethod
    def write_data_to_wdm(wdm,dsn,statid, location, description,tstype, storm, tcode, tstep, design_storm_data):
        '''
        tstype: 必须不大于4个字符
        storm： 必须不大于8个字符
        否则抛异常：TypeError: unsupported operand type(s) for +: 'int' and 'str'
        '''
        # tstype = 'PREC'
        base_year = 1948
        # tcode -> 2 minute 3 hour
        # tstep = 1
        # statid = storm
        # description = storm + "5 min Design Storm"
        scenario = storm # "OBSERVED"
        constituent = tstype
        tsfill = 0.0  #TODO need to provide feedback on fill
        wdmtoolbox.createnewdsn(wdm, dsn, tstype, base_year, tcode, tstep, statid, scenario, location, description, constituent, tsfill)
        design_storm_data = design_storm_data.shift()  # tshift deprecated
        wdmtoolbox._writetodsn(wdm, dsn,  design_storm_data)

    def read_filled_data_from_wdm(self, wdm, start_date=None, end_date=None):
        self.filled_data = wdmtoolbox.extract(wdm, self.h2_number).fillna(0)
        if start_date is not None and end_date is not None:
            self.filled_data = self.filled_data.loc[start_date:end_date]
        pass

    def read_raw_data_from_wdm(self, wdm, start_date=None, end_date=None):
        self.raw_data = wdmtoolbox.extract(wdm, self.h2_number + 1000)
        if start_date is not None and end_date is not None:
            self.raw_data = self.raw_data.loc[start_date:end_date]
        pass

    def peak_precipitation(self, beg, end):
        beg = beg.replace(tzinfo=None)
        end = end.replace(tzinfo=None)
        return np.float32(self.filled_data.loc[beg:end].max()[0])

    def total_precipitation(self, beg, end):
        beg = beg.replace(tzinfo=None)
        end = end.replace(tzinfo=None)
        total_precipitation = np.float32(self.filled_data.loc[beg:end].sum()[0])
        return total_precipitation

def excel_2_wdm(dir,prjName,metrodata_spreadsheet,point_sources_discharging,point_sources_locaton):
    import os
    output_wdm = os.path.join(dir,prjName+'.wdm')
    wdmtoolbox.createnewwdm(output_wdm, True)

    # 写气象数据
    #region
    metrodata = pd.read_excel(metrodata_spreadsheet, index_col="Date", header=0)    
    dnsprefix=2000
    scenario='test-qx' # 8个字符    
    dns=dnsprefix

    for i in range(151):
        statid ='st' + str(i+1) # 气象站id，气象数据来自noaa再分析栅格数据，与子流域求交后的栅格气象数据作为子流域气象数据，因此取了流域名称
        location=str(i+1) # 所在流域
        description=str(i+1) + ', produced by noaa'
        dns +=1
        # MetroDataIO.write_metrodata_to_wdm(output_wdm, 2001, location,description,'PREC','SCS24hr2yr', 3, 1, metrodata['PREC'])
        # PrecipitationGage.write_design_storm_to_wdm(output_wdm, 2002, scenario, 2, 5, scs24hr_2_year_storm)
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'PREC',scenario, 3, 1, metrodata['PREC'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns,statid,  location,description,'EVAP',scenario, 3, 1, metrodata['EVAP'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'ATEM',scenario, 3, 1, metrodata['ATEM'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns,statid,  location,description,'WIND',scenario, 3, 1, metrodata['WIND'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'SOLR',scenario, 3, 1, metrodata['SOLR'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'PEVT',scenario, 3, 1, metrodata['PEVT'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'DEWP',scenario, 3, 1, metrodata['DEWP'])
        dns+=1
        MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,'CLOU',scenario, 3, 1, metrodata['CLOU'])
    #endregion

    # 写入点源排放数据
    #region
    if os.path.exists(point_sources_discharging):
        dischargingdata = pd.read_excel(point_sources_discharging, index_col="Date", header=0)
        ptsdata = {}

        with open(point_sources_locaton, 'r') as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                xh = row['xh']
                sname = row['SNAME']
                ptsdata[xh] = sname

        dnsprefix=4000
        scenario='PT-'
        dns=dnsprefix
        for key, value in ptsdata.items():
            dns+=1
            scenario=('PT-'+str(key))[-8:]
            statid = str(key)
            location = str(value)
            description='W'+str(key) # + ', wwtp discharging'
            MetroDataIO.write_data_to_wdm(output_wdm, dns,statid, location,description,'FLOW',scenario, 3, 1, dischargingdata['Q'])
            dns+=1
            MetroDataIO.write_data_to_wdm(output_wdm, dns,statid, location,description,'COD',scenario, 3, 1, dischargingdata['COD'])
            dns+=1
            MetroDataIO.write_data_to_wdm(output_wdm, dns,statid, location,description,'NH3N',scenario, 3, 1, dischargingdata['NH3-N'])
            dns+=1
            MetroDataIO.write_data_to_wdm(output_wdm, dns,statid, location,description,'TN',scenario, 3, 1, dischargingdata['TN'])
            dns+=1
            MetroDataIO.write_data_to_wdm(output_wdm, dns,statid, location,description,'TP',scenario, 3, 1, dischargingdata['TP'])
    #endregion
def csvdata_2_wdm(dir,prjName,csvdata):
    '''
    不识别中文路径
    csvdata:[
        {
            'id':'1',
            'location':'',
            'data':
                [{
                    'key': 'CLOU',                    
                    'data':[]
                }]
        }
    ]
    '''
    import os
    output_wdm = os.path.join(r'd:\\',prjName+'.wdm') # 不识别中文路径
    wdmtoolbox.createnewwdm(output_wdm, True)
    dnsprefix=1000
    scenario='fnl_qx' # 8个字符    
    dns=dnsprefix
    for station_data in csvdata:
        statid = station_data['id'] # 气象站id，气象数据来自noaa再分析栅格数据，与子流域求交后的栅格气象数据作为子流域气象数据，因此取了流域名称
        location= station_data['location'] # 所在流域
        description=statid + ', produced by noaa'
        for kv in station_data['data']:
            MetroDataIO.write_data_to_wdm(output_wdm, dns, statid, location,description,kv['key'],scenario, 3, 1, kv['data'])
            dns +=1

# 将面excel_2_wdm生成的wdm文件导入Basins生成uci文件
# 然后利用下面程序将点源时间排放数据写入uci文件，进行关联
def update_uci_pointSourceBlock(dir,prjName):
    # WDM1  7012 BOD,     ENGL              DIV  RCHRES   1     INFLOW IVOL
    # "WDM1  " + "{:4d}".format(rg) + " PREC     ENGL    " + "{:4.3f}".format(rg_multiplier) + "          PERLND   1 999 EXTNL  PREC\n" +\
    
    wdmfile = os.path.join(dir,prjName+'.wdm')
    ucifile = os.path.join(dir,prjName+'.uci')
    # 读取wdm文件，获取点源时间序列
    wdm = wdmtoolbox.listdsns(wdmfile)
    pt_lines=[]
    for key, value in wdm.items():
        if value['IDSCEN'][:3]=='PT-': # 点源时间时间序列
            dns=value['DSN']
            tstype=value['IDCONS']
            SFCLAS='     '
            RCHRE=value['IDLOCN']
            source = \
                "WDM2  " + "{:4d}".format(dns) + tstype[:4].rjust(5)+SFCLAS+'METR'+'              '+'SAME RCHRES'+RCHRE[:4].rjust(4)+'     INFLOW IVOL'
            # print(source)
            pt_lines.append(source)
    
    # 写入uci文件
    # Read lines from the file
    with open(ucifile, "r") as file:
        lines = file.readlines()
    new_lines = []
    for line in lines:
        if line.strip() == "END EXT SOURCES":
            for pt in pt_lines:
                new_lines.append(pt+'\n')
        new_lines.append(line)

    # Write the modified lines back to the file
    with open(ucifile, "w") as file:
        file.writelines(new_lines)

if __name__== "__main__":
    dir=r'C:\sypcloud_git\projects\114_yn_jsj\model_csh\hspf'
    dir=r'C:\sypcloud_git\projects\114_yn_jsj\model\basins'
    dir=r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\ncar'
    
    prjName='csh'
    prjName='jsj'
    prjName='dlx'
    
    # output_wdm = r'C:\sypcloud_git\projects\114_yn_jsj\model_csh\hspf\csh-yc.wdm'
    # output_wdm = r'C:\sypcloud_git\projects\114_yn_jsj\model\basins\jsj.wdm'
    # output_wdm = r'C:\sypcloud_git\projects\108_cweis\gis\model\大陆溪流域\hspf\dlx.wdm'
    
    metro_data_dir=r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230825'
    metro_data_dir=r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fnl-2015-2022'
    metro_data_dir=r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\ncar'
    metro_data_dir=r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\ncar'
    
    # metrodata_spreadsheet = r'D:\psyp\psyp\modelbase\hspf\testcase\1\metro\metrodata.xlsx'
    # point_sources_discharging = r'D:\psyp\psyp\modelbase\hspf\testcase\1\metro\wwtp_discharging.xlsx'
    # point_sources_locaton = r'D:\psyp\psyp\modelbase\hspf\testcase\1\metro\wwtp_location.csv'

    # excel_2_wdm(dir,prjName,metrodata_spreadsheet,point_sources_discharging,point_sources_locaton)
    # update_uci_pointSourceBlock(dir,prjName)

    #region    
    file = os.path.join(metro_data_dir,'ATEM.csv')
    df1 = pd.read_csv(file, index_col=0, header=0)
    df1.index = pd.to_datetime(df1.index)
    file = os.path.join(metro_data_dir,'CLOU.csv')
    df2 = pd.read_csv(file, index_col=0, header=0)
    df2.index = pd.to_datetime(df2.index)
    file = os.path.join(metro_data_dir,'DEWP.csv')
    df3 = pd.read_csv(file, index_col=0, header=0)
    df3.index = pd.to_datetime(df3.index)
    file = os.path.join(metro_data_dir,'PEVT.csv')
    df4 = pd.read_csv(file, index_col=0, header=0)
    df4.index = pd.to_datetime(df4.index)
    file = os.path.join(metro_data_dir,'PREC.csv')
    df5 = pd.read_csv(file, index_col=0, header=0)
    df5.index = pd.to_datetime(df5.index)
    file = os.path.join(metro_data_dir,'SOLR.csv')
    df6 = pd.read_csv(file, index_col=0, header=0)
    df6.index = pd.to_datetime(df6.index)
    file = os.path.join(metro_data_dir,'WIND.csv')
    df7 = pd.read_csv(file, index_col=0, header=0)
    df7.index = pd.to_datetime(df7.index)
    #endregion

    csvdata=[]
    for idx in range(df1.shape[1]): # 遍历站点
        i=idx
        data_tmp=[]
        station_data = {
            'key':'ATEM',            
            'data':df1[df1.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'CLOU',
            'data':df2[df2.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'DEWP',
            'data':df3[df3.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'PEVT',
            'data':df4[df4.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'PREC',
            'data':df5[df5.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'SOLR',
            'data':df6[df6.columns[i]]
        }
        data_tmp.append(station_data)
        station_data = {
            'key':'WIND',
            'data':df7[df7.columns[i]]
        }
        data_tmp.append(station_data)
        csvdata.append({
            'id':'st-'+str(i),
            'location':str(i),
            'data':data_tmp,
            })
    
    csvdata_2_wdm(dir,prjName,csvdata)


    
    