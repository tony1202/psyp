import numpy as np

def calculate_sum(code, data,f1,f2,f3):
    # 找到所有与给定编码关联的项
    related_items = [x for x in data if int(x[f2]) == code]
    
    if not related_items:
        return [],0  # 找不到项时返回0
    
    # 计算所有上游项的第三列和
    upstream_sum = 0
    upstream_code=[]
    for item in related_items:
        upstream_sum += item[f3]
        upstream_code.append(str(int(item[f1])))
        codes,sum_area = calculate_sum(item[f1], data,f1,f2,f3)
        upstream_code = list(np.append(upstream_code,codes))
        upstream_sum += sum_area

    return upstream_code,upstream_sum

def get_upper_subasins(topo_file,outfile):
    '''
    根据河流上下游关系递归找出所有上游河段
    river_topo.csv文件结构
    0：河段对应的子流域编码
    1：河段相连下游河段对应子流域编码
    2: 河段对应的子流域面积
    '''
    outfile = topo_file
    data = np.genfromtxt(topo_file, delimiter=',')
    od = []
    for d in data:
        area = [x for x in data if int(x[0]) == int(d[0])][0][2]
        upstream_code = [x for x in data if int(x[0]) == int(d[0])][0][0]
        codes, sum_area = calculate_sum(int(d[0]),data,0,1,2)
        area =area+ sum_area
        codes.append(str(int(upstream_code)))
        od.append([str(int(d[0])),str(area),'|'.join(codes)])
    with open(topo_file,'w') as f:
        for d in od:
            f.write(','.join(d)+'\n')
    f.close()

def calculate_sum2(code, data,f1,f2,f3):
    # 找到所有与给定编码关联的项
    related_items = [x for x in data if str(x[f2]) == code]
    
    if not related_items:
        return [],0  # 找不到项时返回0
    
    # 计算所有上游项的第三列和
    upstream_sum = 0.0
    upstream_code=[]
    # 遍历时去重，计算面积时不去重
    distinct_items = list({sublist[f1] for sublist in related_items})
    for item in distinct_items:
        upstream_sum += sum(float(x[f3]) for x in  related_items if str(x[f1])==item)
        upstream_code.append(item)
        codes,sum_area = calculate_sum2(item, data,f1,f2,f3)
        upstream_code = list(np.append(upstream_code,codes))
        upstream_sum += sum_area

    return upstream_code,upstream_sum

def get_upper_subasins2(topo_file,outfile):
    '''
    根据河流上下游关系递归找出所有上游河段(适用于计算单元切分的子流域)
    river_topo.csv文件结构
    0：计算河段编码
    1：计算河段相连下游计算河段编码
    2: 计算河段对应的子流域面积
    '''
    outfile = topo_file[:-4]+'-res.csv'
    data = np.genfromtxt(topo_file, delimiter=',',dtype='unicode')
    file = open(outfile,'w')
    od = []
    try:        
        for d in data:
            area =sum((float(y[2]) for y in [x for x in data if str(x[0]) == str(d[0])]))
            upstream_code = [x for x in data if str(x[0]) == str(d[0])][0][0]
            codes, sum_area = calculate_sum2(str(d[0]),data,0,1,2)
            area = area+ sum_area
            codes.append(str(upstream_code))
            od.append([str(d[0]),str(area),'|'.join(codes)])
        # with open(topo_file,'w') as f:
        for d in od:
            file.write(','.join(d)+'\n')
        file.close()
    except Exception as ex:
        for d in od:
            file.write(','.join(d)+'\n')
        file.close()

def ncar_fnl_metro_2_csv(prefix1, prefix2, sstime, setime,sdir,csvfile,locations,fieldname):
    import os
    import math
    import netCDF4
    from netCDF4 import Dataset
    from datetime import datetime, timedelta
    import csv
    import pandas as pd

    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    locations: [{'lat':23,'lon':102}]
    clou: T_CDC_L234_Avg_1
    solr: DSWRF_L1_Avg_1
    atmp: TMP_L103
    dewp: DPT_L103
    pevt: PEVPR_L1
    prec: A_PCP_L1_Accum_1
    wind: U_GRD_L103、V_GRD_L103
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    times=[]
    ts_loc_data=[]
    loc_index = [] # 查询位置的索引号    

    while stime <= etime:
        loc_data = [] # 当前步所有查询位置的标量值
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fnl = os.path.join(sdir, filename)
        if os.path.exists(fnl):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(fnl, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['lat'])
                print('--------------lon----------------------')
                print(data.variables['lon'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname])

                # 读取数据值
                lat = data.variables['lat'][:].data
                lon = data.variables['lon'][:].data
                time = data.variables['time'][:].data

                # 查询网格点索引号
                for loc in locations:
                    # Find index of original latitude and longitude arrays
                    lat_index = np.abs(lat - loc['lat']).argmin()
                    lon_index = np.abs(lon - loc['lon']).argmin()
                    loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            # 读取数据值        
            for idx in loc_index:
                if fieldname=='U_GRD_L103':
                    data_u = data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    data_v = data.variables['V_GRD_L103'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    uv = math.sqrt(data_u*data_u+data_v*data_v)
                    loc_data.append(uv)
                elif fieldname=='TMP_L103' or fieldname=='DPT_L103':
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                elif fieldname=='TMP_L1' or fieldname=='DPT_L1':
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                else:
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']])
            ts_loc_data.append(loc_data)
            times.append(stime + timedelta(hours=8))  # 换算成北京时间 
            i = i+1

        stime = stime + timedelta(hours=6)        
    
    result = []
    for i in range(len(times)):
        result.append([times[i]] + ts_loc_data[i])

    # Create a DataFrame from the list
    # df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3', 'value4', 'value5', 'value6', 'value7', 'value8', 'value9'])
    df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3'])

    # Convert the 'date' column to datetime type
    df['date'] = pd.to_datetime(df['date'])

    # Set 'date' column as the index
    df.set_index('date', inplace=True)

    # Resample to hourly data and interpolate missing values
    df_hourly = df.resample('H').interpolate()
    df_hourly.to_csv(csvfile)
    # with open(csvfile, 'w', newline='') as cf:
    #     writer = csv.writer(cf)
    #     writer.writerows(result)

def ncar_fnl_fns_metro_2_csv(prefix1, prefix2, sstime, sftime,sdir,sdir2,csvfile,locations,fieldname):
    import os
    import math
    import netCDF4
    from netCDF4 import Dataset
    from datetime import datetime, timedelta
    import csv
    import pandas as pd

    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    locations: [{'lat':23,'lon':102}]
    clou: T_CDC_L234_Avg_1
    solr: DSWRF_L1_Avg_1
    atmp: TMP_L103
    dewp: DPT_L103
    pevt: PEVPR_L1
    prec: A_PCP_L1_Accum_1
    wind: U_GRD_L103、V_GRD_L103
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt) 
    ftime = datetime.strptime(sftime, fmt) 
    i = 0
    times=[]
    ts_loc_data=[]
    loc_index = [] # 查询位置的索引号    
    # 提取历史数据，fnl数据
    while stime <= ftime:
        loc_data = [] # 当前步所有查询位置的标量值
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fnl = os.path.join(sdir, filename)
        if os.path.exists(fnl):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(fnl, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['lat'])
                print('--------------lon----------------------')
                print(data.variables['lon'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname])

                # 读取数据值
                lat = data.variables['lat'][:].data
                lon = data.variables['lon'][:].data
                time = data.variables['time'][:].data

                # 查询网格点索引号
                for loc in locations:
                    # Find index of original latitude and longitude arrays
                    lat_index = np.abs(lat - loc['lat']).argmin()
                    lon_index = np.abs(lon - loc['lon']).argmin()
                    loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            time = data.variables['time'][:].data
            # 读取数据值        
            for idx in loc_index:
                if fieldname=='U_GRD_L103':
                    data_u = data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    data_v = data.variables['V_GRD_L103'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    uv = math.sqrt(data_u*data_u+data_v*data_v)
                    loc_data.append(uv)
                elif fieldname=='TMP_L103' or fieldname=='DPT_L103':
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                else:
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']])
            ts_loc_data.append(loc_data)
            times.append(stime + timedelta(hours=8))  # 换算成北京时间 
            i = i+1

        stime = stime + timedelta(hours=6)        
    # 追加预报数据，fns数据
    filename = prefix1 + str(ftime.year) + \
            '{0:0>2d}'.format(ftime.month) + \
            '{0:0>2d}'.format(ftime.day) + \
            '00.'
    files = [file for file in os.listdir(sdir2) if file.startswith(filename)]
    i=0
    for file in files:    
        loc_data = [] # 当前步所有查询位置的标量值
        fns = os.path.join(sdir2, file)
        if os.path.exists(fns):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(fns, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['lat'])
                print('--------------lon----------------------')
                print(data.variables['lon'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname])

                # 读取数据值
                lat = data.variables['lat'][:].data
                lon = data.variables['lon'][:].data
                time = data.variables['time'][:].data
            time = data.variables['time'][:].data
                # # 查询网格点索引号
                # for loc in locations:
                #     # Find index of original latitude and longitude arrays
                #     lat_index = np.abs(lat - loc['lat']).argmin()
                #     lon_index = np.abs(lon - loc['lon']).argmin()
                #     loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            # 读取数据值        
            for idx in loc_index:
                if fieldname=='U_GRD_L103':
                    data_u = data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    data_v = data.variables['V_GRD_L103'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    uv = math.sqrt(data_u*data_u+data_v*data_v)
                    loc_data.append(uv)
                elif fieldname=='TMP_L103' or fieldname=='DPT_L103':
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                else:
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']])
            ts_loc_data.append(loc_data)
            times.append(ftime + timedelta(hours=8+int(time[0])))  # 换算成北京时间 
            i = i+1
    result = []
    for i in range(len(times)):
        result.append([times[i]] + ts_loc_data[i])

    # Create a DataFrame from the list
    df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3', 'value4', 'value5', 'value6', 'value7', 'value8', 'value9'])

    # Convert the 'date' column to datetime type
    df['date'] = pd.to_datetime(df['date'])

    # Set 'date' column as the index
    df.set_index('date', inplace=True)

    # Resample to hourly data and interpolate missing values
    df_hourly = df.resample('H').interpolate()
    df_hourly.to_csv(csvfile)

def merge_ncar_ncep_fns_2_csv(prefix1, prefix2, sstime, sftime,sdir,sdir2,csvfile,locations,fieldname,fieldname2): 
    import os
    import math
    import netCDF4
    from netCDF4 import Dataset
    from datetime import datetime, timedelta
    import csv
    import pandas as pd
    import re

    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    locations: [{'lat':23,'lon':102}]
    clou: T_CDC_L234_Avg_1
    solr: DSWRF_L1_Avg_1
    atmp: TMP_L103
    dewp: DPT_L103
    pevt: PEVPR_L1
    prec: A_PCP_L1_Accum_1
    wind: U_GRD_L103、V_GRD_L103
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt) 
    ftime = datetime.strptime(sftime, fmt) 
    i = 0
    times=[]
    ts_loc_data=[]
    loc_index = [] # 查询位置的索引号    
    # 提取历史数据，fnl数据
    while stime <= ftime:
        loc_data = [] # 当前步所有查询位置的标量值
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fnl = os.path.join(sdir, filename)
        if os.path.exists(fnl):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(fnl, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['lat'])
                print('--------------lon----------------------')
                print(data.variables['lon'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname])

                # 读取数据值
                lat = data.variables['lat'][:].data
                lon = data.variables['lon'][:].data
                time = data.variables['time'][:].data

                # 查询网格点索引号
                for loc in locations:
                    # Find index of original latitude and longitude arrays
                    lat_index = np.abs(lat - loc['lat']).argmin()
                    lon_index = np.abs(lon - loc['lon']).argmin()
                    loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            time = data.variables['time'][:].data
            # 读取数据值        
            for idx in loc_index:
                if fieldname=='U_GRD_L103':
                    data_u = data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    data_v = data.variables['V_GRD_L103'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    uv = math.sqrt(data_u*data_u+data_v*data_v)
                    loc_data.append(uv)
                elif fieldname=='TMP_L103' or fieldname=='DPT_L103':
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                else:
                    loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']])
            ts_loc_data.append(loc_data)
            times.append(stime + timedelta(hours=8))  # 换算成北京时间 
            i = i+1

        stime = stime + timedelta(hours=6)        
    # 追加预报数据，fns数据
    files = [file for file in os.listdir(sdir2) if file.endswith('.nc')]
    i=0
    for file in files:  
        timeidx =int( re.search(r'\.f(\d{3})\.nc', file).group(1)  )
        loc_data = [] # 当前步所有查询位置的标量值
        fns = os.path.join(sdir2, file)
        if os.path.exists(fns):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(fns, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['latitude'])
                print('--------------lon----------------------')
                print(data.variables['longitude'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname2])

                # 读取数据值
                lat = data.variables['latitude'][:].data
                lon = data.variables['longitude'][:].data
                time = data.variables['time'][:].data
            time = data.variables['time'][:].data
                # # 查询网格点索引号
                # for loc in locations:
                #     # Find index of original latitude and longitude arrays
                #     lat_index = np.abs(lat - loc['lat']).argmin()
                #     lon_index = np.abs(lon - loc['lon']).argmin()
                #     loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            # 读取数据值        
            for idx in loc_index:
                if fieldname2=='UGRD_10maboveground':
                    data_u = data.variables[fieldname2][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    data_v = data.variables['VGRD_10maboveground'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                    uv = math.sqrt(data_u*data_u+data_v*data_v)
                    loc_data.append(uv)
                elif fieldname2=='DPT_2maboveground' or fieldname2=='TMP_2maboveground':
                    loc_data.append(data.variables[fieldname2][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                else:
                    loc_data.append(data.variables[fieldname2][:].data[0][idx['lat_idx']][idx['lon_idx']])
            ts_loc_data.append(loc_data)
            times.append(ftime + timedelta(hours=timeidx))  # 换算成北京时间 
            i = i+1
    result = []
    for i in range(len(times)):
        result.append([times[i]] + ts_loc_data[i])

    # Create a DataFrame from the list
    # df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3', 'value4', 'value5', 'value6', 'value7', 'value8', 'value9'])
    df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3'])

    # Convert the 'date' column to datetime type
    df['date'] = pd.to_datetime(df['date'])

    # Set 'date' column as the index
    df.set_index('date', inplace=True)

    # Resample to hourly data and interpolate missing values
    df_hourly = df.resample('H').interpolate()
    df_hourly.to_csv(csvfile)

def era5_metro_2_csv(prefix1,sstime,setime,sdir,csvfile,locations,fieldname):
    import os
    import math
    import netCDF4
    from netCDF4 import Dataset
    from datetime import datetime, timedelta
    import csv
    import pandas as pd

    '''
    '''
    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)

    i = 0
    times=[]
    ts_loc_data=[]
    loc_index = [] # 查询位置的索引号  

    while stime <= etime:        
        ncfile = os.path.join(sdir,prefix1+str(stime.year)+str(stime.month).zfill(2)+'.nc')
        # i=0
        if os.path.exists(ncfile):
            # 默认为读文件，此处 'r' 可省略
            data = Dataset(ncfile, 'r')
            if i==0:
                # 查看nc文件信息
                print('------------文件信息--------------------')
                print(data)

                # 查看nc文件中的变量
                print('--------------文件变量-------------------')
                for var in data.variables.keys():
                    print(var)

                # 查看每个变量的信息
                print('--------------变量的信息----------------------')
                print('--------------lat----------------------')
                print(data.variables['latitude'])
                print('--------------lon----------------------')
                print(data.variables['longitude'])
                print('--------------time----------------------')
                print(data.variables['time'])
                print('--------------DSWRF_L1_Avg_1----------------------')
                print(data.variables[fieldname])

                # 读取数据值
                lat = data.variables['latitude'][:].data
                lon = data.variables['longitude'][:].data
                time = data.variables['time'][:].data

                # 查询网格点索引号
                for loc in locations:
                    # Find index of original latitude and longitude arrays
                    lat_index = np.abs(lat - loc['lat']).argmin()
                    lon_index = np.abs(lon - loc['lon']).argmin()
                    loc_index.append({'lat_idx':lat_index,'lon_idx':lon_index})
            # 读取数据值        
            # 遍历小时
            time_count = len(data.variables[fieldname][:,0,0])
            for tidx in range(time_count):
                loc_data = [] # 当前步所有查询位置的标量值
                for idx in loc_index:
                    if fieldname=='U_GRD_L103':
                        data_u = data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]
                        data_v = data.variables['V_GRD_L103'][:].data[0][idx['lat_idx']][idx['lon_idx']]
                        uv = math.sqrt(data_u*data_u+data_v*data_v)
                        loc_data.append(uv)
                    elif fieldname=='TMP_L103' or fieldname=='DPT_L103':
                        loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                    elif fieldname=='TMP_L1' or fieldname=='DPT_L1':
                        loc_data.append(data.variables[fieldname][:].data[0][idx['lat_idx']][idx['lon_idx']]-273.15)
                    elif fieldname=='tp':
                        if len(data.variables)>4:
                            loc_data.append(data.variables[fieldname][tidx,0,idx['lat_idx'],idx['lon_idx']]*100) # 换算为mm
                        else:
                            loc_data.append(data.variables[fieldname][tidx,idx['lat_idx'],idx['lon_idx']]*100) # 换算为mm
                    else:
                        loc_data.append(data.variables[fieldname][tidx,idx['lat_idx'],idx['lon_idx']])
                ts_loc_data.append(loc_data)
                stime = stime + timedelta(hours=1)
                times.append(stime)   
            i = i+1
    result = []
    for i in range(len(times)):
        result.append([times[i]+timedelta(hours=8)] + ts_loc_data[i]) # 转换为北京时间
    # Create a DataFrame from the list
    # df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3', 'value4', 'value5', 'value6', 'value7', 'value8', 'value9'])
    df = pd.DataFrame(result, columns=['date', 'value1', 'value2', 'value3'])

    # Convert the 'date' column to datetime type
    df['date'] = pd.to_datetime(df['date'])

    # Set 'date' column as the index
    df.set_index('date', inplace=True)

    # Resample to hourly data and interpolate missing values
    # df_hourly = df.resample('H').interpolate()
    df.to_csv(csvfile)
    # with open(csvfile, 'w', newline='') as cf:
    #     writer = csv.writer(cf)
    #     writer.writerows(result)

if __name__=='__main__':
# region
    import os
    import csv

    # 提取点的坐标
    stationfile = r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\metro_stations.csv'
    locations = []
    with open(stationfile, 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            key = int(row[0])
            lon = float(row[1])
            lat = float(row[2])
            locations.append( {'lat': lat, 'lon': lon})
    # 数据源 era5
    data_fields= {
    # 'DEWP': 'DPT_L103',
    # 'ATEM': 'TMP_L1', # TMP_L1 TMP_L103
    # 'PEVT': 'PEVPR_L1',
    'PREC': 'tp',
    # 'WIND': 'U_GRD_L103', # V_GRD_L103
    # 'CLOU':  'T_CDC_L244', # 'T_CDC_L234_Avg_1' - fnl数据的high cloud cover；T_CDC_L244 - fns数据的converntion layer
    # 'SOLR': 'DSWRF_L1_Avg_1',    
    }
    sdir = r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\era'
    for key, value in data_fields.items():        
        era5_dir=os.path.join(sdir,key)
        csvfile =os.path.join(sdir,key+'.csv')
        prefix1='era5-'+ key+'-'
        era5_metro_2_csv(prefix1,'2003-01-01 00:00:00', \
            '2023-12-31 23:00:00',era5_dir,csvfile,locations,value)
# endregion

# region
#    topo_file = r'C:\sypcloud_git\projects\108_cweis\gis\model\dlx\hspf\river_topo.csv'
#    outfile = r''
#    get_upper_subasins2(topo_file,outfile) 
# endregion

# region
#     import csv
#     import os

#     # 提取点的坐标
#     stationfile = r'C:\sypcloud_git\projects\114_yn_jsj\model\basins\metro_stations.csv'
#     stationfile = r'C:\sypcloud_git\projects\114_yn_jsj\model_csh\hspf\metro_stations.csv'
#     stationfile = r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\metro_stations.csv'
#     locations = []
#     with open(stationfile, 'r') as file:
#         reader = csv.reader(file)
#         for row in reader:
#             key = int(row[0])
#             lon = float(row[1])
#             lat = float(row[2])
#             locations.append( {'lat': lat, 'lon': lon})
    
#     # 数据源 ncar
#     data_fields= {
#     'DEWP': 'DPT_L103',
#     'ATEM': 'TMP_L1', # TMP_L1 TMP_L103
#     'PEVT': 'PEVPR_L1',
#     'PREC': 'A_PCP_L1_Accum_1',
#     'WIND': 'U_GRD_L103', # V_GRD_L103
#     'CLOU':  'T_CDC_L244', # 'T_CDC_L234_Avg_1' - fnl数据的high cloud cover；T_CDC_L244 - fns数据的converntion layer
#     'SOLR': 'DSWRF_L1_Avg_1',    
#     }
#     # 数据源 ncep grib2nc
#     data_fields2= {
#     'DEWP': 'DPT_2maboveground',
#     'ATEM': 'TMP_2maboveground',
#     'PEVT': 'PEVPR_surface',
#     'PREC': 'APCP_surface',
#     'WIND': 'UGRD_10maboveground', # VGRD_10maboveground
#     'CLOU':  'TCDC_convectivecloudlayer', # 'T_CDC_L234_Avg_1' - fnl数据的high cloud cover；T_CDC_L244 - fns数据的converntion layer
#     'SOLR': 'DSWRF_surface',    
#     }
#     dir = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fnl-2015-2022'
#     dir = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-202301-0722-0824'
#     dir = r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\ncar'
#     dir2 = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230723-20230825'
#     dir2 = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-20230825-384'
#     dir2 = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230825'
#     dir2 = r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\ncar'
#     for key, value in data_fields.items():        
#         fnl_dir=os.path.join(dir,key)
#         fns_dir=os.path.join(dir2,key)
#         csvfile =os.path.join(dir2,key+'.csv')
#         # csvfile =os.path.join(dir,key+'.csv')
        
#         # # gfs
#         # prefix1='gfs.0p25.'
#         # prefix2='.f003.grib2.nc'

#         # fnl
#         prefix1='gdas1.fnl0p25.'
#         prefix2='.f03.grib2.nc'

#         ncar_fnl_metro_2_csv(prefix1,prefix2,'2016-01-01 00:00:00', \
#             '2023-12-31 23:00:00',fnl_dir,csvfile,locations,value)
        
#         # ncar_fnl_fns_metro_2_csv(prefix1,prefix2,'2023-01-01 00:00:00', \
#         #     '2023-07-23 00:00:00',fnl_dir,fns_dir,csvfile,locations,value)
        
#         # merge_ncar_ncep_fns_2_csv(prefix1,prefix2,'2023-01-01 00:00:00', \
#         #     '2023-08-25 00:00:00',fnl_dir,fns_dir,csvfile,locations,value,data_fields2[key])
# endregion