from psyp.utility.draw_windrose import draw_wndrose
from psyp.utility.draw_windrose_map import draw_wndrose_map
import mikeio as mio
import numpy as np
import pandas as pd

def dfs02wndrose(dfs0file,imgfile):
    ds = mio.read(dfs0file).to_dataframe()
    ws = ds[ds.columns[0]]
    wd = ds[ds.columns[1]]
    draw_wndrose(ws,wd,imgfile)

def dfs02wndroseMap(dfs0file,imgfile, \
    minlon, maxlon, minlat, maxlat, \
    cham_lon,cham_lat ):
    ds = mio.read(dfs0file).to_dataframe()
    ws = ds[ds.columns[0]]
    wd = ds[ds.columns[1]]
    draw_wndrose_map(ws,wd,imgfile, \
    minlon, maxlon, minlat, maxlat, \
    cham_lon,cham_lat )
