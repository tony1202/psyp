# -*- coding: utf-8 -*-
# @Author   : Dfsu_flood_risk: YuXiang @QQ: 498181320
#             dfsu2png: 绯村剑心 @QQ：596140558
# @Modified : jip, upgrade mikeio to v1.1.0

import math
import os
import mikeio as mio
import pandas as pd
import geopandas as gpd
import shapefile
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, BoundaryNorm
import numpy as np

class Dfsu_flood_risk:
    def __init__(self,dfs_filename,shp_filename,depth_threshold=[0.15,0.25,0.5],time_threshold=[30,60]):
        self.dfs_file=dfs_filename
        self.shp_file=shp_filename
        self.depth_threshold=depth_threshold
        self.time_threshold=time_threshold
        self.geo = []
        self.record = []

    def parse_dfsu(self):
        ds = mio.read(self.dfs_file,items="Total water depth")
        self.time_step = ds.timestep
        self.data_base = ds[0].to_numpy()
        self.shapes = ds.geometry.to_shapely()
        # 遍历网格
        for index in ds.geometry.element_ids:
            depth_list = self.data_base[:,index]
            # 当前网格所有时间水深
            risk_type=self.get_risk_type(depth_list)
            self.geo.append(self.shapes.geoms[index])
            self.record.append(risk_type)

    def get_risk_type(self,depth_list):        
        risk_low = sum(self.depth_threshold[0] < depth <= self.depth_threshold[1] for depth in depth_list[1:])
        risk_mid = sum(self.depth_threshold[1] < depth <= self.depth_threshold[2] for depth in depth_list[1:])
        risk_high = sum(self.depth_threshold[2] < depth for depth in depth_list[1:])
        if risk_high > 0:
            risk_type="高风险"
        else:
            if risk_mid*self.time_step/60 > self.time_threshold[0]:
                risk_type = "高风险"
            elif 0 < risk_mid*self.time_step/60 <= self.time_threshold[0]:
                risk_type = "中风险"
            else:
                if risk_low * self.time_step / 60 > self.time_threshold[0]:
                    risk_type = "中风险"
                elif 0 < risk_low*self.time_step/60 <= self.time_threshold[0]:
                    risk_type = "低风险"
                else:
                    risk_type = "none"
        return risk_type

    def to_shp(self):
        shp = shapefile.Writer(self.shp_file)
        shp.field("Type", "C", 20)
        for i in range(len(self.record)):
            shp.shape(self.geo[i])
            shp.record(self.record[i])
        shp.close()

class Dfsu2png:
    def __init__(self,dfsu_file,shape_file,png_path,background=None,item_num=-1,start_step=-1,end_step=-1, contours=5,zuobiao=None):
        self.dfsu_file=dfsu_file
        self.background=background
        self.shape_file=shape_file
        self.png_path = png_path
        self.item_num=item_num
        self.start_step= start_step
        self.end_step = end_step
        self.contours = contours
        self.zuobiao = zuobiao
        self.dfsufilename = os.path.basename(dfsu_file)
        if os.path.exists(self.png_path) is False:
            os.mkdir(self.png_path)
                

    def dfsu2png(self):
        ds=mio.read(self.dfsu_file)#  读取dfsu文件的地理信息
        x=[]
        y=[]
        for i in ds.geometry.node_coordinates:#读取每个网格节点的XY坐标
            x.append(i[0])
            y.append(i[1])
        triangles=ds.geometry.element_table#读取DFSU每个网格的构成信息，比如1号网格由[1，50,89]三个节点构成的
        # print(triangles)
        tria=[]
        for i in triangles:
            a=[]
            for j in i:
                a.append(int(j))
            tria.append(a)#将网格的构成信息构成信息转成整数型，以用于画图
        
        sf = shapefile.Reader(self.shape_file)
        box=sf.bbox
        shapes = sf.shapes()
        for i in shapes:
            pts = i.points
            x1, y1 = zip(*pts)  # 把经纬度分别给到x,y
        # 遍历所有items
        for item_num in range(0,ds.n_items):
            if self.item_num>=0:
                if item_num != self.item_num:
                    continue
            item_name = str(ds.items[item_num]).split("<")[1].split(">")[0]
            # 遍历所有时间
            for step in range(0,ds.n_timesteps):
                if self.start_step>0:
                    if step < self.start_step or step > self.end_step:
                        continue
                zfaces=ds[item_num][step].to_numpy()
                z=[]
                for i in zfaces:
                    if i > 0:
                        z.append(i)
                    else:
                        continue
                z_arr=np.array(z)#读取非空网格的数据，并用于后面画图
                if len(z_arr)==0:
                    continue
                # tpc = ax3.tripcolor(x, y, tria, facecolors=zfaces, edgecolors='k',alpha=0.5,cmap="rainbow",vmin=vmin,vmax=vmax)
                step_interval = np.max(z_arr)/np.min(z_arr)
                contour_value = [np.min(z_arr)+i*step_interval for i in range(0,self.contours)]
                cmap = ListedColormap([[1,0,0], [0,1,0], [0,0,1],[1,1,3]])
                norm = BoundaryNorm(contour_value, cmap.N)
                
                fig3, ax3 = plt.subplots(figsize=(8,8))        
                ax3.set_aspect('equal')
                if self.background is not None:
                    img=plt.imread(self.background)
                    if self.zuobiao is None:
                        plt.imshow(img,extent=[np.array(x).min(),np.array(x).max(), np.array(y).min(),np.array(y).max()],alpha=1)
                    else:
                        plt.imshow(img, extent=[self.zuobiao[0],self.zuobiao[2],self.zuobiao[1],self.zuobiao[3]], alpha=1)
                tpc = ax3.tripcolor(x, y, tria, facecolors=zfaces,alpha=0.8,cmap="rainbow",vmin=z_arr.min(),vmax=z_arr.max())
                # tpc = ax3.tripcolor(x, y, tria, facecolors=zfaces,alpha=1,cmap=cmap,norm=norm)
                fig3.colorbar(tpc)
                ax3.set_title(item_name)
                ax3.set_xlabel('X (m)')
                ax3.set_ylabel('Y (m)')
                ax3.plot(x1, y1, '-', lw=1, color='r')
                imgfile = os.path.join(self.png_path,"{0}-{1}-{2}.png".format(self.dfsufilename.split(".")[0],item_name,str(step)))
                plt.savefig(imgfile,dpi=400)#保存图片
            # end time step iter
        # end items iter

class Dfsu2shp:
    def __init__(self,dfsu_file,shape_path,contour_interval,start_step=-1,end_step=-1,item_num=-1):
        self.dfsu_file=dfsu_file
        self.shape_path = shape_path
        self.start_step= start_step
        self.end_step = end_step
        self.contour_interval=contour_interval
        self.item_num = item_num
        self.dfsufilename = os.path.basename(dfsu_file)
        if os.path.exists(self.shape_path) is False:
            os.mkdir(self.shape_path)

    def dfsu_2shp(self):
        ds = mio.read(self.dfsu_file)
        shp = ds.geometry.to_shapely()
        poly_list = [p for p in shp.geoms]
        # get coordinates
        ec = ds.geometry.element_coordinates
        lon = ec[:,0]
        lat = ec[:,1]

        # Interpolate to cartesian grid
        from  scipy.interpolate import griddata
        numcols, numrows = 200, 200
        xi = np.linspace(lon.min(), lon.max(), numcols)
        yi = np.linspace(lat.min(), lat.max(), numrows)
        xi, yi = np.meshgrid(xi, yi)

        for item_num in range(0,ds.n_items):
            if self.item_num>=0:
                if item_num != self.item_num:
                    continue
            # 遍历所有时间
            for step in range(0,ds.n_timesteps):
                if self.start_step>0:
                    if step < self.start_step or step > self.end_step:
                        continue
            item_name = str(ds.items[item_num]).split("<")[1].split(">")[0]            
            dt = ds[item_num][step]
            m = dt.to_numpy()
            df = pd.DataFrame({item_name:m})
            gdf = gpd.GeoDataFrame(df,geometry=poly_list)
            shpfile = os.path.join(self.shape_path,"{0}_{1}_{2}_{3}.shp".format(self.dfsufilename.split(".")[0],item_name.replace(" ",""),item_num,str(step)))
            gdf.to_file(shpfile)

            # 生成等值线
            grid_z = griddata(points=ec[:,0:2],values=m,xi=(xi,yi),method='cubic')
            if self.contour_interval>np.average(m):
                self.contour_interval = abs((np.average(m) - np.min(m)))/3
            contour_levels=np.arange(np.min(m), np.max(m), self.contour_interval)
            cn = plt.contour(xi,yi,grid_z,levels=contour_levels)
            
            from shapely.geometry import LineString
            poly_list2 = []

            for i in range(len(cn.collections)):
                path = cn.collections[i].get_paths()
                if len(path) == 0:
                    contour_levels = np.delete(contour_levels,i)
                    continue
                p = path[0]
                v = p.vertices
                x = v[:,0]
                y = v[:,1]
                poly = LineString([(i[0], i[1]) for i in zip(x,y)])
                if(poly.is_empty):
                    print(f"{i} is empty")
                poly_list2.append(poly)
            # Clip to domain
            domain = shp.buffer(0)
            poly_list2 = [p.intersection(domain) for p in poly_list2]
            # Create GeoDataframe
            df2 = pd.DataFrame({item_name:contour_levels})
            gdf2 = gpd.GeoDataFrame(df2,geometry=poly_list2)
            shpfile = os.path.join(self.shape_path,"{0}_{1}_{2}_{3}_contours.shp".format(self.dfsufilename.split(".")[0],item_name.replace(" ",""),item_num,str(step)))
            gdf2.to_file(shpfile)

def dfsu_flood_risk_2shp(dfsfile,shpfile,depth_threshold=[0.15,0.25,0.5],time_threshold=[30,60]):
    obj=Dfsu_flood_risk(dfsfile,shpfile,depth_threshold,time_threshold)
    obj.parse_dfsu()
    obj.to_shp()
    return True

def dfsu_2png(dfsu_file,shape_file,png_path, background=None,item_num=-1,start_step=-1,end_step=-1, contours=5,zuobiao=None):
    obj = Dfsu2png(dfsu_file,shape_file,png_path,background, item_num,start_step,end_step, contours,zuobiao)
    obj.dfsu2png()
    return True

def dfsu_2shp(dfsu_file,shape_path,contour_interval,start_step=-1,end_step=-1,item_num=-1):
    obj = Dfsu2shp(dfsu_file,shape_path,contour_interval,start_step,end_step, item_num)
    obj.dfsu_2shp()
    return True

if __name__=="__main__":
    dfs_filename=r"hd.dfsu"
    shp_filename="风险等级.shp"
    depth_threshold=[0.15,0.25,0.5]
    time_threshold=[30,60]
    obj=Dfsu_flood_risk(dfs_filename,shp_filename,depth_threshold,time_threshold)
    obj.parse_dfsu()
    # print(obj.data_base.head())
    # print(obj.data_base.loc[0].to_list())
    obj.to_shp()
