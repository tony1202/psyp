import os

def pdf_2_csv():
    import csv
    import camelot

    pdf_folder = r'D:\psyp\psyp\dataio\webfetch\pwxkz\pdf'
    #pdf_name = '06煤炭开采和洗选业行业系数手册.pdf'
    for filename in os.listdir(pdf_folder):
        pdf_path = os.path.join(pdf_folder,filename)
        csv_path = os.path.join(pdf_folder,filename)

        # Read all tables from the PDF file
        tables = camelot.read_pdf(pdf_path, pages='all')

        # Write each table to the CSV file
        for i, table in enumerate(tables):
            table.to_csv(csv_path + f'_table_{i+1}.csv')

def pdf_2_img():
    from pdf2image import convert_from_path
    dir = r'C:\Users\jip\Downloads'
    pdffile = os.path.join(dir,'信用中国-内蒙古自治区生态环境科学研究院.pdf')
    # Store Pdf with convert_from_path function
    images = convert_from_path(pdffile)
    for i in range(len(images)):   
      # Save pages as images in the pdf
        images[i].save(os.path.join(dir, 'page'+ str(i) +'.jpg'), 'JPEG')

if __name__ == '__main__':
    pdf_2_img()





