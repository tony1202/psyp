from email import header
from psyp.utility.webrequest import webrequest, webrequest2
import json
import os
from datetime import date, datetime, timedelta
import time

def fectch_bjsq_data(outfile,stime,etime):
    csv = open(outfile,'w',encoding='utf-8-sig') 
    csv.write('水系,河流,站名,站码,时间,水位,流量,日均流量\n')   
    url = 'http://nsbd.swj.beijing.gov.cn/service/cityRiverList/list'
    headers = {
        'Accept': 'application/json, text/plain, */*',
        # 'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en,zh-CN;q=0.9,zh;q=0.8',
        'Cache-Control': 'no-cache',
        'Content-Length': 26,
        'Content-Type': 'application/json;charset=UTF-8',
        'Host': 'nsbd.swj.beijing.gov.cn',
        'Origin': 'http://nsbd.swj.beijing.gov.cn',
        'Pragma': 'no-cache',
        'Proxy-Connection': 'keep-alive',
        'Referer': 'http://nsbd.swj.beijing.gov.cn/cshhsq.html',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
        # 'Cookie': '_trs_uv=k47pcjnp_365_icxq; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%221716e8f98c31a9-09ad9756b7b3da-6313f69-1204049-1716e8f98c470f%22%7D; yfx_c_g_u_id_10002881=_ck21122021333015334667271388114; yfx_f_l_v_t_10002881=f_t_1640007210517__r_t_1640007210517__v_t_1640007210517__r_c_0; yfx_key_10002881=; yfx_mr_10002881=%3A%3Amarket_type_free_search%3A%3A%3A%3Abaidu%3A%3A%3A%3A%3A%3A%3A%3Awww.baidu.com%3A%3A%3A%3Apmf_from_free_search; __jsluid_h=911372bf0c668e40e3369ecbc3a08dd0; _va_ref=%5B%22%22%2C%22%22%2C1644486037%2C%22https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3D-cszgGS9g-O78nk2qZgznFQ4ekaE46sGk-OB0db4BTJwd-1QfaPRenpWBtl5pqD-fjEXbbv1Nr0n52T3G1nye2JeEm930xZirD6xjNsf9cFR9R523B9DSabg0jEAPi1V%26wd%3D%26eqid%3D83205bcd002f26b30000000261c08622%22%5D; _va_ses=*; _va_id=c873a0546da494b5.1638961992.3.1644486999.1644486037.'
    }
    fmt = '%Y-%m-%d'
    stime = datetime.strptime(stime,fmt)
    etime = datetime.strptime(etime,fmt)
    while stime<=etime:        
        currenttime = datetime.strftime(stime,fmt)
        params={
        "queryDate": currenttime
        }
        html = webrequest2(url=url, params=params,headers=headers)
        data = json.loads(html)
        stime = stime+timedelta(days=1)
        # 写入csv文件
        for basin in data['data']['river_data']:
            basin_name = basin['river_system']
            for river in basin['river_detail']:
                river_name = river['river']
                station_name = river['river_name']
                station_code = river['stcdt']
                water_level = river['Z']
                discharge = river['Q']
                daily_avg_q = river['AQ']
                csv.write(basin_name+','+river_name+','+station_name+\
                    ','+station_code+','+ currenttime +','+water_level+','+discharge+','+\
                        daily_avg_q +'\n')
    csv.close()
# ---------------------------------------------------------
if __name__ == '__main__':
    fectch_bjsq_data('2022-2-9','2022-2-10')
