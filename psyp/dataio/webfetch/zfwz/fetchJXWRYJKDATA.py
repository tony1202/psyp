from psyp.utility.webrequest import webrequest
import json
import os
from datetime import date, datetime, timedelta
import time

def capture_webinfo_jxwryjk(stime,etime,cookies):
    # 查询监控企业基本信息
    params = {
        'monitorType': 'outletType2',
        'data_Type': 'day',
        'stationState': '',
        'County_Code': '',
        'Attention_Level': '',
        'Enterprise_Name': '',
        'outletType': -1,
        'outType': -1,
        'page': 1,
        'rows': 1000
    }
    
    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Content-Length': 15,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Cookie': cookies,
        'Host': '111.75.227.207:19500',
        'Origin': 'http://111.75.227.207:19500',
        'Pragma': 'no-cache',
        'Referer': 'http://111.75.227.207:19500/wryjk/tMonitorRealDataView/listInterface.htm?menuId=5194&hasActionConfig=0',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest'
    }

    url= 'http://111.75.227.207:19500/wryjk/tMonitorRealDataView/queryRealData.json?'

    html = webrequest(url=url, params=params,headers=headers)
    data = json.loads(html)
    outfile = os.path.join('D:/python/psyp/psyp/dataio/webfetch', '九江市重点污染源监控日排放数据.csv')
    file = open(outfile, 'w')

    tableheaders=[]
    isheader = True
    try:
        # 遍历监控企业
        for index, item_ent in enumerate(data['rows']):
            enter_id = item_ent['enter_id']
            monitor_id = item_ent['monitor_id']
            
            # 查询标准
            params = {
                'monitorType': 'outletType2',
                'data_type': 'day',
                'enter_id': enter_id,
                'monitor_id': monitor_id,
                'alarmFlag': 0,
                'outletType': 1,
                'dayStartTime': datetime.strftime(datetime.now() + timedelta(days=-1),'%Y-%m-%d'),
                'dayEndTime': datetime.strftime( datetime.now(),'%Y-%m-%d'),
                'page': 1,
                'rows': 100000
            }
            url = 'http://111.75.227.207:19500/wryjk/tMonitorRealDataView/queryDetailHistory.json?'
            html = webrequest(url=url, params=params,headers=headers)
            hdata = json.loads(html)
            for index, item in enumerate(hdata['rows']):
                if('standard_value_001' in item):
                    standard_value_001 = item['standard_value_001'] # PH
                else: standard_value_001='null'
                if 'standard_value_011' in item:
                    standard_value_011 = item['standard_value_011'] # 化学需氧量
                else: standard_value_011='null'
                if 'standard_value_060' in item:
                    standard_value_060 = item['standard_value_060'] # 氨氮
                else: standard_value_060='null'
                if 'standard_value_065' in item:
                    standard_value_065 = item['standard_value_065'] # 总氮
                else: standard_value_065='null'
                if 'standard_value_101' in item:
                    standard_value_101 = item['standard_value_101'] # 总磷
                else: standard_value_101='null'
                if 'standard_value_B01' in item:
                    standard_value_B01 = item['standard_value_B01'] #
                else: standard_value_B01='null'
                if 'standard_value_029' in item:
                    standard_value_029 = item['standard_value_029'] # 总铜
                else: standard_value_029 = 'null'
                if 'standard_value_027' in item:
                    standard_value_027 = item['standard_value_027'] # 总镍
                else:standard_value_027 = 'null'
                if 'standard_value_028' in item:
                    standard_value_028 = item['standard_value_028'] # 总铅
                else: standard_value_028 = 'null'
                if 'standard_value_024' in item:
                    standard_value_024 = item['standard_value_024'] # 六价铬
                else: standard_value_024 = 'null'
                if 'standard_value_070' in item: # 氰化物
                    standard_value_070 = item['standard_value_070']
                else: standard_value_070='null'
                if 'standard_value_w01010' in item:
                    standard_value_w01010 = item['standard_value_w01010']
                else: standard_value_w01010 = 'null'

            # 污染物及标准字典
            wrw_standard = {
                'w01018': standard_value_011,
                'w21001': standard_value_065,
                'w21011': standard_value_101,
                'w00000': standard_value_B01,
                'w21003': standard_value_060,
                'w01001': standard_value_001,
                'w20116': standard_value_070,
                'w20122': standard_value_029,
                'w20117': standard_value_024,
                'w20121': standard_value_027,
                'w01010': standard_value_w01010
            }
            wrw_standard2 = {
                '011': standard_value_011,
                '065': standard_value_065,
                '101': standard_value_101,
                'B01': standard_value_B01,
                '060': standard_value_060,
                '001': standard_value_001,
                '070': standard_value_070,
                '029': standard_value_029,
                '024': standard_value_024,
                '027': standard_value_027,
                'w01010': standard_value_w01010
            }
            # 污染物监测值
            wrwjcItems = []
            for jcidx, wrwjcitem in enumerate(wrw_standard):
                wrwjcItems.append(wrwjcitem+'-'+'Cou')
                wrwjcItems.append(wrwjcitem+'-'+'Min')
                wrwjcItems.append(wrwjcitem+'-'+'Avg')
                wrwjcItems.append(wrwjcitem+'-'+'Max')
                wrwjcItems.append(wrwjcitem+'-'+'Flag')
            wrwjcItems2 = []
            for jcidx, wrwjcitem in enumerate(wrw_standard2):
                wrwjcItems.append(wrwjcitem+'-'+'Cou')
                wrwjcItems.append(wrwjcitem+'-'+'Min')
                wrwjcItems.append(wrwjcitem+'-'+'Avg')
                wrwjcItems.append(wrwjcitem+'-'+'Max')
                wrwjcItems.append(wrwjcitem+'-'+'Flag')
            # 查询当前企业历史数据-原始数据包
            params = {
                'monitorType': 'outletType2',
                'dataType': 2031,
                'enter_id': enter_id,
                'monitorId': monitor_id,
                'timeType': 'createTime',
                'createDateStart': stime,
                'createDateEnd': etime
            }
            url = 'http://111.75.227.207:19500/wryjk/monitorDataPacket/queryDetailDataPacket.json?'
            html = webrequest(url=url, params=params,headers=headers)
            hdata = json.loads(html)
            
            for indexd, ditem in enumerate(hdata):
                if(isheader):
                    tableheaders.append('企业名称')
                    tableheaders.append('行政区划')
                    tableheaders.append('监测位置')
                enter_name = item_ent['Enterprise_ShortName']
                file.write(item_ent['Enterprise_ShortName']+',')
                file.write(item_ent['Area_Name']+',')
                file.write(item_ent['Dis_Monitor_Name']+',')

                monitorTime = ditem['monitorTime']
                monitorTime = time.localtime(int(monitorTime)/1000)
                monitorTime = time.strftime("%Y-%m-%d %H:%M:%S", monitorTime)
                if(isheader):
                    tableheaders.append('监测时间')
                file.write(monitorTime+',')
                dataPacket = ditem['dataPacket']
                datalist = dataPacket.split(';')
                # 遍历监测项目（带统计tag）
                for jcxmidx, jcxm in enumerate(wrwjcItems):
                    # 遍历当前企业的监测项目
                    isfind=False                           
                    for indexdwc, dwcitem in enumerate(datalist):
                        if(dwcitem.startswith('w')):
                            wrwlist = dwcitem.split(',') 
                            # 遍历当前企业的当前项目的统计项目
                            for indexdw, dwitem in enumerate(wrwlist):
                                dwitemdic = dwitem.split('=')
                                wrwname = dwitemdic[0]
                                wrwdata = dwitemdic[1]                                
                                if jcxm == wrwname:
                                    file.write(wrwdata.strip()+',')
                                    isfind = True
                                    break
                            if isfind == False:
                                file.write('null'+',')
                                break
                            else: break
                    if(isheader and isfind):
                        tableheaders.append(jcxm)
                        tableheaders.append('排放标准')
                    file.write(wrw_standard[jcxm.split('-')[0]]+',')        
                isheader=False
                file.write('\n')
    except:
        print(enter_name)
    file.close()
    with open(outfile, "r+") as f:
        old = f.read()
        f.seek(0)
        for indexh, hd in enumerate(tableheaders):
            f.write(hd+',')
        f.write('\n')
        f.write(old)
        f.close()