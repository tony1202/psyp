from logging import exception
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
from datetime import datetime
import os
import re

def fetch_dcgl(sdir,start_dt,end_dt):
    start_dt = datetime.strptime(start_dt,'%Y-%m-%d') # 抓取开始时间
    end_dt = datetime.strptime(end_dt,'%Y-%m-%d') # 抓取结束时间
    dxal_contents = [] # 典型案例
    general_contents = [] # 一般案例
    fkbg_contents = [] # 反馈报告
    # 典型案例
    file1 = open(os.path.join(sdir, 'zyhbdc-dxal'+str(start_dt.year)+'.txt'), 'a+',encoding='utf-8')
    lastInfo1 = file1.read()
    file1.seek(0)
    # 一般案例
    file2 = open(os.path.join(sdir, 'zyhbdc-ybal'+str(start_dt.year)+'.txt'), 'a+',encoding='utf-8')
    lastInfo2 = file2.read()
    file2.seek(0)
    # 反馈报告
    file3 = open(os.path.join(sdir, 'zyhbdc-fkbg'+str(start_dt.year)+'.txt'), 'a+',encoding='utf-8')
    lastInfo3 = file3.read()
    file3.seek(0)
    
    isstop = False 
    isdcbg = False

    href=''
    
    try:
        # 督察管理页面
        url = 'http://mee.gov.cn/ywgz/zysthjbhdc/dcjl/'
        driver = webdriver.Chrome()
        driver.get(url=url)
        time.sleep(3)
        pager = driver.find_elements_by_css_selector('[class="badoo1_nr2"]')[0]
        next_page = pager.find_element_by_xpath('/html/body/div[4]/div/div/div/div[3]/div/div/a[8]')
        total_pages = driver.find_elements_by_css_selector('[class="badoo1_txt"]')
        # total_pages = [int(s) for s in total_pages[1].text.split() if s.isdigit()][0]
        total_pages =int( re.findall(r'\d+', total_pages[1].text)[0])
        for idx in range(total_pages):
            isdcbg = False
            tblist_li = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div/div[2]/div/ul/li')
            for li_el in tblist_li:
                sdt= li_el.find_elements_by_xpath('span')[0].get_attribute('innerHTML')
                dt = datetime.strptime(sdt,'%Y-%m-%d')
                if dt<start_dt:
                    isstop= True
                    break
                if dt>end_dt:
                    break
                a_el = li_el.find_elements_by_xpath('a')
                if(len(a_el)==0):
                    continue
                href = a_el[0].get_attribute("href")
                title = a_el[0].get_attribute('innerHTML')
                if '反馈督察' in title:
                    isdcbg=True
                a_el[0].click()
                time.sleep(3)
                handles = driver.window_handles
                driver.switch_to.window(handles[2]) # 根据情况确定是第几个窗口

                # 典型案例页面
                al_list = driver.find_elements_by_css_selector('[class="Custom_UnionStyle"]')
                tag=''
                if len(al_list)==0:
                    tag = '/html/body/div[4]/div/div/div[1]/div/div/div/div[2]/div/div/a'
                    al_list = driver.find_elements_by_css_selector('[class="TRS_Editor"]')               
                else:
                    tag = '/html/body/div[4]/div/div/div[1]/div/div/div/div[2]/div/div/div/a'
                a_el_list = driver.find_elements_by_xpath(tag)
                if len(a_el_list)>0 : # 典型案例页面
                    for aidx in range(len(a_el_list)):
                        a_el = driver.find_elements_by_xpath(tag)[aidx]
                        href = a_el.get_attribute("href")
                        title = a_el.get_attribute('innerHTML')
                        a_el.click()
                        time.sleep(3)
                        handles = driver.window_handles
                        driver.switch_to.window(handles[2]) # 根据情况确定是第几个窗口
                        text_divs = driver.find_elements_by_css_selector('[class="neiright_JPZ_GK_CP"]')
                        content = text_divs[0].text
                        content = sdt +'\n'+ title + '\n' + content
                        dxal_contents.append(content)
                        driver.back()
                        driver.switch_to.window(handles[2])
                    driver.close()
                    driver.switch_to.window(handles[1])
                elif isdcbg == True : # 督察报告页面
                    title_div = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div[1]/div/div/h2')
                    title = title_div[0].get_attribute('innerHTML')
                    zw_div = driver.find_element_by_xpath('/html/body/div[4]/div/div/div[1]/div/div/div/div[2]')
                    content = sdt + '\n' + title + '\n' + zw_div.text
                    fkbg_contents.append(content)
                    driver.close()
                    driver.switch_to.window(handles[1])
                    isdcbg = False
                else: # 一般案例页面
                    title_div = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div[1]/div/div/h2')
                    if len(title_div)==0:
                        driver.close()
                        driver.switch_to.window(handles[1])
                        continue
                    title = title_div[0].get_attribute('innerHTML')
                    if len(al_list)==0:
                        zw_div = driver.find_element(By.XPATH,'/html/body/div[4]/div/div/div[1]/div/div/div/div[2]')
                        content = sdt + '\n' + title + '\n' + zw_div.text
                    else:
                        content = sdt + '\n' + title + '\n' + al_list[0].text
                    general_contents.append(content)
                    driver.close()
                    driver.switch_to.window(handles[1])

            if isstop == True:
                print(datetime.strftime(dt,'%Y-%m-%d') +'\n i am break!')
                break
            # 下一页
            next_page = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[3]/div/div/a[8]')
            next_page.click()
            time.sleep(3)

        # append to file
        lines1 ='\n-----------------\n'.join(s for s in dxal_contents)
        lines2 ='\n-----------------\n'.join(s for s in general_contents)
        lines3 ='\n-----------------\n'.join(s for s in fkbg_contents)
        

        file1.write(lines1+'\n'+lastInfo1)
        file2.write(lines2+'\n'+lastInfo2)
        file3.write(lines3+'\n'+lastInfo3)
        file1.close()
        file2.close()
        file3.close()
        print('抓取成果')

    except Exception as ex:
        print(ex)
        print(href)
        # append to file
        lines1 ='\n-----------------\n'.join(s for s in dxal_contents)
        lines2 ='\n-----------------\n'.join(s for s in general_contents)
        lines3 ='\n-----------------\n'.join(s for s in fkbg_contents)
        file1.write(lines1+'\n'+lastInfo1)
        file2.write(lines2+'\n'+lastInfo2)
        file3.write(lines3+'\n'+lastInfo3)
        file1.close()
        file2.close()
        file3.close()

if __name__ == '__main__':
    fetch_dcgl(r'tests\testdata\nlp\emd','2016-1-1','2016-12-31')