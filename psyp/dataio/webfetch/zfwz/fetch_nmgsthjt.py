import json
import os

file_path = r'C:\sypcloud_git\projects\108_cweis\gis\质量监测\乌海市\内蒙空气质量监测点.json'

with open(file_path, "r", encoding='utf-8') as json_file:
     # Load JSON data
    res=[]
    json_data = json.load(json_file)
    citys = json_data['HttpContent']
    for city in citys:
        stations = city['children']
        for station in stations:
            res.append({
                'city':station['cityName'],
                'name': station['label'],
                'code':station['code'],
                'lat':station['lat'],
                'lon':station['lon'],
                'type':station['stype'][0]
            })

with open(file_path+'.csv', "w", newline="") as csv_file:
    csv_file.write('城市,站名,站码,经度,纬度,控制级别\n')
    for item in res:
        csv_file.writelines(item['city']+','+item['name']+','+item['code']+','+str(item['lon'])+','+str(item['lat'])+','+item['type'][0]+'\n')
