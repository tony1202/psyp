from psyp.utility.webrequest import webrequest
import json
import os
from datetime import date, datetime, timedelta
import time
import lxml.etree

def capture_webinfo_hljrmzf(cookies,keyword='生态文明',distict='haerbin'):
    params = {
       'pageNum' : 1,
       'keyword': keyword,
       'searchField': '',
       'dataSourceId': 65536,
       'district': distict,
       'dateRange': 0,
       'sortMode':1,
       'siteName': 'hlj'
    }
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en,zh-CN;q=0.9,zh;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Cookie': cookies,
        'Host': 'search.hlj.gov.cn',
        'Pragma': 'no-cache',
        'Referer': 'https://search.hlj.gov.cn/hlj/cn/getAllResult.jsp',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': "Windows",
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': 1,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36'
    }
    url='https://search.hlj.gov.cn/hlj/searchall.do?' 
    
    html = webrequest(url=url, params=params,headers=headers)
    data = lxml.etree.HTML(str(html).strip())
    data = data.xpath('//body/div')
    itemlist = data[4].xpath('./span')
    print(data)