from email import header
from psyp.utility.webrequest import webrequest, webrequest2
import json
import os
# import lxml.etree
from bs4 import BeautifulSoup

def getProductsInfo():

    url = 'https://www.plastics-database.com/standard/main/index'
    
    # driver = webdriver.Chrome()
    # driver.get(url=url)
    # search = driver.find_elements(By.CLASS_NAME, "fa fa-search fa-stack-1x fa-inverse")
    # data = BeautifulSoup(driver.page_source, 'lxml')
    
    # html = requests.get(url).text
    # data = BeautifulSoup(html, 'lxml')
    
    html = webrequest(url=url, params='',headers='')
    # data = lxml.etree.HTML(str(html).strip())
    # data = data.xpath('//*[@id="gradelistbody"]')
    data = BeautifulSoup(str(html).strip(), 'lxml')
    table = data.find("table",id="gradelistbody")
    rows = data.find_all('tr')
    
    for idx,item in enumerate(data):
        name=''

