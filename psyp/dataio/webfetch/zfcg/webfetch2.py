from psyp.utility.webrequest import webrequest
import urllib.request
import lxml.etree
import time
import os
from datetime import datetime, timedelta
# from docx import Document
# from docx.shared import Inches

def checkBytitle(title):
    matches = ['流域', '水环境', '智慧环保', '排污口','环境影响评价','环保管家','环境影响评估']
    # matches = ['环境影响评价','环保管家']
    exmatches = ['监理']
    if any(x in title for x in matches):
        find = True
        if any(x in title for x in exmatches):
            find = False
        return find
    else:
        return False

# def writehtml(list):
#     file = open('results2.html','r+')
#     file.tell() #0L
#     for line in list:
#         file.write(line)
#         file.write('\n')
#     file.close()


def capture_webinfo_zyczxm(setime, sdir):
    """
    抓取财政项目招投标信息
    :setime :截止日期，从当前时间抓到过去的某个时间
    :sdir : 抓取信息保存路径
    """
    # post params
    params = {
        'searchtype': 1,
        'page_index': 1,
        'bidSort': 0,
        'buyerName':'',
        'projectId':'',        
        'pinMu': 0,
        'bidType': 0,
        'dbselect': 'bidx',
        'kw': '',
        'start_time': '',
        'end_time': '',
        'timeType': 6,  # 5 近半年 6 指定时间
        'displayZone':'',
        'zoneId':'',
        'pppStatus': 0,
        'agentName': ''
    }
    # cookies= 'JSESSIONID=xSFrL8cbuOdDNIcaa_zyY7uaS8vUp0YWAeOKtkAiYoFcyDEpS9U8!-1094063090; Hm_lvt_9f8bda7a6bb3d1d7a9c7196bfed609b5=1642146060,1642415419,1642475801; Hm_lpvt_9f8bda7a6bb3d1d7a9c7196bfed609b5=1642475801; Hm_lvt_9459d8c503dd3c37b526898ff5aacadd=1642145895,1642475804; Hm_lpvt_9459d8c503dd3c37b526898ff5aacadd=1642475850'
    # headers = {
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    #     'Accept-Encoding': 'gzip, deflate, br',
    #     'Accept-Language': 'en,zh-CN;q=0.9,zh;q=0.8',
    #     'Cache-Control': 'no-cache',
    #     'Connection': 'keep-alive',
    #     'Cookie': cookies,
    #     'Host': 'search.ccgp.gov.cn',
    #     'Pragma': 'no-cache',  
    #     'Upgrade-Insecure-Requests':1,
    #     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
    # }

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'
    currentDate = datetime.strptime(
        datetime.strftime(datetime.now(), fmt), fmt)
    # endDate = currentDate+ timedelta(days=-1)
    currentDate = datetime.strptime('2021-12-31 23:00:00', fmt)
    endDate = datetime.strptime(setime, fmt)
    params['start_time']=datetime.strftime(endDate,fmt2)
    params['end_time']=datetime.strftime(currentDate,fmt2)
    #results = ['<div>'+currentTime + '</div>']
    url = 'http://search.ccgp.gov.cn/bxsearch?'
    pages = 1051000
    num = 0

    # results.append('<ol>')

    file = open(os.path.join(sdir, 'results2.html'), 'r+')
    lastInfo = file.read()
    file.seek(0)
    file.write('<div>'+datetime.strftime(currentDate, fmt) + '</div>')
    file.write('\n')
    file.write('<ol>')

    while (num < pages):
        print(num)
        params['page_index'] = num
        user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
        headers = { 'User-Agent' : user_agent }
        html = webrequest(url=url, params=params,headers=headers)
        if html == -1:
            continue

        data = lxml.etree.HTML(html)
        data = data.xpath('//body/div')

        if len(data) < 4:
            time.sleep(1)
            continue

        itemlist = data[4].xpath('./div//div//div//div//ul//li')

        for item in itemlist:
            dt = item.xpath('./span')[0].text.split('\r\n')
            dt = datetime.strptime(dt[0], '%Y.%m.%d %H:%M:%S')
            # dt = datetime.strptime(datetime.strftime(dt,fmt),fmt)
            if dt < endDate:
                num = pages
                break
            a = item.xpath('./a')
            href = a[0].attrib['href']
            title = a[0].text.strip()
            find = checkBytitle(title)
            if (find):
                rowstr = '<li><a href=' + href + ' title=' + title + '>' + title+'</a></li>'
                # results.append(rowstr)
                file.write(rowstr)
                file.write('\n')
        num = num+1
    file.write('</ol>\n'+lastInfo)
    file.close()
    # results.append('</ol>')
    # writehtml(results)

# ---------------------------------------------------------
if __name__ == '__main__':
    capture_webinfo_zyczxm('2021-10-25 12:32:15','D:/python/psyp/psyp/dataio/webfetch')
    print('successfully finished!')