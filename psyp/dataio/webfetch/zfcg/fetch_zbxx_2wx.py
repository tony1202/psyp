from selenium import webdriver
import time
from datetime import datetime
import os

def fetch_zbxx_gj(sdir,start_time,end_time):
    '''
    start_time: '2022:1:1'
    end_time: '2022:1:2'
    '''
    matches = ['流域', '水环境', '智慧环保', '排污口','环境影响评价','环保管家','环境影响评估','监管能力','排污许可']
    exmatches = ['监理']
    
    idx = 1
    sleeptime=4
    zhaiyao_ur=['']
    href=''
    title=''
    lastInfo=''
    file = open(os.path.join(sdir, 'results.md'), 'w')
    
    try:
        fmt = '%Y-%m-%d %H:%M:%S'
        currentDate = datetime.strptime(
        datetime.strftime(datetime.now(), fmt), fmt)
        file.write('## '+ start_time +' 政府采购信息')
        file.write('\n')    
        
        url='http://search.ccgp.gov.cn/bxsearch?searchtype=1&page_index='+str(idx)+'&bidSort=&buyerName=&projectId=&pinMu=&bidType=&dbselect=bidx&kw=&start_time='+ start_time +'&end_time='+ end_time +'&timeType=6&displayZone=&zoneId=&pppStatus=0&agentName='
        print(url)
        # chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument('--ignore-certificate-errors')
        # chrome_options.add_argument('--ignore-ssl-errors')
        # driver = webdriver.Chrome(options=chrome_options)
        driver = webdriver.Chrome()
        driver.get(url=url)
        # 延迟5秒等待搜索结果加载完毕
        time.sleep(sleeptime)
        # 读取总记录数
        total_ps = driver.find_elements_by_css_selector('[class="pager"]')
        total_as = total_ps[0].find_elements_by_xpath('a')
        total_pages=int(total_as[len(total_as)-2].get_attribute('innerHTML'))
        next_page = total_as[len(total_as)-1]
        print(total_pages)
        
        # 循环总页数-1次 因为默认加载了第一页
        for idx in range(idx,total_pages-1):
            print(str(idx))
            zhaiyao_ur = driver.find_elements_by_css_selector('[class="vT-srch-result-list-bid"]')
            if len(zhaiyao_ur)==0:
                continue
            li_list = zhaiyao_ur[0].find_elements_by_xpath('li')
            idxx=0
            for li_el in li_list:
                a_el = li_el.find_elements_by_xpath('a')
                if(len(a_el)==0):
                    continue
                href = a_el[0].get_attribute("href")
                print(href)
                a_el[0].click() 
                time.sleep(sleeptime)
                handles = driver.window_handles
                driver.switch_to.window(handles[1]) # 根据情况确定是第几个窗口
                print('切换到新窗口*************')
                # 读取标题
                title_div = driver.find_elements_by_css_selector('[class="vF_detail_header"]')
                if(len(title_div)==0):
                    driver.close()
                    driver.switch_to.window(handles[0])
                    li_list = zhaiyao_ur[0].find_elements_by_xpath('li')
                    continue
                title = title_div[0].find_elements_by_xpath('h2')[0].get_attribute('innerHTML')
                print(title)
                # 读取内容
                # content_div = driver.find_elements_by_css_selector('[class="vF_detail_content"]')
                # contents = content_div[0].get_attribute('innerHTML')
                # 模糊搜索
                find = False
                if any(x in title for x in matches):
                    find = True
                    if any(x in title for x in exmatches):
                        find = False                        
                if(find):
                    rowstr = '- ['+title+'](' + href + ')'
                    # results.append(rowstr)
                    file.write(rowstr)
                    file.write('\n')
                
                print('关闭新窗口并切换至主窗口*************')
                driver.close()
                driver.switch_to.window(handles[0])
                li_list = zhaiyao_ur[0].find_elements_by_xpath('li')
                print('next item!')
            #读完上一页 翻页
            print('next page')
            href=''
            title=''
            # forward_url='http://search.ccgp.gov.cn/bxsearch?searchtype=1&page_index='+str(idx+2)+'&bidSort=&buyerName=&projectId=&pinMu=&bidType=&dbselect=bidx&kw=&start_time='+ start_time +'&end_time='+ end_time +'&timeType=6&displayZone=&zoneId=&pppStatus=0&agentName='
            # driver.get(url=forward_url)
            total_ps = driver.find_elements_by_css_selector('[class="pager"]')
            total_as = total_ps[0].find_elements_by_xpath('a')
            next_page = total_as[len(total_as)-1]
            next_page.click()
            time.sleep(sleeptime)
            #print('i am ok')
            #handles = driver.window_handles
            #driver.switch_to.window(handles[0])
            #next_page = driver.find_elements_by_xpath('/html/body/div[5]/div[2]/div/div/div[1]/p/a[8]')[0]
            print('next page ok')
        file.close()
        print('full completed!')
    except Exception as ex:
        file.write(ex.args[0]+'\n')
        file.write('idx: '+str(idx)+'\n')
        file.write('zhaiyao_ur: '+zhaiyao_ur[0]+'\n')
        file.write('href: '+href+'\n')
        file.write('title: '+title+'\n')
        file.write('-------------------------------'+'\n')
        file.write(lastInfo)
        file.close()
        print(ex) 

if __name__ == '__main__':
    fetch_zbxx_gj('D:\psyp\psyp\dataio\webfetch\zfcg','2022:8:10', '2022:8:10')      