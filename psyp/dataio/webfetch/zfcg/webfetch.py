import urllib.request
import lxml.etree
import time

def webrequest( url ):
    with urllib.request.urlopen(url) as respoonse:
        html = respoonse.read()
    return html

def checkBytitle( title ):
   matches = ['流域','水环境','智慧环保']
   if any(x in title for x in matches):
       return True
   else:
       return False

def writehtml(list):
    file = open('results.html','r+')
    file.tell() #0L
    for line in list:
        file.write(line)
        file.write('\n')
    file.close()

currentTime = time.strftime('%Y-%m-%d')
results = ['<div>'+currentTime + '</div>']
url = 'http://www.ccgp.gov.cn/cggg/dfgg/'
pages = 24
num=0

results.append('<ol>')

while (num < pages):
    if num == 0:
        tag =url+ 'index.htm'
    else:
        tag =url+ 'index_'+ str(num) +'.htm'
    num=num+1
    
    html = webrequest(tag)
    
    data = lxml.etree.HTML(html)
    itemlist = data.xpath('//body//div//div//div//div//div//div//ul//li')
    
    for item in itemlist:
       a = item.xpath('./a') 
       href = a[0].attrib['href']
       title = a[0].attrib['title']
       find = checkBytitle(title)
       if (find):
           rowstr = '<li><a href=' + url + href +' title='+ title +'>'+ title+'</a></li>'
           results.append(rowstr)

results.append('</ol>')
writehtml(results)
 