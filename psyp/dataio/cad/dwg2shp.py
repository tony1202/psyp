def dwg2shp():
    import geopandas as gpd
    shape= gpd.read_file("cad.dwg")
    shape.head()
    shape['Layer'].value_counts()
    arr=shape['Layer'].unique()
    arr

    for i in arr:
        export=shape[(shape.Layer == i)]
        export.to_file(driver = 'ESRI Shapefile', filename= i+".shp")

def create_dxf(dxf_file,xlist,ylist,zlist):
    import ezdxf
    # Create a new DXF document
    doc = ezdxf.new("R2010")
    
    msp = doc.modelspace()
    style = doc.styles.get('Standard')

    for i, x in enumerate(xlist):
        # Create a new text object
        text = msp.add_text(zlist[i])

        # Set the text style
        text.dxf.style = style.dxf.handle

        # Set the text position
        text.dxf.insert = (x, ylist[i], zlist[i])

    # Save the DXF document as a file
    doc.saveas(dxf_file)

def test():
    import os
    folder_path = r'E:\sypcloud_git\DataBase\mq\地形'
    x_coords = []
    y_coords = []
    z_coords = []

    dxf = os.path.join(folder_path,"地形.dxf")

    for filename in os.listdir(folder_path):
    # Check if the file is a text file with .txt extension
        if filename.endswith(".xyz"):
            # Open the file for reading
            with open(os.path.join(folder_path, filename), "r") as f:               

                # Loop through each line in the file
                for line in f:
                    # Split the line into x, y, and z values
                    x, y, z = line.split()

                    # Convert the values to floats and append to the respective lists
                    x_coords.append(float(x))
                    y_coords.append(float(y))
                    z_coords.append(float(z))
    create_dxf(dxf,x_coords,y_coords,z_coords)

def parralle_pts_text_dxf(folder_path,dxf):
    import os
    import concurrent.futures
    import ezdxf
    from ezdxf.enums import TextEntityAlignment

    # Set the folder path
    # folder_path = "path/to/folder"

    # Initialize the AutoCAD document
    doc = ezdxf.new("R12", setup=True)
    msp = doc.modelspace()

    # Create a new text style
    style = doc.styles.new("MyStyle")
    style.dxf.font = "Arial"
    style.dxf.height = 10

    # Define a function to process each file
    def process_file(filename):
        # Open the file for reading
        with open(os.path.join(folder_path, filename), "r") as f:
            # Initialize empty lists for x, y, and z coordinates
            x_coords = []
            y_coords = []
            z_coords = []

            # Loop through each line in the file
            for line in f:
                # Split the line into x, y, and z values
                x, y, z = line.split()

                # Convert the values to floats and append to the respective lists
                x_coords.append(float(x))
                y_coords.append(float(y))
                z_coords.append(float(z))

            # # Create a new text object for each file
            # msp = doc.modelspace()
            for i, (x, y, z) in enumerate(zip(x_coords, y_coords, z_coords)):
                # Create a new text object with the x, y, and z values as the text content
                # Use method set_placement() to define the TEXT alignment, because the
                # relations between the DXF attributes 'halign', 'valign', 'insert' and
                # 'align_point' are tricky.
                text = msp.add_text(str(z)).set_placement(
                    (x, y),
                    align=TextEntityAlignment.MIDDLE_RIGHT
                )
                # Set the text style to the new style we created
                text.dxf.style = style.dxf.handle

                # # Set the text position based on the index of the x, y, and z values
                # text.dxf.insert = (x,y,z)

    # Use a thread pool to process each file in parallel
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Loop through each file in the folder
        for filename in os.listdir(folder_path):
            # Check if the file is a text file with .txt extension
            if filename.endswith(".xyz"):
                # Submit the file to the thread pool for processing
                executor.submit(process_file, filename)

    # Save the DXF document as a file
    doc.saveas(dxf)

if __name__ == "__main__":
    import os
    folder_path = r'E:\sypcloud_git\DataBase\mq\地形'
    parralle_pts_text_dxf(folder_path,os.path.join(folder_path,"地形2.dxf"))