import os


# 为CWEIS准备数据
def process_data_4_cweis(root,folder,folder_py,code):
    '''
    root:   数据包根目录，已区域或流域名称命名，如九江市、大陆溪流域
    folder: 区域或流域名称命名，如九江市、大陆溪流域
    code:   区域或流域代码，这层结构主要是为应对一个区域有多个流域的情况
    '''

    dir=os.path.join(root, '污染源\\'+folder+'\\'+code)
    dir2 = root+'\\污染源\\'+folder+'\\许可\\'
    # if folder=='大陆溪流域':
    #     folder_py = 'FAAADLX'
    # else: folder_py =get_first_letters(folder)
    
    dem_layerfile = os.path.join(root,'DEM\\'+folder_py+'\\'+code+'\\dem_prj.tif')
    
    basin_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\流域.shp')
    rivers_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\河流_prj.shp')
    subbasin_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域.shp')
    subbasin_half_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域（左右岸）.shp')
    subbasin_tau_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域-tau.shp')
    crosssection_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\横断面.shp')
    
    xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\镇.shp')
    if os.path.isfile(xz_layerfile) is False:
        xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\乡镇.shp')
    
    # 1.区流域基本情况数据
    #   在https://geofolio.org/上上传区流域zip数据
    #   生成报告后，执行以下命令转换为Markdown文本：
    #   pandoc 基本情况.docx --extract-media=. -o 基本情况.md
    #   打开文本替换'./media'地址为发布的服务器地址

    # 2.处理水文分析数据
    # 删除子流域中的无效数据，在子流域编号相同的feature里保留面积最大的
    from psyp.dataio.shp import shpio
    # shpio.delete_useless_features(subbasin_layerfile,'cat')
    # shpio.delete_useless_features(subbasin_half_layerfile,'cat')
    # shpio.delete_useless_features(subbasin_tau_layerfile,'cat')
    # shpio.join_attrs_by_max_overlap(subbasin_layerfile[:-4]+'-rm.shp',
    # subbasin_tau_layerfile[:-4]+'-rm.shp','1')
    shpio.join_attrs_by_max_overlap(subbasin_half_layerfile[:-4]+'-rm.shp',
    subbasin_layerfile[:-4]+'-rm-join.shp','2')

    # 与乡镇图层相交，给拓扑数据赋值行政区划属性
    # 子流域、子流域（左右岸）、河流、控制河段、控制断面

    # 根据河流水系、横断面、DEM，生成河段的纵剖面数据和随机横断面数据
    # res = shpio.get_elevation_along_line_with_distance(dem_layerfile,rivers_layerfile,30)
    # import json
    # import numpy as np
    # # Custom encoder class to handle non-serializable types
    # class NumpyEncoder(json.JSONEncoder):
    #     def default(self, obj):
    #         if isinstance(obj, np.uint16):
    #             return int(obj)  # Convert uint16 to int
    #         return json.JSONEncoder.default(self, obj)
    # with open(rivers_layerfile[:-3]+'json', 'w') as json_file:
    #     json.dump(res, json_file, cls=NumpyEncoder,indent=4)
    # json_object = json.dumps(res, cls=NumpyEncoder)
    return

def get_first_letters(chinese_str):
    # import jieba
    from pypinyin import pinyin, lazy_pinyin, Style
   # 使用pinyin函数获取完整拼音，并设置异读字读音方式为第一个读音
    pinyin_list = pinyin(chinese_str, style=Style.FIRST_LETTER, strict=True)
    flat_list = [item for sublist in pinyin_list for item in sublist if isinstance(item, str)]

    # 将拼音列表转换为字符串
    first_letters = ''.join(flat_list)
    
    return first_letters
# def delete_useless_features(data_dir):
#     '''
#     删除子流域中的无效数据，在子流域编号相同的feature里保留面积最大的
#     '''
#     return 0