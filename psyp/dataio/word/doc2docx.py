import os
from win32com import client as wc
import time

# pandoc test.docx --extract-media=. -o test.md

def save_doc_to_docx_dir(rawpath):  # doc转docx
    '''
    :param rawpath: 传入和传出文件夹的路径
    :return: None
    '''
    
    # 不能用相对路径，老老实实用绝对路径
    # 需要处理的文件所在文件夹目录
    filenamelist = os.listdir(rawpath)
    for i in os.listdir(rawpath):
        # 找出文件中以.doc结尾并且不以~$开头的文件（~$是为了排除临时文件的）
        if i.endswith('.doc') and not i.startswith('~$'):
            word = wc.Dispatch("Word.Application")        
            print(i)
            # try
            # 打开文件
            docxfile = rawpath + i+'x'
            if os.path.isfile(docxfile) is True:
                continue
            doc = word.Documents.Open(rawpath + i)
            # # 将文件名与后缀分割
            rename = os.path.splitext(i)
            # 将文件另存为.docx
            doc.SaveAs(rawpath + rename[0] + '.docx', FileFormat=16)  # 12表示docx格式
            doc.Close(False)
            os.remove(rawpath + i)
            word.Quit()
            time.sleep(1)
            

def save_doc_to_docx(path,docfile):  # doc转docx
    '''
    :param docfile: 传入doc的路径
    :return: None
    '''
    word = wc.Dispatch("Word.Application")
    # 不能用相对路径，老老实实用绝对路径
    # 打开文件
    docfile = path + docfile
    docxfile =path + docfile +'x'
    if os.path.isfile(docxfile) is True:
        return
    doc = word.Documents.Open(docfile)
    # 将文件另存为.docx
    print(docxfile)
    # # 将文件名与后缀分割
    rename = os.path.basename(docfile)
    doc.SaveAs(path + rename + 'x', 12)  # 12表示docx格式
    doc.Close()
    os.remove(docfile)
            
    word.Quit()

def convert_doc_2_docx(dir):
    from docx import Document
    for filename in os.listdir(dir):
        if filename.endswith(".doc") or filename.endswith(".DOC"):
            doc_file = os.path.join(dir, filename)
            doc = Document(doc_file)
            docx_file = doc_file.replace('.doc', '.docx')
            docx_file = docx_file.replace('.DOC', '.docx')
            print(doc_file)
            doc.save(docx_file)
            os.remove(doc_file)

# pandco不支持从doc转docx
# def convert_doc_2_docx2(dir):
#     '''
#     pandco不支持从doc转docx
#     '''
#     import os  
#     import subprocess  

#     # 遍历源目录中的所有.doc文件  
#     for filename in os.listdir(dir):  
#         if filename.endswith('.doc'):  
#             # 构造源文件和目标文件的完整路径  
#             source_file = os.path.join(dir, filename)  
#             target_file = os.path.join(dir, os.path.splitext(filename)[0] + '.docx')  
              
#             # 使用pandoc进行转换  
#             try:  
#                 subprocess.run(['C:\\Users\\Administrator\\AppData\\Local\\Pandoc\\pandoc.exe','-s', source_file, '-o', target_file], check=True)  
#                 print(f"Converted {source_file} to {target_file}")  
#             except subprocess.CalledProcessError as e:  
#                 print(f"Error converting {source_file}: {e}")  
  

if __name__ == '__main__':
    # 注意：目录的格式必须写成双反斜杠
    path = 'D:\\乌海市\\污染源\\乌海市\\许可\\'
    save_doc_to_docx_dir(path)
    # docfile = ''
    # save_doc_to_docx(path,docfile)