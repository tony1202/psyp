from docx import Document
import os

def docx2txt(docxfile):
    txtfile=docxfile[:-4]+'txt'
    f = open(txtfile,'w',encoding="utf-8")
    doc = Document(docxfile)
    text_content = []

    for paragraph in doc.paragraphs:
        text_content.append(paragraph.text)

    with open(txtfile, 'w', encoding='utf-8') as txt_file:
        txt_file.write('\n'.join(text_content))

def docx_header_2_txt(docxfile):
    document = Document(docxfile)
    section = document.sections[0]
    header = section.header
    txtfile=docxfile[:-4]+'txt'
    f = open(txtfile,'w',encoding="utf-8")
    for paragraph in header.paragraphs:
        f.write(paragraph.text)
        # print(paragraph.text) # or whatever you have in mind
    f.close()

if __name__ == '__main__':
#    docxfile = r'tests\testdata\dataio\docx2txt\“十四五”环境影响评价与排污许可工作实施方案.docx' 
#    docxfile = r'C:\Users\jip\Downloads\环评估发〔2022〕9号.docx'
#    print(docxfile)
#    docx2txt(docxfile)
    # docxfile=r'C:\sypcloud_git\projects\109_cz\doc\模型及可视化接口开发委托\成果\接口文档0801.docx'
    # docx_header_2_txt(docxfile)
    docxfile = r'D:\psyp\tests\testdata\nlp\keras\环评文件\report_245022.docx'
    docx2txt(docxfile)