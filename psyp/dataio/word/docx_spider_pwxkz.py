import json
import docx
from docx.document import Document
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph
import pandas as pd
import os
import re
from psyp.dataio.word import  doc2docx as doc

def iter_block_items(parent):
    """
    Yield each paragraph and table child within *parent*, in document order.
    Each returned value is an instance of either Table or Paragraph. *parent*
    would most commonly be a reference to a main Document object, but
    also works for a _Cell object, which itself can contain paragraphs and tables.
    """
    if isinstance(parent, Document):
        parent_elm = parent.element.body
    elif isinstance(parent, _Cell):
        parent_elm = parent._tc
    else:
        raise ValueError("something's not right")

    for child in parent_elm.iterchildren():
        if isinstance(child, CT_P):
            yield Paragraph(child, parent)
        elif isinstance(child, CT_Tbl):
            yield Table(child, parent)

def read_table(table):
    return [[cell.text for cell in row.cells] for row in table.rows]

def read_word(word_path):
    doc = docx.Document(word_path)
    for block in iter_block_items(doc):
        if isinstance(block, Paragraph):
            print("text", [block.text])
        elif isinstance(block, Table):
            print("table", read_table(block))

def get_table_ids(word_path):
    table_names = ['排污单位基本信息表','废水直接排放口基本情况表', '废水间接排放口基本情况表', \
        '主要产品及产能信息表','主要产品及产能信息补充表','主要原辅材料及燃料信息表', \
            '废气产排污节点、污染物及污染治理设施信息表','废水类别、污染物及污染治理设施信息表',\
                '排污单位生产线基本情况表','生活污水进水信息','工业废水进水信息']
    table_dict = {}
    table_name_key = ""
    doc = docx.Document(word_path)
    for block in iter_block_items(doc):
        if isinstance(block, Paragraph):
            table_name = match_table_by_name(block.text, table_names)
            if table_name != "":
                table_name_key = table_name
            else:
                table_name_key = ""
        elif isinstance(block, Table):
            if table_name_key != "":
                table_dict.setdefault(table_name_key, block)
                table_name_key = ""
    return table_dict

def match_table_by_name(text, table_names):
    for table_name in table_names:
        if table_name in text and len(text) < len(table_name) + 10:
            return table_name
    return ""

def read_docx(table_dict):
    # 表1 排污单位基本信息表
    table = table_dict.get("排污单位基本信息表")
    if table != None:
        json_result = parse_pwdwjbxx(table)
        print(json_result)

    # 表6 废水间接排放口基本情况表
    table = table_dict.get("废水间接排放口基本情况表")
    if table != None:
        json_result = parse_fsjjpfkjbqk(table)
        print(json_result)

    # 表17 主要产品及产能信息表
    table = table_dict.get("主要产品及产能信息表")
    if table != None:
        json_result = parse_zycpjcnxx(table)
        print(json_result)

    # 表20 废水类别、污染物及污染治理设施信息表
    table = table_dict.get("废水类别、污染物及污染治理设施信息表")
    if table != None:
        json_result = parse_fslbwrwjwrzlssxx(table)
        print(json_result)

    # table_zycpjcnxx = tables[16] # 表17 主要产品及产能信息表
    # table_fslbwrwjwrzlssxx = tables[19] # 表20 废水类别、污染物及污染治理设施信息表

def parse_pwdwjbxx(table_pwdwjbxx):
    '''
    获取副本中单位基本信息
    '''
    json_pwdwjbxx = {}
    fields = ['投产日期', '统一社会信用代码', '所属工业园区名称', '排污许可证管理类别']
    rows = table_pwdwjbxx.rows
    key = None
    for row in rows:
        cells = row.cells
        for cell in cells:
            text = cell.text.strip()
            if text in fields:
                key = text
            elif key != None:
                json_pwdwjbxx.setdefault(key, text)
                key = None
    # return json.dumps(json_pwdwjbxx, ensure_ascii=False)
    return json_pwdwjbxx

def parse_fszjpfkjbqk(table_fszjpfkjbqk):
    '''
    获取副本中的直接排放口基本情况
    '''
    json_fszjpfkjbqk = {}
    id_name = '排放口编号'
    col_names = ['排放口名称','经度','纬度','排放去向', '排放规律','名称']
    col_ids = {}
    id_id = -1
    rows = table_fszjpfkjbqk.rows
    parse_title = True
    for row in rows:
        cells = row.cells
        for i in range(len(cells)):
            if parse_title:
                cell = cells[i]
                text = cell.text
                if text == id_name:
                    id_id = i
                elif text in col_names:
                    col_ids.setdefault(text, i)
            if len(cell.tables) > 0 and id_id >= 0:
                parse_title = False
                t = cell.tables[0]
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {}
                    for (col_name, col_id) in col_ids.items():
                        single_dict.setdefault(col_name, rr.cells[col_id].text)
                    json_fszjpfkjbqk.setdefault(id_value, single_dict)
    # return json.dumps(json_fszjpfkjbqk, ensure_ascii=False)
    return json_fszjpfkjbqk

def parse_fsjjpfkjbqk(table_fsjjpfkjbqk):
    '''
    获取副本间接排放口基本情况
    '''
    json_fsjjpfkjbqk = {}
    id_name = '排放口编号'
    col_names = ['排放口名称','经度','纬度','排放去向', '排放规律','名称']
    col_ids = {}
    id_id = -1
    rows = table_fsjjpfkjbqk.rows
    parse_title = True
    for row in rows:
        cells = row.cells
        for i in range(len(cells)):
            if parse_title:
                cell = cells[i]
                text = cell.text
                if text == id_name:
                    id_id = i
                elif text in col_names:
                    col_ids.setdefault(text, i)
            if len(cell.tables) > 0 and id_id >= 0:
                parse_title = False
                t = cell.tables[0]
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {}
                    for (col_name, col_id) in col_ids.items():
                        single_dict.setdefault(col_name, rr.cells[col_id].text)
                    json_fsjjpfkjbqk.setdefault(id_value, single_dict)
    # return json.dumps(json_fsjjpfkjbqk, ensure_ascii=False)
    return json_fsjjpfkjbqk

def parse_zycpjcnxx(table_zycpjcnxx):
    '''
    获取副本主要产品及产能信息
    '''
    json_zycpjcnxx = {}

    # col_ids ={'生产线名称': 1, '产品名称': 3, '生产能力': 5, '计量单位': 4, '设计年生产时间': 6}
    # Create a dictionary to store the indices
    col_ids = {}
    #
    # 通过测试发现，许可平台生成的副本的主要产品及产能表有以下几种情况
    # 表述生产线的字段有：'生产线名称','生产线类型','生产单元名称','生产线编号及名称','生产线编号和名称'
    # 表述生产能力的字段有：'生产能力','设计值'
    # 表述产品名称的字段有：'产品名称','产品（介质）名称'
    #
    col_to_retrieve =['生产线名称','生产线类型','生产单元名称','生产线编号及名称','生产线编号和名称','主体工程',\
        '产品名称','产品（介质）名称','其他设施参数信息','生产能力','计量单位','设计年生产时间','设计年生产天数（天）',\
        '工艺名称','生产工艺','生产设施名称','参数名称','设计值','计量单位（']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table_zycpjcnxx.rows[0].cells]
    include_ssinfo=any(element.startswith('设施参数') for element in columns)
    if include_ssinfo: # 有些副本主表有设施，有些没有；主表有设施的，同时也有可能有副表，格式不统一
        columns = [cell.text.strip() for cell in table_zycpjcnxx.rows[1].cells]
    
    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table_zycpjcnxx.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_zycpjcnxx:
                    break
                for rr in t.rows:
                    single_dict = {}
                    cpmc=''
                    id_value = rr.cells[id_id].text
                    if '产品名称' in col_ids:
                        cpmc = rr.cells[col_ids['产品名称']].text
                    elif '产品（介质）名称' in col_ids:
                        cpmc= rr.cells[col_ids['产品（介质）名称']].text
                    elif '其他设施参数信息' in col_ids:
                        cpmc= rr.cells[col_ids['其他设施参数信息']].text
                    
                    single_dict = {
                            '产品名称': cpmc,
                            '生产能力': rr.cells[col_ids['生产能力'] if '生产能力' in col_ids else col_ids['设计值']].text,
                            '设计年生产时间(天)': rr.cells[col_ids['设计年生产天数（天）']].text if '设计年生产天数（天）' in col_ids else None,
                            '设计年生产时间': rr.cells[col_ids['设计年生产时间']].text if '设计年生产时间' in col_ids else None,
                            '计量单位':''                           
                        }
                    if '计量单位' in col_ids:
                        single_dict['计量单位']=rr.cells[col_ids['计量单位']].text
                    if include_ssinfo:
                        single_dict['工艺名称']= rr.cells[col_ids['工艺名称']].text if '工艺名称' in col_ids else rr.cells[col_ids['生产工艺']].text
                        single_dict['生产设施名称']= rr.cells[col_ids['生产设施名称']].text if '生产设施名称' in col_ids else None
                        single_dict['参数名称']= rr.cells[col_ids['参数名称']].text
                        single_dict['设计值']= rr.cells[col_ids['设计值']].text
                        single_dict['计量单位2']= rr.cells[col_ids['计量单位（']].text if '计量单位（' in col_ids else None
                                                
                    if id_value not in json_zycpjcnxx:
                        json_zycpjcnxx[id_value]=[]
                    json_zycpjcnxx[id_value].append(single_dict) if single_dict['产品名称'] not in [item.get('产品名称', '') for item in json_zycpjcnxx[id_value]] else None               
                    
    # return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    return json_zycpjcnxx

def parse_zycpjcnxxEx(table_zycpjcnxx):
    '''
    获取副本主要产品及产能信息补充表
    '''
    if table_zycpjcnxx is None: return
    
    json_zycpjcnxx = {}

    # col_ids = {'主要生产单元名称': 1, '主要工艺名称': 4, '主要生产设施名称':5,'参数名称': 7, '设计值': 9, '计量单位': 8}
    col_ids = {}
    #
    # 通过测试发现，许可平台生成的副本的主要产品及产能信息补充表有以下几种情况
    # 表述生产线的字段有：'生产线名称','生产线类型','生产单元名称','生产线编号及名称','生产线编号和名称'
    # 表述生产能力的字段有：'生产能力','设计值'
    #
    col_to_retrieve =['生产线编号和名称','排污单位类型编号','生产线名称','生产线类型','生产单元名称',\
        '工艺名称','生产设施名称','参数名称','设计值','计量单位']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table_zycpjcnxx.rows[1].cells]

    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table_zycpjcnxx.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_zycpjcnxx:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {
                            '工艺名称': rr.cells[col_ids['工艺名称']].text,
                            '生产设施名称': rr.cells[col_ids['生产设施名称']].text,
                            '参数名称': rr.cells[col_ids['参数名称']].text,
                            '设计值': rr.cells[col_ids['设计值']].text,
                            '计量单位': rr.cells[col_ids['计量单位']].text
                        }
                    if id_value not in json_zycpjcnxx:
                        json_zycpjcnxx[id_value]=[]
                    json_zycpjcnxx[id_value].append(single_dict) 

                    # if id_value == "公用单元" and zygymc == "污水处理单元":
                    #     single_dict = {
                    #         '主要生产单元名称': "公用单元",
                    #         '主要工艺名称': "污水处理单元",
                    #         '参数名称': rr.cells[col_ids['参数名称']].text,
                    #         '设计值': rr.cells[col_ids['设计值']].text,
                    #         '计量单位': rr.cells[col_ids['计量单位1']].text
                    #     }
                    #     json_zycpjcnxx.setdefault(id_value, single_dict)
                    # elif id_value == "织造单元" and zygymc == "喷水织造":
                    #     single_dict = {
                    #         '主要生产单元名称': "织造单元",
                    #         '主要工艺名称': "喷水织造",
                    #         '产品名称': rr.cells[col_ids['产品名称']].text,
                    #         '产品设计产能': rr.cells[col_ids['产品设计产能']].text,
                    #         '计量单位': rr.cells[col_ids['计量单位2']].text,
                    #         '设计年运行时间': rr.cells[col_ids['设计年运行时间']].text
                    #     }
                    #     json_zycpjcnxx.setdefault(id_value, single_dict)
                    # if "公用单元" in json_zycpjcnxx.keys() and "织造单元" in json_zycpjcnxx.keys():
                    #    return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    
    # return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    return json_zycpjcnxx

def parse_zyyfclxx(table_zyyfclxx):
    '''
    获取副本主要原辅材料及燃料信息
    key-产品名称
    '''
    json_zyyfclxx = {}

    if table_zyyfclxx is None: return None

    # col_ids = col_ids ={'产品类型':1,'种类': 2, '名称': 3, '年最大使用量': 4, '计量单位': 5}
    col_ids = {}
    col_to_retrieve =['产品类型','种类',\
        '名称','年最大使用量','设计年使用量','设计年加工量','年设计使用量','年使用量','计量单位']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table_zyyfclxx.rows[0].cells]
    columns = [element for index, element in enumerate(columns) if element not in columns[:index]]
    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}
    if '产品名称' not in columns:
        col_ids['产品名称']=0
    id_name = '产品名称'
    id_id = col_ids[id_name]
    rows = table_zyyfclxx.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_zyyfclxx:
                    break
                for rr in t.rows:
                    if '产品名称' not in columns:
                        id_value=''
                    else: id_value = rr.cells[id_id].text
                    single_dict = {
                            '种类': rr.cells[col_ids['种类']].text,
                            '名称': rr.cells[col_ids['名称']].text,
                            '计量单位': rr.cells[col_ids['计量单位']].text
                        }
                    if '年最大使用量' in col_ids:
                        single_dict['年最大使用量']= rr.cells[col_ids['年最大使用量']].text 
                    elif '设计年使用量' in col_ids:
                        single_dict['年最大使用量']= rr.cells[col_ids['设计年使用量'] ].text
                    elif '设计年加工量' in col_ids:
                        single_dict['年最大使用量']= rr.cells[col_ids['设计年加工量'] ].text
                    elif '年设计使用量' in col_ids:
                        single_dict['年最大使用量']= rr.cells[col_ids['年设计使用量'] ].text
                    elif '年使用量' in col_ids:
                        single_dict['年最大使用量']= rr.cells[col_ids['年使用量'] ].text

                    if id_value not in json_zyyfclxx:
                        json_zyyfclxx[id_value]=[]
                    json_zyyfclxx[id_value].append(single_dict) if single_dict['名称'] not in [item.get('名称', '') for item in json_zyyfclxx[id_value]] else None               
                    
    # return json.dumps(json_zyyfclxx, ensure_ascii=False)
    return json_zyyfclxx

def parse_fslbwrwjwrzlssxx(table):
    '''
    获取副本废水类别、污染物及污染治理设施信息表
    '''
    json_fslbwrwjwrzlssxx = {}

    if table is None: return None

    # col_ids = {'废水类别': 1, '产污环节': 2, '污染物种类': 3, '污染治理设施编号': 4, '污染治理设施名称': 5, '污染治理设施工艺': 6, '是否为可行技术': 7,
    #            '污染治理设施其他信息': 8, '排放去向': 9, '排放方式': 10, '排放规律': 11, '排放口编号': 12,
    #            '排放口名称': 13, '排放口设置是否符合要求': 14, '排放口类型': 15, '其他信息': 16}
    col_ids = {}
    col_to_retrieve =['废水类别','产污环节','污染物种类','污染治理设施编号',\
        '污染治理设施名称','污染治理设施工艺','是否为可行技术','污染治理设施其他信息',\
            '排放去向','排放方式','排放规律','排放口编号','排放口名称','排放口设置是否符合要求',\
                '排放口类型','其他信息']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[1].cells]

    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows
    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_fslbwrwjwrzlssxx:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {}
                    for (col_name, col_id) in col_ids.items():
                        single_dict.setdefault(col_name, rr.cells[col_id].text)
                    json_fslbwrwjwrzlssxx.setdefault(id_value, single_dict)
    # return json.dumps(json_fslbwrwjwrzlssxx, ensure_ascii=False)
    return json_fslbwrwjwrzlssxx

def parse_fslbwrwjwrzlssxx2(table):
    '''
    获取副本废水类别、污染物及污染治理设施信息表
    return: key-生产设施名称；value-[] 治理设施名称和工艺
    '''
    if table is None: return
    json_fslbwrwjwrzlssxx = {}

    # col_ids = {'产污设施名称': 4,  '治理技术名称': 5,'排放去向': 8, '排放方式': 9, '排放规律': 10, '排放口编号': 11,
    #            '排放口名称': 12, '排放口设置是否符合要求': 13, '排放口类型': 14, '其他信息': 15}
    col_ids = {}
    col_to_retrieve =['防治设施名称','治理设施名称','防治设施工艺','治理设施工艺',\
        '排放去向','排放方式','排放规律','排放口编号','排放口名称','排放口设置是否符合要求',\
            '排放口类型','其他信息']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[1].cells]

    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}
    col_ids['其他信息']=len(columns)-1
    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows
    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_fslbwrwjwrzlssxx:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {
                            '治理设施名称': rr.cells[col_ids.get('防治设施名称', col_ids.get('治理设施名称'))].text, # 废水的治理设施名称=产污设施名称
                            '治理技术名称': rr.cells[col_ids.get('防治设施工艺', col_ids.get('治理设施名称'))].text,
                            '排放去向': rr.cells[col_ids['排放去向']].text,
                            '排放方式': rr.cells[col_ids['排放方式']].text,
                            '排放规律': rr.cells[col_ids['排放规律']].text,
                            '排放口编号': rr.cells[col_ids['排放口编号']].text,
                            '排放口名称': rr.cells[col_ids['排放口名称']].text,
                            '排放口设置是否符合要求': rr.cells[col_ids['排放口设置是否符合要求']].text,
                            '排放口类型': rr.cells[col_ids['排放口类型']].text,
                            '其他信息': rr.cells[col_ids['其他信息']].text,
                        }
                    if id_value not in json_fslbwrwjwrzlssxx:
                        json_fslbwrwjwrzlssxx[id_value]=[]
                    json_fslbwrwjwrzlssxx[id_value].append(single_dict) 
    # return json.dumps(json_fslbwrwjwrzlssxx, ensure_ascii=False)
    return json_fslbwrwjwrzlssxx

def parse_fqlbwrwjwrzlssxx(table):
    '''
    获取副本废气产排污节点、污染物及污染治理设施信息表
    return: key-生产设施名称；value-[] 治理设施名称和工艺
    '''
    if table is None: return
    json_fqlbwrwjwrzlssxx = {}

    # col_ids = {'产污设施名称': 2, '治理设施名称': 7, '治理技术名称': 8}
    col_ids = {}
    col_to_retrieve =['产污设施名称','防治设施名称','防治设施工艺','治理设施名称','治理技术名称','治理设施工艺','污染治理设施其他信息']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[1].cells]

    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows
    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_fqlbwrwjwrzlssxx:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict={
                        '治理设施名称':'',
                        '治理技术名称':''
                    }
                    if '防治设施名称' in col_ids:
                        single_dict['治理设施名称'] = rr.cells[col_ids['防治设施名称']].text
                    if '防治设施工艺' in col_ids:
                        single_dict['治理技术名称'] = rr.cells[col_ids['防治设施工艺']].text
                    if '治理设施名称' in col_ids:
                        single_dict['治理设施名称'] = rr.cells[col_ids['治理设施名称']].text
                    if '治理技术名称' in col_ids:
                        single_dict['治理技术名称'] = rr.cells[col_ids['治理技术名称']].text
                    if '治理设施工艺' in col_ids:
                        single_dict['治理技术名称'] = rr.cells[col_ids['治理设施工艺']].text
                    if '污染治理设施其他信息' in col_ids and '治理设施工艺' not in col_ids and '治理技术名称' not in col_ids and '防治设施工艺' not in col_ids:
                        single_dict['治理技术名称'] = rr.cells[col_ids['污染治理设施其他信息']].text

                    if id_value not in json_fqlbwrwjwrzlssxx:
                        json_fqlbwrwjwrzlssxx[id_value]=[]
                    json_fqlbwrwjwrzlssxx[id_value].append(single_dict) 
    # return json.dumps(json_fqlbwrwjwrzlssxx, ensure_ascii=False)
    return json_fqlbwrwjwrzlssxx

def parse_shwsjsxx(table):
    '''
    获取污水处理厂许可副本中的生活污水进水信息表
    '''
    if table is None: return
    json_results = {}
    col_ids = {}
    col_to_retrieve =['序号','进水水量（m3/d）']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[1].cells]
    
    # 计算列索引号
    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_results:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {
                            '进水水量': rr.cells[col_ids['进水水量（m3/d）'] ].text,                         
                        }
                                                
                    if id_value not in json_results:
                        json_results[id_value]=[]
                    json_results[id_value].append(single_dict)               
                    
    # return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    return json_results

def parse_gyfsjsxx(table):
    '''
    获取污水处理厂许可副本中的工业废水进水信息表
    '''
    if table is None: return
    json_results = {}
    col_ids = {}
    col_to_retrieve =['序号','进水水量（m3/d）']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[1].cells]
    
    # 计算列索引号
    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_results:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    if len(rr.cells)<len(columns):  # 发现部分副本，程序识别的表头列数比数据列数多
                        single_dict = {
                                '进水水量': rr.cells[col_ids['进水水量（m3/d）'] - (len(columns)-len(rr.cells)) ].text,                         
                            }
                    else:
                        single_dict = {
                            '进水水量': rr.cells[col_ids['进水水量（m3/d）']].text,                         
                        }
                                                
                    if id_value not in json_results:
                        json_results[id_value]=[]
                    json_results[id_value].append(single_dict)               
                    
    # return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    return json_results

def parse_wsclcscxxx(table):
    '''
    获取污水处理厂许可副本中的排污单位生产线基本情况表
    '''
    if table is None: return
    json_results = {}
    col_ids = {}
    col_to_retrieve =['生产线名称或编号','设计处理能力','年运行时间（h）','污染治理设施名称']    
    # Extract column names from the first row of the table
    columns = [cell.text.strip() for cell in table.rows[0].cells]
    
    # 计算列索引号
    # Iterate through each element in col_to_retrieve
    for column in col_to_retrieve:
        indices = [i for i, value in enumerate(columns) if column in value]
        # Check if any indices were found
        if indices:
            col_ids[column] = indices[0]
    # # Create a dictionary to store the indices
    # col_ids = {column: [i for i, value in enumerate(columns) if column in value] for column in col_to_retrieve}

    id_name = next(iter(col_ids),None)
    id_id = col_ids[id_name]
    rows = table.rows

    for row in rows:
        cells = row.cells
        for cell in cells:
            if len(cell.tables) > 0:
                t = cell.tables[0]
                if t.rows[0].cells[id_id].text in json_results:
                    break
                for rr in t.rows:
                    id_value = rr.cells[id_id].text
                    single_dict = {
                            '生产能力': rr.cells[col_ids['设计处理能力'] ].text,
                            '设计年生产时间': rr.cells[col_ids['年运行时间（h）']].text, 
                            '治理设施名称': rr.cells[col_ids['污染治理设施名称'] ].text,                       
                        }
                                                
                    if id_value not in json_results:
                        json_results[id_value]=[]
                    json_results[id_value].append(single_dict)               
                    
    # return json.dumps(json_zycpjcnxx, ensure_ascii=False)
    return json_results

def fill_gdyjbxxb_by_xkzfb(data_dir,dir2,file,sheet=0):
    '''
    从许可证副本里抽取相关信息，填补到固定源基本信息表（该表由许可证平台爬取而成）
    file: excel 或 csv文件
    '''
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.xlsx':
        data_df = pd.read_excel(os.path.join(data_dir,file),sheet_name = sheet)
    if file_extension == '.csv':
        data_df = pd.read_csv(os.path.join(data_dir,file))
    
    # 遍历每一个污染源
    # for wry in data_df['排污许可证（登记）编号'].unique():
    for index, row in data_df[(data_df['排污许可证管理类别'] !='登记')].iterrows():
        xkzcode = row['排污许可证（登记）编号']
        hylb = row['行业类别']
        if '饲养' in hylb:
            wrylx = '规模畜禽养殖'
        elif '养殖' in hylb:
            wrylx = '规模水产养殖'
        elif '污水处理及其再生利用' in hylb:
            wrylx = '集中式污水治理设施'
        else:
            wrylx = '工业源'
        xkzfb = os.path.join(dir2,xkzcode+'.docx')
        # tmp code
        tmp = os.path.join(dir2,xkzcode+'.doc.docx')
        if os.path.isfile(tmp) is True:
            os.rename(tmp,xkzfb)
        # end tmp code
        if os.path.isfile(xkzfb) is False:
            if os.path.isfile(xkzfb[:-1]):
                doc.save_doc_to_docx(dir2,xkzcode+'.doc')
        if os.path.isfile(xkzfb) is True:
            table_dict = get_table_ids(xkzfb)
            # 表1 排污单位基本信息表
            table = table_dict.get("排污单位基本信息表")
            if table != None:
                json_result = parse_pwdwjbxx(table)
                # print(json_result)
                # Change the data type of the column
                # data_df['投产日期'] = data_df['投产日期'].astype('str')
                data_df.loc[index,'投产日期'] = json_result['投产日期'] if '投产日期' in json_result else '' # json_result['投产日期']
                data_df.loc[index,'统一社会信用代码'] = json_result['统一社会信用代码']
                data_df.loc[index,'所属工业园区名称'] = json_result['所属工业园区名称'] if '所属工业园区名称' in json_result else '' # json_result['所属工业园区名称']
                data_df.loc[index,'排污许可证管理类别'] = json_result['排污许可证管理类别'] if '排污许可证管理类别' in json_result else '' 
                data_df.loc[index,'污染源类型'] = wrylx
                # data_df.to_excel(os.path.join(data_dir,filename+'-M.xlsx'), index=False)
    data_df.to_excel(os.path.join(data_dir,filename+'-副本补全.xlsx'), index=False)

def fill_gdypfkxxb_by_xkzfb(data_dir,dir2,file,pfkxxb,sheet=0):
    '''
    从许可证副本里抽取排放口信息，填补到固定源排放口信息表（该表由许可证平台爬取而成）
    file: excel 或 csv文件
    '''
    filename, file_extension = os.path.splitext(file)
    pfkfilename, file_extension2 = os.path.splitext(pfkxxb)
    if file_extension == '.xlsx':
        data_df = pd.read_excel(os.path.join(data_dir,file),sheet_name = sheet)
    if file_extension == '.csv':
        data_df = pd.read_csv(os.path.join(data_dir,file))
    if file_extension2 =='.xlsx':
        pfk_df = pd.read_excel(os.path.join(data_dir,pfkxxb),sheet_name=sheet)
    if file_extension2 == '.csv':
        pfk_df = pd.read_csv(os.path.join(data_dir,pfkxxb))

    # 遍历每一个污染源
    # for wry in data_df['排污许可证（登记）编号'].unique():
    for index, row in data_df[(data_df['排污许可证管理类别'] !='登记')].iterrows():
        xkzcode = row['排污许可证（登记）编号']
        xkzfb = os.path.join(dir2,xkzcode+'.docx')
        if os.path.isfile(xkzfb) is True:
            table_dict = get_table_ids(xkzfb)
            # 表7 废水直接排放口基本情况表
            table = table_dict.get("废水直接排放口基本情况表")
            if table != None:
                json_result = parse_fszjpfkjbqk(table)
                # print(json_result)
                for key,value in json_result.items():
                    # Create a new row
                    pattern = r"(\d+)°(\d+)′([\d.]+)″"
                    match = re.match(pattern, value['经度'])
                    if match:
                        degrees = float(match.group(1))
                        minutes = float(match.group(2))
                        seconds = float(match.group(3))
                    else:
                        raise ValueError("Invalid input string")
                    logtd = degrees + minutes/60 + seconds/3600
                    match = re.match(pattern, value['纬度'])
                    if match:
                        degrees = float(match.group(1))
                        minutes = float(match.group(2))
                        seconds = float(match.group(3))
                    else:
                        raise ValueError("Invalid input string")
                    latd = degrees + minutes/60 + seconds/3600
                    new_row = pd.Series([row['序号'], '',xkzcode,key,value['排放口名称'],\
                        '直接排放',logtd, latd,'',value['排放去向'],value['排放规律'],\
                            '','','',value['名称'],''],index=pfk_df.columns)
                    # Append the new row to the DataFrame
                    pfk_df = pfk_df.append(new_row, ignore_index=True)
            # 表6 废水间接排放口基本情况表
            table = table_dict.get("废水间接排放口基本情况表")
            if table != None:
                json_result = parse_fsjjpfkjbqk(table)
                # print(json_result)
                for key,value in json_result.items():
                    # Create a new row
                    pattern = r"(\d+)°(\d+)′([\d.]+)″"
                    match = re.match(pattern, value['经度'])
                    if match:
                        degrees = float(match.group(1))
                        minutes = float(match.group(2))
                        seconds = float(match.group(3))
                    else:
                        raise ValueError("Invalid input string")
                    logtd = degrees + minutes/60 + seconds/3600
                    match = re.match(pattern, value['纬度'])
                    if match:
                        degrees = float(match.group(1))
                        minutes = float(match.group(2))
                        seconds = float(match.group(3))
                    else:
                        raise ValueError("Invalid input string")
                    latd = degrees + minutes/60 + seconds/3600
                    new_row = pd.Series([row['序号'], '',xkzcode,key,value['排放口名称'],\
                        '间接排放',logtd, latd,'',value['排放去向'],value['排放规律'],\
                            '','',value['名称'],'',''],index=pfk_df.columns)
                    # Append the new row to the DataFrame
                    pfk_df = pfk_df.append(new_row, ignore_index=True)
    pfk_df.to_excel(os.path.join(data_dir,pfkfilename+'-追加许可.xlsx'), index=False)

def fill_gdyhdsp_by_xkzfb(data_dir, dir2, gdyjbxxb_file,gdyhdsp_gyy_file,gdyhdsp_wsc_file,sheet=0):
    '''
    从许可证副本里抽取活动水平数据，补充到登记类排污单位活动水平信息表中（该表由爬取代码生成）
    data_dir: 
    dir2: 许可证副本doc文件路径
    gdyjbxxb_file: 固定源基本信息表文件名
    gdyhdsp_gyy_file: 固定源活动水平表文件名
    '''
    file = open(os.path.join(data_dir, 'log.txt'), 'w',encoding='utf-8')
    data_df_gyy=None
    data_df_wsc=None
    xkzcode=''

    try:
        # 打开固定源基本信息表，遍历根据源类型及许可证号确定要读取的副本word文件及要写入的表单文件
        filename, file_extension = os.path.splitext(gdyjbxxb_file)
        if file_extension == '.xlsx':
            data_df = pd.read_excel(os.path.join(data_dir,gdyjbxxb_file),sheet_name = sheet)
        if file_extension == '.csv':
            data_df = pd.read_csv(os.path.join(data_dir,gdyjbxxb_file))
        
        xkzcode_xk_all_list = data_df[(data_df['排污许可证管理类别']!='登记') ]['排污许可证（登记）编号'].values

        # 工业源活动水平信息表
        filename_gyy, file_extension_gyy = os.path.splitext(gdyhdsp_gyy_file)
        if file_extension_gyy == '.xlsx':
            data_df_gyy = pd.read_excel(os.path.join(data_dir,gdyhdsp_gyy_file),sheet_name = sheet)
        if file_extension_gyy == '.csv':
            data_df_gyy = pd.read_csv(os.path.join(data_dir,gdyhdsp_gyy_file)) 
        
        xkzcode_xk_gyy_list = data_df_gyy['排污许可证（登记）编号'].values

        # 集中式污水治理设施活动水平信息表 
        filename_wsc, file_extension_wsc = os.path.splitext(gdyhdsp_wsc_file)
        if file_extension_wsc == '.xlsx':
            data_df_wsc = pd.read_excel(os.path.join(data_dir,gdyhdsp_wsc_file),sheet_name = sheet)
        if file_extension_wsc == '.csv':
            data_df_wsc = pd.read_csv(os.path.join(data_dir,gdyhdsp_wsc_file))   
        
        xkzcode_xk_wsc_list = data_df_wsc['排污许可证（登记）编号'].values

        # 遍历每一个污染源
        # for wry in data_df['排污许可证（登记）编号'].unique():
        # for index, row in data_df[(data_df['排污许可证管理类别'] !='登记')].iterrows():
        for code in xkzcode_xk_all_list:
            if code in xkzcode_xk_gyy_list or code in xkzcode_xk_wsc_list:
                continue
            filterd_rows = data_df[data_df['排污许可证（登记）编号'] == code]
            row  = filterd_rows.iloc[0]
            xkzcode = code # row['排污许可证（登记）编号'] #  row['pwxkz_dj_b'] # 
            print(xkzcode)
            xkzfb = os.path.join(dir2, xkzcode+'.docx')
            # tmp code
            tmp = os.path.join(dir2,xkzcode+'.doc.docx')
            if os.path.isfile(tmp) is True:
                os.rename(tmp,xkzfb)
            # end tmp code
            if os.path.isfile(xkzfb) is False:
                if os.path.isfile(xkzfb[:-1]):
                    doc.save_doc_to_docx(dir2,xkzcode+'.doc')
            if os.path.isfile(xkzfb) is True:
                # 如果是加油站类企业，跳过
                if row['行业类别']==('机动车燃油零售'):
                    continue
                # 判断污染源类型
                wrylx = row['污染源类型'] # row['wrylx'] # 
                
                # 读取副本docx文件
                table_dict = get_table_ids(xkzfb)

                if wrylx == '工业源':
                    # filename_gyy, file_extension_gyy = os.path.splitext(gdyhdsp_gyy_file)
                    # if file_extension_gyy == '.xlsx':
                    #     data_df_gyy = pd.read_excel(os.path.join(data_dir,gdyhdsp_gyy_file),sheet_name = sheet)
                    # if file_extension_gyy == '.csv':
                    #     data_df_gyy = pd.read_csv(os.path.join(data_dir,gdyhdsp_gyy_file))   
                    # table_dict = get_table_ids(xkzfb)
                    # 表17 主要产品及产能信息表
                    table = table_dict.get("主要产品及产能信息表")
                    if table != None:
                        json_result = parse_zycpjcnxx(table)
                        json_result_ex = parse_zycpjcnxxEx(table_dict.get('主要产品及产能信息补充表'))
                        json_result_yfl = parse_zyyfclxx(table_dict.get('主要原辅材料及燃料信息表'))
                        json_result_fq = parse_fqlbwrwjwrzlssxx(table_dict.get('废气产排污节点、污染物及污染治理设施信息表'))
                        json_result_fs = parse_fslbwrwjwrzlssxx2(table_dict.get('废水类别、污染物及污染治理设施信息表'))
                        # print(json_result)
                        # 遍历工段
                        for key,value in json_result.items(): # key 为生产线、工段
                            # 遍历产品
                            for cp in value:
                                if cp['产品名称']=='': continue
                                if json_result_yfl is not None:
                                    if next(iter(json_result_yfl),None)=='':
                                        ypclmc ='|'.join([item['名称'] for item in json_result_yfl['']]) # 原料名称
                                        ypclyl ='|'.join([item['年最大使用量'] for item in json_result_yfl['']])
                                        ypcldw ='|'.join([item['计量单位'] for item in json_result_yfl['']])
                                    else: 
                                        ypclmc ='|'.join([item['名称'] for item in json_result_yfl[cp['产品名称']]]) # 原料名称
                                        ypclyl ='|'.join([item['年最大使用量'] for item in json_result_yfl[cp['产品名称']]])
                                        ypcldw ='|'.join([item['计量单位'] for item in json_result_yfl[cp['产品名称']]])
                                if json_result_ex is not None and key in json_result_ex:
                                    zygymc ='|'.join([item['工艺名称'] for item in json_result_ex[key] if '工艺名称' in json_result_ex[key]]) # 以工段为索引
                                else: zygymc ='|'.join([item['工艺名称'] for item in json_result[key] if '工艺名称' in json_result[key]]) # 以工段为索引
                                # 根据 生产设施名称关联查询废气、废水治理设施，废水生产设施名称=治理设施名称
                                # 遍历生产设施
                                fqzlss=[]
                                fqzlgy=[]
                                fszlss=[]
                                fszlgy=[]
                                pfkbh=[]
                                if json_result_ex is not None: 
                                    if key in json_result_ex:                         
                                        for scssmc in [item['生产设施名称'] for item in json_result_ex[key]]:
                                            # 废气治理
                                            if json_result_fq is not None and scssmc in json_result_fq:
                                                fqzlss.append([item['治理设施名称'] for item in json_result_fq[scssmc]])
                                                fqzlgy.append([item['治理技术名称'] for item in json_result_fq[scssmc]])
                                            # 废水治理
                                            if json_result_fs is not None and scssmc in json_result_fs:
                                                fszlss.append([item['治理设施名称'] for item in json_result_fs[scssmc]])
                                                fszlgy.append([item['治理技术名称'] for item in json_result_fs[scssmc]])
                                                pfkbh.append([item['排放口编号'] for item in json_result_fs[scssmc]])
                                    else: continue            
                                else:                                 
                                    for scssmc in [item['生产设施名称'] for item in json_result[key]]:
                                        # 废气治理
                                        if json_result_fq is not None and scssmc in json_result_fq:
                                            fqzlss.append([item['治理设施名称'] for item in json_result_fq[scssmc]])
                                            fqzlgy.append([item['治理技术名称'] for item in json_result_fq[scssmc]])
                                        # 废水治理
                                        if json_result_fs is not None and scssmc in json_result_fs:
                                            fszlss.append([item['治理设施名称'] for item in json_result_fs[scssmc]])
                                            fszlgy.append([item['治理技术名称'] for item in json_result_fs[scssmc]])
                                            pfkbh.append([item['排放口编号'] for item in json_result_fs[scssmc]])

                                zlss = '|'.join([element for sublist in fqzlss for element in sublist]+\
                                    [element for sublist in fszlss for element in sublist])
                                zlgy = '|'.join([element for sublist in fqzlgy for element in sublist]+\
                                    [element for sublist in fszlgy for element in sublist])
                                
                                pfkbh_list = [list(t) for t in set(tuple(element) for element in pfkbh)]
                                pfk = '|'.join([element for sublist in pfkbh_list for element in sublist])

                                sjnscsj = cp['设计年生产时间'] # 小时
                                sjnscsj2 = cp['设计年生产时间(天)']
                                if sjnscsj2 is not None:
                                    sjnscsj =int(sjnscsj2)*24

                                if json_result_ex is None: 
                                    new_row = pd.Series([row['序号'], '',xkzcode,key,ypclmc,\
                                        cp['产品名称'],zygymc,'',cp['生产能力'],\
                                        cp['计量单位2'],ypclyl,ypcldw,'','',sjnscsj,\
                                        '','','',zlss,zlgy, '',pfk,'产排污系数法' ],index=data_df_gyy.columns)
                                else:
                                    new_row = pd.Series([row['序号'], '',xkzcode,key,ypclmc,\
                                        cp['产品名称'],zygymc,'',cp['生产能力'],\
                                        cp['计量单位'],ypclyl,ypcldw,'','',sjnscsj,\
                                        '','','',zlss,zlgy, '',pfk,'产排污系数法' ],index=data_df_gyy.columns)
                                # Append the new row to the DataFrame
                                data_df_gyy = data_df_gyy.append(new_row, ignore_index=True)
                                    
                if wrylx == '集中式污水治理设施':
                    # filename_wsc, file_extension_wsc = os.path.splitext(gdyhdsp_wsc_file)
                    # if file_extension_wsc == '.xlsx':
                    #     data_df_wsc = pd.read_excel(os.path.join(data_dir,gdyhdsp_wsc_file),sheet_name = sheet)
                    # if file_extension_wsc == '.csv':
                    #     data_df_wsc = pd.read_csv(os.path.join(data_dir,gdyhdsp_wsc_file))   
                    # table_dict = get_table_ids(xkzfb)
                    table = table_dict.get("排污单位生产线基本情况表")
                    if table != None:
                        json_result_scx = parse_wsclcscxxx(table)
                        json_result_shwsjcxx = parse_shwsjsxx(table_dict.get('生活污水进水信息'))
                        json_result_gywsjcxx = parse_gyfsjsxx(table_dict.get('工业废水进水信息'))

                        fszlss=[]
                        for key,value in json_result_scx.items():
                            for scx in value:
                                sjnl = int(re.search(r'\d+', scx['生产能力']).group()) if re.search(r'\d+', scx['生产能力']) else None
                                yxsj = int(scx['设计年生产时间'])
                                fszlss.append(scx['治理设施名称'])                    
                        zlss = '|'.join(fszlss)

                        # 对收集的生活污水量累计求和
                        shwscll = 0.0
                        if json_result_shwsjcxx is not None:
                            for key,value in json_result_shwsjcxx.items():
                                for wc in value:
                                    if wc['进水水量']=='/':
                                        shwscll+=0.0
                                    else:
                                        shwscll+=float(wc['进水水量'])
                            shwscll = shwscll*365/10000 # m3/日 换算为 万吨/年
                        
                        # 对收集的工业污水量累计求和
                        gywscll = 0.0
                        if json_result_shwsjcxx is not None:
                            for key,value in json_result_gywsjcxx.items():
                                for wc in value:
                                    gywscll+=float(wc['进水水量'])
                            
                            gywscll = gywscll*365/10000 # m3/日 换算为 万吨/年

                        new_row = pd.Series([row['序号'], '',xkzcode,sjnl,'',shwscll,\
                            gywscll,0,0,zlss,yxsj,yxsj,'','','','','','','','','','','','','',\
                                '','','','','','','','','','','','','',\
                                '','','监测数据法', \
                                    '','','','','','','','','','','','','',\
                                        '','','','','','','','','','','','','',\
                                            '','','','','','','','','','','','','',\
                                                '','','','','','','','','','','','','',\
                                                    '','','','','',''\
                                        ],index=data_df_wsc.columns)
                        # Append the new row to the DataFrame
                        data_df_wsc = data_df_wsc.append(new_row, ignore_index=True)
            else: 
                # 记录没有副本的许可证编号
                file.write(xkzcode+'\n')
                continue
            # if wrylx == '规模畜禽养殖':
            #     gdyhdsp_xqyz_file=os.path.join(data_dir,'活动水平_畜禽养殖_登记.csv')
            #     data_df_xqyz = pd.read_csv(gdyhdsp_xqyz_file)
            # if wrylx == '规模水产养殖':
            #     gdyhdsp_scyz_file=os.path.join(data_dir,'活动水平_水产_登记.csv')
            #     data_df_scyz = pd.read_csv(gdyhdsp_scyz_file)
            # if wrylx == '集中式污水治理设施':
            #     gdyhdsp_wsclc_file=os.path.join(data_dir,'活动水平_集中式污水治理设施.csv')
            #     data_df_wsclc = pd.read_csv(gdyhdsp_wsclc_file)
            # if data_df_gyy is not None:
            #     data_df_gyy.to_excel(os.path.join(data_dir,filename_gyy+'.xlsx'), index=False)
            # if data_df_wsc is not None:
            #     data_df_wsc.to_excel(os.path.join(data_dir,filename_wsc+'.xlsx'), index=False)
        if data_df_gyy is not None:
            data_df_gyy.to_excel(os.path.join(data_dir,filename_gyy+'.xlsx'), index=False)
        if data_df_wsc is not None:
            data_df_wsc.to_excel(os.path.join(data_dir,filename_wsc+'.xlsx'), index=False)
        file.close()
    except Exception as ex:
        if data_df_gyy is not None:
            data_df_gyy.to_excel(os.path.join(data_dir,filename_gyy+'.xlsx'), index=False)
        if data_df_wsc is not None:
            data_df_wsc.to_excel(os.path.join(data_dir,filename_wsc+'.xlsx'), index=False)
        
        file.write('\n'+ ex.args[0]+'\n')
        file.write(xkzcode+'\n')
        file.close()
        

# 临时处理爬取的许可登记基本信息表中污染源类型字段
def fill_wrylx_by_hylb(data_dir,file,sheet=0):
    '''
    设置污染源类型
    '''
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.xlsx':
        data_df = pd.read_excel(os.path.join(data_dir,file),sheet_name = sheet)
    if file_extension == '.csv':
        data_df = pd.read_csv(os.path.join(data_dir,file))
        
    # 遍历每一个污染源
    # for wry in data_df['排污许可证（登记）编号'].unique():
    for index, row in data_df.iterrows():
        xkzcode = row['排污许可证（登记）编号']
        hylb = row['行业类别']
        if '饲养' in hylb or '畜牧业' in hylb:
            wrylx = '规模畜禽养殖'
        elif '养殖' in hylb:
            wrylx = '规模水产养殖'
        elif '污水处理及其再生利用' in hylb:
            wrylx = '集中式污水治理设施'
        else:
            wrylx = '工业源'
        data_df.loc[index,'污染源类型'] = wrylx
    data_df.to_excel(os.path.join(data_dir,filename+'-副本补全2.xlsx'), index=False)

def fill_pfqxdm_by_pfqx(data_dir,file,sheet=0):
    '''
    设置排放去向
    '''
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.xlsx':
        data_df = pd.read_excel(os.path.join(data_dir,file),sheet_name = sheet)
    if file_extension == '.csv':
        data_df = pd.read_csv(os.path.join(data_dir,file))
    
    # 遍历每一个污染源
    # for wry in data_df['排污许可证（登记）编号'].unique():
    for index, row in data_df.iterrows():
        pfqx = row['排放去向']
        psqxdm='A'
        pffs='直接排放'
        if '直接进入海域' in pfqx:
            psqxdm='A'
            pffs='直接排放'
        elif '直接进入江河' in pfqx:
            psqxdm='B'
            pffs='直接排放'
        elif '进入城市下水道（再入江河' in pfqx:
            psqxdm='C'
            pffs='直接排放'
        elif '进入城市下水道（再入沿海' in pfqx:
            psqxdm='D'
            pffs='直接排放'
        elif '进入城市污水处理厂' in pfqx:
            psqxdm='E'
            pffs='间接排放'
        elif '直接进入污灌农田' in pfqx:
            psqxdm='F'
            pffs='间接排放'
        elif '进入地渗或蒸发地' in pfqx:
            psqxdm='G'
            pffs='间接排放'
        elif '进入其他单位' in pfqx:
            psqxdm='H'
            pffs='间接排放'
        elif '工业废水集中处理厂' in pfqx:
            psqxdm='L'
            pffs='间接排放'
        elif '排至厂内综合污水处理站' in pfqx:
            psqxdm='L'
            pffs='间接排放'
        elif '不外排' in pfqx or '包括回喷' in pfqx:
            psqxdm='K'
            pffs='间接排放'
        else:
            psqxdm='K'
            pffs='间接排放'
        data_df.loc[index,'排水去向代码'] = psqxdm
        data_df.loc[index,'排放方式'] = pffs
    data_df.to_excel(os.path.join(data_dir,filename+'-计算排水去向代码.xlsx'), index=False)

def merge_hdspxx(data_dir,source_file,file_xk,file_dj,wrylb,sheet=0):
    '''
    将许可副本提取的活动水平信息表与系统爬取的登记表活动水平数据合并
    source_file: 点源数据文件，为目标源清单数据
    file_xk: 根据source_file抽取的许可副本活动水平数据
    file_dj: 为系统上登记类许可按照行政区划下载的活动水平数据
    '''
    data_df_xk=None
    xkzcode=''
    file = open(os.path.join(data_dir, 'log.txt'), 'a+',encoding='utf-8')
    try:
        # 打开固定源基本信息表，遍历根据源类型及许可证号确定要读取的副本word文件及要写入的表单文件
        filename, file_extension = os.path.splitext(source_file)
        if file_extension == '.xlsx':
            data_df = pd.read_excel(os.path.join(data_dir,source_file),sheet_name = sheet)
        if file_extension == '.csv':
            data_df = pd.read_csv(os.path.join(data_dir,source_file))

        filename_xk, file_extension_xk = os.path.splitext(file_xk)
        if file_extension_xk == '.xlsx':
            data_df_xk = pd.read_excel(os.path.join(data_dir,file_xk),sheet_name = sheet)
        if file_extension_xk == '.csv':
            data_df_xk = pd.read_csv(os.path.join(data_dir,file_xk))

        filename_dj, file_extension_dj = os.path.splitext(file_dj)
        if file_extension_dj == '.xlsx':
            data_df_dj = pd.read_excel(os.path.join(data_dir,file_dj),sheet_name = sheet)
        if file_extension_dj == '.csv':
            data_df_dj = pd.read_csv(os.path.join(data_dir,file_dj))
        
        # xkzcode_list = data_df[(data_df['pwxkzgllb']=='登记') & (data_df['wrylx']==wrylb)]['pwxkz_dj_b'].values
        xkzcode_list = data_df[(data_df['排污许可证管理类别']=='登记') & (data_df['污染源类型']==wrylb)]['排污许可证（登记）编号'].values
        # 遍历每一个污染源
        for xkzcode in xkzcode_list:
            print(xkzcode)
            data_dj = data_df_dj[data_df_dj['排污许可证（登记）编号'] == xkzcode]
            if data_dj.shape[0]==0:
                file.write(xkzcode+'\n')
                continue
            data_df_xk=data_df_xk.append(data_dj, ignore_index=True)
        
        data_df_xk.to_excel(os.path.join(data_dir,filename_xk+'-追加登记.xlsx'), index=False)
        file.close()
    except Exception as ex:
        file.write('\n'+ ex.args[0]+'\n')
        file.write(xkzcode+'\n')
        file.close()
        if data_df_xk is not None:
            data_df_xk.to_excel(os.path.join(data_dir,filename_xk+'-追加登记.xlsx'), index=False)


if __name__ == '__main__':

    # table_ids = get_table_ids(r'F:\CWEI\固定源\咸宁市\许可\124212004212254512001U.docx')
    # read_docx(table_ids)
    # xkzfb = r'C:\sypcloud_git\projects\108_cweis\gis\污染源\泸州市\许可\51510525MAACKULL2W001Q.docx'
    # table_dict = get_table_ids(xkzfb)
    # res = parse_zyyfclxx(table_dict.get('主要原辅材料及燃料信息表'))
    # # 表7 废水直接排放口基本情况表
    # table = table_dict.get("废水直接排放口基本情况表")

    dir = r'C:\sypcloud_git\projects\108_cweis\gis\污染源\大陆溪流域'
    dir2 = r'C:\\sypcloud_git\\projects\\108_cweis\\gis\\污染源\\大陆溪流域\\许可\\'
    file = '点源.xlsx'
    file2 = '抽取-活动水平-工业源（许可&登记）.xlsx'
    file2 = '抽取-活动水平-畜禽养殖（许可&登记）.xlsx'
    file3 = '抽取-活动水平-集中式污水治理设施.xlsx'
    # fill_gdyhdsp_by_xkzfb(dir,dir2,file,file2,file3)
    file4 = '活动水平_工业源_登记.xlsx'
    file4 = '活动水平-畜禽养殖-登记.xlsx'
    # merge_hdspxx(dir,file,file2,file4,'工业源')
    merge_hdspxx(dir,file,file2,file4,'规模畜禽养殖')
    # fill_gdyjbxxb_by_xkzfb(dir,dir2,file)
    # pfkxxb = '固定源排放口信息表.xlsx'
    # file = '固定源基本信息（许可）-副本补全-追加登记.xlsx'
    # fill_gdypfkxxb_by_xkzfb(dir,file,pfkxxb)
    # fill_pfqxdm_by_pfqx(dir,pfkxxb)
