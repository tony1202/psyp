import os

def process_pwxksj(root,folder,folder_py,code,xkz_csvfile,dj_csvfile,pfkb_dj,tag=0):
    '''
    按源清单思路处理排污许可数据
    tag: 0-流域 1-区域 2-市级行政区划
    '''
    # 1. 爬取许可、登记，需要输入登录密码
    # from .fetch_pwxkz import *
    # 生成许可基本信息表
    # getxxgkContent2()

    # 对没有批量下载到副本文件的许可证再进行补充下载
    # getxxgkContent_by_xkzcodes()

    # from .fetch_pwxkdj import *
    # 生成登记基本信息表、登记排放口表、登记类工业、畜禽、水产活动水平信息表
    # getxkzdjxx()

    # dir=os.path.join(root, '污染源\\'+folder)
    # dir2 = root+'\\污染源\\'+folder+'\\许可\\'
    # basin_layerfile = os.path.join(root,'河流水系\\'+folder+'\\流域.shp')
    # xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\镇.shp')
    # if os.path.isfile(xz_layerfile) is False:
    #     xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\乡镇.shp')
    
    dir=os.path.join(root, '污染源\\'+folder+'\\'+code)
    dir2 = root+'\\污染源\\'+folder+'\\许可\\'
    
    # if folder=='大陆溪流域':
    #     folder_py = 'FAAADLX'
    # else: folder_py =get_first_letters(folder)
    
    dem_layerfile = os.path.join(root,'DEM\\'+folder_py+'\\'+code+'\\dem_prj.tif')
    
    basin_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\流域.shp')
    rivers_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\河流_prj.shp')
    subbasin_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域.shp')
    subbasin_half_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域（左右岸）.shp')
    subbasin_tau_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\子流域-tau.shp')
    crosssection_layerfile = os.path.join(root,'河流水系\\'+folder+'\\'+code+'\\横断面.shp')
    
    region_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\area.shp')
    city_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\市.shp')
    xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\镇.shp')
    if os.path.isfile(xz_layerfile) is False:
        xz_layerfile = os.path.join(root,'全国行政区划（四级）\\'+folder+'\\'+code+'\\乡镇.shp')
    
    clip_layerfile = basin_layerfile
    if tag==0:
        clip_layerfile = basin_layerfile
    elif tag==1: clip_layerfile = region_layerfile
    else: clip_layerfile = city_layerfile

    # xkz_csvfile = os.path.join(dir,xkz_csvfile)
    # dj_csvfile = os.path.join(dir,dj_csvfile)
    # pfkb_dj = os.path.join(dir,pfkb_dj)

    # 2. 许可副本doc转docx
    from psyp.dataio.word import doc2docx    
    # doc2docx.save_doc_to_docx_dir(dir2)

    # 3. 清洗许可、登记数据
    from psyp.dataio.webfetch.pwxkz import process
    # process.delete_history_xkxx(dir,xkz_csvfile)
    # process.delete_history_xkxx(dir,dj_csvfile)

    # 4. 合并许可、登记数据
    # process.merge_xk_dj(dir,xkz_csvfile[:-4]+'-清洗.csv',dj_csvfile[:-4]+'-清洗.csv')

    # 5. 手动合并不同行政区的许可、登记数据、排放口_登记数据、活动水平数据

    # 6. 结合QGIS按流域筛选生成需要的清单,或不执行，直接作为行政区划的清单
    #    注意删除生成的表格里自动追加的相交分析相关字段
    # process.intersect_with_basinLayer(dir,xkz_csvfile[:-4]+'-清洗-追加登记.xlsx',\
    #     clip_layerfile,
    #     '经营场所地址中心经度','经营场所地址中心纬度',\
    #     )
    # process.intersect_with_basinLayer(dir,pfkb_dj,clip_layerfile,\
    #     '排放口经度','排放口纬度')

    # 7. 结合QGIS叠加乡镇图层填充所属乡镇属性
    # process.intersect_with_countyLayer(dir,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析.xlsx',\
    #     xz_layerfile)

    # 8. 抽取副本补全许可基本信息表、在登记排放口表追加许可排放口数据
    #    生成的清单里如果污染源类型为空，一般情况该污染源已不存在(!不完全是)，可直接删除
    #    注意删除生成的表格里自动追加的相交分析相关字段
    from psyp.dataio.word import docx_spider_pwxkz
    # docx_spider_pwxkz.fill_gdyjbxxb_by_xkzfb(dir,dir2,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇.xlsx')    
    # docx_spider_pwxkz.fill_gdypfkxxb_by_xkzfb(dir,dir2,\
    #     (xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx'),pfkb_dj[:-4]+'-相交分析.xlsx')
    # docx_spider_pwxkz.fill_pfqxdm_by_pfqx(dir,pfkb_dj[:-4]+'-相交分析-追加许可.xlsx')

    # 9. 如作为数据分析需要拆解许可量
    # process.handle_xkpfl(dir,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx')

    # 10. 从许可证副本获取许可排污单位的活动水平, 并与登记类排污单位活动水平表合并
    #    将以下模板文件拷贝至目标文件夹
    #    抽取-活动水平-畜禽养殖.xlsx
    #    抽取-活动水平-工业源.xlsx
    #    抽取-活动水平-集中式污水治理设施.xlsx
    #    抽取-活动水平-水产养殖.xlsx
    # print('第9步：从许可证副本获取许可排污单位的活动水平, 并与登记类排污单位活动水平表合并')
    # docx_spider_pwxkz.fill_gdyhdsp_by_xkzfb(dir,dir2,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx',\
    #     '抽取-活动水平-工业源.xlsx','抽取-活动水平-集中式污水治理设施.xlsx')    
    # docx_spider_pwxkz.merge_hdspxx(dir,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx',\
    #     '抽取-活动水平-工业源.xlsx','活动水平_工业源_登记.csv','工业源')
    # docx_spider_pwxkz.merge_hdspxx(dir,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx',\
    #     '抽取-活动水平-畜禽养殖.xlsx','活动水平_畜禽养殖_登记.csv','规模畜禽养殖')
    # docx_spider_pwxkz.merge_hdspxx(dir,xkz_csvfile[:-4]+'-清洗-追加登记-相交分析-补充乡镇-副本补全.xlsx',\
    #     '抽取-活动水平-水产养殖.xlsx','活动水平_水产_登记.csv','规模水产养殖')



