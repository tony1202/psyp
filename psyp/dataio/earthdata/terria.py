# activate conda pygmt
import pygmt

# 乌海及周边地区
# region = [106.4, 107.3, 38.8, 40.1] # 小范围
# region = [104,108,37,41] # 大范围
region = [118.1,119.1,24.5,25.4] # 大范围
perspective=[180, 90] # 正视图
perspective=[130, 30]
zsize="3c"
# Load sample earth relief data
grid = pygmt.datasets.load_earth_relief(resolution="15s", region=region)

frame =  ["xa1f0.25","ya1f0.25", "z2000+lmeters", "wSEnZ"]
pygmt.makecpt(
        cmap='geo',
        series=f'1000/4000/100',
        continuous=True
    )

fig = pygmt.Figure()
fig.grdview(
    grid=grid,
    # Set the azimuth to -130 (230) degrees and the elevation to 30 degrees
    perspective=perspective,
    frame= frame, #["xaf", "yaf", "WSnE"],
    projection="M15c",
    zsize=zsize,
    surftype='i',#"s",
    cmap="geo",
    # plane="1000+ggrey",
    plane="1000+gazure",
    shading=0,
    # Set the contour pen thickness to "0.1p"
    contourpen="0.1p",
)
fig.basemap(
    perspective=True,
    rose="jTL+w3c+l+o-2c/-1c" #map directional rose at the top left corner 
)
fig.colorbar(perspective=True, frame=["a500", "x+lElevation", "y+lm"])
fig.show()