import requests
import os
import time

dir=r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230825'
foercastTime = '20230825'

# hspf 模型需要气象参数
var_level = {
    # 'DEWP':{  # 露点温度
    #     'varName': 'DPT',
    #     'level': '2_m_above_ground'
    # },
    # 'WIND': [{
    #     'varName': 'UGRD',
    #     'level': '10_m_above_ground'
    # },{
    #     'varName': 'VGRD',
    #     'level': '10_m_above_ground'
    # },],
    # 'PREC':{
    #     'varName':'APCP',
    #     'level':'surface'
    # },
    # 'SOLR':{ # 短波辐射
    #     'varName':'DSWRF',
    #     'level':'surface'
    # },
    # 'PEVT':{ # 潜在蒸发
    #     'varName':'PEVPR',
    #     'level':'surface'
    # },
    # 'CLOU':{ # 总云量
    #     'varName':'TCDC',
    #     'level':'convective_cloud_layer'
    # },
    'ATEM':{ # 气温
        'varName':'TMP',
        'level':'2_m_above_ground'
    },
}

# subregion
toplat = 30
leftlon = 97
rightlon = 106
bottomlat = 24

params=''
for variable in var_level:    
    if isinstance(var_level[variable],list):        
        for item in var_level[variable]:
            params = params + 'var_'+ item['varName']+'=on&'
        params = params+'lev_'+item['level']+'=on&'
    else:
        params = 'var_'+ var_level[variable]['varName']+'=on&lev_'+var_level[variable]['level']+'=on&'
    # 下载384小时预报
    for i in range(156, 385):
        timeIdx = '{:03}'.format(i)
        # 拼接url
        # https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?dir=%2Fgfs.20230825%2F00%2Fatmos&file=gfs.t00z.pgrb2.0p25.f001&var_UGRD=on&var_VGRD=on&lev_10_m_above_ground=on&subregion=&toplat=30&leftlon=97&rightlon=106&bottomlat=24
        # https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?dir=%2Fgfs.20230825%2F00%2Fatmos&file=gfs.t00z.pgrb2.0p25.f002&var_UGRD=on&var_VGRD=on&lev_10_m_above_ground=on&subregion=&toplat=30&leftlon=97&rightlon=106&bottomlat=24
        # https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?dir=%2Fgfs.20230825%2F00%2Fatmos&file=gfs.t00z.pgrb2.0p25.f001&var_DPT=on&lev_2_m_above_ground=on&subregion=&toplat=30&leftlon=97&rightlon=106&bottomlat=24
        url = 'https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?dir=%2Fgfs.'+foercastTime+'%2F00%2Fatmos&file=gfs.t00z.pgrb2.0p25.f' + \
            timeIdx+'&'+params+'subregion=&toplat='+str(toplat) + \
            '&leftlon='+str(leftlon)+'&rightlon='+str(rightlon)+'&bottomlat='+str(bottomlat)
        path = os.path.join(dir,variable)
        if os.path.exists(path) is False:
            os.mkdir(path)
        downloadfile = os.path.join(path,'gfs.t00z.pgrb2.0p25.f'+timeIdx)
        if os.path.exists(downloadfile):
            continue

        response = requests.get(url)

        if response.status_code == 200:
            with open(downloadfile, 'wb') as file:
                file.write(response.content)
            while not os.path.exists(downloadfile):
                time.sleep(1)
            file_size = os.path.getsize(downloadfile)
            if file_size == 0:
                os.remove(downloadfile)
        else:
            print(f"Failed to download the file {url}. Status code: {response.status_code}")
