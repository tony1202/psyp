import subprocess
import os

gribexe=r'C:\wgrib\wgrib2.exe'

grbfiledir=r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230825\WIND'
prefix='gfs.t00z.pgrb2.0p25.f'
files = [file for file in os.listdir(grbfiledir) if file.startswith(prefix)]
for file in files:
    file_path = os.path.join(grbfiledir,file)
    subprocess.run([gribexe]+[file_path,'-netcdf',os.path.join(grbfiledir,file.replace('pgrb2','nc')+'.nc')],check=True)
