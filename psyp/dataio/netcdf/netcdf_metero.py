from re import U
from cv2 import sqrt
import netCDF4
from netCDF4 import Dataset
import numpy as np
import math
import sys
import os
import time
from datetime import datetime, timedelta
import geopandas
import pandas as pd
# import rioxarray
import xarray
from shapely.geometry import mapping
import matplotlib.pyplot as plt
from mikeio.dfs2 import Dfs2
from mikeio.dfs0 import Dfs0
from mikeio.eum import EUMType, ItemInfo, EUMUnit
from mikeio.dataset import Dataset as MikeDataset


def cal_statistic_metero():
    '''
    不支持中文路径
    '''
    # 文件路径
    fn = './tests/testdata/dataio/netcdf/CN05.1_Pre_1961_2017_daily_025x025.nc' # 该文件太大，移动到了DataBase文件夹
    clipshp = './tests/testdata/dataio/netcdf/jimai.shp'
    pre_data = xarray.open_dataarray(fn)
    pre_data.rio.set_spatial_dims(x_dim="lon", y_dim="lat", inplace=True)
    pre_data.rio.write_crs("epsg:4326", inplace=True)
    jimai_up = geopandas.read_file(clipshp, crs="epsg:4326")
    clipped = pre_data.rio.clip(jimai_up.geometry.apply(
        mapping), jimai_up.crs, drop=False)
    day_avg = clipped.mean('time')
    month_avg = clipped.groupby('time.month').mean('time')

    for i in month_avg.month:
        month_avg[i-1].plot()
    # Quick plot to show the results
    day_avg.plot()
    print('ok')

def ucar_temperature_nc2dfs2(prefix1, prefix2, sstime, setime,dfs2file,sdir):
    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    times=[]
    dfsdata=[]

    while stime <= etime:
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fn = os.path.join(sdir, filename)

        # 默认为读文件，此处 'r' 可省略
        data = Dataset(fn, 'r')

        # 查看nc文件信息
        print('------------文件信息--------------------')
        print(data)

        # 查看nc文件中的变量
        print('--------------文件变量-------------------')
        for var in data.variables.keys():
            print(var)

        # 查看每个变量的信息
        print('--------------变量的信息----------------------')
        print('--------------lat----------------------')
        print(data.variables['lat'])
        print('--------------lon----------------------')
        print(data.variables['lon'])
        print('--------------time----------------------')
        print(data.variables['time'])
        print('--------------TMP_L1----------------------')
        print(data.variables['TMP_L1'])

        # 读取数据值
        lat = data.variables['lat'][:].data
        lon = data.variables['lon'][:].data
        time = data.variables['time'][:].data
        temp = data.variables['TMP_L1'][:].data
        
        k = 0
        m = 0

        if i==0:
            dt0= datetime.strptime(sstime, fmt),
            nx = len(lon)
            ny = len(lat)            
            npd = np.random.random([nt,ny, nx])

        for latd in lat:  # k
            for logtd in lon:  # m
                tempdata = temp[0][k][m].real
                if math.isnan(tempdata):
                    tempdata = -1e-032                          
                npd[i, k, m] = round(tempdata - 273.15,2)
                m = m+1
            k = k+1
            m = 0

        times.append(stime + timedelta(hours=6))  # 换算成北京时间      
        stime = stime + timedelta(hours=6)
        i = i+1
    
    dfsdata.append(npd)
    # write dfs2 file
    ds = MikeDataset(
        data=dfsdata,
        time=times,
        items=[ItemInfo('Temperature', EUMType.Temperature)],
    )
    dfs = Dfs2()
    dfs.write(
        filename=dfs2file,
        data=ds,
        # start_time= dt0, 新版本已不支持该参数
        dt=6*3600,
        # datetimes= times, 新版本已不支持该参数
        items=[ItemInfo('Temperature', EUMType.Temperature)],
        coordinate=["LONG/LAT", lon.min(), lat.min(), 0],
        dx=0.25,
        dy=0.25,
        title='surface Temperature from ucar'
    )

def ucar_temperature_nc2dfs0(prefix1, prefix2, sstime, setime, loc_lat,loc_lon, dfs0file,sdir):
    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    times=[]
    dfsdata=[]

    while stime <= etime:
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fn = os.path.join(sdir, filename)

        # 默认为读文件，此处 'r' 可省略
        data = Dataset(fn, 'r')

        # 查看nc文件信息
        print('------------文件信息--------------------')
        print(data)

        # 查看nc文件中的变量
        print('--------------文件变量-------------------')
        for var in data.variables.keys():
            print(var)

        # 查看每个变量的信息
        print('--------------变量的信息----------------------')
        print('--------------lat----------------------')
        print(data.variables['lat'])
        print('--------------lon----------------------')
        print(data.variables['lon'])
        print('--------------time----------------------')
        print(data.variables['time'])
        print('--------------TMP_L1----------------------')
        print(data.variables['TMP_L1'])

        # 读取数据值
        lat = data.variables['lat'][:].data
        lon = data.variables['lon'][:].data
        time = data.variables['time'][:].data
        temp = data.variables['TMP_L1'][:].data
        
        k = 0
        m = 0

        if i==0:
            dt0= datetime.strptime(sstime, fmt),
            nx = len(lon)
            ny = len(lat)            
            npd = np.random.random([nt])

        for latd in lat:  # k
            for logtd in lon:  # m
                if loc_lat>=(latd-0.25/2) and loc_lat<(latd+0.25/2) \
                    and loc_lon>=(logtd-0.25/2) and loc_lon<(logtd+0.25/2):
                    print(latd)
                    print(logtd)
                    tempdata = temp[0][k][m].real
                    if math.isnan(tempdata):
                        tempdata = -1e-032                          
                    npd[i] = round(tempdata - 273.15,2) # unit: K
                    break
                else:
                    m=m+1
            else:
                m=0
                k=k+1
                continue
            break

        times.append(stime + timedelta(hours=8))  # 换算成北京时间      
        stime = stime + timedelta(hours=6)
        i = i+1
    
    dfsdata.append(npd)
    # write dfs2 file
    ds = MikeDataset(
        data=dfsdata,
        time=times,
        items=[ItemInfo('Temperature', EUMType.Temperature)],
    )
    dfs = Dfs0()
    dfs.write(
        filename=dfs0file,
        data=ds,
        # start_time= dt0, 新版本已不支持该参数
        # dt=6*3600,
        # datetimes= times, 新版本已不支持该参数
        # items=[ItemInfo('Temperature', EUMType.Temperature)],
        title='surface Temperature from ucar'
    )

def ucar_wind_nc2dfs2(prefix1, prefix2, sstime, setime,dfs2file,wind_sdir,pressure_sdir):
    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    times=[]
    dfsdata=[]

    while stime <= etime:
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fn_wind = os.path.join(wind_sdir, filename)
        fn_pressure = os.path.join(pressure_sdir, filename)

        # 默认为读文件，此处 'r' 可省略
        data_wind = Dataset(fn_wind, 'r')
        data_pressure = Dataset(fn_pressure, 'r')

        # 查看nc文件信息
        print('------------文件信息--------------------')
        print(data_wind)

        # 查看nc文件中的变量
        print('--------------文件变量-------------------')
        for var in data_wind.variables.keys():
            print(var)
        for var in data_pressure.variables.keys():
            print(var)

        # 查看每个变量的信息
        print('--------------变量的信息----------------------')
        print('--------------lat----------------------')
        print(data_wind.variables['lat'])
        print('--------------lon----------------------')
        print(data_wind.variables['lon'])
        print('--------------time----------------------')
        print(data_wind.variables['time'])
        print('--------------U_GRD_L103----------------------')
        print(data_wind.variables['U_GRD_L103'])
        print('--------------V_GRD_L103----------------------')
        print(data_wind.variables['V_GRD_L103'])
        print('--------------PRES_L1----------------------')
        print(data_pressure.variables['PRES_L1'])

        # 读取数据值
        lat = data_wind.variables['lat'][:].data
        lon = data_wind.variables['lon'][:].data
        time = data_wind.variables['time'][:].data
        temp_wuv = data_wind.variables['U_GRD_L103'][:].data
        temp_wvv = data_wind.variables['V_GRD_L103'][:].data
        temp_pre = data_pressure.variables['PRES_L1'][:].data
        
        k = 0
        m = 0

        if i==0:
            dt0= datetime.strptime(sstime, fmt),
            nx = len(lon)
            ny = len(lat)            
            npd_wuv = np.random.random([nt,ny, nx])
            npd_wvv = np.random.random([nt,ny, nx])
            npd_pre = np.random.random([nt,ny, nx])

        for latd in lat:  # k
            for logtd in lon:  # m
                # U
                tempdata = temp_wuv[0][k][m].real
                if math.isnan(tempdata):
                    tempdata = -1e-032                          
                npd_wuv[i, k, m] = round(tempdata,2)
                # V
                tempdata = temp_wvv[0][k][m].real
                if math.isnan(tempdata):
                    tempdata = -1e-032                          
                npd_wvv[i, k, m] = round(tempdata,2)
                # pressure
                tempdata = temp_pre[0][k][m].real
                if math.isnan(tempdata):
                    tempdata = -1e-032                          
                npd_pre[i, k, m] = round(tempdata,2)
                m = m+1
            k = k+1
            m = 0

        times.append(stime + timedelta(hours=8))  # 换算成北京时间        
        stime = stime + timedelta(hours=6)
        i = i+1
    
    dfsdata.append(npd_wuv)
    dfsdata.append(npd_wvv)
    dfsdata.append(npd_pre)
    # write dfs2 file
    ds = MikeDataset(
        data=dfsdata,
        time=times,
        items=[
            ItemInfo('U Velocity', EUMType.Wind_Velocity),
            ItemInfo('V Velocity', EUMType.Wind_Velocity),
            ItemInfo('Pressure', EUMType.Pressure),
        ],
    )
    dfs = Dfs2()
    dfs.write(
        filename=dfs2file,
        data=ds,
        # start_time= dt0, 新版本已不支持该参数
        dt=6*3600,
        # datetimes= times, 新版本已不支持该参数
        # items=[ItemInfo('Temperature', EUMType.Temperature)],
        coordinate=["LONG/LAT", lon.min(), lat.min(), 0],
        dx=0.25,
        dy=0.25,
        title='data from ucar'
    )

def ucar_wind_nc2dfs0(prefix1, prefix2, sstime, setime, loc_lat,loc_lon, dfs0file,wind_sdir,pressure_sdir):
    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    times=[]
    dfsdata=[]

    while stime <= etime:
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fn_wind = os.path.join(wind_sdir, filename)
        fn_pressure = os.path.join(pressure_sdir, filename)

        # 默认为读文件，此处 'r' 可省略
        data_wind = Dataset(fn_wind, 'r')
        data_pressure = Dataset(fn_pressure, 'r')       

        # 读取数据值
        lat = data_wind.variables['lat'][:].data
        lon = data_wind.variables['lon'][:].data
        time = data_wind.variables['time'][:].data
        temp_wuv = data_wind.variables['U_GRD_L103'][:].data
        temp_wvv = data_wind.variables['V_GRD_L103'][:].data
        temp_pre = data_pressure.variables['PRES_L1'][:].data
        
        k = 0
        m = 0

        if i==0:
            dt0= datetime.strptime(sstime, fmt),
            nx = len(lon)
            ny = len(lat)            
            npd_wuv = np.random.random([nt])
            npd_wvv = np.random.random([nt])
            npd_pre = np.random.random([nt])

        for latd in lat:  # k
            for logtd in lon:  # m
                if loc_lat>=(latd-0.25/2) and loc_lat<(latd+0.25/2) \
                    and loc_lon>=(logtd-0.25/2) and loc_lon<(logtd+0.25/2):
                    # U
                    tempdata = temp_wuv[0][k][m].real
                    if math.isnan(tempdata):
                        tempdata = -1e-032                          
                    npd_wuv[i] = round(tempdata,2) 
                    # V
                    tempdata = temp_wvv[0][k][m].real
                    if math.isnan(tempdata):
                        tempdata = -1e-032                          
                    npd_wvv[i] = round(tempdata ,2) 
                    # pressure
                    tempdata = temp_pre[0][k][m].real
                    if math.isnan(tempdata):
                        tempdata = -1e-032                          
                    npd_pre[i] = round(tempdata,2) 
                    break
                else:
                    m=m+1
            else:
                m=0
                k=k+1
                continue
            break

        times.append(stime + timedelta(hours=8))  # 换算成北京时间      
        stime = stime + timedelta(hours=6)
        i = i+1
    
    dfsdata.append(npd_wuv)
    dfsdata.append(npd_wvv)
    dfsdata.append(npd_pre)
    # write dfs2 file
    ds = MikeDataset(
        data=dfsdata,
        time=times,
        items=[
            ItemInfo('U Velocity', EUMType.Wind_Velocity),
            ItemInfo('V Velocity', EUMType.Wind_Velocity),
            ItemInfo('Pressure', EUMType.Pressure),
        ],
    )
    dfs = Dfs0()
    dfs.write(
        filename=dfs0file,
        data=ds,
        # start_time= dt0, 新版本已不支持该参数
        # dt=6*3600,
        # datetimes= times, 新版本已不支持该参数
        # items=[ItemInfo('Temperature', EUMType.Temperature)],
        title='data from ucar'
    )

def ucar_wind_nc2dfs0_ex(prefix1, prefix2, sstime, setime, loc_lat,loc_lon, dfs0file,wind_sdir):
    '''
    不支持中文路径
    prefix1: 'gfs.0p25.'
    prefix2: '.f003.grib2.nc'
    '''
    # sstime = '2010-01-01 00:00:00'
    # setime = '2021-3-14 00:00:00'
    # sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    stime = datetime.strptime(sstime, fmt)
    etime = datetime.strptime(setime, fmt)
    nt =int( (etime-stime).total_seconds()/3600/6) + 1 
    i = 0
    deg=180.0/np.pi
    rad=np.pi/180.0
    times=[]
    dfsdata=[]

    while stime <= etime:
        filename = prefix1 + str(stime.year) + \
            '{0:0>2d}'.format(stime.month) + \
            '{0:0>2d}'.format(stime.day) + \
            '{0:0>2d}'.format(stime.hour) + \
            prefix2
        fn_wind = os.path.join(wind_sdir, filename)

        # 默认为读文件，此处 'r' 可省略
        data_wind = Dataset(fn_wind, 'r')      

        # 读取数据值
        lat = data_wind.variables['lat'][:].data
        lon = data_wind.variables['lon'][:].data
        time = data_wind.variables['time'][:].data
        temp_wuv = data_wind.variables['U_GRD_L103'][:].data
        temp_wvv = data_wind.variables['V_GRD_L103'][:].data
        
        k = 0
        m = 0

        if i==0:
            dt0= datetime.strptime(sstime, fmt),
            nx = len(lon)
            ny = len(lat)            
            npd_spd = np.random.random([nt])
            npd_dir = np.random.random([nt])

        for latd in lat:  # k
            for logtd in lon:  # m
                if loc_lat>=(latd-0.25/2) and loc_lat<(latd+0.25/2) \
                    and loc_lon>=(logtd-0.25/2) and loc_lon<(logtd+0.25/2):
                    # U
                    u = temp_wuv[0][k][m].real
                    if math.isnan(u):
                        u = -1e-032
                    # V
                    v = temp_wvv[0][k][m].real
                    if math.isnan(v):
                        v = -1e-032                          
                    npd_spd[i] = round(math.sqrt(u*u+v*v) ,2) 
                    
                    # dir
                    npd_dir[i] = round(np.mod(270-math.atan2(v,u)*deg,360) ,2) 
                    
                    break
                else:
                    m=m+1
            else:
                m=0
                k=k+1
                continue
            break

        times.append(stime + timedelta(hours=8))  # 换算成北京时间      
        stime = stime + timedelta(hours=6)
        i = i+1
    
    dfsdata.append(npd_spd)
    dfsdata.append(npd_dir)
    # write dfs2 file
    ds = MikeDataset(
        data=dfsdata,
        time=times,
        items=[
            ItemInfo('wind speed', EUMType.Wind_speed),
            ItemInfo('wind direction', EUMType.Wind_Direction),
        ],
    )
    dfs = Dfs0()
    dfs.write(
        filename=dfs0file,
        data=ds,
        # start_time= dt0, 新版本已不支持该参数
        # dt=6*3600,
        # datetimes= times, 新版本已不支持该参数
        # items=[ItemInfo('Temperature', EUMType.Temperature)],
        title='data from ucar'
    )

def test_era5data(fn):
    # 默认为读文件，此处 'r' 可省略
    data = Dataset(fn, 'r')

    # 查看nc文件信息
    print('------------文件信息--------------------')
    print(data)

    # 查看nc文件中的变量
    print('--------------文件变量-------------------')
    for i in data.variables.keys():
        print(i)    

if __name__ == '__main__':
    ncfile=r'C:\sypcloud_git\projects\108_cweis\gis\metro\大陆溪流域\era5-wind-uv-200301.nc'
    test_era5data(ncfile)
    # cal_statistic_metero()
    # ucar_temperature_nc2dfs2('2021-06-01 03:00:00','2021-06-01 12:00:00', \
    #     r'D:\Temp\tmpdata\gfs-temperature.dfs2', \
    #     r'D:\Temp\tmpdata')
    # ucar_temperature_nc2dfs0(
    #     'gfs.0p25.',
    #     '.f003.grib2.nc', 
    #     '2021-06-01 00:00:00','2021-06-30 00:00:00', \
    #     40.102, 116.179, 
    #     r'C:\sypcloud_git\projects\gfs-temperature.dfs0', \
    #     r'F:\sypcloud_git\DataBase\metero_data\gfs-temperature')
    # ucar_wind_nc2dfs2('gdas1.fnl0p25.','.f03.grib2.nc', \
    #     '2021-06-01 00:00:00','2021-06-02 00:00:00',
    #     r'D:\Temp\tmpdata\fnl-wind.dfs2', \
    #     r'D:\Temp\tmpdata\fnl-wind',
    #     r'D:\Temp\tmpdata\fnl-pressure'
    # )
    # ucar_wind_nc2dfs0(
    #     'gdas1.fnl0p25.',
    #     '.f03.grib2.nc', 
    #     '2021-06-01 00:00:00','2021-06-30 00:00:00', 
    #     40.102, 116.179, 
    #     r'D:\Temp\tmpdata\fnl-wind.dfs0', 
    #     r'D:\Temp\tmpdata\fnl-wind',
    #     r'D:\Temp\tmpdata\fnl-pressure'
    #     )
    # ucar_wind_nc2dfs0_ex(
        # 'gfs.0p25.',
        # '.f003.grib2.nc', 
        # '2021-06-01 00:00:00','2021-06-30 00:00:00', 
        # 40.102, 116.179, 
        # r'F:\sypcloud_git\DataBase\metero_data\gfs-wind.dfs0', 
        # r'F:\sypcloud_git\DataBase\metero_data\gfs-wind'
        # )
