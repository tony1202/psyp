import netCDF4
from netCDF4 import Dataset

ncfile = r'C:\sypcloud_git\projects\114_yn_jsj\rawdata\ncar\fns-384-20230825\WIND\gfs.t00z.nc.0p25.f001.nc'
data = Dataset(ncfile, 'r')
# 查看nc文件信息
print('------------文件信息--------------------')
print(data)
# 查看nc文件中的变量
print('--------------文件变量-------------------')
for var in data.variables.keys():
    print(var)
# 读取数据值
print( data.variables['latitude'][:].data)
print( data.variables['longitude'][:].data)
print( data.variables['time'][:].data)