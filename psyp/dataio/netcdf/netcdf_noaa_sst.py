import netCDF4
from netCDF4 import Dataset
import numpy as np
import sys
import os
import time
from datetime import datetime,timedelta
from mikeio.dfs2 import Dfs2
from mikeio.eum import EUMType, ItemInfo, EUMUnit

'''
不支持中文路径
'''
#文件路径
fn= './testdata/dataio/netcdf/X223.70.140.162.66.18.9.16.nc'

# 默认为读文件，此处 'r' 可省略
data = Dataset(fn, 'r')
 
# 查看nc文件信息
print('------------文件信息--------------------')
print(data)
 
# 查看nc文件中的变量
print('--------------文件变量-------------------')
for i in data.variables.keys():
    print(i)
 
# 查看每个变量的信息
print('--------------变量的信息----------------------')
print('--------------lat----------------------')
print(data.variables['lat'])
print('--------------lon----------------------')
print(data.variables['lon'])
print('--------------time----------------------')
print(data.variables['time'])
print('--------------sst----------------------')
print(data.variables['sst'])

# 读取数据值
lat = data.variables['lat'][:].data
lon = data.variables['lon'][:].data
time = data.variables['time'][:].data
temp = data.variables['sst'][:].data

fmt='%Y-%m-%d %H:%M:%S'
fmt2='%Y-%m-%d'
dt0 = datetime.strptime('1800-1-1 0:0:0',fmt)
i=0
j=0
m=0

nt = len(time)
nx = len(lon)
ny = len(lat)
data = []
d = np.random.random([nt, ny, nx])
start_time = dt0 + timedelta(days=time[0].real)

for dt in time:
   dts = dt0 + timedelta(days=dt)
   #outfilename = '{0}-{1}.txt'.format(fn[0:len(fn)-2], datetime.strftime(dts,fmt2))
   #file = open(outfilename,'w')
   for latd in lat:
       for logtd in lon:
           tempdata = temp[i][ny-j-1][m].real
           if tempdata == -9.969209968386869e36 :
               tempdata = -1e-032
           #   file.writelines('{0},{1},{2}\n'.format(logtd.real,latd.real,tempdata))
           d[i,j,m] = tempdata
           m=m+1
       j=j+1
       m=0
   i=i+1
   j=0
   #file.close()

data.append(d)

# write dfs2 file
dfs = Dfs2()
dfs.write(
    filename=fn+'.dfs2',
    data=data,
    start_time=start_time,
    dt=3600*24,
    items=[ItemInfo('Temperature',EUMType.Temperature)],
    coordinate=["LONG/LAT", lon.min(), lat.min(), 0],
    dx=0.25,
    dy=0.25,
    title='sea surface temperature from noaa'
)

