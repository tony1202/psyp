from psyp.utility.calDayOfYear import get_day_of_year
import netCDF4
from netCDF4 import Dataset
import numpy as np
import sys
import os
import math
import time
from datetime import datetime, timedelta
from mikeio.dfs2 import Dfs2
from mikeio.eum import EUMType, ItemInfo, EUMUnit
import requests
import socket


def convert_noaa_sss_to_dfs2(url,sstime,setime,sdir):
    '''
    不支持中文路径
    '''
    #sstime = '2010-01-01'
    #setime = '2021-3-14'
    #sdir = 'D:/DataBase/noaa海水盐度数据'

    fmt = '%Y-%m-%d %H:%M:%S'
    fmt2 = '%Y-%m-%d'

    # url = 'https://www.star.nesdis.noaa.gov/data/socd1/coastwatch/products/miras/nc/SM_D'
    stime = datetime.strptime(sstime, fmt2)
    etime = datetime.strptime(setime, fmt2)

    while stime < etime:
        day = get_day_of_year(stime.year, stime.month, stime.day)
        filename = str(stime.year)+'{0:0>3d}'.format(day)+'_Map_SATSSS_data_1day.nc'
        stime = stime + timedelta(days=1)
        nurl = url+filename
        filename = os.path.join(sdir, filename)

        timeout = 20
        socket.setdefaulttimeout(timeout)
        try:
            r = requests.get(nurl,timeout=timeout)
        except socket.timeout:
            continue       
        if int(r.headers['Content-length']) < 1*1024 *1024:
            continue

        with open(filename, "wb") as nc:
            nc.write(r.content)
        # 文件路径
        fn = filename  # './testdata/dataio/netcdf/SP_D2021067_Map_SATSSS_data_1day.nc'

        # 默认为读文件，此处 'r' 可省略
        data = Dataset(fn, 'r')

        # 查看nc文件信息
        print('------------文件信息--------------------')
        print(data)

        # 查看nc文件中的变量
        print('--------------文件变量-------------------')
        for i in data.variables.keys():
            print(i)

        # 查看每个变量的信息
        print('--------------变量的信息----------------------')
        print('--------------lat----------------------')
        print(data.variables['lat'])
        print('--------------lon----------------------')
        print(data.variables['lon'])
        print('--------------time----------------------')
        print(data.variables['time'])
        print('--------------altitude----------------------')
        print(data.variables['altitude'])
        print('--------------sss----------------------')
        print(data.variables['sss'])

        # 读取数据值
        lat = data.variables['lat'][:].data
        lon = data.variables['lon'][:].data
        time = data.variables['time'][:].data
        altitude = data.variables['altitude'][:].data
        temp = data.variables['sss'][:].data

        dt0 = datetime.strptime('1970-1-1 0:0:0', fmt)
        i = 0
        j = 0
        k = 0
        m = 0

        nt = len(time)
        nh = len(altitude)
        nx = len(lon)
        ny = len(lat)
        data = []
        d = np.random.random([nt, 45, 21])
        start_time = dt0 + timedelta(seconds=time[0].real)

        for dt in time:  # i
            dts = dt0 + timedelta(seconds=dt)
            #outfilename = '{0}-{1}.txt'.format(fn[0:len(fn)-2], datetime.strftime(dts,fmt2))
            #file = open(outfilename,'w')
            for alt in altitude:  # j
                for latd in lat:  # k
                    for logtd in lon:  # m
                        if k >= 484 and k <= 528 and m >= 1188 and m <= 1208:
                            tempdata = temp[i][j][k][m].real
                            if math.isnan(tempdata):
                                tempdata = -1e-032
                            #   file.writelines('{0},{1},{2}\n'.format(logtd.real,latd.real,tempdata))
                            d[i, 44-(k-484), m-1188] = tempdata
                        m = m+1
                    k = k+1
                    m = 0
                j = j+1
                k = 0
            i = i+1
            j = 0

        # file.close()

        data.append(d)

        # write dfs2 file
        dfs = Dfs2()
        dfs.write(
            filename=fn+'.dfs2',
            data=data,
            start_time=start_time,
            dt=1,
            items=[ItemInfo('Salinity', EUMType.Salinity)],
            coordinate=["LONG/LAT", lon.min(), lat.min(), 0],
            dx=0.25,
            dy=0.25,
            title='sea surface Salinity from noaa'
        )
