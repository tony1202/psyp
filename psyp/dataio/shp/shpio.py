import os
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from shapely.ops import orient
from osgeo import gdal, ogr, osr
from shapely.geometry import LineString
import numpy as np


def dataframe_intersect_shapefile(df_data,longitude_field,latitude_field,\
    attribute_field,target_field,shapefile_path):
    '''
    excel或csv与shp相交分析，获取相交对象的shp属性值
    '''    
    # Step 2: Read Shapefile
    gdf_polygons = gpd.read_file(shapefile_path)

    # Step 3: Convert CSV data to GeoDataFrame
    geometry = [Point(xy) for xy in zip(df_data[longitude_field], df_data[latitude_field])]
    crs = {'init': 'epsg:4326'}  # Assuming the coordinate reference system is WGS84
    gdf_points = gpd.GeoDataFrame(df_data, crs=crs, geometry=geometry)

    # Step 4: Spatial Intersection
    intersections = gpd.sjoin(gdf_points, gdf_polygons, how="left", op='intersects')

# Reset index of intersections DataFrame to ensure unique index labels
    intersections.reset_index(drop=True, inplace=True)
    
    # Step 5: Extract 'ssxz' field value
    ssxz_values = intersections[attribute_field]

    # Step 6: Update 'ssxz' column in CSV file
    df_data[target_field] = ssxz_values

    # Save updated CSV file
    return df_data

def dataframe_intersect_shapefile2(df_data,longitude_field,latitude_field,\
    attribute_field,target_field,shapefile_path):
    '''
    excel或csv与shp相交分析，获取相交对象的shp属性值
    返回只相交的数据
    '''    
    # Step 2: Read Shapefile
    gdf_polygons = gpd.read_file(shapefile_path)
    # df_data_n = df_data[df_data[longitude_field].str.len()!=0]
    df_data_n = df_data.dropna(subset=[longitude_field]) 
    # Step 3: Convert CSV data to GeoDataFrame
    # Apply the preprocess_coordinate function to the longitude and latitude columns
    df_data_n[longitude_field] = df_data_n[longitude_field].apply(preprocess_coordinate)
    df_data_n[latitude_field] = df_data_n[latitude_field].apply(preprocess_coordinate)

    df_data_n['coordinates'] = list(zip(df_data_n[longitude_field], df_data_n[latitude_field]))  
    df_data_n['geometry'] = df_data_n['coordinates'].apply(Point)  

    # geometry = [Point(float(lon),float(lat)) for lon, lat in zip(df_data_n[longitude_field], df_data_n[latitude_field])]
    crs = {'init': 'epsg:4326'}  # Assuming the coordinate reference system is WGS84
    # gdf_points = gpd.GeoDataFrame(df_data_n, crs=crs, geometry=geometry)

    # 将DataFrame转换为GeoDataFrame  
    gdf_points = gpd.GeoDataFrame(df_data_n,crs=crs,  geometry='geometry')  
  
    # Reset the index of gdf_points and gdf_polygons
    gdf_points = gdf_points.reset_index(drop=True)
    gdf_polygons = gdf_polygons.reset_index(drop=True)
    
    # Fix polygon orientation
    gdf_polygons['geometry'] = gdf_polygons['geometry'].apply(lambda x: orient(x))
    # Check and fix invalid geometries
    gdf_polygons['geometry'] = gdf_polygons['geometry'].apply(lambda x: x.buffer(0) if not x.is_valid else x)
    # Create spatial index
    gdf_polygons.sindex

    # # Step 4: Spatial Intersection
    # intersections = gpd.sjoin(gdf_points, gdf_polygons, how="left", op='within')

    # if attribute_field is not None:
    #     # Step 5: Extract 'ssxz' field value
    #     ssxz_values = intersections[attribute_field]

    #     # Step 6: Update 'ssxz' column in CSV file
    #     intersections[target_field] = ssxz_values

    # 执行空间相交分析  
    # 使用sjoin进行左连接，保留所有点，即使它们不在任何多边形内  
    # 使用'within'作为连接操作，表示点是否在多边形内  
    result = gpd.sjoin(gdf_points, gdf_polygons, how="left", op='within')  
    
    # 提取位于面矢量内的点数据  
    inside_points = result[result.index_right.notnull()]  
    
    # 移除用于相交的额外列（可选）  
    inside_points = inside_points.drop(columns=['index_right'])  
  
    # Save updated CSV file
    return inside_points

# Function to preprocess coordinate strings
def preprocess_coordinate(coord_str):
    try:
        parts = str(coord_str).split('.')
        if len(parts) == 3:
            # Concatenate the second and third parts to form minutes and seconds
            minutes_seconds = float(parts[1])/60+float(parts[2])/3600
            # Return the concatenation of the first part (degrees) and the modified minutes and seconds
            return float(parts[0])+minutes_seconds
        else:
            return float(coord_str)
    except Exception as ex:
        print(ex)

def shp2json(sdir,shpfile_name):
    shpfile = os.path.join(sdir,shpfile_name+'.shp')
    jsonfile = os.path.join(sdir,shpfile_name+'.geojson')
    gp = gpd.read_file(shpfile)
    gp.to_file(jsonfile, driver='GeoJSON')
    return True

def json2shp(sdir,filename):
    shpfile = os.path.join(sdir,filename+'.shp')
    jsonfile = os.path.join(sdir,filename+'.json') 
    gdf = gpd.read_file(jsonfile)
    gdf.to_file(shpfile)  

def delete_useless_features(shpfile,field):
    # 读取shapefile  
    gdf = gpd.read_file(shpfile)   
    grouped = gdf.groupby(field) 
    # 应用函数并获取结果  
    result = grouped.apply(get_largest_area).reset_index(drop=True)  
    # 将结果保存为新的shapefile  
    result.to_file(shpfile[:-4]+'-rm.shp', driver='ESRI Shapefile')
    # return result

# 定义一个函数，用于从每个组中获取面积最大的记录  
def get_largest_area(group):  
    return group.sort_values('area', ascending=False).head(1)  

def join_attrs_by_max_overlap(layer1,layer2,suffix):
    # import geopandas as gpd  
    # from shapely.geometry import Polygon  
    from shapely.ops import unary_union  
    # import pandas as pd  
    
    # 读取两个面矢量图层  
    gdf1 = gpd.read_file(layer1)  
    gdf2 = gpd.read_file(layer2)  
    
    # 确保两个图层都有有效的几何对象  
    gdf1 = gdf1[gdf1.is_valid]  
    gdf2 = gdf2[gdf2.is_valid]  
    
    # 对第二个图层进行空间索引，以加速空间连接  
    sindex = gdf2.sindex  
    
    # 检查两个图层的列名，找出冲突的列名  
    conflicting_columns = set(gdf1.columns).intersection(gdf2.columns) - {'geometry'}  
    
    # 创建一个字典，用于重命名第二个图层中冲突的列名  
    renamed_columns = {col: (f'{col}'+suffix) for col in conflicting_columns}  
    
    # 重命名第二个图层中冲突的列名  
    gdf2_renamed = gdf2.rename(columns=renamed_columns)  
    
    # 初始化一个字典，用于存储最大重叠面积的属性  
    max_overlap_attributes = {}  
    
    # 遍历第一个图层的每个要素  
    for index, row1 in gdf1.iterrows():  
        # 初始化最大重叠面积和对应的属性  
        max_overlap_area = 0  
        max_overlap_attrs = None  
        
        # 找到可能与当前要素重叠的候选要素  
        possible_matches_index = list(sindex.intersection(row1.geometry.bounds))  
        possible_matches = gdf2_renamed.iloc[possible_matches_index]  
        
        # 计算与当前要素重叠的面积，并找出最大重叠面积及其对应的属性  
        for _, row2 in possible_matches.iterrows():  
            overlap_area = row1.geometry.intersection(row2.geometry).area  
            if overlap_area > max_overlap_area:  
                max_overlap_area = overlap_area  
                max_overlap_attrs = row2.drop(columns=['geometry'])  # 移除geometry列  
        
        # 将最大重叠面积的属性添加到字典中
        if max_overlap_attrs is not None:  
            max_overlap_attributes[index] = max_overlap_attrs
    
    # 将最大重叠面积的属性转换为DataFrame  
    max_overlap_df = pd.DataFrame.from_dict(max_overlap_attributes, orient='index')  
    
    # 将最大重叠面积的属性join到第一个图层（不包含geometry列）  
    gdf1_with_max_overlap = gdf1.join(max_overlap_df.drop(columns=['geometry']))  
    
    # 保存处理后的第一个图层  
    gdf1_with_max_overlap.to_file(layer1[:-4]+'-join.shp', driver='ESRI Shapefile') 

def get_elevation_along_line_with_distance(dem_path, shp_path, distance_interval):
    # 打开DEM栅格文件
    # dem_path = 'path_to_your_dem.tif'
    dem_ds = gdal.Open(dem_path)
    dem_band = dem_ds.GetRasterBand(1)

    # 获取DEM栅格的地理转换参数
    geotransform = dem_ds.GetGeoTransform()

    # Extract pixel size
    pixel_x_size = geotransform[1]
    pixel_y_size = geotransform[5]

    
    # 打开线矢量文件
    # shp_path = 'path_to_your_line_vector.shp'
    driver = ogr.GetDriverByName('ESRI Shapefile')
    shp_ds = driver.Open(shp_path, 0)
    shp_layer = shp_ds.GetLayer()

    all_elevations = []

    # 遍历矢量文件中的每条线段
    for feature in shp_layer:
        eles=[]
        dists=[]
        feature_attrs = {}
        for field in feature.keys():
            feature_attrs[field] = feature.GetField(field)
        geometry = feature.GetGeometryRef()
        if geometry.GetGeometryType() == ogr.wkbLineString:
            line = geometry
            # 计算线段的长度
            line_length = line.Length()
            
            # 计算等距点的数量，确保至少有一个点
            num_points = max(2, int(line_length // distance_interval) + 1)            
            
            # 计算等距点
            # uniform_points = calculate_equal_distance_points(line, distance_interval)
            uniform_points,distances = calculate_uniform_points(line,num_points)

            # 遍历等距点，计算高程和距离
            for i, point in enumerate(uniform_points):
                # 将点的坐标转换为像素坐标
                pixel_x = int((point.x-geotransform[0])  / pixel_x_size)
                pixel_y = int((point.y-geotransform[3]) / pixel_y_size)
                
                # 检查点是否在栅格范围内
                if 0 <= pixel_x < dem_ds.RasterXSize and 0 <= pixel_y < dem_ds.RasterYSize:
                    # 读取高程值
                    elevation = dem_band.ReadAsArray()[pixel_y, pixel_x]
                    eles.append(elevation)
                    # # 计算点到线段起点的距离
                    # distance = line.Distance(line.GetPoint(0), point)
                    # dists.append(distance)
            
            all_elevations.append({'attributes':feature_attrs,'elevations':eles,'distance':distances})
    # 关闭数据源
    dem_ds = None
    dem_band = None
    shp_ds = None
    return all_elevations

# 计算线段上等距点的函数
def calculate_equal_distance_points(line, distance_interval):
    # 获取线段的起点和终点
    start_point = line.GetPoint(0)
    end_point = line.GetPoint(line.GetPointCount() - 1)
    
    # 计算线段的长度
    line_length = line.Length()
    
    # 计算等距点的数量，确保至少有一个点
    num_points = max(2, int(line_length // distance_interval) + 1)
    
    # 计算等距点
    points = []
    current_distance = 0
    for i in range(num_points - 1):
        point = line.InterpolatePoint(current_distance)
        points.append(point)
        current_distance += distance_interval
    
    # 添加终点作为最后一个点
    points.append(end_point)
    
    return points

 # 计算线段上均匀化顶点的函数
def calculate_uniform_points(line, num_points):
    # 获取线段的起点和终点
    start_point = line.GetPoint(0)
    end_point = line.GetPoint(line.GetPointCount() - 1)
    
    # 创建Shapely LineString对象
    shapely_line = LineString([start_point, end_point])
    
    # 计算等距点
    distances = [d for d in np.linspace(0, shapely_line.length, num_points + 1)]
    points = [shapely_line.interpolate(d) for d in distances]
    
    # # # 计算从起点到每个点的累计距离
    # cumulative_distances = []
    # current_distance = 0
    # # for point in points:
    # #     current_distance += calculate_distance(start_point[1],start_point[0], point.y,point.x)
    # #     cumulative_distances.append(current_distance)
    # for d in distances:
    #     current_distance+=d
    #     cumulative_distances.append(current_distance)
    
    return points, distances

def calculate_distance(lat1, lon1, lat2, lon2):
    import math
    # 将角度转换为弧度
    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat2)
    lon2 = math.radians(lon2)

    # 计算纬度差和经度差
    delta_lat = lat2 - lat1
    delta_lon = lon2 - lon1

    # 计算地球的平均半径（单位：千米）
    r = 6371

    # 使用Haversine公式计算距离
    a = math.sin(delta_lat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(delta_lon / 2) ** 2
    c = 2 * math.asin(math.sqrt(a))
    distance = r * c

    return distance

# ---------------------------------------------------------
if __name__ == '__main__':
    shp2json(r'C:\sypcloud_git\projects\114_yn_jsj\model\pkg01\hspf',
    'stream')
