# psyp
## ~~~开发中需要安装开源包~~~环境部署
~~~### 采用了 conda 和 pipenv 两个包管理~~~
~~~### 运行 pipenv install packagename~~~
~~~### 导出包清单 pipenv run pip freeze > requirements.txt~~~
~~~### 2021-7-29 切换为了conda包管理，涉及netcdf、geopandas，环境为geo_env~~~
- 安装anaconda，添加conda环境变量，python版本3.9
  ```
  conda upgrade --all
  ```
- 基于本项目 environment.yaml 新建一个环境
  ```
  conda env create -f environment.yaml
  conda env update -f environment.yaml --prune 如果base环境已经存在
  ```
- 在服务端配置运行环境
  ```
  在开发端，base环境运行
  pip list --format=freeze > requirements.txt
  在运行端，确保安装ms build tools 2017以上
  然后直接运行pip_install.py安装环境
  部分需要直接安装whl，下载地址：https://www.lfd.uci.edu/~gohlke/pythonlibs/#rasterio
  - opencv_python-4.5.5-cp39-cp39-win_amd64.whl
  - GDAL-3.4.3-cp39-cp39-win_amd64.whl
  - Fiona-1.8.21-cp39-cp39-win_amd64.whl
  - geopandas-0.10.2-py2.py3-none-any.whl
  - rasterio-1.2.10-cp39-cp39-win_amd64.whl
  - rioxarray-0.11.1-py3-none-any.whl
  ```
~~~## 换环境部署时运行以下命令~~~
~~~### cd psyp~~~
~~~### pipenv install~~~
- swmmio包，如版本更新了，参照原包本地化修订情况更新下载包相关代码 <https://gitee.com/hydrojip/swmmio>

## 部署环境 问题集锦
    * 提示ModuleNotFoundError: No module named 'matplotlib._contour'
    解决方法：卸载重装matplotlib。
    * opencv 包提示dll加载不了问题时，直接在 <https://www.lfd.uci.edu/~gohlke/pythonlibs/#opencv> 下载相应python版本的安装包
  
## 开发问题集锦
    * ~~开发时，先确保以下环境~~
    * 全部在base环境最省事
    * vscode插件 python、pylance
    * 测试清单出不来，大概率是版本冲突，先确保版本，然后再包文件夹内删除不需要的版本文件夹
```js
> conda env list
> conda activate ~~geo_env~~ base
```
    * 在test工程debug时，python print 函数在控制台输出不了信息，请按当前文件调试。
    * geopandas 库安装有问题时，直接conda建立一个新的环境，按照geopandas官网说明的纯净安装
    * pyswmm swmmio出现dll未加载的问题，检查包swmm\toolkit文件夹下是否有swmm的dll，如没有拷贝过去即可。
    * shp2json 引用的geopandas库在vscode的terminal里运行报缺失dll的情况下，可尝试在run in python terminal

## 打包
- ~~pyinstaller -F main.py --add-binary "D:\Anaconda3\Lib\site-packages\swmm_toolkit\*.dll;swmm_toolkit" --add-data D:\Anaconda3\Lib\site-packages\swmmio\defs\inp_sections.yml;defs"~~ 打包的exe没有调用，还得研究。
  

## 常用命令
```
conda config --remove-key channels 恢复到默认源
在C:\Users\当前用户\.condarc文件中插入以下，指定清华源
channels:
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
  - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/

conda env export > environment.yaml 导出环境

conda env update --file environment.yaml --prune

pip 批量导出包含环境中所有组件的requirements.txt文件

pip freeze > requirements.txt
pip 批量安装requirements.txt文件中包含的组件依赖

pip install -r requirements.txt

conda 批量导出包含环境中所有组件的requirements.txt文件

conda list -e > requirements.txt
conda 批量安装requirements.txt文件中包含的组件依赖

conda install --yes --file requirements.txt    #这种执行方式，一遇到安装不上就整体停止不会继续下面的包安装
FOR /F "delims=~" %f in (requirements.txt) DO conda install --yes "%f" #这个执行能解决上面出现的不执行后续包的问题
```

## 致谢
- 


