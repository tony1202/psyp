# -*- coding: utf-8 -*-
import os
from random import Random
import uuid
import fire
import json

# region 解析解模型


def cal_c_estuary_2d_center_plot(title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    from psyp.scenario.modeladpter import mcore
    
    suid = str(uuid.uuid1())
    res = mcore.cal_c_estuary_2d_center_plot(suid,
                                             'D:/sypcloud_server/ClientScenarioes',
                                             title,
                                             Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy,
                                             num,
                                             fill == 1)
    print(json.dumps(res))


def cal_c_river_2d_side_plot(title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    from psyp.scenario.modeladpter import mcore

    suid = str(uuid.uuid1())
    res = mcore.cal_c_river_2d_side_plot(suid,
                                         'D:/sypcloud_server/ClientScenarioes',
                                         title,
                                         Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy,
                                         num,
                                         fill == 1)
    print(json.dumps(res))


def cal_c_river_2d_side_reflex_plot(title, Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy, num, fill):
    from psyp.scenario.modeladpter import mcore

    suid = str(uuid.uuid1())
    res = mcore.cal_c_river_2d_side_reflex_plot(suid,
                                                'D:/sypcloud_server/ClientScenarioes',
                                                title,
                                                Qp, Cp, Ch, u, h, Ey, k, x, y, Δx, Δy,
                                                num,
                                                fill == 1)
    print(json.dumps(res))


def cal_c_river_1d_plot(title, k, Ex, u, B, A, Cp, Qp, Ch, Qh, x1, x2, Δx):
    from psyp.scenario.modeladpter import mcore
 
    suid = str(uuid.uuid1())
    res = mcore.cal_c_river_1d_plot(suid,
                                    'D:/sypcloud_server/ClientScenarioes',
                                    title,
                                    k, Ex, u, B, A, Cp, Qp, Ch, Qh, x1, x2, Δx)
    print(json.dumps(res))


def cal_c_river_1d_instant_plot(appid, title, k, Ex, u, A, M, Ch, Qh, x, Δx, t, Δt, num):
    from psyp.scenario.modeladpter import mcore

    res = mcore.cal_c_river_1d_instant_plot(appid,
                                            'D:/sypcloud_server/ClientScenarioes',
                                            title,
                                            k, Ex, u, A, M, Ch, Qh, x, Δx, t, Δt, num)
    print(json.dumps(res))


def cal_c_river_2d_side_instant_plot(appid, title, Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill):
    from psyp.scenario.modeladpter import mcore

    res = mcore.cal_c_river_2d_side_instant_plot(appid,
                                                 'D:/sypcloud_server/ClientScenarioes',
                                                 title,
                                                 Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill)
    print(json.dumps(res))


def cal_c_river_2d_side_reflex_instant_plot(appid, title, Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill):
    from psyp.scenario.modeladpter import mcore
    
    res = mcore.cal_c_river_2d_side_reflex_instant_plot(appid,
                                                        'D:/sypcloud_server/ClientScenarioes',
                                                        title,
                                                        Ch, M, h, Ex, Ey, u, k, x, y, t, Δx, Δy, Δt, num, fill)
    print(json.dumps(res))

# endregion

# region swmm


def extractSwmmModelTopoJson(appid, sdir, prjname, crs_in, crs_out):
    from psyp.scenario.modeladpter import mcore
    
    geojson = mcore.inp2geojson(appid,sdir, prjname, crs_in, crs_out)
    sjson = json.dumps(geojson)

    jsonfile = os.path.join(sdir, prjname[:-4]+'.json')
    f = open(jsonfile, 'w', encoding='utf-8')
    f.write(sjson)
    f.close()

    print(sjson)


def runAndExtractSubcatchmentResults(appid, sdir, prjname, m11tsFolder,
                                     swmm_m11_mapfile):
    from psyp.scenario.modeladpter import mcore

    res = mcore.runAndExtractSubcatchmentResults(appid, sdir, prjname, m11tsFolder,
                                                 swmm_m11_mapfile)
    print(res)


def runAndExtractJuncionResults(appid, sdir, prjname, m11tsFolder,
                                swmm_m11_mapfile):
    from psyp.scenario.modeladpter import mcore

    res = mcore.runAndExtractJuncionResults(appid, sdir, prjname, m11tsFolder,
                                            swmm_m11_mapfile)
    print(res)


def runAndExtractResults(appid, sdir, prjname, objectId, dataType, out2file=1):
    from psyp.scenario.modeladpter import mcore

    res = mcore.runAndExtractResults(appid, sdir, prjname, objectId, dataType,
                                     True if out2file != 1 else False)
    print(res)


def runAndExtractCatchmentsResults4HecRas(appid, sdir, prjname, tsFolder, mapfile):
    from psyp.scenario.modeladpter import mcore

    res = mcore.runAndExtractCatchmentsResults4HecRas(
        appid, sdir, prjname, tsFolder, mapfile)
    print(json.dumps(res))


def getSwmmModelRainData(appid, sdir, prjname):
    from psyp.scenario.modeladpter import mcore
    
    res = mcore.getSwmmModelRainData(
        appid,
        sdir,
        prjname
    )
    print(json.dumps(res))

def getSwmmSimulationTime(appid,sdir,prjname):
    from psyp.scenario.modeladpter import mcore    
    res = mcore.getSwmmSimulationTime(
        appid,
        sdir,
        prjname
    )
    print(json.dumps(res))

def updateSwmmRaingageTsdata(appid, sdir, prjname, startTime, forecastngTime, endTime, forecastRain):
    from psyp.scenario.modeladpter import mcore
    
    mcore.update_raingage_tsdata(
        appid,
        sdir,
        prjname,
        startTime,
        forecastngTime,
        endTime,
        forecastRain # str(Random.randrange(0,7))
    )

def updateSwmmRaingageTsdata2(appid, sdir, prjname, rainData_time,rainData_value):
    from psyp.scenario.modeladpter import mcore
    
    mcore.update_raingage_tsdata2(
        appid,
        sdir,
        prjname,
        rainData_time,rainData_value
    )

def updateSwmmSimulationTime(appid,sdir,prjname,startTime,endTime):
    from psyp.scenario.modeladpter import mcore
    
    mcore.update_options_simulationTime(
        appid,
        sdir,
        prjname,
        startTime,
        endTime
    )

# endregion

# region 数据处理

# region 爬虫
def getxxgkContent():
    from psyp.dataio.webfetch.pwxkz import fetch_pwxkz

    fetch_pwxkz.getxxgkContent2(r'D:\psyp\psyp\dataio\webfetch\pwxkz')


def fetch_bjsq_data():
    from psyp.dataio.webfetch.syq import fetch_bjsq
    
    fetch_bjsq.fectch_bjsq_data('d:/bjsq.csv', '2022-2-12', '2022-7-15')


def fetch_zbxx_gj(sdir, start_time, end_time):
    from psyp.dataio.webfetch.zfcg import fetch_zbxx_2wx
    
    fetch_zbxx_2wx.fetch_zbxx_gj(sdir, start_time, end_time)

# endregion 

# region MIKEIO
def ucar_wind_nc2dfs0(prefix1, prefix2, sstime, setime, loc_lat, loc_lon, dfs0file, wind_sdir, pressure_sdir):
    from psyp.dataio.netcdf import netcdf_metero
    
    netcdf_metero.ucar_wind_nc2dfs0(
        prefix1, prefix2, sstime, setime, loc_lat, loc_lon, dfs0file, wind_sdir, pressure_sdir)

def ucar_wind_nc2dfs0_ex(prefix1, prefix2, sstime, setime, loc_lat, loc_lon, dfs0file, wind_sdir):
    from psyp.dataio.netcdf import netcdf_metero
    
    netcdf_metero.ucar_wind_nc2dfs0_ex(
        prefix1, prefix2, sstime, setime, loc_lat, loc_lon, dfs0file, wind_sdir)

def Dfsu_flood_risk(appid,dfs_filename,shp_filename,depth_threshold,time_threshold):
    from psyp.dataio.mike import dfsuio
    res = dfsuio.dfsu_flood_risk_2shp(dfs_filename,shp_filename,depth_threshold,time_threshold)
    print(res)

def dfsu_2png(appid,dfsu_file,shape_file,png_path, background=None,item_num=-1,start_step=-1,end_step=-1, contours=5,zuobiao=None):
    from psyp.dataio.mike import dfsuio

    res = dfsuio.dfsu_2png(dfsu_file,shape_file,png_path,background, item_num,start_step,end_step, contours,zuobiao)
    print(res) 

def dfsu_2shp(appid,dfsu_file,shape_path,contour_interval,start_step=-1,end_step=-1,item_num=-1):
    from psyp.dataio.mike import dfsuio

    res =dfsuio.dfsu_2shp(dfsu_file,shape_path,contour_interval,start_step,end_step, item_num)
    print(res)
# endregion MIKEIO

# region shpio
def shp2json(appid,dir,shape_name):
    from psyp.dataio.shp import shpio
    res = shpio.shp2json(dir,shape_name)
    print(res)
# endregion

# region 许可数据处理
def make_water_discharging_inventory():
    from psyp.dataio.pwxk import pwxkio
    import os

    root=r'C:\sypcloud_git\projects\108_cweis\gis'
    # root = r'G:\server\python\psyp\psyp\dataio\webfetch\pwxkz' # 服务器地址
    folder = '大陆溪流域'

    xkz_csvfile='固定源基本信息（许可）.csv'
    dj_csvfile='固定源基本信息（登记）.csv'
    pfkb_dj = '排放口_登记.csv'
    pwxkio.process_pwxksj(root,folder,xkz_csvfile,\
        dj_csvfile,pfkb_dj)
# endregion 许可数据处理

# endregion

# region ltp 机器学习


def handle_eventMining(txtfile):
    from psyp.modelbase.hitltp import eventMining, wordscloud
    
    content = '''
    （原标题：中科院研究生遇害案：凶手系同乡学霸，老师同学已为死者发起捐款）

    6月14日下午6点多，中科院信息工程研究所硕士研究生谢雕在饭馆招待自重庆远道而来的高中同学周凯旋时，被周凯旋用匕首杀害。随后，周凯旋被北京警方抓获。

    周凯旋被抓后，他的家人向被警方递交了精神鉴定材料，称周凯旋患有精神性疾病。

    谢雕的家人罗发明告诉南都记者，谢雕被害后，他的研究生老师和同学发起了捐款。并说，谢雕的遗体已经进行尸检，等尸检结果出来后，家人将会把火化后的骨灰带回老家安葬，之后，他们将等待北京检察机关的公诉。

    高中同学千里赴京去杀人

    今年25岁的谢雕生长于重庆垫江县的一个小山村，谢雕和周凯旋同在垫江中学读高中，两人学习成绩名列前茅，周凯旋经常考年级第一，两人都是垫江中学的优秀毕业生，谢雕考上了西安电子科技大学，周凯旋考取了四川大学。

    微信图片_20180627174901_副本.jpg案发现场的行凶者周凯旋（受访者提供）。

    学习优秀的周凯旋认为自己应该能考上北大清华等名校，于是在入读四川大学两三个月后，选择了退学复读。经过半年多的苦读，周凯旋以优异成绩考取了西安交通大学，来到了谢雕所在的城市，且是硕博连读。

    但周凯旋因大学本科期间因沉迷游戏，考试不及格，最终失掉了硕博连读的机会，本科毕业后就回到重庆寻找就业机会。谢雕自西安电子科技大学毕业后，在2016年考取了中国科学院大学的硕士研究生，所读专业隶属于中科院信息工程研究所。

    谢雕的家人告诉南都记者，6月14日下午6点，谢雕在西五环外的中科院信息工程研究所门口见到了久未见面的高中同学周凯旋。把他带到旁边的饭馆吃饭，两人还合影发到了高中同学微信群。这时，谢雕还没意识到周凯旋即将对他带来致命伤害。

    南都记者在谢雕遇害现场视频中看到，在谢雕点菜时，周凯旋用匕首刺向他胸部，谢雕中刀站起后退时，周凯旋用匕首又刺向他颈部，谢雕倒地后，周凯旋又从背部向他连刺几刀。之后，又持刀割断了谢雕的颈部动脉。这时，有食客拿起椅子砸向正在行凶的周凯旋。刺死谢雕后，周凯旋举起双手挥舞，随后扬长而去。后来，周凯旋被北京警方抓获。

    同学聚会时自己觉得受伤害起杀心

    罗发明告诉南都记者，作为被害人家属，他们向北京警方了解到，凶案原因来自两年前的一场同学聚会，谢雕的一些话对周凯旋带来很大心理压力，让他不能释怀。

    两年前的一次高中同学聚会中，大家聊的话题很多，也聊到了周凯旋喜欢打游戏的事情，谢雕说了一些激励周凯旋的话，让他不要再打游戏，要振作起来。在参与聚会的同学们看来，这些话是常理之中的，但在周凯旋看来，对他带来很大伤害，两年来给他带来很大心理压力。

    参与那次聚会的同学后来回忆，在一起玩“狼人杀”游戏时，谢雕、周凯旋发生了争执，但不愉快的瞬间很快就过去了，大家也都没当回事。

    那次聚会之后的春节，不少同学发现被周凯旋拉黑，中断了联系。直至一年之后，周凯旋才加入了高中同学微信群。

    谢雕的家人说，周凯旋在网上购买了杀人凶器匕首，收货地址填写了北京，他在北京拿到网购的匕首后，才暗藏在身前来面见谢雕。

    师生捐款助他家人渡难关

    周凯旋被北京警方抓获后，他的家人向警方称周凯旋患有精神病，并提供了一些证明材料，希望得到从轻处置。


    谢雕遇害后，他的学校为失去这么优秀的学生感到惋惜。谢雕的老师说，“谢雕家境并不富裕，本科尚有2.5万助学贷款未偿还，前不久还向同学借款1万，父亲也患有鼻咽癌。”

    谢雕的老师和同学发起了捐款，希望能帮助谢雕的家人暂时渡过难关。

    谢雕的家人告诉南都记者，他们向谢雕的学校提出要求，希望案件能尽快解决。

    罗发明对南都记者说，谢雕的遗体已经进行尸检，尸检后十天至十五天出来结果，等拿到尸检报告后，他们会尽快火化谢雕的遗体，把他的骨灰带回重庆老家安葬。

    对于这一案件，谢雕的家人告诉南都记者，他们将等待北京的检察机关提起公诉。

    '''
    # txtfile = r'tests\testdata\dataio\docx2txt\“十四五”环境影响评价与排污许可工作实施方案.txt'
    # txtfile = r'C:\Users\jip\Downloads\环评估发〔2022〕9号.txt'
    print(txtfile)
    f = open(txtfile, encoding="utf-8")
    content = f.read()
    f.close()
    eventMining.eventMining2network(content)
    wordscloud.eventMining2wordscloud(content)


def train_predict(dir, int_text_filename, keywords):
    from psyp.modelbase.keras import train_prediction
    
    train_prediction.train_predict(dir, int_text_filename, keywords)


def generate_seq(outfile, keywords, seq_length, model_dir):
    from psyp.modelbase.keras import train_prediction
    
    modelfile = os.path.join(model_dir, 'model.h5')
    mappingfile = os.path.join(model_dir, 'mapping.pkl')
    out_text = train_prediction.generate_seq(
        modelfile, mappingfile, keywords, seq_length)
    f = open(outfile, 'w')
    f.write(out_text)
    f.close()
# endregion

# region air diffusion model
def run_air_diffusion_gauss_model(appid,dir,method,
                                  x0, y0, crs_in, crs_out,
                                  A,
                                  q, Qv, h, us, usa, Ts,
                                  D, surfaceType,
                                  u, wind_angle, Ta, w,
                                  k=0.0052194, Pa=10.057,
                                  h_fix_method='Holland',
                                  deltx=50, delty=50, radius=3500, delt_angle=30):
    '''
    扩散浓度场 "d:/sypcloud_server/ClientScenarioes\9511f464-1382-4115-a523-bca8cf4d738e" 1
     107.6314 37.73866 "EPSG:4490" "EPSG:4544"
      100
       100 141.95 80 3.42 2.5 393
        4 0
         2.5 90 290 D
          0.0052194 10.057
           Holland
            50 50 3500 30
    '''
    from psyp.scenario.modeladpter import mcore
    res = mcore.run_air_diffusion_gauss_model(appid,dir,method,
                                  x0, y0, crs_in, crs_out,
                                  A,
                                  q, Qv, h, us, usa, Ts,
                                  D, surfaceType,
                                  u, wind_angle, Ta, w,
                                  k, Pa,
                                  h_fix_method,
                                  deltx, delty, radius, delt_angle)
    print(res)
# endregion

def sayhello():
    print('hello')


def sayhello2():
    print('hello2')


if __name__ == '__main__':
    fire.Fire()
